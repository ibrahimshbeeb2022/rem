﻿using REM_Dto.CommonDto;
using REM_Dto.PropertyType;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;

namespace REM_Repository.PropertyType;

public interface IPropertyTypeRepo
{
    Task<PropertyTypeDto> AddAsync(PropertyTypeFormDto propertyTypeFormDto);
    Task UpdateAsync(PropertyTypeFormDto propertyTypeFormDto);
    Task RemoveAsync(Guid id);
    Task<CommonResponseDto<List<PropertyTypeDto>>> GetAllAsync(string search, PropertyCategory? propertyCategory, PropertyTypeSort propertyTypeSort, bool isAsc);
    Task<CommonResponseDto<PropertyTypeDto>> GetAsync(Guid id);
}