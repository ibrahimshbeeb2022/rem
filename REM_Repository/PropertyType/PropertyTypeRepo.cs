﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using REM_Database.Context;
using REM_Database.Model.Master;
using REM_Dto.CommonDto;
using REM_Dto.PropertyType;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using REM_Shared.Resource;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.PropertyType;

public class PropertyTypeRepo : BaseRepo, IPropertyTypeRepo
{
    private readonly IStringLocalizer<PropertyResource> propertyLocalizer;
    public PropertyTypeRepo(REMContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IStringLocalizer<PropertyResource> propertyLocalizer)
        : base(context, configuration, httpContextAccessor)
    {
        this.propertyLocalizer = propertyLocalizer;
    }
    public async Task<PropertyTypeDto> AddAsync(PropertyTypeFormDto propertyTypeFormDto)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<PropertyTypeModel>(m => m.IncrementNumber);

        var x = await context.PropertyType.AddAsync(new PropertyTypeModel
        {
            Title = propertyTypeFormDto.Title,
            IncrementNumber = NextIncrementNumber,
            TitleEn = propertyTypeFormDto.TitleEn,
            Category = propertyTypeFormDto.Category,
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.PT, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        return new PropertyTypeDto
        {
            Id = x.Entity.Id,
            DocCode = x.Entity.DocCode,
            Title = propertyTypeFormDto.Title,
            TitleEn = propertyTypeFormDto.TitleEn,
            Category = propertyTypeFormDto.Category,
        };
    }
    public async Task UpdateAsync(PropertyTypeFormDto propertyTypeFormDto)
    {
        var x = await context.PropertyType.Where(c => c.Id == propertyTypeFormDto.Id)
            .ExecuteUpdateAsync(c => c.SetProperty(c => c.Title, propertyTypeFormDto.Title)
            .SetProperty(c => c.Category, propertyTypeFormDto.Category)
            .SetProperty(c => c.TitleEn, propertyTypeFormDto.TitleEn));

        if (x == 0)
            throw new NotFoundException(propertyLocalizer[ErrorMessages.PropertyTypeNotFound]);
    }

    public async Task RemoveAsync(Guid id)
    {
        if (await context.PropertyType.Where(c => c.Id == id && c.Properties.Any(p => p.IsValid)).AnyAsync())
            throw new ValidationException(propertyLocalizer[ErrorMessages.PropertyTypeUsed]);

        var x = await context.PropertyType.Where(c => c.Id == id).ExecuteUpdateAsync(c => c.SetProperty(c => c.IsValid, false));
        if (x == 0)
            throw new NotFoundException(propertyLocalizer[ErrorMessages.PropertyTypeNotFound]);
    }
    public async Task<CommonResponseDto<List<PropertyTypeDto>>> GetAllAsync(string search, PropertyCategory? propertyCategory, PropertyTypeSort propertyTypeSort, bool isAsc)
    {
        Expression<Func<PropertyTypeModel, bool>> expression = pt => (!propertyCategory.HasValue || pt.Category == propertyCategory)
        && (string.IsNullOrEmpty(search) || pt.TitleEn.Contains(search) || pt.Title.Contains(search) || pt.DocCode.Contains(search));

        var propertyTypeQuery = context.PropertyType.Where(expression);

        if (propertyTypeSort == PropertyTypeSort.Category)
        {
            if (isAsc)
                propertyTypeQuery = propertyTypeQuery.OrderBy(pt => pt.Category.ToString());
            else
                propertyTypeQuery = propertyTypeQuery.OrderByDescending(pt => pt.Category.ToString());
        }
        else
            propertyTypeQuery = CustomOrderBy(propertyTypeQuery, propertyTypeSort, isAsc);

        var propertyTypesDto = await propertyTypeQuery.Select(c => new PropertyTypeDto
        {
            Id = c.Id,
            Title = c.Title,
            DocCode = c.DocCode,
            TitleEn = c.TitleEn,
            Category = c.Category,
        }).ToListAsync();
        return new(propertyTypesDto);
    }
    public async Task<CommonResponseDto<PropertyTypeDto>> GetAsync(Guid id)
    {
        var propertyTypeDto = (await context.PropertyType.Where(pt => pt.Id == id)
            .Select(c => new PropertyTypeDto
            {
                Id = c.Id,
                Title = c.Title,
                DocCode = c.DocCode,
                TitleEn = c.TitleEn,
                Category = c.Category,
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(propertyLocalizer[ErrorMessages.PropertyTypeNotFound]);
        return new(propertyTypeDto);
    }
}