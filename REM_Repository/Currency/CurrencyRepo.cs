﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using REM_Database.Context;
using REM_Database.Model.Currency;
using REM_Dto.CommonDto;
using REM_Dto.Currency;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using REM_Shared.Resource;
using System.ComponentModel.DataAnnotations;

namespace REM_Repository.Currency;

public class CurrencyRepo : BaseRepo, ICurrencyRepo
{
    private readonly IStringLocalizer<CurrencyResource> currencyLocalizer;
    public CurrencyRepo(REMContext context, IStringLocalizer<CurrencyResource> currencyLocalizer, IHttpContextAccessor httpContextAccessor,
        IConfiguration configuration)
        : base(context, configuration, httpContextAccessor)
    {
        this.currencyLocalizer = currencyLocalizer;
    }
    public async Task<CurrencyDto> AddAsync(CurrencyFormDto currencyFormDto)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<CurrencyModel>(m => m.IncrementNumber);
        var x = await context.Currency.AddAsync(new CurrencyModel
        {
            Name = currencyFormDto.Name,
            NameEn = currencyFormDto.NameEn,
            Symbol = currencyFormDto.Symbol,
            Parity = currencyFormDto.Parity,
            IsActive = currencyFormDto.IsActive,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.CU, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        return (await GetAsync(x.Entity.Id)).Data;
    }
    public async Task UpdateAsync(CurrencyFormDto currencyFormDto)
    {
        var effectedRows = await context.Currency.Where(c => c.Id == currencyFormDto.Id)
            .ExecuteUpdateAsync(c => c.SetProperty(c => c.Name, currencyFormDto.Name)
            .SetProperty(c => c.IsActive, currencyFormDto.IsActive)
            .SetProperty(c => c.NameEn, currencyFormDto.NameEn)
            .SetProperty(c => c.Parity, currencyFormDto.Parity)
            .SetProperty(c => c.Symbol, currencyFormDto.Symbol));
        if (effectedRows == 0)
            throw new NotFoundException(currencyLocalizer[ErrorMessages.CurrencyNotFound]);
    }

    public async Task RemoveAsync(Guid id)
    {
        if (await context.Currency.Where(c => c.Id == id && c.Countries.Any()).AnyAsync())
            throw new ValidationException(currencyLocalizer[ErrorMessages.CurrencyUsed]);

        var effectedRows = await context.Currency.Where(c => c.Id == id).ExecuteUpdateAsync(c => c.SetProperty(c => c.IsValid, false));
        if (effectedRows == 0)
            throw new NotFoundException(currencyLocalizer[ErrorMessages.CurrencyNotFound]);
    }
    public async Task<CommonResponseDto<List<CurrencyDto>>> GetAllAsync(string search, CurrencySort currencySort, bool isAsc, bool? isActive)
    {
        var currenciesQuery = context.Currency.Where(c => (string.IsNullOrEmpty(search) || c.NameEn.Contains(search) || c.Name.Contains(search)
        || c.Symbol.Contains(search) || c.Parity.ToString().Contains(search) || c.DocCode.Contains(search))
        && (!isActive.HasValue || c.IsActive == isActive));

        currenciesQuery = CustomOrderBy(currenciesQuery, currencySort, isAsc);

        var currenciesDto = await currenciesQuery.Select(c => new CurrencyDto
        {
            Id = c.Id,
            Name = c.Name,
            DocCode = c.DocCode,
            NameEn = c.NameEn,
            Parity = c.Parity,
            Symbol = c.Symbol,
            IsActive = c.IsActive
        }).ToListAsync();
        return new(currenciesDto);
    }
    public async Task<CommonResponseDto<CurrencyDto>> GetAsync(Guid id)
    {
        var currencyDto = (await context.Currency.Where(c => c.Id == id)
            .Select(c => new CurrencyDto
            {
                Id = c.Id,
                Name = c.Name,
                DocCode = c.DocCode,
                NameEn = c.NameEn,
                Parity = c.Parity,
                Symbol = c.Symbol,
                IsActive = c.IsActive
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(currencyLocalizer[ErrorMessages.CurrencyNotFound]);
        return new(currencyDto);
    }
}