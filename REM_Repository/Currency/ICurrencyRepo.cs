﻿using REM_Dto.CommonDto;
using REM_Dto.Currency;
using REM_Shared.Enum.Sort;

namespace REM_Repository.Currency;

public interface ICurrencyRepo
{
    Task<CurrencyDto> AddAsync(CurrencyFormDto currencyFormDto);
    Task UpdateAsync(CurrencyFormDto currencyFormDto);
    Task RemoveAsync(Guid id);
    Task<CommonResponseDto<List<CurrencyDto>>> GetAllAsync(string search, CurrencySort currencySort, bool isAsc, bool? isActive);
    Task<CommonResponseDto<CurrencyDto>> GetAsync(Guid id);
}