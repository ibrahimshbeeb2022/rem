﻿using REM_Dto.CommonDto;
using REM_Dto.Unit;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;

namespace REM_Repository.Unit;

public interface IUnitRepo
{
    Task<GetUnitDto> AddAsync(UnitFormDto unitDto);
    Task UpdateAsync(UnitFormDto unitDto);
    Task RemoveAsync(Guid unitId);
    Task<CommonResponseDto<List<GetUnitDto>>> GetAllAsync(string search, Guid? unitType, Guid? propertyId, Guid? tenantId, Guid? landLordId, UnitSort unitSort, bool isAsc, int pageSize, int pageNum, bool? rentedUnits, TenancyContractStatus? status);
    Task<CommonResponseDto<List<GetAllUnitSummaryDto>>> GetAllSummaryAsync(Guid? tenantId, Guid? propertyId, bool? rentedUnits);
    Task<CommonResponseDto<GetUnitDto>> GetByIdAsync(Guid Id);
}