﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using REM_Database.Context;
using REM_Database.Model.Unit;
using REM_Dto.CommonDto;
using REM_Dto.Unit;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using REM_Shared.Resource;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.Unit;

public class UnitTypeRepo : BaseRepo, IUnitTypeRepo
{
    private readonly IStringLocalizer<UnitResource> unitLocalizer;
    public UnitTypeRepo(REMContext context,
        IHttpContextAccessor httpContextAccessor,
        IConfiguration configuration,
        IStringLocalizer<UnitResource> unitLocalizer)
        : base(context, configuration, httpContextAccessor)
    {
        this.unitLocalizer = unitLocalizer;
    }
    public async Task<UnitTypeDto> AddAsync(string name, string nameEn, bool isActive)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<UnitTypeModel>(m => m.IncrementNumber);

        var x = await context.UnitType.AddAsync(new UnitTypeModel
        {
            Name = name,
            NameEn = nameEn,
            IsActive = isActive,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.UT, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        return new UnitTypeDto
        {
            Id = x.Entity.Id,
            DocCode = x.Entity.DocCode,
            IsActive = isActive,
            Name = name,
            NameEn = nameEn
        };
    }

    public async Task UpdateAsync(Guid id, string name, string nameEn, bool isActive)
    {
        var x = await context.UnitType.Where(c => c.Id == id)
            .ExecuteUpdateAsync(c => c.SetProperty(c => c.Name, name)
            .SetProperty(c => c.IsActive, isActive)
            .SetProperty(c => c.NameEn, nameEn));
        if (x == 0)
            throw new NotFoundException(unitLocalizer[ErrorMessages.UnitTypeNotFound]);
    }

    public async Task RemoveAsync(Guid unityTypeId)
    {
        if (await context.Unit.Where(u => u.UnitTypeId == unityTypeId).AnyAsync())
            throw new ValidationException(ErrorMessages.UnitTypeHasUnit);

        var x = await context.UnitType.Where(c => c.Id == unityTypeId).ExecuteUpdateAsync(c => c.SetProperty(c => c.IsValid, false));
        if (x == 0)
            throw new NotFoundException(unitLocalizer[ErrorMessages.UnitTypeNotFound]);
    }

    public async Task<CommonResponseDto<List<UnitTypeDto>>> GetAllAsync(string search, UnitTypeSort unitTypeSort, bool isAsc, bool? isActive)
    {
        Expression<Func<UnitTypeModel, bool>> expression = ut => (!isActive.HasValue || ut.IsActive == isActive)
        && (string.IsNullOrEmpty(search) || ut.Name.Contains(search) || ut.NameEn.Contains(search) || ut.DocCode.Contains(search));

        var unitTypes = context.UnitType.Where(expression);
        unitTypes = CustomOrderBy(unitTypes, unitTypeSort, isAsc);

        var unitTypesDto = await unitTypes
            .Select(c => new UnitTypeDto
            {
                Id = c.Id,
                Name = c.Name,
                DocCode = c.DocCode,
                NameEn = c.NameEn,
                IsActive = c.IsActive
            }).ToListAsync();
        return new(unitTypesDto);
    }
    public async Task<CommonResponseDto<UnitTypeDto>> GetAsync(Guid id)
    {
        var unitTypeDto = await context.UnitType.Where(ut => ut.Id == id)
            .Select(c => new UnitTypeDto
            {
                Id = c.Id,
                Name = c.Name,
                DocCode = c.DocCode,
                NameEn = c.NameEn,
                IsActive = c.IsActive
            }).FirstOrDefaultAsync();
        return new(unitTypeDto);
    }
}