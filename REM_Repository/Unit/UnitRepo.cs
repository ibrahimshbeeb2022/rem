﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Master;
using REM_Database.Model.Unit;
using REM_Dto.Base;
using REM_Dto.CommonDto;
using REM_Dto.Currency;
using REM_Dto.Location;
using REM_Dto.Notification;
using REM_Dto.Property;
using REM_Dto.Unit;
using REM_Repository.Base;
using REM_Repository.Notification;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.Unit;

public class UnitRepo : BaseRepo, IUnitRepo
{
    private readonly INotificationRepo notificationRepo;

    public UnitRepo(
        REMContext context,
        IConfiguration configuration,
        INotificationRepo notificationRepo,
        IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
    {
        this.notificationRepo = notificationRepo;
    }
    public async Task<GetUnitDto> AddAsync(UnitFormDto unitDto)
    {
        #region Validation
        if (!await CheckEntityExist<UnitTypeModel>(m => m.Id == unitDto.UnitTypeId))
        {
            throw new NotFoundException(ErrorMessages.UnitTypeNotFound);
        }
        if (!await CheckEntityExist<PropertyModel>(m => m.Id == unitDto.PropertyId))
        {
            throw new NotFoundException(ErrorMessages.PropertyNotFound);
        }
        #endregion

        var NextIncrementNumber = await GetNextIncrementNumber<UnitModel>(m => m.IncrementNumber);
        var unitModel = new UnitModel()
        {
            Id = Guid.NewGuid(),
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.U, NextIncrementNumber),
            IncrementNumber = NextIncrementNumber,
            Description = unitDto.Description,
            UnitNumber = unitDto.UnitNumber,
            UnitName = unitDto.UnitName,
            UnitSpace = unitDto.UnitSpace,
            RentValue = unitDto.RentValue,
            SewerAccountNumber = unitDto.SewerAccountNumber,
            ElectricityAndWaterAccountNumber = unitDto.ElectricityAndWaterAccountNumber,
            FloorNumber = unitDto.FloorNumber,
            HasParking = unitDto.HasParking,
            ParkingNumber = unitDto.ParkingNumber,
            ParkingRentValue = unitDto.ParkingRentValue,
            PropertyId = unitDto.PropertyId,
            UnitTypeId = unitDto.UnitTypeId,
            TenancyContractType = unitDto.TenancyContractType,
        };
        await context.AddAsync(unitModel);
        await context.SaveChangesAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.NewUnit,
            SenderId = CurrentUserId,
            ObjectId = unitModel.Id,
        });

        return (await GetByIdAsync(unitModel.Id)).Data;
    }
    public async Task UpdateAsync(UnitFormDto unitDto)
    {
        #region Validation
        if (!await CheckEntityExist<UnitTypeModel>(m => m.Id == unitDto.UnitTypeId))
        {
            throw new NotFoundException(ErrorMessages.UnitTypeNotFound);
        }
        if (!await CheckEntityExist<PropertyModel>(m => m.Id == unitDto.PropertyId))
        {
            throw new NotFoundException(ErrorMessages.PropertyNotFound);
        }
        #endregion

        var unitModel = await context.Unit.Where(m => m.Id == unitDto.Id).FirstOrDefaultAsync() ??
            throw new NotFoundException(ErrorMessages.UnitNotFound);

        unitModel.Description = unitDto.Description;
        unitModel.UnitNumber = unitDto.UnitNumber;
        unitModel.UnitName = unitDto.UnitName;
        unitModel.UnitSpace = unitDto.UnitSpace;
        unitModel.RentValue = unitDto.RentValue;
        unitModel.SewerAccountNumber = unitDto.SewerAccountNumber;
        unitModel.ElectricityAndWaterAccountNumber = unitDto.ElectricityAndWaterAccountNumber;
        unitModel.HasParking = unitDto.HasParking;
        unitModel.ParkingNumber = unitDto.HasParking ? unitDto.ParkingNumber : string.Empty;
        unitModel.FloorNumber = unitDto.HasParking ? unitDto.FloorNumber : string.Empty;
        unitModel.ParkingRentValue = unitDto.HasParking ? unitDto.ParkingRentValue : 0;
        unitModel.PropertyId = unitDto.PropertyId;
        unitModel.UnitTypeId = unitDto.UnitTypeId;
        unitModel.TenancyContractType = unitDto.TenancyContractType;

        await context.SaveChangesAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.UpdateUnit,
            SenderId = CurrentUserId,
            ObjectId = unitModel.Id,
        });
    }
    public async Task<CommonResponseDto<List<GetUnitDto>>> GetAllAsync
        (string search, Guid? unitType, Guid? propertyId, Guid? tenantId, Guid? landLordId, UnitSort unitSort,
         bool isAsc, int pageSize, int pageNum, bool? rentedUnits, TenancyContractStatus? status)
    {

        if (CurrentUserRole == UserType.LandLord.ToString())
        {
            landLordId = CurrentUserId;
        }

        Expression<Func<UnitModel, bool>> expression = m =>
          (!unitType.HasValue || unitType.Value == m.UnitTypeId) &&
          (!landLordId.HasValue || landLordId.Value == m.Property.LandLordId) &&
          (!tenantId.HasValue || m.TenancyContracts.Any(m => m.TenantId == tenantId)) &&
          (!propertyId.HasValue || propertyId.Value == m.PropertyId) &&
          (
           !rentedUnits.HasValue ||
           (rentedUnits.Value && m.TenancyContracts.Any()) ||
           (!rentedUnits.Value && (!m.TenancyContracts.Any() || m.TenancyContracts.All(tc => DateTime.UtcNow.Date > tc.EndDate.Date)))
          );

        var unitsQuery = context.Unit.Where(expression);

        unitsQuery = FilterByStringProperty(unitsQuery, search);

        switch (unitSort)
        {
            case UnitSort.UnitTypeAr:
                if (isAsc)
                    unitsQuery = unitsQuery.OrderBy(c => c.UnitType.Name);
                else
                    unitsQuery = unitsQuery.OrderByDescending(c => c.UnitType.Name);
                break;
            case UnitSort.UnitTypeEn:
                if (isAsc)
                    unitsQuery = unitsQuery.OrderBy(c => c.UnitType.NameEn);
                else
                    unitsQuery = unitsQuery.OrderByDescending(c => c.UnitType.NameEn);
                break;

            case UnitSort.Property:
                if (isAsc)
                    unitsQuery = unitsQuery.OrderBy(c => c.UnitType.Name);
                else
                    unitsQuery = unitsQuery.OrderByDescending(c => c.UnitType.Name);
                break;

            default:
                unitsQuery = CustomOrderBy(unitsQuery, unitSort, isAsc);
                break;
        }
        var totalRecords = await unitsQuery.CountAsync();

        var data = await unitsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(m => new GetUnitDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                TenancyContractType = m.TenancyContractType,
                Description = m.Description,
                ElectricityAndWaterAccountNumber = m.ElectricityAndWaterAccountNumber,
                FloorNumber = m.FloorNumber,
                HasParking = m.HasParking,
                ParkingNumber = m.ParkingNumber,
                ParkingRentValue = m.ParkingRentValue,
                RentValue = m.RentValue,
                SewerAccountNumber = m.SewerAccountNumber,
                UnitNumber = m.UnitNumber,
                UnitName = m.UnitName,
                UnitSpace = m.UnitSpace,
                CreateDate = m.CreateDate,
                Status = !m.TenancyContracts.Any() ? TenancyContractStatus.UnRented : m.TenancyContracts.OrderByDescending(m => m.CreateDate).Select(m => m.Status).FirstOrDefault(),
                VacantSince = !m.TenancyContracts.Any() ? m.CreateDate : m.TenancyContracts.OrderByDescending(m => m.CreateDate).Select(m => m.EndDate).FirstOrDefault(),
                UnitType = new UnitTypeDto
                {
                    Id = m.UnitType.Id,
                    Name = m.UnitType.Name,
                    NameEn = m.UnitType.NameEn,
                },
                Property = new GetPropertySummaryDto
                {
                    Id = m.PropertyId,
                    DocCode = m.DocCode,
                    Name = m.Property.Name,
                    BondDate = m.Property.BondDate,
                    BondNumber = m.Property.BondNumber,
                    BondType = m.Property.BondType,
                    PieceNumber = m.Property.PieceNumber,
                    PropertyNumber = m.Property.PropertyNumber,
                    GovernmentNumber = m.Property.GovernmentNumber,
                    City = new GetBaseNameDto
                    {
                        Id = m.Property.CityId,
                        Name = m.Property.City.Name,
                    },
                    Region = new GetBaseNameDto
                    {
                        Id = m.Property.RegionId,
                        Name = m.Property.Region.Name,
                    },
                    Country = new CountryDto
                    {
                        Id = m.Property.CountryId,
                        Name = m.Property.Country.Name,
                        Currency = new CurrencyDto
                        {
                            Id = m.Property.Country.Currency.Id,
                            Name = m.Property.Country.Currency.Name,
                        },
                    },
                },
            }).ToListAsync();

        var nowDate = DateTime.Now.Date;

        data.ForEach(m =>
        {
            if (m.Status == TenancyContractStatus.New || m.Status == TenancyContractStatus.ReNew)
                m.Status = m.VacantSince.Date >= nowDate ? TenancyContractStatus.Occupied : TenancyContractStatus.Expired;
        });

        data = data.Where(m => !status.HasValue || status.Value == m.Status).ToList();

        return new(data, pageSize, pageNum, totalRecords);
    }
    public async Task<CommonResponseDto<List<GetAllUnitSummaryDto>>> GetAllSummaryAsync(Guid? tenantId, Guid? propertyId, bool? rentedUnits)
    {
        var data = await context.Unit
            .Where(m =>
                (!propertyId.HasValue || m.PropertyId == propertyId.Value) &&
                (!tenantId.HasValue || m.TenancyContracts.Any(m => m.TenantId == tenantId)) &&
                (
                  !rentedUnits.HasValue ||
                  (rentedUnits.Value && m.TenancyContracts.Any()) ||
                  (!rentedUnits.Value && (!m.TenancyContracts.Any() || m.TenancyContracts.All(tc => DateTime.UtcNow.Date > tc.EndDate.Date)))
                )
                )
            .Select(m => new GetAllUnitSummaryDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                Number = m.UnitNumber,
                UnitName = m.UnitName,
                RentValue = m.RentValue,
                TenancyContractType = m.TenancyContractType,
                PropertyId = m.PropertyId,
            }).ToListAsync();
        return new(data);
    }
    public async Task<CommonResponseDto<GetUnitDto>> GetByIdAsync(Guid Id)
    {
        var data = await context.Unit
            .Where(m => m.Id == Id)
            .Select(m => new GetUnitDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                Description = m.Description,
                ElectricityAndWaterAccountNumber = m.ElectricityAndWaterAccountNumber,
                FloorNumber = m.FloorNumber,
                HasParking = m.HasParking,
                ParkingNumber = m.ParkingNumber,
                ParkingRentValue = m.ParkingRentValue,
                RentValue = m.RentValue,
                SewerAccountNumber = m.SewerAccountNumber,
                UnitNumber = m.UnitNumber,
                UnitName = m.UnitName,
                UnitSpace = m.UnitSpace,
                TenancyContractType = m.TenancyContractType,
                CreateDate = m.CreateDate,
                UnitType = new UnitTypeDto
                {
                    Id = m.UnitType.Id,
                    Name = m.UnitType.Name,
                },
                Property = new GetPropertySummaryDto
                {
                    Id = m.PropertyId,
                    DocCode = m.DocCode,
                    Name = m.Property.Name,
                    City = new GetBaseNameDto
                    {
                        Id = m.Property.CityId,
                        Name = m.Property.City.Name,
                    },
                    Region = new GetBaseNameDto
                    {
                        Id = m.Property.RegionId,
                        Name = m.Property.Region.Name,
                    },
                    Country = new CountryDto
                    {
                        Id = m.Property.CountryId,
                        Name = m.Property.Country.Name,
                        Currency = new CurrencyDto
                        {
                            Id = m.Property.Country.Currency.Id,
                            Name = m.Property.Country.Currency.Name,
                        },
                    },
                },
            }).FirstOrDefaultAsync();
        return new(data);
    }
    public async Task RemoveAsync(Guid unitId)
    {
        var unitModel = await context.Unit.Where(m => m.Id == unitId).FirstOrDefaultAsync() ??
            throw new NotFoundException(ErrorMessages.PropertyNotFound);

        if (await context.TenancyContract.AnyAsync(m => m.UnitId == unitId))
            throw new ValidationException(ErrorMessages.UnitHasContract);

        unitModel.IsValid = false;
        await context.SaveChangesAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.RemoveUnit,
            SenderId = CurrentUserId,
            ObjectId = unitModel.Id,
        });
    }
}