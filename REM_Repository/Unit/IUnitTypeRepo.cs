﻿using REM_Dto.CommonDto;
using REM_Dto.Unit;
using REM_Shared.Enum.Sort;

namespace REM_Repository.Unit;

public interface IUnitTypeRepo
{
    Task<UnitTypeDto> AddAsync(string name, string nameEn, bool isActive);
    Task UpdateAsync(Guid id, string name, string nameEn, bool isActive);
    Task RemoveAsync(Guid unityTypeId);
    Task<CommonResponseDto<List<UnitTypeDto>>> GetAllAsync(string search, UnitTypeSort unitTypeSort, bool isAsc, bool? isActive);
    Task<CommonResponseDto<UnitTypeDto>> GetAsync(Guid id);
}