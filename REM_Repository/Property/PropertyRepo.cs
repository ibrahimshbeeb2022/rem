﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Guard;
using REM_Database.Model.Location;
using REM_Database.Model.Master;
using REM_Dto.Base;
using REM_Dto.CommonDto;
using REM_Dto.Currency;
using REM_Dto.LandLord;
using REM_Dto.Location;
using REM_Dto.Notification;
using REM_Dto.Property;
using REM_Dto.PropertyType;
using REM_Repository.Base;
using REM_Repository.Notification;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.Property;

public class PropertyRepo : BaseRepo, IPropertyRepo
{
    private readonly INotificationRepo notificationRepo;

    public PropertyRepo(
        REMContext context,
        IConfiguration configuration,
        INotificationRepo notificationRepo,
        IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
    {
        this.notificationRepo = notificationRepo;
    }

    public async Task AddAsync(AddPropertyDto propertyDto)
    {
        #region Validation
        if (!await CheckEntityExist<LandLordModel>(m => m.Id == propertyDto.LandLordId))
        {
            throw new NotFoundException(ErrorMessages.LandLordNotFound);
        }
        if (propertyDto.GuardId.HasValue && !await CheckEntityExist<GuardModel>(m => m.Id == propertyDto.GuardId))
        {
            throw new NotFoundException(ErrorMessages.GuardNotFound);
        }
        if (!await CheckEntityExist<PropertyTypeModel>(m => m.Id == propertyDto.PropertyTypeId))
        {
            throw new NotFoundException(ErrorMessages.PropertyTypeNotFound);
        }
        if (!await CheckEntityExist<CountryModel>(m => m.Id == propertyDto.CountryId))
        {
            throw new NotFoundException(ErrorMessages.CountryNotFound);
        }
        if (!await CheckEntityExist<CityModel>(m => m.Id == propertyDto.CityId))
        {
            throw new NotFoundException(ErrorMessages.CityNotFound);
        }
        if (!await CheckEntityExist<RegionModel>(m => m.Id == propertyDto.RegionId))
        {
            throw new NotFoundException(ErrorMessages.RegionNotFound);
        }
        #endregion

        var NextIncrementNumber = await GetNextIncrementNumber<PropertyModel>(m => m.IncrementNumber);
        var propertyModel = new PropertyModel()
        {
            Id = Guid.NewGuid(),
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.P, NextIncrementNumber),
            IncrementNumber = NextIncrementNumber,
            Name = propertyDto.Name,
            Description = propertyDto.Description,
            SupervisorName = propertyDto.SupervisorName,
            PropertyNumber = propertyDto.PropertyNumber,
            GovernmentNumber = propertyDto.GovernmentNumber,
            PieceNumber = propertyDto.PieceNumber,
            PropertyOwnType = propertyDto.PropertyOwnType,
            IsActive = propertyDto.IsActive,
            LandLordId = propertyDto.LandLordId,
            GuardId = propertyDto.GuardId,
            PropertyTypeId = propertyDto.PropertyTypeId,
            FloorsCount = propertyDto.FloorsCount,
            ApartmentsCount = propertyDto.ApartmentsCount,
            ParkingFloorsCount = propertyDto.ParkingFloorsCount,
            ResidentialFloorsCount = propertyDto.ResidentialFloorsCount,
            ParkingsCount = propertyDto.ParkingsCount,
            StoresCount = propertyDto.StoresCount,
            BondDate = propertyDto.BondDate,
            BondNumber = propertyDto.BondNumber,
            BondType = propertyDto.BondType,
            CityId = propertyDto.CityId,
            CountryId = propertyDto.CountryId,
            RegionId = propertyDto.RegionId,
        };
        await context.AddAsync(propertyModel);
        await context.SaveChangesAsync();
        await context.File
                     .Where(m => propertyDto.FilesIds.Contains(m.Id))
                     .ExecuteUpdateAsync(m => m.SetProperty(c => c.PropertyId, propertyModel.Id));

        await context.SaveChangesAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.NewProperty,
            SenderId = CurrentUserId,
            ObjectId = propertyModel.Id,
        });
    }
    public async Task UpdateAsync(UpdatePropertyDto propertyDto)
    {
        #region Validation
        if (!await CheckEntityExist<LandLordModel>(m => m.Id == propertyDto.LandLordId))
        {
            throw new NotFoundException(ErrorMessages.LandLordNotFound);
        }
        if (propertyDto.GuardId.HasValue && !await CheckEntityExist<GuardModel>(m => m.Id == propertyDto.GuardId))
        {
            throw new NotFoundException(ErrorMessages.GuardNotFound);
        }
        if (!await CheckEntityExist<PropertyTypeModel>(m => m.Id == propertyDto.PropertyTypeId))
        {
            throw new NotFoundException(ErrorMessages.PropertyTypeNotFound);
        }
        if (!await CheckEntityExist<CountryModel>(m => m.Id == propertyDto.CountryId))
        {
            throw new NotFoundException(ErrorMessages.CountryNotFound);
        }
        if (!await CheckEntityExist<CityModel>(m => m.Id == propertyDto.CityId))
        {
            throw new NotFoundException(ErrorMessages.CityNotFound);
        }
        if (!await CheckEntityExist<RegionModel>(m => m.Id == propertyDto.RegionId))
        {
            throw new NotFoundException(ErrorMessages.RegionNotFound);
        }
        #endregion

        var propertyModel = await context.Property.Where(m => m.Id == propertyDto.Id).FirstOrDefaultAsync();
        if (propertyModel == null)
        {
            throw new NotFoundException(ErrorMessages.PropertyNotFound);
        }
        propertyModel.Name = propertyDto.Name;
        propertyModel.Description = propertyDto.Description;
        propertyModel.SupervisorName = propertyDto.SupervisorName;
        propertyModel.PropertyNumber = propertyDto.PropertyNumber;
        propertyModel.GovernmentNumber = propertyDto.GovernmentNumber;
        propertyModel.PieceNumber = propertyDto.PieceNumber;
        propertyModel.PropertyOwnType = propertyDto.PropertyOwnType;
        propertyModel.IsActive = propertyDto.IsActive;
        propertyModel.LandLordId = propertyDto.LandLordId;
        propertyModel.GuardId = propertyDto.GuardId;
        propertyModel.FloorsCount = propertyDto.FloorsCount;
        propertyModel.ApartmentsCount = propertyDto.ApartmentsCount;
        propertyModel.ResidentialFloorsCount = propertyDto.ResidentialFloorsCount;
        propertyModel.ParkingFloorsCount = propertyDto.ParkingFloorsCount;
        propertyModel.ParkingsCount = propertyDto.ParkingsCount;
        propertyModel.StoresCount = propertyDto.StoresCount;
        propertyModel.BondDate = propertyDto.BondDate;
        propertyModel.BondNumber = propertyDto.BondNumber;
        propertyModel.BondType = propertyDto.BondType;
        propertyModel.PropertyTypeId = propertyDto.PropertyTypeId;
        propertyModel.CityId = propertyDto.CityId;
        propertyModel.CountryId = propertyDto.CountryId;
        propertyModel.RegionId = propertyDto.RegionId;

        await context.File
                 .Where(m => propertyDto.FilesIds.Contains(m.Id))
                 .ExecuteUpdateAsync(m => m.SetProperty(c => c.PropertyId, propertyDto.Id));

        var deletedUrls = new List<string>();
        if (propertyDto.DeletedFiles.Any())
        {
            var deletedFiles = await context.File.Where(m => propertyDto.DeletedFiles.Contains(m.Id)).ToListAsync();
            deletedUrls = deletedFiles.Select(m => m.Url).ToList();
            context.RemoveRange(deletedFiles);
        }
        await context.SaveChangesAsync();
        FileManagement.DeleteFiles(deletedUrls);

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.UpdateProperty,
            SenderId = CurrentUserId,
            ObjectId = propertyModel.Id,
        });
    }
    public async Task<CommonResponseDto<List<GetPropertyDto>>> GetAllAsync
        (string search, Guid? landLord, Guid? guard, PropertySort propertySort, bool isAsc,
        DateTime? from, DateTime? to, PropertyOwnType? propertyOwnType, int pageSize, int pageNum)
    {


        if (CurrentUserRole == UserType.LandLord.ToString())
        {
            landLord = CurrentUserId;
        }

        Expression<Func<PropertyModel, bool>> expression = p =>
        (!from.HasValue || p.CreateDate >= from)
        && (!to.HasValue || p.CreateDate <= to)
        && (!from.HasValue || p.BondDate >= from)
        && (!to.HasValue || p.BondDate <= to)
        && (!propertyOwnType.HasValue || p.PropertyOwnType == propertyOwnType)
        && (!landLord.HasValue || p.LandLordId == landLord.Value)
        && (!guard.HasValue || p.GuardId == guard.Value);

        var propertyQuery = context.Property.Where(expression);
        propertyQuery = FilterByStringProperty(propertyQuery, search);

        switch (propertySort)
        {
            case PropertySort.LandLordName:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.LandLord.FullName);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.LandLord.FullName);
                break;
            case PropertySort.CountryNameAr:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.Country.Name);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.Country.Name);
                break;
            case PropertySort.CountryNameEn:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.Country.NameEn);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.Country.NameEn);
                break;
            case PropertySort.CityNameAr:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.City.Name);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.City.Name);
                break;
            case PropertySort.CityNameEn:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.City.NameEn);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.City.NameEn);
                break;
            case PropertySort.RegionNameAr:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.Region.Name);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.Region.Name);
                break;
            case PropertySort.RegionNameEn:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.Region.NameEn);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.Region.NameEn);
                break;
            case PropertySort.PropertyTypeAr:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.PropertyType.Title);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.PropertyType.Title);
                break;
            case PropertySort.PropertyTypeEn:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.PropertyType.TitleEn);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.PropertyType.TitleEn);
                break;
            case PropertySort.PropertyOwnType:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.PropertyOwnType.ToString());
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.PropertyOwnType.ToString());
                break;
            case PropertySort.GuardAr:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.Guard.Name);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.Guard.Name);
                break;
            case PropertySort.GuardEn:
                if (isAsc)
                    propertyQuery = propertyQuery.OrderBy(p => p.Guard.NameEn);
                else
                    propertyQuery = propertyQuery.OrderByDescending(p => p.Guard.NameEn);
                break;

            default:
                propertyQuery = CustomOrderBy(propertyQuery, propertySort, isAsc);
                break;
        }

        var data = await propertyQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(m => new GetPropertyDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                Name = m.Name,
                Description = m.Description,
                SupervisorName = m.SupervisorName,
                PropertyNumber = m.PropertyNumber,
                GovernmentNumber = m.GovernmentNumber,
                PieceNumber = m.PieceNumber,
                PropertyOwnType = m.PropertyOwnType,
                IsActive = m.IsActive,
                FloorsCount = m.FloorsCount,
                ApartmentsCount = m.ApartmentsCount,
                ParkingFloorsCount = m.ParkingFloorsCount,
                ResidentialFloorsCount = m.ResidentialFloorsCount,
                ParkingsCount = m.ParkingsCount,
                StoresCount = m.StoresCount,
                BondDate = m.BondDate,
                BondNumber = m.BondNumber,
                BondType = m.BondType,
                LandLord = new GetLandLordSummaryDto
                {
                    Id = m.LandLord.Id,
                    DocCode = m.LandLord.DocCode,
                    FullName = m.LandLord.FullName,
                    Email = m.LandLord.Email,
                    TaxNumber = m.LandLord.TaxNumber,
                    Address = m.LandLord.Address,
                    //City = new GetBaseNameDto
                    //{
                    //    Id = m.LandLord.CityId,
                    //    Name = m.LandLord.City.Name,
                    //    NameEn = m.LandLord.City.NameEn,
                    //},
                    //Region = new GetBaseNameDto
                    //{
                    //    Id = m.LandLord.RegionId,
                    //    Name = m.LandLord.Region.Name,
                    //    NameEn = m.LandLord.Region.NameEn,
                    //},
                    //Country = new CountryDto
                    //{
                    //    Id = m.LandLord.CountryId,
                    //    Name = m.LandLord.Country.Name,
                    //    Currency = new CurrencyDto
                    //    {
                    //        Id = m.LandLord.Country.Currency.Id,
                    //        Name = m.LandLord.Country.Currency.Name,
                    //        NameEn = m.LandLord.Country.Currency.NameEn,
                    //    },
                    //},
                },
                City = m.CityId.HasValue ? new GetBaseNameDto
                {
                    Id = m.CityId.Value,
                    Name = m.City.Name,
                    NameEn = m.City.NameEn,
                } : null,
                Region = m.RegionId.HasValue ? new GetBaseNameDto
                {
                    Id = m.RegionId.Value,
                    Name = m.Region.Name,
                    NameEn = m.Region.NameEn,
                } : null,
                Country = m.CountryId.HasValue ? new CountryDto
                {
                    Id = m.CountryId.Value,
                    Name = m.Country.Name,
                    NameEn = m.Country.NameEn,
                    Currency = new CurrencyDto
                    {
                        Id = m.Country.Currency.Id,
                        Name = m.Country.Currency.Name,
                        NameEn = m.Country.Currency.NameEn,
                    },
                } : null,
                Guard = m.GuardId.HasValue ? new GetBaseNameDto
                {
                    Id = m.GuardId.Value,
                    Name = m.Guard.Name,
                    NameEn = m.Guard.NameEn
                } : null,
                PropertyType = new PropertyTypeDto
                {
                    Id = m.PropertyTypeId,
                    Category = m.PropertyType.Category,
                    Title = m.PropertyType.Title,
                    DocCode = m.PropertyType.DocCode,
                    TitleEn = m.PropertyType.TitleEn,
                },
                Files = m.Files.Select(m => new GetBaseFileDto
                {
                    Id = m.Id,
                    FileKind = m.FileKind,
                    FileType = m.FileType,
                    Url = m.Url
                }).ToList(),
            }).ToListAsync();
        var totalRecords = propertyQuery.Count();
        return new(data, pageSize, pageNum, totalRecords);
    }
    public async Task<CommonResponseDto<List<GetPropertySummaryDto>>> GetAllSummaryAsync()
    {
        var data = await context.Property
            .Select(m => new GetPropertySummaryDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                Name = m.Name,
                BondDate = m.BondDate,
                BondNumber = m.BondNumber,
                BondType = m.BondType,
                GovernmentNumber = m.GovernmentNumber,
                PieceNumber = m.PieceNumber,
                PropertyNumber = m.PropertyNumber,
                City = m.CityId.HasValue ? new GetBaseNameDto
                {
                    Id = m.CityId.Value,
                    Name = m.City.Name,
                    NameEn = m.City.NameEn,
                } : null,
                Region = m.RegionId.HasValue ? new GetBaseNameDto
                {
                    Id = m.RegionId.Value,
                    Name = m.Region.Name,
                    NameEn = m.Region.NameEn,
                } : null,
                Country = m.CountryId.HasValue ? new CountryDto
                {
                    Id = m.CountryId.Value,
                    Name = m.Country.Name,
                    NameEn = m.Country.NameEn,
                    Currency = new CurrencyDto
                    {
                        Id = m.Country.Currency.Id,
                        Name = m.Country.Currency.Name,
                        NameEn = m.Country.Currency.NameEn,
                    },
                } : null,
                LandLord = new GetLandLordSummaryDto
                {
                    Id = m.LandLordId,
                    DocCode = m.LandLord.DocCode,
                    FullName = m.LandLord.FullName,
                    TaxNumber = m.LandLord.TaxNumber,
                    Email = m.LandLord.Email,
                    Address = m.LandLord.Address,
                    //City = new GetBaseNameDto
                    //{
                    //    Id = m.LandLord.CityId,
                    //    Name = m.LandLord.City.Name,
                    //    NameEn = m.LandLord.City.NameEn,
                    //},
                    //Region = new GetBaseNameDto
                    //{
                    //    Id = m.LandLord.RegionId,
                    //    Name = m.LandLord.Region.Name,
                    //    NameEn = m.LandLord.Region.NameEn,
                    //},
                    //Country = new CountryDto
                    //{
                    //    Id = m.LandLord.CountryId,
                    //    Name = m.LandLord.Country.Name,
                    //    NameEn = m.LandLord.Country.NameEn,
                    //    Currency = new CurrencyDto
                    //    {
                    //        Id = m.LandLord.Country.Currency.Id,
                    //        Name = m.LandLord.Country.Currency.Name,
                    //        NameEn = m.LandLord.Country.Currency.NameEn,
                    //    },
                    //},
                }
            }).ToListAsync();
        return new(data);
    }
    public async Task<CommonResponseDto<GetPropertyDto>> GetByIdAsync(Guid Id)
    {
        var data = (await context.Property
            .Where(m => m.Id == Id)
            .Select(m => new GetPropertyDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                Name = m.Name,
                Description = m.Description,
                SupervisorName = m.SupervisorName,
                PropertyNumber = m.PropertyNumber,
                GovernmentNumber = m.GovernmentNumber,
                PieceNumber = m.PieceNumber,
                PropertyOwnType = m.PropertyOwnType,
                IsActive = m.IsActive,
                FloorsCount = m.FloorsCount,
                ApartmentsCount = m.ApartmentsCount,
                ParkingFloorsCount = m.ParkingFloorsCount,
                ResidentialFloorsCount = m.ResidentialFloorsCount,
                ParkingsCount = m.ParkingsCount,
                StoresCount = m.StoresCount,
                BondDate = m.BondDate,
                BondNumber = m.BondNumber,
                BondType = m.BondType,
                LandLord = new GetLandLordSummaryDto
                {
                    Id = m.LandLord.Id,
                    DocCode = m.LandLord.DocCode,
                    FullName = m.LandLord.FullName,
                    TaxNumber = m.LandLord.TaxNumber,
                    Email = m.LandLord.Email,
                    Address = m.LandLord.Address,
                    //City = new GetBaseNameDto
                    //{
                    //    Id = m.LandLord.CityId,
                    //    Name = m.LandLord.City.Name,
                    //    NameEn = m.LandLord.City.NameEn,
                    //},
                    //Region = new GetBaseNameDto
                    //{
                    //    Id = m.LandLord.RegionId,
                    //    Name = m.LandLord.Region.Name,
                    //    NameEn = m.LandLord.Region.NameEn,
                    //},
                    //Country = new CountryDto
                    //{
                    //    Id = m.LandLord.CountryId,
                    //    Name = m.LandLord.Country.Name,
                    //    NameEn = m.LandLord.Country.NameEn,
                    //    Currency = new CurrencyDto
                    //    {
                    //        Id = m.LandLord.Country.Currency.Id,
                    //        Name = m.LandLord.Country.Currency.Name,
                    //        NameEn = m.LandLord.Country.Currency.NameEn,
                    //    },
                    //},
                },
                City = m.CityId.HasValue ? new GetBaseNameDto
                {
                    Id = m.CityId.Value,
                    Name = m.City.Name,
                    NameEn = m.City.NameEn,
                } : null,
                Region = m.RegionId.HasValue ? new GetBaseNameDto
                {
                    Id = m.RegionId.Value,
                    Name = m.Region.Name,
                    NameEn = m.Region.NameEn,
                } : null,
                Country = m.CountryId.HasValue ? new CountryDto
                {
                    Id = m.CountryId.Value,
                    Name = m.Country.Name,
                    NameEn = m.Country.NameEn,
                    Currency = new CurrencyDto
                    {
                        Id = m.Country.Currency.Id,
                        Name = m.Country.Currency.Name,
                        NameEn = m.Country.Currency.NameEn,
                    },
                } : null,
                Guard = m.GuardId.HasValue ? new GetBaseNameDto
                {
                    Id = m.GuardId.Value,
                    Name = m.Guard.Name,
                    NameEn = m.Guard.NameEn,
                } : null,
                PropertyType = new PropertyTypeDto
                {
                    Id = m.PropertyTypeId,
                    Category = m.PropertyType.Category,
                    Title = m.PropertyType.Title,
                    DocCode = m.PropertyType.DocCode,
                    TitleEn = m.PropertyType.TitleEn,
                },
                Files = m.Files.Select(m => new GetBaseFileDto
                {
                    Id = m.Id,
                    FileKind = m.FileKind,
                    FileType = m.FileType,
                    Url = m.Url
                }).ToList(),
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(ErrorMessages.PropertyNotFound);

        return new(data);
    }
    public async Task RemoveAsync(Guid propertyId)
    {
        if (await context.Unit.AnyAsync(m => m.PropertyId == propertyId))
            throw new ValidationException(ErrorMessages.PropertyHasUnits);

        if (await context.TenancyContract.AnyAsync(m => m.PropertyId == propertyId))
            throw new ValidationException(ErrorMessages.PropertyHasContract);

        var propertyModel = await context.Property.Where(m => m.Id == propertyId).FirstOrDefaultAsync() ??
            throw new NotFoundException(ErrorMessages.PropertyNotFound);

        propertyModel.IsValid = false;
        await context.SaveChangesAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.RemoveProperty,
            SenderId = CurrentUserId,
            ObjectId = propertyModel.Id,
        });
    }
}