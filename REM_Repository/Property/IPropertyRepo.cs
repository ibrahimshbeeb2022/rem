﻿using REM_Dto.CommonDto;
using REM_Dto.Property;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;

namespace REM_Repository.Property;

public interface IPropertyRepo
{
    Task AddAsync(AddPropertyDto propertyDto);
    Task UpdateAsync(UpdatePropertyDto propertyDto);
    Task RemoveAsync(Guid propertyId);
    Task<CommonResponseDto<List<GetPropertySummaryDto>>> GetAllSummaryAsync();
    Task<CommonResponseDto<List<GetPropertyDto>>> GetAllAsync(string search, Guid? landLord, Guid? guard, PropertySort propertySort, bool isAsc, DateTime? from, DateTime? to, PropertyOwnType? propertyOwnType, int pageSize, int pageNum);
    Task<CommonResponseDto<GetPropertyDto>> GetByIdAsync(Guid Id);
}