﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Master;
using REM_Dto.Base;
using REM_Dto.CommonDto;
using REM_Dto.Finance;
using REM_Dto.LandLord;
using REM_Dto.Notification;
using REM_Dto.TenancyContract;
using REM_Dto.Tenant;
using REM_Dto.Unit;
using REM_Repository.Base;
using REM_Repository.Finance;
using REM_Repository.Notification;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.TenancyContract;

public class TenancyContractRepo : BaseRepo, ITenancyContractRepo
{
    private readonly IFinanceRepo financeRepo;
    private readonly INotificationRepo notificationRepo;

    public TenancyContractRepo(
        IFinanceRepo financeRepo,
        INotificationRepo notificationRepo,
        REMContext context,
        IConfiguration configuration,
        IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
    {
        this.financeRepo = financeRepo;
        this.notificationRepo = notificationRepo;
    }
    public async Task<CommonResponseDto<GetTenancyContractDto>> ActionToWaitingList(ActionToWaitingListDto tc)
    {
        var tran = await context.Database.BeginTransactionAsync();
        var uaeDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time"));

        #region Validation

        if (!await CheckEntityExist<LandLordModel>(m => m.Id == tc.LandLordId))
        {
            throw new NotFoundException(ErrorMessages.LandLordNotFound);
        }
        if (!await CheckEntityExist<UnitModel>(m => m.Id == tc.UnitId))
        {
            throw new NotFoundException(ErrorMessages.UnitTypeNotFound);
        }
        if (!await CheckEntityExist<TenantModel>(m => m.Id == tc.TenantId))
        {
            throw new NotFoundException(ErrorMessages.TenantNotFound);
        }
        if (!await CheckEntityExist<PropertyModel>(m => m.Id == tc.PropertyId))
        {
            throw new NotFoundException(ErrorMessages.PropertyNotFound);
        }
        if (tc.Details.Where(m => m.Type == TenancyContractOperationType.Rent).Count() != 1)
        {
            throw new ValidationException(ErrorMessages.AllowOnlyOneRentValue);
        }
        if (await CheckEntityExist<TenancyContractModel>(m => m.Id != tc.Id && m.UnitId == tc.UnitId && m.EndDate.Date >= uaeDate.Date))
        {
            throw new NotFoundException(ErrorMessages.UnitHasContract);
        }
        if (await CheckEntityExist<TenancyContractModel>(m => m.TenancyNumber == tc.TenancyNumber && m.Id != tc.Id))
        {
            throw new NotFoundException(ErrorMessages.TenancyContractNumberAlreadyExsist);
        }
        var unitModel = await context.Unit.Where(m => m.Id == tc.UnitId).FirstOrDefaultAsync();
        var vatValue = unitModel.TenancyContractType == TenancyContractType.Residential ? 0 : Constants.Vat;

        if (tc.Details.Where(m => m.Type != TenancyContractOperationType.Insurance).Any(m => Math.Round(m.Value * vatValue, 2) != m.Tax))
        {
            throw new ValidationException(ErrorMessages.InvalidVatValue);
        }
        if (tc.Details.Any(m => m.Total != m.Value + m.Tax))
        {
            throw new ValidationException(ErrorMessages.InvalidPaymentTotalValue);
        }
        if (tc.Details.Sum(m => m.Total) != tc.PaymentPlan.Sum(m => m.Value))
        {
            throw new ValidationException(ErrorMessages.PaymentPlanValueNotEqualToTotalValue);
        }
        #endregion

        await context.File.Where(m => tc.DeletedFilesIds.Contains(m.Id)).ExecuteDeleteAsync();
        await context.TenancyContractDetails.Where(m => m.TenancyContractId == tc.Id).ExecuteDeleteAsync();

        var NextIncrementNumber = await GetNextIncrementNumber<TenancyContractModel>(m => m.IncrementNumber);
        var landLordTenancyContractNumber =
            !await context.TenancyContract.AnyAsync(m => m.LandLordId == tc.LandLordId) ? 1 :
             await context.TenancyContract.Where(m => m.LandLordId == tc.LandLordId).MaxAsync(m => m.LandLordTenancyContractNumber) + 1;

        var landLordDocCode = await context.LandLord.Where(m => m.Id == tc.LandLordId).Select(m => m.DocCode).FirstOrDefaultAsync();

        var tcModel = await context.TenancyContract
                                   .Where(m => m.Id == tc.Id)
                                   .FirstOrDefaultAsync();

        var paymentPlanModel = await context.TenancyContractPaymentPlan.Where(m => m.TenancyContractId == tc.Id).Include(m => m.Files).ToListAsync();

        var isAdded = false;
        if (tcModel == null)
        {
            isAdded = true;
            tcModel = new TenancyContractModel()
            {
                Id = Guid.NewGuid(),
                LandLordTenancyContractNumber = landLordTenancyContractNumber,
                LandLordTenancyContractCode = StringHelper.CreateLandLordTenancyContractCode(landLordTenancyContractNumber, landLordDocCode),
                DocCode = StringHelper.CreateDocumentCode(DocumentNumber.TC, NextIncrementNumber),
                IncrementNumber = NextIncrementNumber,
            };
        }
        var allFilesIds = tc.PaymentPlan.SelectMany(p => p.FilesId, (m, p) => p).ToList();
        var filesModel = await context.File.Where(m => allFilesIds.Contains(m.Id)).ToListAsync();


        var totalValueWithoutInsurance = tc.Details.Where(m => m.Type != TenancyContractOperationType.Insurance).Sum(m => m.Value);
        var insuranceValue = tc.Details.Where(m => m.Type == TenancyContractOperationType.Insurance).Sum(m => m.Value);
        var totalVatValue = tc.Details.Sum(m => m.Tax);

        tcModel.PropertyId = tc.PropertyId;
        tcModel.TenantId = tc.TenantId;
        tcModel.LandLordId = tc.LandLordId;
        tcModel.UnitId = tc.UnitId;
        tcModel.EndDate = tc.EndDate;
        tcModel.StartDate = tc.StartDate;
        tcModel.Value = totalValueWithoutInsurance;
        tcModel.SubValue = totalValueWithoutInsurance - totalVatValue;
        tcModel.VatValue = totalVatValue;
        tcModel.Status = tc.Status;
        tcModel.TenancyReason = tc.TenancyReason;
        tcModel.TenancyNumber = tc.TenancyNumber;
        tcModel.TenancyContractDetails = tc.Details.Select(m => new TenancyContractDetailsModel
        {
            Total = m.Total,
            Tax = m.Tax,
            Type = m.Type,
            Value = m.Value,
        }).ToList();


        var NextIncrementNumberForCheks = await GetNextIncrementNumber<TenancyContractPaymentPlanModel>(m => m.IncrementNumber);

        var deletedPaymentPlan = paymentPlanModel.Where(m => !tc.PaymentPlan.Select(m => m.Id).Contains(m.Id)).ToList();
        var deletedPaymentPlanFiles = deletedPaymentPlan.Where(m => m.Files.Any()).SelectMany(p => p.Files, (m, p) => p).ToList();

        var addedPaymentPlanModel = new List<TenancyContractPaymentPlanModel>();

        foreach (var item in tc.PaymentPlan)
        {
            var docCode = string.Empty;
            var id = Guid.NewGuid();

            if (paymentPlanModel.Any(m => m.Id == item.Id))
            {
                var currentPayment = paymentPlanModel.First(m => m.Id == item.Id);
                currentPayment.BankTypeId = item.BankId;
                currentPayment.Number = item.Number;
                currentPayment.Value = item.Value;
                currentPayment.PayDate = item.PayDate;
                currentPayment.Description = item.Description;
                currentPayment.Type = item.Type;
                currentPayment.LastCheckStatus = CheckStatus.UnClear;
                currentPayment.LastNotes = $"{item.Number} - {item.Description} - {item.Bank}";
                filesModel.Where(m => item.FilesId.Contains(m.Id)).ToList().ForEach(m =>
                {
                    m.CheckId = currentPayment.Id;
                });
            }
            else
            {
                var paymentModel = new TenancyContractPaymentPlanModel
                {
                    Id = id,
                    TenancyContractId = tcModel.Id,
                    DocCode = docCode,
                    BankTypeId = item.BankId,
                    Number = item.Number,
                    Value = item.Value,
                    PayDate = item.PayDate,
                    Description = item.Description,
                    Type = item.Type,
                    LastCheckStatus = CheckStatus.UnClear,
                    LastNotes = $"{item.Number} - {item.Description} - {item.Bank}",
                };
                addedPaymentPlanModel.Add(paymentModel);
                filesModel.Where(m => item.FilesId.Contains(m.Id)).ToList().ForEach(m =>
                {
                    m.CheckId = paymentModel.Id;
                });
            }
        }

        if (isAdded)
            await context.AddAsync(tcModel);
        else
            context.Update(tcModel);

        if (addedPaymentPlanModel.Any())
            await context.AddRangeAsync(addedPaymentPlanModel);

        if (deletedPaymentPlanFiles.Any())
            context.RemoveRange(deletedPaymentPlanFiles);

        if (deletedPaymentPlan.Any())
            context.RemoveRange(deletedPaymentPlan);

        await context.SaveChangesAsync();

        await context.File.Where(m => tc.FilesIds.Contains(m.Id)).ExecuteUpdateAsync(m => m.SetProperty(m => m.TenancyContractId, tcModel.Id));

        await tran.CommitAsync();

        return await GetByIdAsync(tcModel.Id);
    }
    public async Task<CommonResponseDto<bool>> ApproveWaitingList(Guid id)
    {
        var tran = await context.Database.BeginTransactionAsync();

        var tcModel = await context.TenancyContract
                                   .Where(m => m.Id == id)
                                   .Include(m => m.Files)
                                   .Include(m => m.TenancyContractDetails)
                                   .Include(m => m.PaymentPlan).ThenInclude(m => m.Files)
                                   .FirstOrDefaultAsync();


        if (tcModel == null || tcModel.Approved)
        {
            throw new NotFoundException(ErrorMessages.TenancyContractNotFound);
        }
        var paymentPlanIds = tcModel.PaymentPlan.Select(m => m.Id).ToList();

        Guid? nullGuid = null;
        await context.File.Where(m => m.TenancyContractId == id).ExecuteUpdateAsync(m => m.SetProperty(m => m.TenancyContractId, nullGuid));
        await context.File.Where(m => m.CheckId.HasValue && paymentPlanIds.Contains(m.CheckId.Value)).ExecuteUpdateAsync(m => m.SetProperty(m => m.CheckId, nullGuid));
        await context.TenancyContractDetails.Where(m => m.TenancyContractId == id).ExecuteDeleteAsync();
        await context.CheckHistory.Where(m => paymentPlanIds.Contains(m.TenancyContractPaymentPlanId)).ExecuteDeleteAsync();
        await context.TenancyContractPaymentPlan.Where(m => m.TenancyContractId == id).ExecuteDeleteAsync();
        await context.TenancyContract.Where(m => m.Id == id).ExecuteDeleteAsync();

        await AddAsync(new AddTenancyContractDto
        {
            EndDate = tcModel.EndDate,
            LandLordId = tcModel.LandLordId,
            TenantId = tcModel.TenantId,
            PropertyId = tcModel.PropertyId,
            UnitId = tcModel.UnitId,
            StartDate = tcModel.StartDate,
            Status = tcModel.Status,
            Value = tcModel.Value,
            TenancyReason = tcModel.TenancyReason,
            TenancyNumber = tcModel.TenancyNumber,
            Details = tcModel.TenancyContractDetails.Select(m => new TenancyContractDetailsDto
            {
                Value = m.Value,
                Tax = m.Tax,
                Total = m.Total,
                Type = m.Type,
            }).ToList(),
            FilesIds = tcModel.Files.Select(m => m.Id).ToList(),
            PaymentPlan = tcModel.PaymentPlan.Select(m => new TenancyContractPaymentPlanDto
            {
                Description = m.Description,
                Number = m.Number,
                PayDate = m.PayDate,
                Type = m.Type,
                BankId = m.BankTypeId,
                Value = m.Value,
                FilesId = m.Files.Select(m => m.Id).ToList(),
            }).ToList()
        }, tran);

        return new(true);
    }
    public async Task<CommonResponseDto<bool>> AddFile(Guid id, Guid fileId)
    {
        if (!await context.TenancyContract.AnyAsync(m => m.Id == id))
        {
            throw new NotFoundException(ErrorMessages.TenancyContractNotFound);
        }
        await context.File
                      .Where(m => m.Id == fileId)
                      .ExecuteUpdateAsync(m => m.SetProperty(m => m.TenancyContractId, id));

        return new(true);
    }
    public async Task<CommonResponseDto<GetTenancyContractDto>> AddAsync(AddTenancyContractDto tc, IDbContextTransaction tran = null)
    {
        if (tran == null)
            tran = await context.Database.BeginTransactionAsync();

        var uaeDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time"));

        #region Validation

        if (!await CheckEntityExist<LandLordModel>(m => m.Id == tc.LandLordId))
        {
            throw new NotFoundException(ErrorMessages.LandLordNotFound);
        }
        if (!await CheckEntityExist<UnitModel>(m => m.Id == tc.UnitId))
        {
            throw new NotFoundException(ErrorMessages.UnitTypeNotFound);
        }
        if (!await CheckEntityExist<TenantModel>(m => m.Id == tc.TenantId))
        {
            throw new NotFoundException(ErrorMessages.TenantNotFound);
        }
        if (!await CheckEntityExist<PropertyModel>(m => m.Id == tc.PropertyId))
        {
            throw new NotFoundException(ErrorMessages.PropertyNotFound);
        }
        if (await CheckEntityExist<TenancyContractModel>(m => m.UnitId == tc.UnitId && m.EndDate.Date >= uaeDate.Date))
        {
            throw new NotFoundException(ErrorMessages.UnitHasContract);
        }
        if (tc.Details.Where(m => m.Type == TenancyContractOperationType.Rent).Count() != 1)
        {
            throw new ValidationException(ErrorMessages.AllowOnlyOneRentValue);
        }
        //if (tc.PaymentPlan.Any(m => m.PayDate.Date < DateTime.Now.Date))
        //{
        //    throw new ValidationException(ErrorMessages.InvalidPaymentDate);
        //}
        var unitModel = await context.Unit.Where(m => m.Id == tc.UnitId).FirstOrDefaultAsync();
        var vatValue = unitModel.TenancyContractType == TenancyContractType.Residential ? 0 : Constants.Vat;

        //   var s = tc.Details.Where(m => m.Type != TenancyContractOperationType.Insurance).Select(m => Math.Round(m.Value * vatValue, 2)).ToList();
        if (tc.Details.Where(m => m.Type != TenancyContractOperationType.Insurance).Any(m => Math.Round(m.Value * vatValue, 2) != m.Tax))
        {
            throw new ValidationException(ErrorMessages.InvalidVatValue);
        }
        if (tc.Details.Any(m => m.Total != m.Value + m.Tax))
        {
            throw new ValidationException(ErrorMessages.InvalidPaymentTotalValue);
        }
        if (tc.Details.Sum(m => m.Total) != tc.PaymentPlan.Sum(m => m.Value))
        {
            throw new ValidationException(ErrorMessages.PaymentPlanValueNotEqualToTotalValue);
        }
        #endregion

        var allFilesIds = tc.PaymentPlan.SelectMany(p => p.FilesId, (m, p) => p).ToList();
        var filesModel = await context.File.Where(m => allFilesIds.Contains(m.Id)).ToListAsync();

        var NextIncrementNumber = await GetNextIncrementNumber<TenancyContractModel>(m => m.IncrementNumber);
        var landLordTenancyContractNumber =
            !await context.TenancyContract.AnyAsync(m => m.LandLordId == tc.LandLordId) ? 1 :
             await context.TenancyContract.Where(m => m.LandLordId == tc.LandLordId).MaxAsync(m => m.LandLordTenancyContractNumber) + 1;

        var landLordDocCode = await context.LandLord.Where(m => m.Id == tc.LandLordId).Select(m => m.DocCode).FirstOrDefaultAsync();

        var totalValueWithoutInsurance = tc.Details.Where(m => m.Type != TenancyContractOperationType.Insurance).Sum(m => m.Value);
        var insuranceValue = tc.Details.Where(m => m.Type == TenancyContractOperationType.Insurance).Sum(m => m.Value);
        var totalVatValue = tc.Details.Sum(m => m.Tax);


        var tcModel = new TenancyContractModel
        {
            Id = Guid.NewGuid(),
            LandLordTenancyContractNumber = landLordTenancyContractNumber,
            LandLordTenancyContractCode = StringHelper.CreateLandLordTenancyContractCode(landLordTenancyContractNumber, landLordDocCode),
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.TC, NextIncrementNumber),
            IncrementNumber = NextIncrementNumber,
            PropertyId = tc.PropertyId,
            TenantId = tc.TenantId,
            LandLordId = tc.LandLordId,
            UnitId = tc.UnitId,
            EndDate = tc.EndDate,
            StartDate = tc.StartDate,
            Value = totalValueWithoutInsurance,
            SubValue = totalValueWithoutInsurance - totalVatValue,
            VatValue = totalVatValue,
            Status = tc.Status,
            TenancyReason = tc.TenancyReason,
            TenancyNumber = tc.TenancyNumber,
            Approved = true,
            TenancyContractDetails = tc.Details.Select(m => new TenancyContractDetailsModel
            {
                Total = m.Total,
                Tax = m.Tax,
                Type = m.Type,
                Value = m.Value,
            }).ToList(),
        };

        var NextIncrementNumberForCheks = await GetNextIncrementNumber<TenancyContractPaymentPlanModel>(m => m.IncrementNumber);

        var paymentPlanModel = new List<TenancyContractPaymentPlanModel>();
        var checksHistory = new List<CheckHistoryModel>();

        foreach (var item in tc.PaymentPlan)
        {
            var docCode = string.Empty;
            var id = Guid.NewGuid();
            if (item.Type == TenancyContractPaymentType.Check)
            {
                docCode = StringHelper.CreateDocumentCode(DocumentNumber.TCC, NextIncrementNumberForCheks);
                NextIncrementNumberForCheks = NextIncrementNumberForCheks + 1;
                checksHistory.Add(new CheckHistoryModel
                {
                    CheckStatus = CheckStatus.UnClear,
                    PayDate = item.PayDate,
                    LandLordApproved = true,
                    //Notes = $"{item.Number} - {item.Description} - {item.Bank}",
                    TenancyContractPaymentPlanId = id,
                });
            }
            var paymentModel = new TenancyContractPaymentPlanModel
            {
                Id = id,
                DocCode = docCode,
                BankTypeId = item.BankId,
                Number = item.Number,
                Value = item.Value,
                PayDate = item.PayDate,
                Description = item.Description,
                Type = item.Type,
                LastCheckStatus = CheckStatus.UnClear,
                LastNotes = $"{item.Number} - {item.Description} - {item.Bank}",
            };
            paymentPlanModel.Add(paymentModel);

            filesModel.Where(m => item.FilesId.Contains(m.Id)).ToList().ForEach(m =>
            {
                m.CheckId = paymentModel.Id;
            });
        }

        tcModel.PaymentPlan = paymentPlanModel;
        await context.AddAsync(tcModel);
        await context.SaveChangesAsync();

        await context.AddRangeAsync(checksHistory);
        await context.File.Where(m => tc.FilesIds.Contains(m.Id)).ExecuteUpdateAsync(m => m.SetProperty(m => m.TenancyContractId, tcModel.Id));

        #region  Fininace Operations



        var increament = 1;
        var operations = new List<AddFinanceOperationDto>
        {
            new()
            {
                AccountType = FinanceAccountType.TenantAccount,
                Type = FinanceOperationType.Debit,
                TenantId = tc.TenantId,
                Value = totalValueWithoutInsurance,
                TransActionIncrement = increament,
                TenancyContractId = tcModel.Id,
                Notes1 = "Rent Flat",
            }
        };
        increament++;
        operations.Add(new AddFinanceOperationDto()
        {
            AccountType = FinanceAccountType.AdvanceRentalAccount,
            Type = FinanceOperationType.Credit,
            Value = totalValueWithoutInsurance,
            TransActionIncrement = increament,
            TenancyContractId = tcModel.Id,
            Notes1 = "Rent Flat",
        });
        if (totalVatValue != 0)
        {
            increament++;
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.TenantAccount,
                Type = FinanceOperationType.Debit,
                TenantId = tc.TenantId,
                Value = totalVatValue,
                TransActionIncrement = increament,
                TenancyContractId = tcModel.Id,
                Notes1 = "OutPut Vat",
            });
            increament++;
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.OutputVATAccount,
                Type = FinanceOperationType.Credit,
                Value = totalVatValue,
                TransActionIncrement = increament,
                TenancyContractId = tcModel.Id,
                Notes1 = "OutPut Vat",
            });
        }
        if (insuranceValue != 0)
        {
            increament++;
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.CashCompanyAccount,
                Type = FinanceOperationType.Debit,
                LandLordId = tc.LandLordId,
                Value = insuranceValue,
                TransActionIncrement = increament,
                TenancyContractId = tcModel.Id,
                Notes1 = "Security Deposit",
            });
            increament++;
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.SecuityDepositAccount,
                Type = FinanceOperationType.Credit,
                Value = insuranceValue,
                TransActionIncrement = increament,
                TenancyContractId = tcModel.Id,
                Notes1 = "Security Deposit",
            });
        }
        increament++;

        foreach (var item in paymentPlanModel)
        {
            if (item.Type == TenancyContractPaymentType.Cash)
            {
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.CashCompanyAccount,
                    Type = FinanceOperationType.Debit,
                    Value = item.Value,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Cash In {item.PayDate}",
                    PaymentDate = item.PayDate,
                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.TenantAccount,
                    Type = FinanceOperationType.Credit,
                    TenantId = tc.TenantId,
                    Value = item.Value,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Cash In {item.PayDate}",
                    PaymentDate = item.PayDate,
                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.AdvanceRentalAccount,
                    Type = FinanceOperationType.Debit,
                    Value = item.Value,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Against First Payment {item.Number}",
                    PaymentDate = item.PayDate,
                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.LandLordAccount,
                    Type = FinanceOperationType.Credit,
                    LandLordId = tc.LandLordId,
                    Value = item.Value,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Against First Payment {item.Number}",
                    PaymentDate = item.PayDate,
                });
            }
            else if (item.Type == TenancyContractPaymentType.Check)
            {
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.PDCReceivablesAccount,
                    Type = FinanceOperationType.Debit,
                    Value = item.Value,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Cheque No: {item.Number} Date: {item.PayDate}",
                    PaymentDate = item.PayDate,
                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.TenantAccount,
                    Type = FinanceOperationType.Credit,
                    TenantId = tc.TenantId,
                    Value = item.Value,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Cheque No: {item.Number} Date: {item.PayDate}",
                    PaymentDate = item.PayDate,
                });
            }
        }
        await financeRepo.AddFinanceOperation(operations, tran);
        #endregion

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.NewTenancyContract,
            SenderId = CurrentUserId,
            ObjectId = tcModel.Id,
        });

        return await GetByIdAsync(tcModel.Id);
    }
    public async Task<CommonResponseDto<List<GetTenancyContractDto>>> GetAllAsync
        (bool approved, int pageSize, int pageNum, string search, TenancyContractSort tenancyContractSort, bool isAsc,
        DateTime? from, DateTime? to, Guid? landlord, Guid? property, Guid? unit, Guid? tenant)
    {

        if (CurrentUserRole == UserType.LandLord.ToString())
        {
            landlord = CurrentUserId;
        }

        Expression<Func<TenancyContractModel, bool>> expression = tc =>
        tc.Approved == approved &&
        (!from.HasValue || tc.StartDate >= from) &&
        (!to.HasValue || tc.StartDate <= to) &&
        (!landlord.HasValue || tc.LandLordId == landlord.Value) &&
        (!unit.HasValue || tc.UnitId == unit.Value) &&
        (!tenant.HasValue || tc.TenantId == tenant.Value) &&
        (!property.HasValue || tc.PropertyId == property.Value);

        var tenancyContractQuery = context.TenancyContract.Where(expression);
        tenancyContractQuery = FilterByStringProperty(tenancyContractQuery, search);

        switch (tenancyContractSort)
        {
            case TenancyContractSort.Type:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.Unit.TenancyContractType.ToString());
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.Unit.TenancyContractType.ToString());
                break;
            case TenancyContractSort.Status:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.Status.ToString());
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.Status.ToString());
                break;
            case TenancyContractSort.LandLordName:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.LandLord.FullName);
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.LandLord.FullName);
                break;
            case TenancyContractSort.TenantName:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.Tenant.FullName);
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.Tenant.FullName);
                break;
            case TenancyContractSort.PropertyDocCode:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.Property.DocCode);
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.Property.DocCode);
                break;
            case TenancyContractSort.UnitDocCode:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.Unit.DocCode);
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.Unit.DocCode);
                break;
            case TenancyContractSort.TenantNationality:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.Tenant.Nationality);
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.Tenant.Nationality);
                break;
            case TenancyContractSort.TenantEmail:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.Tenant.Email);
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.Tenant.Email);
                break;
            case TenancyContractSort.TenantPhone:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.Tenant.Phone);
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.Tenant.Phone);
                break;
            case TenancyContractSort.ContractNumber:
                if (isAsc)
                    tenancyContractQuery = tenancyContractQuery.OrderBy(tc => tc.TenancyNumber);
                else
                    tenancyContractQuery = tenancyContractQuery.OrderByDescending(tc => tc.TenancyNumber);
                break;
            default:
                tenancyContractQuery = CustomOrderBy(tenancyContractQuery, tenancyContractSort, isAsc);
                break;
        }

        var data = await tenancyContractQuery
            .Skip(pageNum * pageSize)
            .Take(pageSize)
            .Select(m => new GetTenancyContractDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                LandLordTenancyContractCode = m.LandLordTenancyContractCode,
                EndDate = m.EndDate,
                StartDate = m.StartDate,
                Status = m.Status,
                CreateDate = m.CreateDate,
                Value = m.Value,
                SubValue = m.SubValue,
                VatValue = m.VatValue,
                TenancyReason = m.TenancyReason,
                TenancyNumber = m.TenancyNumber,
                Unit = new GetAllUnitSummaryDto
                {
                    Id = m.UnitId,
                    DocCode = m.Unit.DocCode,
                    Number = m.Unit.UnitNumber,
                    UnitName = m.Unit.UnitName,
                    Description = m.Unit.Description,
                    FloorNumber = m.Unit.FloorNumber,
                    TenancyContractType = m.Unit.TenancyContractType,
                    UnitType = new UnitTypeDto
                    {
                        Id = m.Unit.UnitType.Id,
                        Name = m.Unit.UnitType.Name,
                        NameEn = m.Unit.UnitType.NameEn,
                        DocCode = m.Unit.UnitType.DocCode,
                    },
                },
                Property = new GetBaseNameWithCode
                {
                    Id = m.PropertyId,
                    DocCode = m.Property.DocCode,
                    Name = m.Property.Name
                },
                LandLord = new GetLandLordSummaryDto
                {
                    Id = m.LandLordId,
                    DocCode = m.LandLord.DocCode,
                    FullName = m.LandLord.FullName,
                    TaxNumber = m.LandLord.TaxNumber,
                },
                Tenant = new GetTenantSummaryDto
                {
                    Id = m.TenantId,
                    DocCode = m.Tenant.DocCode,
                    FullName = m.Tenant.FullName,
                    Nationality = m.Tenant.Nationality,
                    Phone = m.Tenant.Phone,
                    Email = m.Tenant.Email
                },
                Files = m.Files.Where(f => f.IsValid).Select(f => new GetBaseFileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList(),
                Details = m.TenancyContractDetails.Select(m => new TenancyContractDetailsDto
                {
                    Tax = m.Tax,
                    Type = m.Type,
                    Value = m.Value,
                    Total = m.Total
                }).ToList(),
                PaymentPlan = m.PaymentPlan.OrderBy(m => m.PayDate).Select(m => new TenancyContractPaymentPlanDto
                {
                    Bank = m.BankTypeId.HasValue ? new REM_Dto.BankType.GetAllBankTypeDto
                    {
                        Id = m.BankType.Id,
                        DocCode = m.BankType.DocCode,
                        Title = m.BankType.Title,
                        TitleEn = m.BankType.TitleEn,
                    } : null,
                    Description = m.Description,
                    PayDate = m.PayDate,
                    Type = m.Type,
                    Number = m.Number,
                    Value = m.Value,
                    Files = m.Files.Select(m => new GetBaseFileDto
                    {
                        FileKind = m.FileKind,
                        FileType = m.FileType,
                        Id = m.Id,
                        Url = m.Url,
                    }).ToList(),
                }).ToList(),
            }).ToListAsync();

        var totalRecords = await tenancyContractQuery.CountAsync();
        return new(data, pageSize, pageNum, totalRecords);
    }
    public async Task<CommonResponseDto<GetTenancyContractDto>> GetByIdAsync(Guid Id)
    {
        var data = await context.TenancyContract
            .Where(m => m.Id == Id)
            .Select(m => new GetTenancyContractDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                LandLordTenancyContractCode = m.LandLordTenancyContractCode,
                EndDate = m.EndDate,
                StartDate = m.StartDate,
                Status = m.Status,
                Value = m.Value,
                SubValue = m.SubValue,
                VatValue = m.VatValue,
                CreateDate = m.CreateDate,
                TenancyReason = m.TenancyReason,
                TenancyNumber = m.TenancyNumber,
                Unit = new GetAllUnitSummaryDto
                {
                    Id = m.UnitId,
                    DocCode = m.Unit.DocCode,
                    UnitName = m.Unit.UnitName,
                    Number = m.Unit.UnitNumber,
                    Description = m.Unit.Description,
                    FloorNumber = m.Unit.FloorNumber,
                    TenancyContractType = m.Unit.TenancyContractType,
                    UnitType = new UnitTypeDto
                    {
                        Id = m.Unit.UnitType.Id,
                        Name = m.Unit.UnitType.Name,
                        NameEn = m.Unit.UnitType.NameEn,
                        DocCode = m.Unit.UnitType.DocCode,
                    },
                },
                Property = new GetBaseNameWithCode
                {
                    Id = m.PropertyId,
                    DocCode = m.Property.DocCode,
                    Name = m.Property.Name
                },
                LandLord = new GetLandLordSummaryDto
                {
                    Id = m.LandLordId,
                    DocCode = m.LandLord.DocCode,
                    FullName = m.LandLord.FullName,
                    TaxNumber = m.LandLord.TaxNumber,
                },
                Tenant = new GetTenantSummaryDto
                {
                    Id = m.TenantId,
                    DocCode = m.Tenant.DocCode,
                    FullName = m.Tenant.FullName,
                    Nationality = m.Tenant.Nationality,
                    Phone = m.Tenant.Phone,
                    Email = m.Tenant.Email
                },
                Files = m.Files.Where(f => f.IsValid).Select(f => new GetBaseFileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList(),
                Details = m.TenancyContractDetails.Select(m => new TenancyContractDetailsDto
                {
                    Tax = m.Tax,
                    Type = m.Type,
                    Value = m.Value,
                    Total = m.Total
                }).ToList(),
                PaymentPlan = m.PaymentPlan.OrderBy(m => m.PayDate).Select(m => new TenancyContractPaymentPlanDto
                {
                    Id = m.Id,
                    Bank = m.BankTypeId.HasValue ? new REM_Dto.BankType.GetAllBankTypeDto
                    {
                        Id = m.BankType.Id,
                        DocCode = m.BankType.DocCode,
                        Title = m.BankType.Title,
                        TitleEn = m.BankType.TitleEn,
                    } : null,
                    Description = m.Description,
                    PayDate = m.PayDate,
                    Type = m.Type,
                    Number = m.Number,
                    Value = m.Value,
                    Files = m.Files.Select(m => new GetBaseFileDto
                    {
                        FileKind = m.FileKind,
                        FileType = m.FileType,
                        Id = m.Id,
                        Url = m.Url,
                    }).ToList(),
                }).ToList(),
            }).FirstOrDefaultAsync();

        return new(data);
    }
    public async Task<CommonResponseDto<GetTenancyContractDto>> GetByNumberAsync(string number)
    {
        var data = await context.TenancyContract
            .Where(m => m.TenancyNumber == number)
            .Select(m => new GetTenancyContractDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                LandLordTenancyContractCode = m.LandLordTenancyContractCode,
                EndDate = m.EndDate,
                StartDate = m.StartDate,
                Status = m.Status,
                Value = m.Value,
                SubValue = m.SubValue,
                VatValue = m.VatValue,
                CreateDate = m.CreateDate,
                TenancyReason = m.TenancyReason,
                TenancyNumber = m.TenancyNumber,
                Unit = new GetAllUnitSummaryDto
                {
                    Id = m.UnitId,
                    DocCode = m.Unit.DocCode,
                    UnitName = m.Unit.UnitName,
                    Number = m.Unit.UnitNumber,
                    Description = m.Unit.Description,
                    FloorNumber = m.Unit.FloorNumber,
                    TenancyContractType = m.Unit.TenancyContractType,
                    UnitType = new UnitTypeDto
                    {
                        Id = m.Unit.UnitType.Id,
                        Name = m.Unit.UnitType.Name,
                        NameEn = m.Unit.UnitType.NameEn,
                        DocCode = m.Unit.UnitType.DocCode,
                    },
                },
                Property = new GetBaseNameWithCode
                {
                    Id = m.PropertyId,
                    DocCode = m.Property.DocCode,
                    Name = m.Property.Name
                },
                LandLord = new GetLandLordSummaryDto
                {
                    Id = m.LandLordId,
                    DocCode = m.LandLord.DocCode,
                    FullName = m.LandLord.FullName,
                    TaxNumber = m.LandLord.TaxNumber,
                },
                Tenant = new GetTenantSummaryDto
                {
                    Id = m.TenantId,
                    DocCode = m.Tenant.DocCode,
                    FullName = m.Tenant.FullName,
                    Nationality = m.Tenant.Nationality,
                    Phone = m.Tenant.Phone,
                    Email = m.Tenant.Email
                },
                Files = m.Files.Where(f => f.IsValid).Select(f => new GetBaseFileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList(),
                Details = m.TenancyContractDetails.Select(m => new TenancyContractDetailsDto
                {
                    Tax = m.Tax,
                    Type = m.Type,
                    Value = m.Value,
                    Total = m.Total
                }).ToList(),
                PaymentPlan = m.PaymentPlan.OrderBy(m => m.PayDate).Select(m => new TenancyContractPaymentPlanDto
                {
                    Id = m.Id,
                    Bank = m.BankTypeId.HasValue ? new REM_Dto.BankType.GetAllBankTypeDto
                    {
                        Id = m.BankType.Id,
                        DocCode = m.BankType.DocCode,
                        Title = m.BankType.Title,
                        TitleEn = m.BankType.TitleEn,
                    } : null,
                    Description = m.Description,
                    PayDate = m.PayDate,
                    Type = m.Type,
                    Number = m.Number,
                    Value = m.Value,
                    Files = m.Files.Select(m => new GetBaseFileDto
                    {
                        FileKind = m.FileKind,
                        FileType = m.FileType,
                        Id = m.Id,
                        Url = m.Url,
                    }).ToList(),
                }).ToList(),
            }).FirstOrDefaultAsync();

        return new(data);
    }

    public async Task ChangeStatus(ChangeTenancyContractStatusDto dto)
    {
        var tran = await context.Database.BeginTransactionAsync();
        var tcModel = await context.TenancyContract.Where(m => m.Id == dto.Id).Include(m => m.TenancyContractDetails).FirstOrDefaultAsync();
        if (tcModel == null)
        {
            throw new NotFoundException(ErrorMessages.TenancyContractNotFound);
        }
        tcModel.Status = dto.Status;
        var operations = new List<AddFinanceOperationDto>();

        if (dto.Status == TenancyContractStatus.Vacant)
        {
            var remainDays = (tcModel.EndDate - DateTime.Now).TotalDays;
            var totalPrice = tcModel.TenancyContractDetails.Sum(m => m.Total);
            var priceOfDay = totalPrice / 365;
            var returnValue = remainDays <= 0 ? 0 : priceOfDay * remainDays;


            var increament = 1;

            if (returnValue != 0)
            {
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.TenantAccount,
                    Type = FinanceOperationType.Credit,
                    TenantId = tcModel.TenantId,
                    Value = returnValue,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Return Value for tenant for tenancy contract: {tcModel.DocCode} in {DateTime.Now}",
                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.AdvanceRentalAccount,
                    Type = FinanceOperationType.Debit,
                    Value = returnValue,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Return Value for tenant  for tenancy contract: {tcModel.DocCode} in {DateTime.Now}",
                });
            }

            if (dto.Fine != 0)
            {
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.TenantAccount,
                    Type = FinanceOperationType.Debit,
                    TenantId = tcModel.TenantId,
                    Value = dto.Fine,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Fine on tenant for tenancy contract: {tcModel.DocCode} in {DateTime.Now}",
                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.AdvanceRentalAccount,
                    Type = FinanceOperationType.Credit,
                    Value = dto.Fine,
                    TransActionIncrement = increament,
                    TenancyContractId = tcModel.Id,
                    Notes1 = $"Fine on tenant for tenancy contract: {tcModel.DocCode} in {DateTime.Now}",
                });
            }
        }
        if (operations.Any())
        {
            await financeRepo.AddFinanceOperation(operations, tran);
        }
        else
        {
            await context.SaveChangesAsync();
            await tran.CommitAsync();
        }

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.ChangeTenancyContractStatus,
            SenderId = CurrentUserId,
            ObjectId = tcModel.Id,
        });
    }
    public async Task RemoveAsync(Guid Id)
    {
        var tcModel = await context.TenancyContract.Where(m => m.Id == Id).FirstOrDefaultAsync() ??
            throw new NotFoundException(ErrorMessages.TenancyContractNotFound);

        tcModel.IsValid = false;
        await context.TenancyContractPaymentPlan.Where(m => m.TenancyContractId == Id).ExecuteUpdateAsync(m => m.SetProperty(m => m.IsValid, false));
        await context.TenancyContractDetails.Where(m => m.TenancyContractId == Id).ExecuteUpdateAsync(m => m.SetProperty(m => m.IsValid, false));
        await context.SaveChangesAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.RemoveTenancyContract,
            SenderId = CurrentUserId,
            ObjectId = tcModel.Id,
        });
    }


    public async Task<CommonResponseDto<List<GetAllChecksDto>>> GetAllChecks(
        int pageSize, int pageNum, DateTime? from, DateTime? to, string search,
        ChequeSort chequeSort, bool isAsc, CheckStatus? chequeStatus, Guid? tenancyContractId
        , Guid? landlord, Guid? property, Guid? unit, Guid? tenant)
    {

        if (CurrentUserRole == UserType.LandLord.ToString())
        {
            landlord = CurrentUserId;
        }

        Expression<Func<TenancyContractPaymentPlanModel, bool>> expression = c =>
        (c.Type == TenancyContractPaymentType.Check)
        && (!from.HasValue || c.PayDate >= from)
        && (!to.HasValue || c.PayDate <= to)
        && (!tenancyContractId.HasValue || c.TenancyContractId == tenancyContractId)
        && (!chequeStatus.HasValue || c.LastCheckStatus == chequeStatus)
        && (!landlord.HasValue || c.TenancyContract.LandLordId == landlord.Value)
        && (!unit.HasValue || c.TenancyContract.UnitId == unit.Value)
        && (!tenant.HasValue || c.TenancyContract.TenantId == tenant.Value)
        && (!property.HasValue || c.TenancyContract.PropertyId == property.Value);

        var chequeQuery = context.TenancyContractPaymentPlan.Where(expression);
        chequeQuery = FilterByStringProperty(chequeQuery, search);
        chequeQuery = CustomOrderBy(chequeQuery, chequeSort, isAsc);

        var data = await chequeQuery
            .Skip(pageNum * pageSize)
            .Take(pageSize)
            .Select(m => new GetAllChecksDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                Bank = m.BankTypeId.HasValue ? new REM_Dto.BankType.GetAllBankTypeDto
                {
                    Id = m.BankType.Id,
                    DocCode = m.BankType.DocCode,
                    Title = m.BankType.Title,
                    TitleEn = m.BankType.TitleEn,
                } : null,
                TenantContractId = m.TenancyContractId,
                Description = m.Description,
                LastCheckStatus = m.LastCheckStatus,
                LastNotes = m.LastNotes,
                Number = m.Number,
                PayDate = m.PayDate,
                Value = m.Value,
                CollectedValue = m.LastCheckStatus == CheckStatus.Clear ? m.Value :
                                  m.FinanceOperations
                                  .Where(m => m.Type == FinanceOperationType.Debit &&
                                             (m.Account.Type == FinanceAccountType.BankAccount) ||
                                             (m.Account.Type == FinanceAccountType.CashCompanyAccount)).Select(m => m.Value).Sum(),

                Balance = m.Value -
                                       (m.LastCheckStatus == CheckStatus.Clear ? m.Value :
                                       m.FinanceOperations
                                      .Where(m => m.Type == FinanceOperationType.Debit &&
                                             (m.Account.Type == FinanceAccountType.BankAccount) ||
                                             (m.Account.Type == FinanceAccountType.CashCompanyAccount)).Select(m => m.Value).Sum()),
                CreateDate = m.CreateDate,
                TenantContractDocCode = m.TenancyContract.DocCode,
                Unit = new GetAllUnitSummaryDto
                {
                    Id = m.TenancyContract.UnitId,
                    DocCode = m.TenancyContract.Unit.DocCode,
                    Number = m.TenancyContract.Unit.UnitNumber,
                    UnitName = m.TenancyContract.Unit.UnitName,
                    Description = m.TenancyContract.Unit.Description,
                    FloorNumber = m.TenancyContract.Unit.FloorNumber,
                    TenancyContractType = m.TenancyContract.Unit.TenancyContractType
                },
                Property = new GetBaseNameWithCode
                {
                    Id = m.TenancyContract.PropertyId,
                    DocCode = m.TenancyContract.Property.DocCode,
                    Name = m.TenancyContract.Property.Name
                },
                LandLord = new GetLandLordSummaryDto
                {
                    Id = m.TenancyContract.LandLordId,
                    DocCode = m.TenancyContract.LandLord.DocCode,
                    FullName = m.TenancyContract.LandLord.FullName,
                    TaxNumber = m.TenancyContract.LandLord.TaxNumber,
                },
                Tenant = new GetTenantSummaryDto
                {
                    Id = m.TenancyContract.TenantId,
                    DocCode = m.TenancyContract.Tenant.DocCode,
                    FullName = m.TenancyContract.Tenant.FullName,
                    Nationality = m.TenancyContract.Tenant.Nationality,
                    Phone = m.TenancyContract.Tenant.Phone,
                    Email = m.TenancyContract.Tenant.Email
                },
                History = m.CheckHistory.Where(h => h.IsValid).OrderByDescending(m => m.CreateDate).Select(m => new GetCheckHistoryDto
                {
                    Id = m.Id,
                    CheckStatus = m.CheckStatus,
                    CreateDate = m.CreateDate,
                    LandLordApproved = m.LandLordApproved,
                    Notes = m.Notes,
                    PayDate = m.PayDate,
                }).ToList(),
                Files = m.Files/*.Where(f => f.IsValid)*/.Select(f => new GetBaseFileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).ToListAsync();

        var totalCounts = await chequeQuery.CountAsync();
        return new(data, pageSize, pageNum, totalCounts);
    }
    public async Task<CommonResponseDto<List<GetAllChecksSummaryDto>>> GetAllChecksNeedPayment(Guid? unitId)
    {
        Expression<Func<TenancyContractPaymentPlanModel, bool>> expression = c =>
        (!unitId.HasValue || unitId.Value == c.TenancyContract.UnitId) &&
        (
            (
               (c.Type == TenancyContractPaymentType.Check) &&
               (c.LastCheckStatus == CheckStatus.Return || c.LastCheckStatus == CheckStatus.Replace)
            ) ||
            (
               (c.Type == TenancyContractPaymentType.Cash)
            )
        );

        var chequeQuery = context.TenancyContractPaymentPlan.Where(expression);


        var data = await chequeQuery
            .Select(m => new GetAllChecksSummaryDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                LastCheckStatus = m.LastCheckStatus,
                Number = m.Number,
                Value = m.Value,
                Type = m.Type,
                CollectedValue = m.FinanceOperations
                                  .Where(fo =>
                                             (
                                                (m.Type == TenancyContractPaymentType.Check && fo.Type == FinanceOperationType.Debit) ||
                                                (m.Type == TenancyContractPaymentType.Cash && fo.Type == FinanceOperationType.Credit)
                                             ) &&
                                             (
                                               (fo.Account.Type == FinanceAccountType.BankAccount) ||
                                               (fo.Account.Type == FinanceAccountType.CashCompanyAccount) ||
                                               (fo.Account.Type == FinanceAccountType.PDCReceivablesAccount)
                                             )
                                         )
                                  .Select(m => m.Value).Sum(),

                Balance = m.Value - m.FinanceOperations
                                    .Where(fo =>
                                             (
                                                (m.Type == TenancyContractPaymentType.Check && fo.Type == FinanceOperationType.Debit) ||
                                                (m.Type == TenancyContractPaymentType.Cash && fo.Type == FinanceOperationType.Credit)
                                             ) &&
                                             (
                                               (fo.Account.Type == FinanceAccountType.BankAccount) ||
                                               (fo.Account.Type == FinanceAccountType.CashCompanyAccount) ||
                                               (fo.Account.Type == FinanceAccountType.PDCReceivablesAccount)
                                             )
                                         )
                                  .Select(m => m.Value).Sum(),

            }).ToListAsync();

        data = data.Where(m => m.Balance > 0).ToList();
        return new(data);
    }
    public async Task<CommonResponseDto<List<GetCheckHistoryDto>>> GetCheckHistory(Guid id)
    {
        var data = await context.CheckHistory
            .Where(m => m.TenancyContractPaymentPlanId == id)
            .OrderByDescending(m => m.CreateDate)
            .Select(m => new GetCheckHistoryDto
            {
                Id = m.Id,
                CheckStatus = m.CheckStatus,
                CreateDate = m.CreateDate,
                LandLordApproved = m.LandLordApproved,
                Notes = m.Notes,
                PayDate = m.PayDate,
            }).ToListAsync();

        return new(data);
    }
    public async Task ChangeCheckStatus(ChangeCheckStatusDto dto)
    {
        var tran = await context.Database.BeginTransactionAsync();

        var checkModel = await context.TenancyContractPaymentPlan
            .Where(m => m.Id == dto.Id)
            .Include(m => m.TenancyContract)
            .FirstOrDefaultAsync();

        var isLandLord = false;
        if (CurrentUserRole == UserType.LandLord.ToString())
        {
            isLandLord = true;
        }

        if (isLandLord && dto.Status != CheckStatus.Clear && dto.Status != CheckStatus.Return)
        {
            throw new NotFoundException(ErrorMessages.InvalidData);
        }
        if (checkModel == null)
        {
            throw new NotFoundException(ErrorMessages.CheckNotFound);
        }
        if (checkModel.Type != TenancyContractPaymentType.Check)
        {
            throw new NotFoundException(ErrorMessages.CheckNotFound);
        }
        if (checkModel.LastCheckStatus != CheckStatus.UnClear && checkModel.LastCheckStatus != CheckStatus.Replace && dto.Status != CheckStatus.UnClear)
        {
            throw new NotFoundException(ErrorMessages.InvalidData);
        }
        if (checkModel.LastCheckStatus == CheckStatus.Replace && dto.Status != CheckStatus.Replace)
        {
            throw new NotFoundException(ErrorMessages.InvalidData);
        }
        if (dto.NewPayDate.HasValue && dto.NewPayDate.Value < checkModel.PayDate)
        {
            throw new NotFoundException(ErrorMessages.InvalidData);
        }

        var checkHistoryModel = new CheckHistoryModel()
        {
            Notes = dto.Notes,
            CheckStatus = dto.Status,
            PayDate = dto.NewPayDate,
            TenancyContractPaymentPlanId = dto.Id,
            LandLordApproved = dto.LandLordApproved,
        };
        checkModel.PayDate = dto.NewPayDate.HasValue ? dto.NewPayDate.Value : checkModel.PayDate;
        checkModel.LastCheckStatus = dto.Status;
        checkModel.LastNotes = string.IsNullOrEmpty(dto.Notes) ? checkModel.LastNotes : dto.Notes;

        var s = await context.File.CountAsync(m => dto.FileIds.Contains(m.Id));
        var sss = await context.File.CountAsync(m => m.CheckId == checkModel.Id && !dto.FileIds.Contains(m.Id));


        await context.File.Where(m => m.CheckId == checkModel.Id && !dto.FileIds.Contains(m.Id))
                          .ExecuteDeleteAsync();

        await context.File.Where(m => dto.FileIds.Contains(m.Id))
                          .ExecuteUpdateAsync(m => m.SetProperty(m => m.CheckId, checkModel.Id));

        await context.AddAsync(checkHistoryModel);

        #region Finance Operations

        var increament = 1;
        var operations = new List<AddFinanceOperationDto>();

        if (dto.Status == CheckStatus.UnClear)
        {
            await context.FinanceOperations.Where(m => m.CheckId == checkModel.Id).ExecuteDeleteAsync();
        }

        else if (dto.Status == CheckStatus.Clear)
        {
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.LandLordAccount,
                Type = FinanceOperationType.Debit,
                LandLordId = checkModel.TenancyContract.LandLordId,
                Value = checkModel.Value,
                TransActionIncrement = increament,
                TenancyContractId = checkModel.TenancyContractId,
                CheckId = checkModel.Id,
                Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                PaymentDate = dto.NewPayDate,
            });
            increament++;
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.PDCReceivablesAccount,
                Type = FinanceOperationType.Credit,
                Value = checkModel.Value,
                TransActionIncrement = increament,
                TenancyContractId = checkModel.TenancyContractId,
                CheckId = checkModel.Id,
                Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                PaymentDate = dto.NewPayDate,
            });
            increament++;
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.AdvanceRentalAccount,
                Type = FinanceOperationType.Debit,
                Value = checkModel.Value,
                TransActionIncrement = increament,
                TenancyContractId = checkModel.TenancyContractId,
                CheckId = checkModel.Id,
                Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                PaymentDate = dto.NewPayDate,
            });
            increament++;
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.LandLordAccount,
                Type = FinanceOperationType.Credit,
                LandLordId = checkModel.TenancyContract.LandLordId,
                Value = checkModel.Value,
                TransActionIncrement = increament,
                TenancyContractId = checkModel.TenancyContractId,
                CheckId = checkModel.Id,
                Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                PaymentDate = dto.NewPayDate,
            });
        }

        else if (dto.Status == CheckStatus.Return)
        {
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.TenantAccount,
                Type = FinanceOperationType.Debit,
                TenantId = checkModel.TenancyContract.TenantId,
                Value = checkModel.Value,
                TransActionIncrement = increament,
                TenancyContractId = checkModel.TenancyContractId,
                CheckId = checkModel.Id,
                Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                PaymentDate = dto.NewPayDate,
            });
            increament++;
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.PDCReceivablesAccount,
                Type = FinanceOperationType.Credit,
                Value = checkModel.Value,
                TransActionIncrement = increament,
                TenancyContractId = checkModel.TenancyContractId,
                CheckId = checkModel.Id,
                Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                PaymentDate = dto.NewPayDate,
            });
        }

        // Hole Status has No Effect on Finance

        else if (dto.Status == CheckStatus.Replace)
        {
            /*  var NextIncrementNumber = await GetNextIncrementNumber<BondsModel>(m => m.IncrementNumber, BondType.ReceiptVouchers, dto.VoucherBondType.Value);

              var docCode =
                  dto.VoucherBondType.Value == VoucherBondTypes.ChequeReceipt ? DocumentNumber.PDCR :
                  dto.VoucherBondType.Value == VoucherBondTypes.CashReceipt ? DocumentNumber.CRV :
                  DocumentNumber.BRV;

              var accountType =
               dto.VoucherBondType == VoucherBondTypes.CashReceipt ? FinanceAccountType.CashCompanyAccount :
               dto.VoucherBondType == VoucherBondTypes.BankReceipt ? FinanceAccountType.BankAccount :
               FinanceAccountType.PDCReceivablesAccount;

              var bondModel = new BondsModel
              {
                  Id = Guid.NewGuid(),
                  IncrementNumber = NextIncrementNumber,
                  DocCode = StringHelper.CreateDocumentCode(docCode, NextIncrementNumber),
                  BondType = BondType.ReceiptVouchers,
                  VoucherBondType = dto.VoucherBondType.HasValue ? dto.VoucherBondType.Value : VoucherBondTypes.CashReceipt,
                  Date = dto.NewPayDate.HasValue ? dto.NewPayDate.Value : DateTime.Now,
                  Notes = dto.Notes,
                  CheckId = checkModel.Id,
                  TenantId = checkModel.TenancyContract.TenantId,
                  UnitId = checkModel.TenancyContract.UnitId,
              };*/

            /*if (dto.FullReplace)
            {
                bondModel.Value = checkModel.Value;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.TenantAccount,
                    Type = FinanceOperationType.Debit,
                    TenantId = checkModel.TenancyContract.TenantId,
                    Value = checkModel.Value,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,
                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.PDCReceivablesAccount,
                    Type = FinanceOperationType.Credit,
                    Value = checkModel.Value,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,
                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = accountType,
                    Type = FinanceOperationType.Debit,
                    Value = checkModel.Value,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.TenantAccount,
                    Type = FinanceOperationType.Credit,
                    TenantId = checkModel.TenancyContract.TenantId,
                    Value = checkModel.Value,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.AdvanceRentalAccount,
                    Type = FinanceOperationType.Debit,
                    Value = checkModel.Value,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.LandLordAccount,
                    Type = FinanceOperationType.Credit,
                    LandLordId = checkModel.TenancyContract.LandLordId,
                    Value = checkModel.Value,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
            }

            else
            {
                bondModel.Value = dto.PartReplaceCashValue;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.TenantAccount,
                    Type = FinanceOperationType.Debit,
                    TenantId = checkModel.TenancyContract.TenantId,
                    Value = checkModel.Value,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.PDCReceivablesAccount,
                    Type = FinanceOperationType.Credit,
                    Value = checkModel.Value,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = accountType,
                    Type = FinanceOperationType.Debit,
                    Value = dto.PartReplaceCashValue,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.TenantAccount,
                    Type = FinanceOperationType.Credit,
                    TenantId = checkModel.TenancyContract.TenantId,
                    Value = dto.PartReplaceCashValue,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.AdvanceRentalAccount,
                    Type = FinanceOperationType.Debit,
                    Value = dto.PartReplaceCashValue,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = FinanceAccountType.LandLordAccount,
                    Type = FinanceOperationType.Credit,
                    LandLordId = checkModel.TenancyContract.LandLordId,
                    Value = dto.PartReplaceCashValue,
                    TransActionIncrement = increament,
                    UnitId = checkModel.TenancyContract.UnitId,
                    CheckId = checkModel.Id,
                    Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                    BondId = bondModel.Id,

                });
            }*/

            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.TenantAccount,
                Type = FinanceOperationType.Debit,
                TenantId = checkModel.TenancyContract.TenantId,
                Value = checkModel.Value,
                TransActionIncrement = increament,
                TenancyContractId = checkModel.TenancyContractId,
                CheckId = checkModel.Id,
                Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                PaymentDate = dto.NewPayDate,
            });
            increament++;
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.PDCReceivablesAccount,
                Type = FinanceOperationType.Credit,
                Value = checkModel.Value,
                TransActionIncrement = increament,
                TenancyContractId = checkModel.TenancyContractId,
                CheckId = checkModel.Id,
                Notes1 = $"Cheque No: {checkModel.Number}  Date: {checkModel.PayDate}",
                PaymentDate = dto.NewPayDate,
            });
        }
        if (operations.Any())
            await financeRepo.AddFinanceOperation(operations, tran);

        else
        {
            await context.SaveChangesAsync();
            await tran.CommitAsync();
        }
        #endregion

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.ChangeChequeStatus,
            SenderId = CurrentUserId,
            ObjectId = dto.Id,
        });
    }
}