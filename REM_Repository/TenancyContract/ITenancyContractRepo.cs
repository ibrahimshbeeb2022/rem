﻿using Microsoft.EntityFrameworkCore.Storage;
using REM_Dto.CommonDto;
using REM_Dto.TenancyContract;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;

namespace REM_Repository.TenancyContract;

public interface ITenancyContractRepo
{
    Task<CommonResponseDto<GetTenancyContractDto>> ActionToWaitingList(ActionToWaitingListDto tc);
    Task<CommonResponseDto<GetTenancyContractDto>> AddAsync(AddTenancyContractDto tc, IDbContextTransaction tran = null);
    Task<CommonResponseDto<bool>> ApproveWaitingList(Guid id);
    Task<CommonResponseDto<bool>> AddFile(Guid id, Guid fileId);
    Task ChangeStatus(ChangeTenancyContractStatusDto dto);
    Task RemoveAsync(Guid Id);
    Task<CommonResponseDto<List<GetTenancyContractDto>>> GetAllAsync
        (bool approved, int pageSize, int pageNum, string search, TenancyContractSort tenancyContractSort,
        bool isAsc, DateTime? from, DateTime? to, Guid? landlord, Guid? property, Guid? unit, Guid? tenant);
    Task<CommonResponseDto<GetTenancyContractDto>> GetByIdAsync(Guid Id);
    Task<CommonResponseDto<GetTenancyContractDto>> GetByNumberAsync(string number);
    Task<CommonResponseDto<List<GetAllChecksDto>>> GetAllChecks(
        int pageSize, int pageNum, DateTime? from, DateTime? to, string search, ChequeSort chequeSort,
        bool isAsc, CheckStatus? chequeStatus, Guid? tenancyContractId
        , Guid? landlord, Guid? property, Guid? unit, Guid? tenant);

    Task<CommonResponseDto<List<GetAllChecksSummaryDto>>> GetAllChecksNeedPayment(Guid? unitId);
    Task<CommonResponseDto<List<GetCheckHistoryDto>>> GetCheckHistory(Guid id);
    Task ChangeCheckStatus(ChangeCheckStatusDto dto);
}