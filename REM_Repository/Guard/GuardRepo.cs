﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using REM_Database.Context;
using REM_Database.Model.Guard;
using REM_Dto.Base;
using REM_Dto.CommonDto;
using REM_Dto.Guard;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using REM_Shared.Resource;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.Guard;

public class GuardRepo : BaseRepo, IGuardRepo
{
    private readonly IStringLocalizer<GuardResource> guardLocalizer;
    public GuardRepo(REMContext context,
        IHttpContextAccessor httpContextAccessor,
        IConfiguration configuration,
        IStringLocalizer<GuardResource> guardLocalizer)
        : base(context, configuration, httpContextAccessor)
    {
        this.guardLocalizer = guardLocalizer;
    }
    public async Task<GuardDto> AddAsync(AddGuardFormDto guardFormDto)
    {
        await context.Database.BeginTransactionAsync();

        var NextIncrementNumber = await GetNextIncrementNumber<GuardModel>(m => m.IncrementNumber);

        var guardEntry = await context.Guard.AddAsync(new GuardModel
        {
            Name = guardFormDto.Name,
            NameEn = guardFormDto.NameEn,
            IdNumber = guardFormDto.IdNumber,
            //BirthDate = guardFormDto.BirthDay,
            //Profession = guardFormDto.Profession,
            IncrementNumber = NextIncrementNumber,
            //ProfessionEn = guardFormDto.ProfessionEn,
            IdExpireDate = guardFormDto.IdExpireDate,
            PassportNumber = guardFormDto.PassportNumber,
            PassportExpireDate = guardFormDto.PassportExpireDate,
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.GU, NextIncrementNumber)
        });
        await context.SaveChangesAsync();

        List<Guid> fileIds = [guardFormDto.IdFileId, guardFormDto.PassportFileId, guardFormDto.ResidenceFileId, guardFormDto.OtherFileId];
        await context.File.Where(f => fileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.GuardId, guardEntry.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();

        return ((await GetAsync(guardEntry.Entity.Id)).Data);
    }
    public async Task UpdateAsync(UpdateGuardFormDto guardFormDto)
    {
        await context.Database.BeginTransactionAsync();

        var effectedRows = await context.Guard.Where(g => g.Id == guardFormDto.Id)
            .ExecuteUpdateAsync(g => g/*.SetProperty(g => g.BirthDate, guardFormDto.BirthDay)*/
            .SetProperty(g => g.IdExpireDate, guardFormDto.IdExpireDate)
            .SetProperty(g => g.Name, guardFormDto.Name)
            .SetProperty(g => g.NameEn, guardFormDto.NameEn)
            //.SetProperty(g => g.Profession, guardFormDto.Profession)
            //.SetProperty(g => g.ProfessionEn, guardFormDto.ProfessionEn)
            .SetProperty(g => g.IdNumber, guardFormDto.IdNumber)
            .SetProperty(g => g.PassportNumber, guardFormDto.PassportNumber)
            .SetProperty(g => g.PassportExpireDate, guardFormDto.PassportExpireDate));
        if (effectedRows == 0)
            throw new NotFoundException(guardLocalizer[ErrorMessages.GuardNotFound]);

        List<string> removedUrls = [];

        if (guardFormDto.PassportFileId != Guid.Empty)
            await context.File.Where(f => f.Id == guardFormDto.PassportFileId).ExecuteUpdateAsync(f => f.SetProperty(f => f.GuardId, guardFormDto.Id));

        if (guardFormDto.RemovePassportFileId != Guid.Empty)
        {
            var url = await context.File.Where(f => f.Id == guardFormDto.RemovePassportFileId).Select(f => f.Url).FirstOrDefaultAsync();
            removedUrls.Add(url);
            await context.File.Where(f => f.Id == guardFormDto.RemovePassportFileId).ExecuteDeleteAsync();
        }

        if (guardFormDto.IdFileId != Guid.Empty)
            await context.File.Where(f => f.Id == guardFormDto.IdFileId).ExecuteUpdateAsync(f => f.SetProperty(f => f.GuardId, guardFormDto.Id));

        if (guardFormDto.RemoveIdFileId != Guid.Empty)
        {
            var url = await context.File.Where(f => f.Id == guardFormDto.RemoveIdFileId).Select(f => f.Url).FirstOrDefaultAsync();
            removedUrls.Add(url);
            await context.File.Where(f => f.Id == guardFormDto.RemoveIdFileId).ExecuteDeleteAsync();
        }

        if (guardFormDto.OtherFileId != Guid.Empty)
            await context.File.Where(f => f.Id == guardFormDto.OtherFileId).ExecuteUpdateAsync(f => f.SetProperty(f => f.GuardId, guardFormDto.Id));

        if (guardFormDto.RemoveOtherFileId != Guid.Empty)
        {
            var url = await context.File.Where(f => f.Id == guardFormDto.RemoveOtherFileId).Select(f => f.Url).FirstOrDefaultAsync();
            removedUrls.Add(url);
            await context.File.Where(f => f.Id == guardFormDto.RemoveOtherFileId).ExecuteDeleteAsync();
        }

        if (guardFormDto.ResidenceFileId != Guid.Empty)
            await context.File.Where(f => f.Id == guardFormDto.ResidenceFileId).ExecuteUpdateAsync(f => f.SetProperty(f => f.GuardId, guardFormDto.Id));

        if (guardFormDto.RemoveResidenceFileId != Guid.Empty)
        {
            var url = await context.File.Where(f => f.Id == guardFormDto.RemoveResidenceFileId).Select(f => f.Url).FirstOrDefaultAsync();
            removedUrls.Add(url);
            await context.File.Where(f => f.Id == guardFormDto.RemoveResidenceFileId).ExecuteDeleteAsync();
        }

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();

        FileManagement.DeleteFiles(removedUrls);
    }
    public async Task RemoveAsync(Guid guardId)
    {
        if (await context.Property.Where(g => g.GuardId == guardId).AnyAsync())
            throw new ValidationException(ErrorMessages.GuardHasProperty);

        await context.Guard.Where(g => g.Id == guardId).ExecuteUpdateAsync(g => g.SetProperty(g => g.IsValid, false));
        await context.File.Where(f => f.GuardId == guardId).ExecuteUpdateAsync(g => g.SetProperty(g => g.IsValid, false));
    }
    public async Task<CommonResponseDto<List<GuardDto>>> GetAllAsync(string search, int pageSize, int pageNum, GuardSort guardSort, bool isAsc, DateTime? from, DateTime? to)
    {
        Expression<Func<GuardModel, bool>> expression = g => (!from.HasValue && !to.HasValue) || ((from.HasValue && to.HasValue) ?
        (/*(g.BirthDate >= from && g.BirthDate <= to) ||*/ (g.IdExpireDate >= from && g.IdExpireDate <= to) || (g.PassportExpireDate >= from && g.PassportExpireDate <= to))
        : (/*(g.BirthDate >= from) ||*/ (g.IdExpireDate >= from) || (g.PassportExpireDate >= from)));
        var guardsQuery = context.Guard.Where(expression).AsQueryable();

        guardsQuery = FilterByStringProperty(guardsQuery, search);
        guardsQuery = CustomOrderBy(guardsQuery, guardSort, isAsc);

        var guardsDto = await guardsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(g => new GuardDto
            {
                Id = g.Id,
                Name = g.Name,
                DocCode = g.DocCode,
                NameEn = g.NameEn,
                IdNumber = g.IdNumber,
                //BirthDay = g.BirthDate,
                //Profession = g.Profession,
                //ProfessionEn = g.ProfessionEn,
                IdExpireDate = g.IdExpireDate,
                PassportNumber = g.PassportNumber,
                PassportExpireDate = g.PassportExpireDate,
                Files = g.Files.Select(f => new GetBaseFileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).ToListAsync();
        var totalRecords = await guardsQuery.CountAsync();
        return new(guardsDto, pageSize, pageNum, totalRecords);
    }
    public async Task<CommonResponseDto<GuardDto>> GetAsync(Guid guardId)
    {
        var guardDto = (await context.Guard.Where(g => g.Id == guardId)
            .Select(g => new GuardDto
            {
                Id = g.Id,
                Name = g.Name,
                DocCode = g.DocCode,
                NameEn = g.NameEn,
                IdNumber = g.IdNumber,
                //BirthDay = g.BirthDate,
                //Profession = g.Profession,
                //ProfessionEn = g.ProfessionEn,
                IdExpireDate = g.IdExpireDate,
                PassportNumber = g.PassportNumber,
                PassportExpireDate = g.PassportExpireDate,
                Files = g.Files.Select(f => new GetBaseFileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(guardLocalizer[ErrorMessages.GuardNotFound]);

        return new(guardDto);
    }
}