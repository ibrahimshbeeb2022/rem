﻿using REM_Dto.CommonDto;
using REM_Dto.Guard;
using REM_Shared.Enum.Sort;

namespace REM_Repository.Guard;

public interface IGuardRepo
{
    Task<GuardDto> AddAsync(AddGuardFormDto guardFormDto);
    Task UpdateAsync(UpdateGuardFormDto guardFormDto);
    Task RemoveAsync(Guid guardId);
    Task<CommonResponseDto<List<GuardDto>>> GetAllAsync(string search, int pageSize, int pageNum, GuardSort guardSort, bool isAsc, DateTime? from, DateTime? to);
    Task<CommonResponseDto<GuardDto>> GetAsync(Guid guardId);
}