﻿using Microsoft.AspNetCore.Http;
using REM_Dto.Base;
using REM_Shared.Enum;

namespace REM_Repository.File;

public interface IFileRepo
{
    Task<GetBaseFileDto> SaveFile(IFormFile file, FileKind fileKind);
    Task DeleteFile(Guid id);
}