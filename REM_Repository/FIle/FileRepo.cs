﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model;
using REM_Dto.Base;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Exception;
using REM_Shared.Helper;

namespace REM_Repository.File;

public class FileRepo : BaseRepo, IFileRepo
{
    private readonly IHostingEnvironment hostingEnvironment;
    public FileRepo(
        REMContext context,
        IConfiguration configuration,
        IHttpContextAccessor httpContextAccessor,
        IHostingEnvironment hostingEnvironment) :
        base(context, configuration, httpContextAccessor)
    {
        this.hostingEnvironment = hostingEnvironment;
    }

    public async Task DeleteFile(Guid id)
    {
        var fileModel = await context.File.Where(m => m.Id == id).FirstOrDefaultAsync() ?? throw new NotFoundException(ErrorMessages.FileFound);
        var urls = new List<string>() { fileModel.Url };
        FileManagement.DeleteFiles(urls.AsEnumerable());

        context.Remove(fileModel);
        await context.SaveChangesAsync();
    }

    public async Task<GetBaseFileDto> SaveFile(IFormFile file, FileKind fileKind)
    {
        var uploadResult = await FileManagement.SaveFiles([file], hostingEnvironment.WebRootPath);

        var fileModel = new FileModel
        {
            Id = Guid.NewGuid(),
            FileKind = fileKind,
            Name = file.FileName,
            FileType = uploadResult.Select(f => f.fileType).FirstOrDefault(),
            Url = uploadResult.Select(f => f.url.FileUrlSimplify()).FirstOrDefault()
        };
        await context.File.AddAsync(fileModel);
        await context.SaveChangesAsync();
        return new GetBaseFileDto
        {
            Id = fileModel.Id,
            Url = fileModel.Url,
            Name = fileModel.Name,
            FileKind = fileModel.FileKind,
            FileType = fileModel.FileType
        };
    }
}