﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Currency;
using REM_Database.Model.Employee;
using REM_Database.Model.Expense;
using REM_Database.Model.Finance;
using REM_Database.Model.Guard;
using REM_Database.Model.Location;
using REM_Database.Model.Master;
using REM_Database.Model.Settings;
using REM_Database.Model.Unit;
using REM_Dto.Base;
using REM_Dto.CommonDto;
using REM_Dto.Settings;
using REM_Repository.Base;
using REM_Shared.Enum;

namespace REM_Repository.Settings
{
    public class SettingsRepo : BaseRepo, ISettingsRepo
    {
        private readonly IHostingEnvironment hostingEnvironment;

        public SettingsRepo(
            REMContext context,
            IHostingEnvironment hostingEnvironment,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
        {
            this.hostingEnvironment = hostingEnvironment;
        }
        public async Task<int> GetLastDocumentCodeAsync(DocumentNumber documentNumber)
       => documentNumber switch
       {
           DocumentNumber.L => await GetNextIncrementNumber<LandLordModel>(m => m.IncrementNumber),
           DocumentNumber.GU => await GetNextIncrementNumber<GuardModel>(m => m.IncrementNumber),
           DocumentNumber.PT => await GetNextIncrementNumber<PropertyTypeModel>(m => m.IncrementNumber),
           DocumentNumber.U => await GetNextIncrementNumber<UnitModel>(m => m.IncrementNumber),
           DocumentNumber.UT => await GetNextIncrementNumber<UnitTypeModel>(m => m.IncrementNumber),
           DocumentNumber.P => await GetNextIncrementNumber<PropertyModel>(m => m.IncrementNumber),
           DocumentNumber.C => await GetNextIncrementNumber<CountryModel>(m => m.IncrementNumber),
           DocumentNumber.CU => await GetNextIncrementNumber<CurrencyModel>(m => m.IncrementNumber),
           DocumentNumber.CY => await GetNextIncrementNumber<CityModel>(m => m.IncrementNumber),
           DocumentNumber.R => await GetNextIncrementNumber<RegionModel>(m => m.IncrementNumber),
           DocumentNumber.T => await GetNextIncrementNumber<TenantModel>(m => m.IncrementNumber),
           DocumentNumber.TC => await GetNextIncrementNumber<TenancyContractModel>(m => m.IncrementNumber),
           DocumentNumber.CPV => await GetNextIncrementNumber<BondsModel>(m => m.IncrementNumber, BondType.PaymentVouchers, VoucherBondTypes.CashReceipt),
           DocumentNumber.PDCP => await GetNextIncrementNumber<BondsModel>(m => m.IncrementNumber, BondType.PaymentVouchers, VoucherBondTypes.ChequeReceipt),
           DocumentNumber.BPV => await GetNextIncrementNumber<BondsModel>(m => m.IncrementNumber, BondType.PaymentVouchers, VoucherBondTypes.BankReceipt),
           DocumentNumber.BRV => await GetNextIncrementNumber<BondsModel>(m => m.IncrementNumber, BondType.ReceiptVouchers, VoucherBondTypes.BankReceipt),
           DocumentNumber.CRV => await GetNextIncrementNumber<BondsModel>(m => m.IncrementNumber, BondType.ReceiptVouchers, VoucherBondTypes.CashReceipt),
           DocumentNumber.PDCR => await GetNextIncrementNumber<BondsModel>(m => m.IncrementNumber, BondType.ReceiptVouchers, VoucherBondTypes.ChequeReceipt),
           DocumentNumber.GEX => await GetNextIncrementNumber<ExpenseModel>(m => m.IncrementNumber, ExpenseType.General),
           DocumentNumber.CEX => await GetNextIncrementNumber<ExpenseModel>(m => m.IncrementNumber, ExpenseType.Company),
           DocumentNumber.CEV => await GetNextIncrementNumber<CompanyExpenseVoucherModel>(m => m.IncrementNumber),
           DocumentNumber.E => await GetNextIncrementNumber<EmployeeModel>(m => m.IncrementNumber),
           DocumentNumber.BT => await GetNextIncrementNumber<BankTypeModel>(m => m.IncrementNumber),
           _ => 0
       };
        public async Task UpdateSettingsAndLawsAsync(List<SettingsLawsFormDto> settingsLawsDtos)
        {
            var allSettingsLaws = await context.SettingsLaws.Include(s => s.Files).ToDictionaryAsync(s => s.Key);

            await context.Database.BeginTransactionAsync();
            foreach (var settingsLawDto in settingsLawsDtos)
            {
                if (allSettingsLaws.TryGetValue(settingsLawDto.Key, out SettingsLawsModel model))
                {
                    model.Value = settingsLawDto.Value;
                    if (settingsLawDto.FileIds.Count != 0)
                        await context.File.Where(f => settingsLawDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.SettingsLawsId, model.Id));
                    if (settingsLawDto.RemoveFileIds.Count != 0)
                        await context.File.Where(f => settingsLawDto.RemoveFileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.IsValid, false));
                }
                else
                {
                    var x = await context.SettingsLaws.AddAsync(new SettingsLawsModel
                    {
                        Key = settingsLawDto.Key,
                        Value = settingsLawDto.Value,
                    });
                    await context.SaveChangesAsync();
                    if (settingsLawDto.FileIds.Count != 0)
                        await context.File.Where(f => settingsLawDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.SettingsLawsId, x.Entity.Id));
                }
            }
            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        public async Task<CommonResponseDto<List<SettingsLawsDto>>> GetAllSettingsLawsAsync()
        {
            var settingsLawsDto = await context.SettingsLaws.Select(s => new SettingsLawsDto
            {
                Id = s.Id,
                Key = s.Key,
                Value = s.Value,
                Files = s.Files.Where(f => f.IsValid).Select(f => new GetBaseFileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).ToListAsync();

            settingsLawsDto.ForEach(m =>
            {
                if (m.Key == SettingsEnum.Logo || m.Key == SettingsEnum.Signature || m.Key == SettingsEnum.Seal || m.Key == SettingsEnum.WaterMark)
                {
                    var url = m.Files.Select(m => m.Url).FirstOrDefault();
                    if (!string.IsNullOrEmpty(url))
                    {
                        var wwwrootPath = hostingEnvironment.WebRootPath;
                        var logoUrl = wwwrootPath + url;
                        if (Path.Exists(logoUrl))
                        {
                            byte[] fileBytes = System.IO.File.ReadAllBytes(logoUrl);
                            m.FileInBase64 = Convert.ToBase64String(fileBytes);
                        }
                    }
                }
            });

            return new(settingsLawsDto);
        }
        public async Task<CommonResponseDto<SettingsLawsDto>> GetSettingsByKeyAsync(SettingsEnum key)
        {
            var settingsLawsDto = await context.SettingsLaws.Where(sl => sl.Key == key)
                .Select(s => new SettingsLawsDto
                {
                    Id = s.Id,
                    Key = s.Key,
                    Value = s.Value,
                    Files = s.Files.Where(f => f.IsValid).Select(f => new GetBaseFileDto
                    {
                        Id = f.Id,
                        Url = f.Url,
                        FileKind = f.FileKind,
                        FileType = f.FileType
                    }).ToList()
                }).FirstOrDefaultAsync();
            return new(settingsLawsDto);
        }
    }
}
