﻿using REM_Dto.CommonDto;
using REM_Dto.Settings;
using REM_Shared.Enum;

namespace REM_Repository.Settings
{
    public interface ISettingsRepo
    {
        Task<int> GetLastDocumentCodeAsync(DocumentNumber documentNumber);
        Task UpdateSettingsAndLawsAsync(List<SettingsLawsFormDto> settingsLawsDtos);
        Task<CommonResponseDto<List<SettingsLawsDto>>> GetAllSettingsLawsAsync();
        Task<CommonResponseDto<SettingsLawsDto>> GetSettingsByKeyAsync(SettingsEnum key);
    }
}
