﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using REM_Database.Context;
using REM_Database.Model.Employee;
using REM_Database.Model.Permission;
using REM_Dto.CommonDto;
using REM_Dto.Employee;
using REM_Dto.Permission;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using REM_Shared.Resource;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.Employee;

public class EmployeeRepo : BaseRepo, IEmployeeRepo
{
    private readonly IStringLocalizer<EmployeeResource> employeeLocalizer;
    public EmployeeRepo(
        REMContext context,
        IHttpContextAccessor httpContextAccessor,
        IConfiguration configuration,
        IStringLocalizer<EmployeeResource> employeeLocalizer)
        : base(context, configuration, httpContextAccessor)
    {
        this.employeeLocalizer = employeeLocalizer;
    }

    public async Task AddAsync(EmployeeFormDto employeeFormDto)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            var employeeModel = await context.Employee.Where(e => e.Email == employeeFormDto.Email.Trim().ToLower() && e.IsValid).FirstOrDefaultAsync();
            if (employeeModel != null)
                throw new ValidationException(employeeLocalizer[ErrorMessages.EmployeeAlreadyExit]);

            if (employeeFormDto.RoleIds.Count == 0)
                throw new ValidationException(employeeLocalizer[ErrorMessages.RoleRequired]);

            var hashPassword = HashPassword(employeeFormDto.Password);
            var NextIncrementNumber = await GetNextIncrementNumber<EmployeeModel>(m => m.IncrementNumber);

            var employee = new EmployeeModel
            {
                IsActive = true,
                HashPassword = hashPassword,
                FullName = employeeFormDto.FullName,
                IncrementNumber = NextIncrementNumber,
                PhoneNumber = employeeFormDto.PhoneNumber,
                Roles = employeeFormDto.RoleIds.Select(rId => new UserRoleModel
                {
                    RoleId = rId
                }).ToList(),
                Email = employeeFormDto.Email.Trim().ToLower(),
                DocCode = StringHelper.CreateDocumentCode(DocumentNumber.E, NextIncrementNumber),
            };
            await context.Employee.AddAsync(employee);
            await context.SaveChangesAsync();

            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task UpdateAsync(EmployeeFormDto employeeFormDto)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            var employeeModel = await context.Employee.Where(e => e.Id == employeeFormDto.Id).Include(e => e.Roles).FirstOrDefaultAsync() ??
                throw new NotFoundException(employeeLocalizer[ErrorMessages.EmployeeNotFound]);

            employeeModel.FullName = employeeFormDto.FullName;
            employeeModel.IsActive = employeeFormDto.IsActive;
            employeeModel.PhoneNumber = employeeFormDto.PhoneNumber;
            employeeModel.Email = employeeFormDto.Email.Trim().ToLower();
            context.UserRole.RemoveRange(employeeModel.Roles);
            await context.UserRole.AddRangeAsync(employeeFormDto.RoleIds.Select(rId => new UserRoleModel
            {
                RoleId = rId,
                UserId = employeeModel.Id,
            }));

            if (!string.IsNullOrEmpty(employeeFormDto.Password))
            {
                var hashPassword = HashPassword(employeeFormDto.Password);
                employeeModel.HashPassword = hashPassword;
            }
            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task RemoveAsync(Guid employeeId)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            await context.UserRole.Where(er => er.UserId == employeeId).ExecuteUpdateAsync(x => x.SetProperty(x => x.IsValid, false));
            await context.Employee.Where(e => e.Id == employeeId).ExecuteUpdateAsync(x => x.SetProperty(x => x.IsValid, false));

            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task ChangeEmployeeRoleAsync(Guid empId, List<Guid> roleIds)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            await context.UserRole.Where(er => er.UserId == empId).ExecuteDeleteAsync();
            await context.UserRole.AddRangeAsync(roleIds.Select(rId => new UserRoleModel
            {
                RoleId = rId,
                UserId = empId
            }));
            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task<CommonResponseDto<List<EmployeeDto>>> GetAllAsync(int pageSize, int pageNum, EmployeeSort employeeSort,
        bool isAsc, string search, bool? isActive, List<Guid> roleIds)
    {
        try
        {
            Expression<Func<EmployeeModel, bool>> expression = e => e.IsValid
            && (!isActive.HasValue || e.IsActive == isActive)
            && (!roleIds.Any() || e.Roles.Any(er => roleIds.Contains(er.RoleId)));

            var employeesQuery = context.Employee.Where(expression);
            CustomOrderBy(employeesQuery, employeeSort, isAsc);

            var employeesDto = await employeesQuery
                .Skip(pageSize * pageNum)
                .Take(pageSize)
                .Select(e => new EmployeeDto
                {
                    Id = e.Id,
                    FullName = e.FullName,
                    Email = e.Email,
                    DocCode = e.DocCode,
                    IsActive = e.IsActive,
                    CreateDate = e.CreateDate,
                    PhoneNumber = e.PhoneNumber,
                    Roles = e.Roles.Select(er => new RoleDto
                    {
                        Id = er.RoleId,
                        Name = er.Role.Name,
                    }).ToList()
                }).ToListAsync();
            var totalRecords = await employeesQuery.CountAsync();
            return new(employeesDto, pageSize, pageNum, totalRecords);
        }
        catch (Exception)
        {
            throw;
        }
    }
    public async Task<CommonResponseDto<EmployeeDto>> GetAsync(Guid id)
    {
        try
        {
            var employeeDto = await context.Employee.Where(e => e.Id == id)
                .Select(e => new EmployeeDto
                {
                    Id = e.Id,
                    FullName = e.FullName,
                    Email = e.Email,
                    DocCode = e.DocCode,
                    IsActive = e.IsActive,
                    CreateDate = e.CreateDate,
                    PhoneNumber = e.PhoneNumber,
                    Roles = e.Roles.Select(er => new RoleDto
                    {
                        Id = er.RoleId,
                        Name = er.Role.Name,
                    }).ToList()
                }).FirstOrDefaultAsync();
            return new(employeeDto);
        }
        catch (Exception)
        {
            throw;
        }
    }
    public async Task ActionRolePermissionAsync(ActionEmployeeDto dto)
    {
        try
        {
            await context.Database.BeginTransactionAsync();
            var roleModel = await context.Role.Where(m => m.Id == dto.Id).FirstOrDefaultAsync();

            var isAdded = false;
            if (roleModel == null)
            {
                roleModel = new RoleModel();
                isAdded = true;
            }

            roleModel.Name = dto.Name;
            roleModel.RolePermissions = dto.Permissions.Select(pId => new PermissionRoleModel
            {
                RoleId = dto.Id,
                PermissionId = pId,
            }).ToList();

            await context.PermissionRole.Where(pr => pr.RoleId == dto.Id).ExecuteDeleteAsync();

            if (isAdded)
            {
                await context.AddAsync(roleModel);
            }


            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task RemoveRoleAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            await context.UserRole.Where(er => er.RoleId == id).ExecuteDeleteAsync();
            await context.PermissionRole.Where(er => er.RoleId == id).ExecuteDeleteAsync();
            await context.Role.Where(r => r.Id == id).ExecuteDeleteAsync();

            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task<CommonResponseDto<List<GetAllPermissionsDto>>> GetAllPermissions(Guid? roleId)
    {
        if (roleId.HasValue)
        {
            var data = await context.PermissionRole
                                    .Where(m => m.RoleId == roleId)
                                    .Select(m => new GetAllPermissionsDto
                                    {
                                        Id = m.PermissionId,
                                        EnumValue = m.Permission.PermissionEnum,
                                    }).ToListAsync();
            return new(data);
        }
        else
        {
            var data = await context.Permission
                                    .Select(m => new GetAllPermissionsDto
                                    {
                                        Id = m.Id,
                                        EnumValue = m.PermissionEnum,
                                    }).ToListAsync();
            return new(data);
        }
    }
    public async Task<List<PermissionEnum>> GetUserPermissions(Guid userId)
    {
        var permissions = await context.UserRole
                       .Where(m => m.UserId == userId)
                       .SelectMany(p => p.Role.RolePermissions, (m, p) => p.Permission.PermissionEnum).ToListAsync();

        return permissions;
    }
    public async Task<CommonResponseDto<List<RoleDto>>> GetAllRoleAsync(string search)
    {
        var rolesModel = await context.Role.Where
            (r =>
            r.IsValid &&
            r.Name != UserType.SuperAdmin.ToString() &&
            r.Name != UserType.LandLord.ToString() &&
            (string.IsNullOrEmpty(search) || r.Name.Contains(search)))
            .Select(r => new RoleDto
            {
                Id = r.Id,
                Name = r.Name,
                Permissions = r.RolePermissions.SelectMany(p => p.Role.RolePermissions, (m, p) => new GetAllPermissionsDto
                {
                    Id = m.Permission.Id,
                    EnumValue = m.Permission.PermissionEnum,
                }).ToList(),
            }).ToListAsync();
        return new(rolesModel);
    }
    public async Task<CommonResponseDto<RoleDto>> GetRoleAsync(Guid id)
    {
        var roleModel = (await context.Role.Where(r => r.Id == id && r.IsValid)
            .Select(r => new RoleDto
            {
                Id = r.Id,
                Name = r.Name,
                Permissions = r.RolePermissions.SelectMany(p => p.Role.RolePermissions, (m, p) => new GetAllPermissionsDto
                {
                    Id = m.Permission.Id,
                    EnumValue = m.Permission.PermissionEnum,
                }).ToList(),
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(employeeLocalizer[ErrorMessages.RoleNotFound]);
        return new(roleModel);
    }
    public async Task<SignInEmployeeDto> SignInIntoDashboardAsync(SignInDto signInDto)
    {
        signInDto.Email = signInDto.Email.Trim().ToLower();

        var userModel = await context.User.Where(e => e.Email == signInDto.Email && e.IsValid)
            .Include(e => e.Roles.Where(er => er.IsValid))
            .FirstOrDefaultAsync() ??
            throw new NotFoundException(employeeLocalizer[ErrorMessages.EmployeeNotFound]);

        if (userModel.UserType == UserType.Employee)
        {
            if (await context.Employee.AnyAsync(m => m.Id == userModel.Id && !m.IsActive))
                throw new ValidationException(employeeLocalizer[ErrorMessages.EmployeeInActive]);
        }

        var isMatchPassword = CheckPassword(signInDto.Password, userModel.HashPassword, out string newHash);

        if (isMatchPassword == PasswordVerificationResult.Failed)
            throw new ValidationException(employeeLocalizer[ErrorMessages.WrongEmailOrPassword]);

        var employeePermission = await context.PermissionRole
            .Where(pr => userModel.Roles.Select(er => er.RoleId).Contains(pr.RoleId))
            .Select(pr => pr.Permission.PermissionEnum)
            .Distinct()
            .ToListAsync();

        return new SignInEmployeeDto
        {
            Id = userModel.Id,
            FullName = userModel.FullName,
            Email = userModel.Email,
            DocCode = userModel.DocCode,
            PhoneNumber = userModel.PhoneNumber,
            Token = GenerateJwtTokenAsync(userModel.Id, userModel.UserType.ToString()),
            Permissions = employeePermission,
            UserType = userModel.UserType,
        };
    }


    #region Seed Auth
    public async Task SeedAuth()
    {
        var tran = await context.Database.BeginTransactionAsync();

        await SeedPermissions();
        await SeedRoles();
        await SeedSuperAdmin();

        await tran.CommitAsync();
    }
    public async Task SeedSuperAdmin()
    {
        var userModel = await context.Employee.Where(m => m.UserType == UserType.SuperAdmin).FirstOrDefaultAsync();

        var adminRole = await context.Role.Where(m => m.Name == UserType.SuperAdmin.ToString()).FirstOrDefaultAsync();
        if (userModel == null)
        {
            userModel = new EmployeeModel
            {
                Id = Guid.NewGuid(),
                UserType = UserType.SuperAdmin,
                Email = "support@ozs.ae",
                IsActive = true,
                Roles = new List<UserRoleModel>
                    {
                        new UserRoleModel
                        {
                            RoleId = adminRole.Id,
                        }
                    },
                HashPassword = HashPassword("123456"),
            };
            await context.AddAsync(userModel);
        }
        await context.SaveChangesAsync();
    }
    public async Task SeedPermissions()
    {
        var permissionModel = await context.Permission.ToListAsync();

        var permission = ((PermissionEnum[])Enum.GetValues(typeof(PermissionEnum))).ToList();

        var addedModel = new List<PermissionModel>();

        var adminRoleId = await context.Role.Where(m => m.Name == UserType.SuperAdmin.ToString()).Select(m => m.Id).FirstOrDefaultAsync();

        foreach (var item in permission)
        {
            if (!permissionModel.Any(m => m.PermissionEnum == item))
            {
                var temp = new PermissionModel
                {
                    Name = item.ToString(),
                    PermissionEnum = item,
                    PermissionRoles = new List<PermissionRoleModel>(),
                };

                if (adminRoleId != Guid.Empty)
                    temp.PermissionRoles.Add(new PermissionRoleModel
                    {
                        RoleId = adminRoleId
                    });

                addedModel.Add(temp);
            }
        }

        await context.AddRangeAsync(addedModel);
        await context.SaveChangesAsync();
    }
    public async Task SeedRoles()
    {
        var rolesModel = await context.Role.ToListAsync();
        var addedrRoleModels = new List<RoleModel>();


        var landLordPermissions = new List<PermissionEnum>()
        {
            PermissionEnum.CanAccessProperty,
            PermissionEnum.CanAccessUnit,
            PermissionEnum.CanAccessOccupancyReport,
            PermissionEnum.CanAccessBonds,
            PermissionEnum.CanAccessTenancyContract,
            PermissionEnum.CanAccessPDCManagement,
            PermissionEnum.CanEditChequeStatus,
            PermissionEnum.CanAccessSettings,
        };
        var adminPermissionsId = await context.Permission.Select(m => m.Id).ToListAsync();
        var landLordPermissionsId = await context.Permission.Where(m => landLordPermissions.Contains(m.PermissionEnum)).Select(m => m.Id).ToListAsync();

        var roles = ((UserType[])Enum.GetValues(typeof(UserType))).ToList();

        foreach (var item in roles)
        {
            if (!rolesModel.Any(m => m.Name == item.ToString()))
            {
                var temp = new RoleModel
                {
                    Name = item.ToString(),
                };

                if (item == UserType.SuperAdmin)
                    temp.RolePermissions = adminPermissionsId.Select(m => new PermissionRoleModel
                    {
                        PermissionId = m,
                    }).ToList();

                if (item == UserType.LandLord)
                    temp.RolePermissions = landLordPermissionsId.Select(m => new PermissionRoleModel
                    {
                        PermissionId = m,
                    }).ToList();

                addedrRoleModels.Add(temp);
            }
        }
        if (addedrRoleModels.Any())
        {
            await context.AddRangeAsync(addedrRoleModels);
            await context.SaveChangesAsync();
        }
    }

    #endregion
}