﻿using REM_Dto.CommonDto;
using REM_Dto.Employee;
using REM_Dto.Permission;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;

namespace REM_Repository.Employee;

public interface IEmployeeRepo
{
    Task AddAsync(EmployeeFormDto employeeFormDto);
    Task UpdateAsync(EmployeeFormDto employeeFormDto);
    Task ChangeEmployeeRoleAsync(Guid empId, List<Guid> roleIds);
    Task<CommonResponseDto<List<EmployeeDto>>> GetAllAsync(int pageSize, int pageNum, EmployeeSort employeeSort, bool isAsc, string search, bool? isActive, List<Guid> roleIds);
    Task<CommonResponseDto<EmployeeDto>> GetAsync(Guid id);
    Task RemoveAsync(Guid employeeId);

    Task ActionRolePermissionAsync(ActionEmployeeDto dto);
    Task<CommonResponseDto<List<RoleDto>>> GetAllRoleAsync(string search);
    Task<CommonResponseDto<RoleDto>> GetRoleAsync(Guid id);
    Task<CommonResponseDto<List<GetAllPermissionsDto>>> GetAllPermissions(Guid? roleId);
    Task<List<PermissionEnum>> GetUserPermissions(Guid userId);
    Task RemoveRoleAsync(Guid id);

    Task<SignInEmployeeDto> SignInIntoDashboardAsync(SignInDto signInDto);
    Task SeedAuth();
}