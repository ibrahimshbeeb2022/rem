﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Expense;
using REM_Dto.Expense;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Exception;
using REM_Shared.Helper;
using System.ComponentModel.DataAnnotations;

namespace REM_Repository.Expense
{
    public class ExpenseRepo : BaseRepo, IExpenseRepo
    {
        public ExpenseRepo(
            REMContext context,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
        {
        }

        public async Task<GetAllExpenseDto> AddAsync(ActionExpenseDto dto, ExpenseType type)
        {
            var NextIncrementNumber = await GetNextIncrementNumber<ExpenseModel>(m => m.IncrementNumber, type);
            var expenseModel = new ExpenseModel
            {
                Id = Guid.NewGuid(),
                Name = dto.Name,
                EnName = dto.EnName,
                Type = type,
                DocCode = StringHelper.CreateDocumentCode(type == ExpenseType.Company ? DocumentNumber.CEX : DocumentNumber.GEX, NextIncrementNumber),
                IncrementNumber = NextIncrementNumber,

            };
            await context.AddAsync(expenseModel);
            await context.SaveChangesAsync();
            return await GetAsync(expenseModel.Id);
        }
        public async Task UpdateAsync(ActionExpenseDto dto)
        {
            var expenseModel = await context.Expenses.Where(m => m.Id == dto.Id).FirstOrDefaultAsync();
            if (expenseModel == null)
            {
                throw new NotFoundException(ErrorMessages.ExpenseNotFound);
            }
            expenseModel.Name = dto.Name;
            expenseModel.EnName = dto.EnName;
            await context.SaveChangesAsync();
        }

        public async Task<List<GetAllExpenseDto>> GetAllAsync(string search, ExpenseType type)
        {
            var data = context.Expenses.Where(m => m.Type == type)
                .Select(m => new GetAllExpenseDto
                {
                    Id = m.Id,
                    DocCode = m.DocCode,
                    EnName = m.EnName,
                    Name = m.Name,
                });

            data = FilterByStringProperty(data, search);
            return new(await data.ToListAsync());
        }

        public async Task<GetAllExpenseDto> GetAsync(Guid id)
        {
            var data = await context.Expenses.Where(m => m.Id == id)
                          .Select(m => new GetAllExpenseDto
                          {
                              Id = m.Id,
                              DocCode = m.DocCode,
                              EnName = m.EnName,
                              Name = m.Name,
                          }).FirstOrDefaultAsync();

            return data;
        }

        public async Task RemoveAsync(Guid id)
        {
            if (!await CheckEntityExist<ExpenseModel>(m => m.Id == id))
            {
                throw new NotFoundException(ErrorMessages.ExpenseNotFound);
            }
            if (await CheckEntityExist<CompanyExpenseVoucherDetailsModel>(m => m.ExpenseId == id))
            {
                throw new ValidationException(ErrorMessages.ExpenseHasRelatedData);
            }

            await context.Expenses.Where(m => m.Id == id).ExecuteDeleteAsync();
        }
    }
}
