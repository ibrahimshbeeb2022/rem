﻿using REM_Dto.Expense;
using REM_Shared.Enum;

namespace REM_Repository.Expense
{
    public interface IExpenseRepo
    {
        Task<GetAllExpenseDto> AddAsync(ActionExpenseDto dto, ExpenseType type);
        Task UpdateAsync(ActionExpenseDto dto);
        Task RemoveAsync(Guid id);
        Task<List<GetAllExpenseDto>> GetAllAsync(string search, ExpenseType type);
        Task<GetAllExpenseDto> GetAsync(Guid id);
    }
}
