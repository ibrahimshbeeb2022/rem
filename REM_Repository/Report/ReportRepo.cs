﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Dto.CommonDto;
using REM_Dto.Report;
using REM_Repository.Base;
using REM_Shared.Enum;

namespace REM_Repository.Report
{
    public class ReportRepo : BaseRepo, IReportRepo
    {
        public ReportRepo(
            REMContext context,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
        {
        }

        public async Task<CommonResponseDto<List<FinalReportDto>>> GetFinalReport(int pageSize, int pageNum, Guid property, DateTime? from, DateTime? to)
        {
            var data = await context.TenancyContract
                                    .Where(m =>
                                       m.PropertyId == property &&
                                      (!from.HasValue || from.Value.Date <= m.StartDate.Date) &&
                                      (!to.HasValue || to.Value.Date >= m.StartDate.Date))
                                    .Skip(pageNum * pageSize)
                                    .Take(pageSize)
                                    .Select(m => new FinalReportDto
                                    {
                                        Id = m.Id,
                                        StartDate = m.StartDate,
                                        EndDate = m.EndDate,
                                        TenantName = m.Tenant.FullName,
                                        UnitNumber = m.Unit.UnitNumber,
                                        TenancyContractNumber = m.TenancyNumber,
                                        PaymentWay = m.PaymentPlan.Count(),
                                        RentValue = m.TenancyContractDetails.Where(m => m.Type == TenancyContractOperationType.Rent).Select(m => m.Value).Sum(),
                                        InsuranceValue = m.TenancyContractDetails.Where(m => m.Type == TenancyContractOperationType.Insurance).Select(m => m.Value).Sum(),
                                        OtherValue = m.TenancyContractDetails.Where(m => m.Type == TenancyContractOperationType.Other).Select(m => m.Value).Sum(),
                                        VatValue = m.VatValue,
                                    }).ToListAsync();

            var paymetPlans = await context.TenancyContractPaymentPlan
                                           .Where(m =>
                                                   m.TenancyContract.PropertyId == property &&
                                                   (!from.HasValue || from.Value.Date <= m.TenancyContract.StartDate.Date) &&
                                                   (!to.HasValue || to.Value.Date >= m.TenancyContract.StartDate.Date)
                                           ).Select(m => new
                                           {
                                               m.TenancyContractId,
                                               CollectedValue = m.LastCheckStatus == CheckStatus.Clear ? m.Value :
                                                                m.FinanceOperations
                                                                .Where(m => m.Type == FinanceOperationType.Debit &&
                                                                           (m.Account.Type == FinanceAccountType.BankAccount) ||
                                                                           (m.Account.Type == FinanceAccountType.CashCompanyAccount))
                                                                .Select(m => m.Value).Sum(),

                                               Balance = m.Value -
                                                                     (m.LastCheckStatus == CheckStatus.Clear ? m.Value :
                                                                     m.FinanceOperations
                                                                    .Where(m => m.Type == FinanceOperationType.Debit &&
                                                                           (m.Account.Type == FinanceAccountType.BankAccount) ||
                                                                           (m.Account.Type == FinanceAccountType.CashCompanyAccount))
                                                                    .Select(m => m.Value).Sum()),
                                           }).ToListAsync();

            data.ForEach(m =>
            {
                m.CollectedValue = paymetPlans.Where(p => p.TenancyContractId == m.Id).Select(m => m.CollectedValue).Sum();
                m.RemainValue = paymetPlans.Where(p => p.TenancyContractId == m.Id).Select(m => m.Balance).Sum();
            });

            var totalRecords = await context.TenancyContract
                                    .CountAsync(m =>
                                                  m.PropertyId == property &&
                                                  (!from.HasValue || from.Value.Date <= m.StartDate.Date) &&
                                                  (!to.HasValue || to.Value.Date >= m.StartDate.Date)
                                                );

            return new(data, pageSize, pageNum, totalRecords);
        }
        public async Task<CommonResponseDto<List<OccupancyReportDto>>> GetOccupancyReport
            (int pageSize, int pageNum, string search, TenancyContractStatus? status, bool isAsc,
             DateTime? from, DateTime? to, Guid? landlord, Guid? property, Guid? unit, Guid? tenant)
        {
            var units = await context.Unit
                .Select(m => new
                {
                    unitId = m.Id,
                    propertyId = m.PropertyId,
                    UnitNumber = m.UnitNumber,
                    UnitName = m.UnitName,
                    UnitType = m.UnitType.Name,
                    landlordFUllName = m.Property.LandLord.FullName,
                    landlordId = m.Property.LandLord.Id,
                    rent = m.RentValue,
                    lastContractId = m.TenancyContracts.Any() ? m.TenancyContracts.OrderByDescending(m => m.CreateDate).FirstOrDefault().Id : Guid.Empty,
                    m.CreateDate,
                }).ToListAsync();


            var contractIds = units.Select(m => m.lastContractId).ToList();

            var contracts = await context.TenancyContract.Where(m => contractIds.Contains(m.Id)).Select(m => new
            {
                id = m.Id,
                StartDate = m.StartDate,
                EndDate = m.EndDate,
                LandLord = m.LandLord.FullName,
                LandLordId = m.LandLord.Id,
                TenantEmail = m.Tenant.Email,
                TenantId = m.Tenant.Id,
                TenantName = m.Tenant.FullName,
                TenantPhone = m.Tenant.Phone,
                status = m.Status,
                m.CreateDate,
                CurrentRent = m.TenancyContractDetails
                                    .Where(m => m.Type == TenancyContractOperationType.Rent)
                                    .Select(m => m.Total)
                                    .FirstOrDefault()
            }).ToListAsync();

            var data = new List<OccupancyReportDto>();
            var nowDate = DateTime.Now.Date;
            units.ForEach(m =>
            {
                var relatedContract = contracts.FirstOrDefault(c => c.id == m.lastContractId);
                if (relatedContract == null)
                {
                    data.Add(new OccupancyReportDto
                    {
                        UnitId = m.unitId,
                        PropertyId = m.propertyId,
                        UnitName = m.UnitName,
                        UnitNumber = m.UnitNumber,
                        UnitType = m.UnitType,
                        LandLordId = m.landlordId,
                        LandLord = m.landlordFUllName,
                        Status = TenancyContractStatus.UnRented,
                        CurrentRent = m.rent,
                        CreateDate = m.CreateDate,
                        VacantSince = m.CreateDate,
                    });
                }
                else
                {
                    var temp = new OccupancyReportDto
                    {
                        UnitId = m.unitId,
                        PropertyId = m.propertyId,
                        UnitName = m.UnitName,
                        UnitNumber = m.UnitNumber,
                        UnitType = m.UnitType,
                        LandLordId = relatedContract.LandLordId,
                        LandLord = relatedContract.LandLord,
                        CurrentRent = relatedContract.CurrentRent,
                        TenantId = relatedContract.TenantId,
                        TenantEmail = relatedContract.TenantEmail,
                        TenantName = relatedContract.TenantName,
                        TenantPhone = relatedContract.TenantPhone,
                        StartDate = relatedContract.StartDate,
                        EndDate = relatedContract.EndDate,
                        CreateDate = relatedContract.CreateDate,

                    };
                    if (
                          relatedContract.status == TenancyContractStatus.New ||
                          relatedContract.status == TenancyContractStatus.ReNew
                       )
                    {
                        temp.Status = relatedContract.EndDate.Date >= nowDate ? TenancyContractStatus.Occupied : TenancyContractStatus.Expired;
                    }
                    else
                    {
                        temp.Status = relatedContract.status;
                        temp.VacantSince = relatedContract.EndDate;
                    }
                    data.Add(temp);
                }
            });
            data = data.Where(tc =>
                             (!from.HasValue || tc.CreateDate >= from) &&
                             (!to.HasValue || tc.CreateDate <= to) &&
                             (!status.HasValue || tc.Status == status.Value) &&
                             (!landlord.HasValue || tc.LandLordId == landlord.Value) &&
                             (!unit.HasValue || tc.UnitId == unit.Value) &&
                             (!tenant.HasValue || tc.TenantId == tenant.Value) &&
                             (!property.HasValue || tc.PropertyId == property.Value)).ToList();

            var totalRecords = data.Count();
            return new(data, pageSize, pageNum, totalRecords);
        }

        public async Task<CommonResponseDto<GetHomePageReportDto>> GetHomePageReport
            (DateTime? expenseFrom, DateTime? expenseTo, DateTime? occupancyFrom, DateTime? occupancyTo, DateTime? checkFrom, DateTime? checkTo)
        {

            Guid? landLordId = null;
            if (CurrentUserRole == UserType.LandLord.ToString())
            {
                landLordId = CurrentUserId;
            }

            var CompanyExpense = await context.CompanyExpenseVoucherDetails
                                             .Where
                                             (m =>
                                                m.ExpenseId.HasValue &&
                                                m.Expense.Type == ExpenseType.Company &&
                                                m.CompanyExpenseVoucher.IsValid &&
                                                (!expenseFrom.HasValue || m.CompanyExpenseVoucher.CreateDate.Date >= expenseFrom.Value.Date) &&
                                                (!expenseTo.HasValue || m.CompanyExpenseVoucher.CreateDate.Date <= expenseTo.Value.Date)
                                             )
                                             .Select(m => new
                                             {
                                                 m.Expense.Name,
                                                 m.Expense.EnName,
                                                 m.Expense.Id,
                                                 m.Total,
                                             }).GroupBy(m => m.Id).Select(m => new CompanyExpenseHomeRoprtDto
                                             {
                                                 Id = m.First().Id,
                                                 EnName = m.First().EnName,
                                                 Name = m.First().Name,
                                                 Count = m.Count(),
                                                 Total = m.Any() ? m.Select(m => m.Total).Sum() : 0,
                                             }).ToListAsync();

            var occupanyReportData = (await GetOccupancyReport(100000, 0, null, null, false, occupancyFrom, occupancyTo, landLordId, null, null, null)).Data;

            var occupanctHomereport = occupanyReportData
                .GroupBy(m => m.Status)
                .Select(m => new OccupancyHomeReportDto
                {
                    Status = m.Key,
                    Count = m.Count(),
                    Total = m.Any() ? m.Select(m => m.CurrentRent).Sum() : 0,
                }).ToList();

            var validStatus = new List<TenancyContractStatus>()
            {
                TenancyContractStatus.Vacant,
                TenancyContractStatus.UnRented,
                TenancyContractStatus.Occupied,
                TenancyContractStatus.Expired,
                TenancyContractStatus.Closed,
            };
            foreach (var item in validStatus)
            {
                if (!occupanctHomereport.Any(m => m.Status == item))
                {
                    occupanctHomereport.Add(new OccupancyHomeReportDto
                    {
                        Status = item,
                        Count = 0,
                        Total = 0,
                    });
                }
            }

            var checks = await context.TenancyContractPaymentPlan
                .Where
                (m =>
                     m.Type == TenancyContractPaymentType.Check &&
                    (!landLordId.HasValue || landLordId.Value == m.TenancyContract.LandLordId) &&
                    (!checkFrom.HasValue || m.PayDate.Date >= checkFrom.Value.Date) &&
                    (!checkTo.HasValue || m.PayDate.Date <= checkTo.Value.Date)
                ).Select(m => new ChecksHomePageReportDto
                {
                    PayDate = m.PayDate,
                    Id = m.Id,
                    DocCode = m.DocCode,
                    Number = m.Number,
                    Value = m.Value,
                    LastCheckStatus = m.LastCheckStatus,
                    LandLordDocCode = m.TenancyContract.LandLord.DocCode,
                    LandLordName = m.TenancyContract.LandLord.FullName,
                    LandLordId = m.TenancyContract.LandLord.Id,
                    TenantId = m.TenancyContract.Tenant.Id,
                    TenantDocCode = m.TenancyContract.Tenant.DocCode,
                    TenantName = m.TenancyContract.Tenant.FullName,
                    UnitId = m.TenancyContract.Unit.Id,
                    UnitDocCode = m.TenancyContract.Unit.DocCode,
                    UnitName = m.TenancyContract.Unit.UnitName,
                    UNitNumber = m.TenancyContract.Unit.UnitNumber,
                    PropertyId = m.TenancyContract.Unit.Property.Id,
                    PropertyName = m.TenancyContract.Unit.Property.Name,
                    PropertyDocCode = m.TenancyContract.Unit.Property.DocCode,
                }).ToListAsync();

            var statisticsDto = new GetHomePageReportDto
            {
                CompanyExpenseReport = CompanyExpense,
                OccupancyReport = occupanctHomereport,
                ChecksReport = checks,
                EmployeesCount = await context.Employee.CountAsync(),
                PropertiesCount = await context.Property.Where(m => (!landLordId.HasValue || landLordId.Value == m.LandLordId)).CountAsync(),
                LandLordsCount = await context.LandLord.CountAsync(),
                UnitsCount = await context.Unit.Where(m => (!landLordId.HasValue || landLordId.Value == m.Property.LandLordId)).CountAsync(),
                TenancyContactsCount = await context.TenancyContract.Where(m => (!landLordId.HasValue || landLordId.Value == m.LandLordId)).CountAsync(),
                AvailableUnitsCount = await context.Unit
                .Where(u =>
                    (!u.TenancyContracts.Any() || u.TenancyContracts.All(x => DateTime.UtcNow > x.EndDate)) &&
                    (!landLordId.HasValue || landLordId.Value == u.Property.LandLordId)
                ).CountAsync(),
            };
            statisticsDto.RentedUnitsCount = statisticsDto.UnitsCount - statisticsDto.AvailableUnitsCount;
            return new(statisticsDto);
        }
    }
}
