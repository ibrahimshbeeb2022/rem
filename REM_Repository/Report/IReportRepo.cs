﻿using REM_Dto.CommonDto;
using REM_Dto.Report;
using REM_Shared.Enum;

namespace REM_Repository.Report
{
    public interface IReportRepo
    {
        Task<CommonResponseDto<List<FinalReportDto>>> GetFinalReport(int pageSize, int pageNum, Guid property, DateTime? from, DateTime? to);
        Task<CommonResponseDto<List<OccupancyReportDto>>> GetOccupancyReport
             (int pageSize, int pageNum, string search, TenancyContractStatus? status, bool isAsc,
              DateTime? from, DateTime? to, Guid? landlord, Guid? property, Guid? unit, Guid? tenant);

        Task<CommonResponseDto<GetHomePageReportDto>> GetHomePageReport
            (DateTime? expenseFrom, DateTime? expenseTo, DateTime? occupancyFrom, DateTime? occupancyTo, DateTime? checkFrom, DateTime? checkTo);
    }
}
