﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using REM_Database.Context;
using REM_Database.Model.Expense;
using REM_Database.Model.Finance;
using REM_Dto.Permission;
using REM_Shared.Enum;
using System.IdentityModel.Tokens.Jwt;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;

namespace REM_Repository.Base;

public class BaseRepo
{
    protected readonly REMContext context;
    protected readonly IConfiguration configuration;
    protected readonly IHttpContextAccessor _httpContextAccessor;

    public BaseRepo(
        REMContext context,
        IConfiguration configuration,
        IHttpContextAccessor httpContextAccessor)
    {
        this.context = context;
        this.configuration = configuration;
        _httpContextAccessor = httpContextAccessor;
    }
    public Guid CurrentUserId
    {
        get
        {
            return GetUserID();
        }
    }
    public string CurrentUserRole
    {
        get
        {
            return GetUserRole();
        }
    }
    public string AcceptedLanguage
    {
        get
        {
            return "";
        }
    }
    public Guid GetUserID()
    {
        if (_httpContextAccessor.HttpContext.User != null
            && _httpContextAccessor.HttpContext.User.Identity != null
            && _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
        {
            var userID = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);
            return new Guid(userID.Value);
        }
        return Guid.Empty;
    }
    public string GetUserRole()
    {
        if (_httpContextAccessor.HttpContext.User != null
            && _httpContextAccessor.HttpContext.User.Identity != null
            && _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
        {
            var userRole = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Role);
            return userRole.Value.ToString();
        }
        return string.Empty;
    }

    public TokenDto GenerateJwtTokenAsync(Guid userId, string userType)
    {
        var secretKey = configuration["JwtConfig:secret"];
        var validIssuer = configuration.GetSection("JwtConfig:validIssuer").Value;
        var validAudience = configuration.GetSection("JwtConfig:validAudience").Value;

        var claims = new List<Claim>
        {
            new (ClaimTypes.NameIdentifier, userId.ToString()),
            new (JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new (ClaimTypes.Role, userType)
        };

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        var expires = DateTime.UtcNow.AddDays(3);

        var token = new JwtSecurityToken(
            validIssuer,
            validAudience,
            claims,
            expires: expires,
            signingCredentials: creds);

        var t = new JwtSecurityTokenHandler().WriteToken(token);
        return new TokenDto("Bearer " + t, token.ValidTo);
    }

    public async Task<bool> CheckEntityExist<T>(Expression<Func<T, bool>> whereExpr) where T : class
    {
        var Exist = await context.Set<T>().AnyAsync(whereExpr);
        return Exist;
    }
    public async Task<int> CountEntityExist<T>(Expression<Func<T, bool>> whereExpr) where T : class
    {
        var count = await context.Set<T>().CountAsync(whereExpr);
        return count;
    }
    public async Task<int> GetNextIncrementNumber<T>(Expression<Func<T, int>> whereExpr) where T : class
    {
        if (await context.Set<T>().AnyAsync())
            return await context.Set<T>().MaxAsync(whereExpr) + 1;
        return 1;
    }
    public async Task<int> GetNextIncrementNumber<T>(Expression<Func<T, int>> whereExpr, BondType type, VoucherBondTypes voucherType) where T : BondsModel
    {
        if (await context.Set<T>().AnyAsync(m => m.BondType == type && m.VoucherBondType == voucherType))
            return await context.Set<T>().Where(m => m.BondType == type && m.VoucherBondType == voucherType).MaxAsync(whereExpr) + 1;
        return 1;
    }
    public async Task<int> GetNextIncrementNumber<T>(Expression<Func<T, int>> whereExpr, ExpenseType type) where T : ExpenseModel
    {
        if (await context.Set<T>().AnyAsync(m => m.Type == type))
            return await context.Set<T>().Where(m => m.Type == type).MaxAsync(whereExpr) + 1;
        return 1;
    }
    public static IQueryable<TEntity> CustomOrderBy<TEntity, TEnum>(IQueryable<TEntity> source, TEnum property, bool ascending = false)
    {
        if (property == null) return source;

        var keySelector = GetKeySelectorExpression<TEntity, TEnum>(property);

        if (keySelector == null) return source;

        if (ascending)
            return source.OrderBy(keySelector);

        else
            return source.OrderByDescending(keySelector);

    }
    private static Expression<Func<TEntity, object>> GetKeySelectorExpression<TEntity, TEnum>(TEnum sortBy)
    {
        var parameter = Expression.Parameter(typeof(TEntity), "entity");

        var s = typeof(TEntity).GetProperties();
        if (!s.Any(m => m.Name == sortBy.ToString()))
            return null;

        var property = Expression.Property(parameter, sortBy.ToString());

        var conversion = Expression.Convert(property, typeof(object));

        var keySelectorExpression = Expression.Lambda<Func<TEntity, object>>(conversion, parameter);

        return keySelectorExpression;
    }
    public static IQueryable<TEntity> FilterByStringProperty<TEntity>(IQueryable<TEntity> data, string searchKey)
    {
        if (string.IsNullOrEmpty(searchKey))
            return data;
        searchKey = searchKey.Trim();

        var parameter = Expression.Parameter(typeof(TEntity), "item");

        var containsMethod = typeof(string).GetMethod("Contains", [typeof(string)]);
        var isNullOrEmptyMethod = typeof(string).GetMethod("IsNullOrEmpty", [typeof(string)]);

        var filterExpression = typeof(TEntity).GetProperties()
            .Where(prop => prop.PropertyType == typeof(string))
            .Select(prop =>
            {
                var propertyExpression = Expression.Property(parameter, prop);
                var isNullOrEmptyCall = Expression.Call(isNullOrEmptyMethod, propertyExpression);
                var containsCall = Expression.Call(propertyExpression, containsMethod, Expression.Constant(searchKey));

                var conditionExpression = Expression.AndAlso(Expression.Not(isNullOrEmptyCall), containsCall);

                return conditionExpression;
            }).Aggregate(Expression.OrElse);

        var lambda = Expression.Lambda<Func<TEntity, bool>>(filterExpression, parameter);
        return data.Where(lambda);
    }
    public static string HashPassword(string password)
    {
        var passwordHasher = new PasswordHasher<object>();
        return passwordHasher.HashPassword(null, password);
    }
    public static PasswordVerificationResult CheckPassword(string password, string hash, out string newHash)
    {
        var passwordHasher = new PasswordHasher<object>();
        var result = passwordHasher.VerifyHashedPassword(null, hash, password);
        if (result == PasswordVerificationResult.SuccessRehashNeeded)
        {
            newHash = HashPassword(password);

            return result;
        }
        else if (result == PasswordVerificationResult.Success)
        {
            newHash = hash;
            return result;
        }
        newHash = null;
        return result;
    }
}