﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Finance;
using REM_Database.Model.Master;
using REM_Dto.Base;
using REM_Dto.CommonDto;
using REM_Dto.LandLord;
using REM_Dto.Notification;
using REM_Repository.Base;
using REM_Repository.Notification;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.LandLord;

public class LandLordRepo : BaseRepo, ILandLordRepo
{
    private readonly INotificationRepo notificationRepo;

    public LandLordRepo(
        REMContext context,
        IConfiguration configuration,
        INotificationRepo notificationRepo,
        IHttpContextAccessor httpContextAccessor) :
        base(context, configuration, httpContextAccessor)
    {
        this.notificationRepo = notificationRepo;
    }

    public async Task AddAsync(AddLandLordDto landLordFormDto)
    {
        var landlordId = Guid.NewGuid();
        var NextIncrementNumber = await GetNextIncrementNumber<LandLordModel>(m => m.IncrementNumber);
        var landLordRoleId = await context.Role.Where(m => m.Name == UserType.LandLord.ToString()).Select(m => m.Id).FirstOrDefaultAsync();

        var hashPassword = HashPassword(landLordFormDto.Password);

        var landLordModel = new LandLordModel()
        {
            Id = landlordId,
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.L, NextIncrementNumber),
            IncrementNumber = NextIncrementNumber,
            HashPassword = hashPassword,
            UserType = UserType.LandLord,
            FullName = landLordFormDto.FullName,
            Email = landLordFormDto.Email,
            SecondPhone = landLordFormDto.SecondPhone,
            PhoneNumber = landLordFormDto.Phone,
            Address = landLordFormDto.Address,
            Job = landLordFormDto.Job,
            Nationality = landLordFormDto.Nationality,
            CardNumber = landLordFormDto.CardNumber,
            IdentityCardNumber = landLordFormDto.IdentityCardNumber,
            IdentityCardExpiryDate = landLordFormDto.IdentityCardExpiryDate,
            PassportExpiryDate = landLordFormDto.PassportExpiryDate,
            PassportNumber = landLordFormDto.PassportNumber,
            Note = landLordFormDto.Note,
            AccountName = landLordFormDto.AccountName,
            AccountNumber = landLordFormDto.AccountNumber,
            BankName = landLordFormDto.BankName,
            BankAddress = landLordFormDto.BankAddress,
            Iban = landLordFormDto.Iban,
            SwiftCode = landLordFormDto.SwiftCode,
            TaxNumber = landLordFormDto.TaxNumber,
            Beneficiary = landLordFormDto.Beneficiary,
            Account = new AccountModel
            {
                Type = FinanceAccountType.LandLordAccount,
                LandLordId = landlordId,
            },
            Roles = new List<REM_Database.Model.Employee.UserRoleModel>()
            {
                new REM_Database.Model.Employee.UserRoleModel
                {
                    RoleId = landLordRoleId,
                }
            }
        };
        await context.Database.BeginTransactionAsync();
        await context.AddAsync(landLordModel);
        await context.SaveChangesAsync();

        await context.File
           .Where(m => landLordFormDto.FilesId.Contains(m.Id))
           .ExecuteUpdateAsync(m => m.SetProperty(c => c.LandLordId, landLordModel.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.NewLandLord,
            SenderId = CurrentUserId,
            ObjectId = landlordId,
        });
    }
    public async Task UpdateAsync(UpdateLandLordDto landLordFormDto)
    {
        #region Validation
        if (!await CheckEntityExist<LandLordModel>(m => m.Id == landLordFormDto.Id))
        {
            throw new NotFoundException(ErrorMessages.LandLordNotFound);
        }
        //if (!await CheckEntityExist<CountryModel>(m => m.Id == landLordFormDto.CountryId))
        //{
        //    throw new NotFoundException(ErrorMessages.CountryNotFound);
        //}
        //if (!await CheckEntityExist<CityModel>(m => m.Id == landLordFormDto.CityId))
        //{
        //    throw new NotFoundException(ErrorMessages.CityNotFound);
        //}
        //if (!await CheckEntityExist<RegionModel>(m => m.Id == landLordFormDto.RegionId))
        //{
        //    throw new NotFoundException(ErrorMessages.RegionNotFound);
        //}
        #endregion

        var landLordModel = await context.LandLord.Where(m => m.Id == landLordFormDto.Id).FirstOrDefaultAsync();

        landLordModel.FullName = landLordFormDto.FullName;
        landLordModel.Email = landLordFormDto.Email;
        landLordModel.SecondPhone = landLordFormDto.SecondPhone;
        landLordModel.PhoneNumber = landLordFormDto.Phone;
        landLordModel.Address = landLordFormDto.Address;
        landLordModel.Job = landLordFormDto.Job;
        landLordModel.Nationality = landLordFormDto.Nationality;
        landLordModel.CardNumber = landLordFormDto.CardNumber;
        landLordModel.IdentityCardNumber = landLordFormDto.IdentityCardNumber;
        landLordModel.IdentityCardExpiryDate = landLordFormDto.IdentityCardExpiryDate;
        landLordModel.PassportExpiryDate = landLordFormDto.PassportExpiryDate;
        landLordModel.PassportNumber = landLordFormDto.PassportNumber;
        landLordModel.AccountName = landLordFormDto.AccountName;
        landLordModel.AccountNumber = landLordFormDto.AccountNumber;
        landLordModel.BankName = landLordFormDto.BankName;
        landLordModel.BankAddress = landLordFormDto.BankAddress;
        landLordModel.Iban = landLordFormDto.Iban;
        landLordModel.SwiftCode = landLordFormDto.SwiftCode;
        landLordModel.TaxNumber = landLordFormDto.TaxNumber;
        landLordModel.Beneficiary = landLordFormDto.Beneficiary;
        landLordModel.Note = landLordFormDto.Note;

        if (!string.IsNullOrEmpty(landLordFormDto.Password))
        {
            var hashPassword = HashPassword(landLordFormDto.Password);
            landLordModel.HashPassword = hashPassword;
        }

        await context.File
                     .Where(m => landLordFormDto.FilesId.Contains(m.Id))
                     .ExecuteUpdateAsync(m => m.SetProperty(c => c.LandLordId, landLordModel.Id));

        var deletedUrls = new List<string>();
        if (landLordFormDto.RemoveFilesId.Any())
        {
            var deletedFiles = await context.File.Where(m => landLordFormDto.RemoveFilesId.Contains(m.Id)).ToListAsync();
            deletedUrls = deletedFiles.Select(m => m.Url).ToList();
            context.RemoveRange(deletedFiles);
        }
        await context.SaveChangesAsync();
        FileManagement.DeleteFiles(deletedUrls);

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.UpdateLandLord,
            SenderId = CurrentUserId,
            ObjectId = landLordModel.Id,
        });
    }
    public async Task<CommonResponseDto<List<GetLandLordSummaryDto>>> GetAllSummaryAsync()
    {
        var data = await context.LandLord
            .Select(m => new GetLandLordSummaryDto
            {
                Id = m.Id,
                AccountId = m.AccountId,
                DocCode = m.DocCode,
                FullName = m.FullName,
                Email = m.Email,
                Address = m.Address,
                //City = new GetBaseNameDto
                //{
                //    Id = m.CityId,
                //    Name = m.City.Name,
                //    NameEn = m.City.NameEn,
                //},
                //Region = new GetBaseNameDto
                //{
                //    Id = m.RegionId,
                //    Name = m.Region.Name,
                //    NameEn = m.Region.NameEn,
                //},
                //Country = new CountryDto
                //{
                //    Id = m.CountryId,
                //    Name = m.Country.Name,
                //    NameEn = m.Country.NameEn,
                //    Currency = new CurrencyDto
                //    {
                //        Id = m.Country.Currency.Id,
                //        Name = m.Country.Currency.Name,
                //        NameEn = m.Country.Currency.NameEn,
                //    },
                //},
            }).ToListAsync();
        return new(data);
    }
    public async Task<CommonResponseDto<List<LandLordDto>>> GetAllAsync(int pageSize, int pageNum, string search, LandLordSort landLordSort, bool isAsc,
        DateTime? from, DateTime? to)
    {
        Expression<Func<LandLordModel, bool>> expression = ll => (!from.HasValue && !to.HasValue) || ((from.HasValue && to.HasValue) ?
        ((ll.IdentityCardExpiryDate >= from && ll.IdentityCardExpiryDate <= to) || (ll.PassportExpiryDate >= from && ll.PassportExpiryDate <= to))
        : ((ll.IdentityCardExpiryDate >= from) || (ll.PassportExpiryDate >= from)));

        var landLordQuery = context.LandLord.Where(expression);
        landLordQuery = FilterByStringProperty(landLordQuery, search);

        landLordQuery = CustomOrderBy(landLordQuery, landLordSort, isAsc);

        var data = await landLordQuery
            .Skip(pageNum * pageSize)
            .Take(pageSize)
            .Select(m => new LandLordDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                FullName = m.FullName,
                Email = m.Email,
                SecondPhone = m.SecondPhone,
                Phone = m.PhoneNumber,
                Job = m.Job,
                Nationality = m.Nationality,
                CardNumber = m.CardNumber,
                IdentityCardNumber = m.IdentityCardNumber,
                IdentityCardExpiryDate = m.IdentityCardExpiryDate,
                PassportExpiryDate = m.PassportExpiryDate,
                PassportNumber = m.PassportNumber,
                Note = m.Note,
                AccountName = m.AccountName,
                AccountNumber = m.AccountNumber,
                BankName = m.BankName,
                BankAddress = m.BankAddress,
                Iban = m.Iban,
                SwiftCode = m.SwiftCode,
                TaxNumber = m.TaxNumber,
                Address = m.Address,
                Beneficiary = m.Beneficiary,
                Files = m.Files.Select(m => new GetBaseFileDto
                {
                    Id = m.Id,
                    Url = m.Url,
                    FileKind = m.FileKind,
                    FileType = m.FileType
                }).ToList(),
            }).ToListAsync();

        var totalRecords = await landLordQuery.CountAsync();
        return new(data, pageSize, pageNum, totalRecords);
    }
    public async Task<CommonResponseDto<LandLordDto>> GetByIdAsync(Guid Id)
    {
        var data = await context.LandLord
            .Where(m => m.Id == Id)
            .Select(m => new LandLordDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                FullName = m.FullName,
                Email = m.Email,
                SecondPhone = m.SecondPhone,
                Phone = m.PhoneNumber,
                Job = m.Job,
                Nationality = m.Nationality,
                CardNumber = m.CardNumber,
                IdentityCardNumber = m.IdentityCardNumber,
                IdentityCardExpiryDate = m.IdentityCardExpiryDate,
                PassportExpiryDate = m.PassportExpiryDate,
                PassportNumber = m.PassportNumber,
                Note = m.Note,
                AccountName = m.AccountName,
                AccountNumber = m.AccountNumber,
                BankName = m.BankName,
                BankAddress = m.BankAddress,
                Iban = m.Iban,
                SwiftCode = m.SwiftCode,
                TaxNumber = m.TaxNumber,
                Address = m.Address,
                Beneficiary = m.Beneficiary,
                //City = new GetBaseNameDto
                //{
                //    Id = m.CityId,
                //    Name = m.City.Name,
                //    NameEn = m.City.NameEn,
                //},
                //Region = new GetBaseNameDto
                //{
                //    Id = m.RegionId,
                //    Name = m.Region.Name,
                //    NameEn = m.Region.NameEn,
                //},
                //Country = new CountryDto
                //{
                //    Id = m.CountryId,
                //    Name = m.Country.Name,
                //    NameEn = m.Country.NameEn,
                //    Currency = new CurrencyDto
                //    {
                //        Id = m.Country.Currency.Id,
                //        Name = m.Country.Currency.Name,
                //        NameEn = m.Country.Currency.NameEn,
                //    },
                //},
                Files = m.Files.Select(m => new GetBaseFileDto
                {
                    Id = m.Id,
                    Url = m.Url,
                    FileKind = m.FileKind,
                    FileType = m.FileType
                }).ToList(),
            }).FirstOrDefaultAsync();

        return new(data);
    }
    public async Task RemoveAsync(Guid landLordId)
    {
        if (await context.Property.AnyAsync(m => m.LandLordId == landLordId))
            throw new ValidationException(ErrorMessages.LandLordHasProperty);

        if (await context.TenancyContract.AnyAsync(m => m.LandLordId == landLordId))
            throw new ValidationException(ErrorMessages.HasTenancyContract);

        var landLordModel = await context.LandLord.Where(m => m.Id == landLordId).FirstOrDefaultAsync() ??
            throw new NotFoundException(ErrorMessages.LandLordNotFound);

        landLordModel.IsValid = false;
        await context.SaveChangesAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.RemoveLandLord,
            SenderId = CurrentUserId,
            ObjectId = landLordModel.Id,
        });
    }
}
