﻿using REM_Dto.CommonDto;
using REM_Dto.LandLord;
using REM_Shared.Enum.Sort;

namespace REM_Repository.LandLord;

public interface ILandLordRepo
{
    Task AddAsync(AddLandLordDto landLordFormDto);
    Task UpdateAsync(UpdateLandLordDto landLordFormDto);
    Task RemoveAsync(Guid countryId);
    Task<CommonResponseDto<List<GetLandLordSummaryDto>>> GetAllSummaryAsync();
    Task<CommonResponseDto<List<LandLordDto>>> GetAllAsync(int pageSize, int pageNum, string search, LandLordSort landLordSort, bool isAsc, DateTime? from, DateTime? to);
    Task<CommonResponseDto<LandLordDto>> GetByIdAsync(Guid Id);
}