﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Expense;
using REM_Database.Model.Finance;
using REM_Dto.CommonDto;
using REM_Dto.Expense;
using REM_Dto.Finance;
using REM_Dto.Notification;
using REM_Repository.Base;
using REM_Repository.Notification;
using REM_Shared.Enum;
using REM_Shared.Exception;
using REM_Shared.Helper;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq.Expressions;

namespace REM_Repository.Finance
{
    public class FinanceRepo : BaseRepo, IFinanceRepo
    {
        private readonly INotificationRepo notificationRepo;

        public FinanceRepo(
            REMContext context,
            INotificationRepo notificationRepo,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
        {
            this.notificationRepo = notificationRepo;
        }

        public async Task AddFinanceOperation(List<AddFinanceOperationDto> operations, IDbContextTransaction tran = null)
        {
            try
            {
                if (tran == null)
                {
                    tran = await context.Database.BeginTransactionAsync();
                }
                var TransactionNumbers = (await context.FinanceOperations.Select(m => m.TransActionId).ToListAsync());
                var MaxTransactionNumber = TransactionNumbers.Any() ? TransactionNumbers.Max() : 0;

                var paymentDate = operations.FirstOrDefault()?.PaymentDate ?? null;
                paymentDate = paymentDate.HasValue ? paymentDate.Value : DateTime.UtcNow;


                var landlordsId = operations.Where(m => m.LandLordId.HasValue).Select(m => m.LandLordId.Value).ToList();
                var tenantsId = operations.Where(m => m.TenantId.HasValue).Select(m => m.TenantId.Value).ToList();

                var landLordaccounts = await context.Account.Where(m => m.LandLordId.HasValue && landlordsId.Contains(m.LandLordId.Value))
                                                            .Select(m => new
                                                            {
                                                                accountId = m.Id,
                                                                userId = m.LandLordId.Value
                                                            }).ToListAsync();

                var tenantaccounts = await context.Account.Where(m => m.TenantId.HasValue && tenantsId.Contains(m.TenantId.Value))
                                                            .Select(m => new
                                                            {
                                                                accountId = m.Id,
                                                                userId = m.TenantId.Value
                                                            }).ToListAsync();

                var financeAccounts = await GetFinanceAccount();

                var operationsModel = new List<FinanceOperationsModel>();

                foreach (var item in operations)
                {
                    if (item.AccountType != FinanceAccountType.LandLordAccount && item.AccountType != FinanceAccountType.TenantAccount)
                    {
                        item.AccountId = financeAccounts.Where(m => m.Type == item.AccountType).Select(m => m.Id).FirstOrDefault();
                    }
                    else
                    {
                        if (!item.AccountId.HasValue)
                        {
                            if (item.LandLordId.HasValue)
                                item.AccountId = landLordaccounts.FirstOrDefault(m => m.userId == item.LandLordId.Value).accountId;

                            else if (item.TenantId.HasValue)
                                item.AccountId = tenantaccounts.FirstOrDefault(m => m.userId == item.TenantId.Value).accountId;
                        }
                    }
                    var Tranincrement = item.TransActionIncrement.HasValue ? item.TransActionIncrement.Value : 1;
                    operationsModel.Add(new FinanceOperationsModel()
                    {
                        TransActionId = MaxTransactionNumber + Tranincrement,
                        Type = item.Type,
                        AccountId = item.AccountId.Value,
                        Value = item.Value,
                        Notes1 = item.Notes1,
                        Notes2 = item.Notes2,
                        CreateDate = paymentDate.Value,
                        BondId = item.BondId,
                        CheckId = item.CheckId,
                        TenancyContractId = item.TenancyContractId,
                        CompanyExpenseVoucherDetailsId = item.CompanyExpenseVoucherId,
                    });
                }
                await context.AddRangeAsync(operationsModel);
                await context.SaveChangesAsync();
                await tran.CommitAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<CommonResponseDto<FinanceReportDto>> GetAccountFinanceReport(
            Guid? accountId, Guid? unitId, Guid? property, Guid? expense, DateTime? from, DateTime? to)
        {
            var data = context.FinanceOperations
                                     .IgnoreQueryFilters()
                                     .Where
                                     (m =>
                                       (!accountId.HasValue || m.AccountId == accountId.Value) &&
                                       (!from.HasValue || m.CreateDate.Date >= from.Value.Date) &&
                                       (!to.HasValue || m.CreateDate.Date <= to.Value.Date) &&
                                       (!expense.HasValue || (m.CompanyExpenseVoucherDetailsId.HasValue && m.CompanyExpenseVoucherDetails.ExpenseId == expense))
                                     )
                                     .Select(m => new FinanceReportOperationsDto()
                                     {
                                         Id = m.Id,
                                         Notes = m.Notes1,
                                         TransActionDate = m.CreateDate,
                                         TransActionId = m.TransActionId,
                                         Value = m.Value,
                                         Type = m.Type,
                                         AccountType = m.Account.Type,

                                         TenantId = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.TenantId : m.BondId.HasValue && m.Bond.TenantId.HasValue ? m.Bond.Tenant.Id : m.TenancyContractId.HasValue ? m.TenancyContract.TenantId : null,
                                         TenantName = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Tenant.FullName : m.BondId.HasValue && m.Bond.TenantId.HasValue ? m.Bond.Tenant.FullName : m.TenancyContractId.HasValue ? m.TenancyContract.Tenant.FullName : string.Empty,
                                         TenantDocCode = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Tenant.DocCode : m.BondId.HasValue && m.Bond.TenantId.HasValue ? m.Bond.Tenant.DocCode : m.TenancyContractId.HasValue ? m.TenancyContract.Tenant.DocCode : string.Empty,

                                         LandLordId = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.Property.LandLordId : m.BondId.HasValue && m.Bond.LandLordId.HasValue ? m.Bond.LandLord.Id : m.TenancyContractId.HasValue ? m.TenancyContract.LandLordId : null,
                                         LandLordName = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.Property.LandLord.FullName : m.BondId.HasValue && m.Bond.LandLordId.HasValue ? m.Bond.LandLord.FullName : m.TenancyContractId.HasValue ? m.TenancyContract.LandLord.FullName : string.Empty,
                                         LandLordDocCode = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.Property.LandLord.DocCode : m.BondId.HasValue && m.Bond.LandLordId.HasValue ? m.Bond.LandLord.DocCode : m.TenancyContractId.HasValue ? m.TenancyContract.LandLord.DocCode : string.Empty,

                                         UnitId = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.Id : m.BondId.HasValue && m.Bond.UnitId.HasValue ? m.Bond.Unit.Id : m.TenancyContractId.HasValue ? m.TenancyContract.UnitId : null,
                                         UNitNumber = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.UnitNumber : m.BondId.HasValue && m.Bond.UnitId.HasValue ? m.Bond.Unit.UnitNumber : m.TenancyContractId.HasValue ? m.TenancyContract.Unit.UnitNumber : string.Empty,
                                         UnitDocCode = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.DocCode : m.BondId.HasValue && m.Bond.UnitId.HasValue ? m.Bond.Unit.DocCode : m.TenancyContractId.HasValue ? m.TenancyContract.Unit.DocCode : string.Empty,
                                         UnitName = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.UnitName : m.BondId.HasValue && m.Bond.UnitId.HasValue ? m.Bond.Unit.UnitName : m.TenancyContractId.HasValue ? m.TenancyContract.Unit.UnitName : string.Empty,

                                         PropertyId = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.PropertyId : m.BondId.HasValue && m.Bond.PropertyId.HasValue ? m.Bond.Property.Id : m.TenancyContractId.HasValue ? m.TenancyContract.PropertyId : null,
                                         PropertyDocCode = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.Property.DocCode : m.BondId.HasValue && m.Bond.PropertyId.HasValue ? m.Bond.Property.DocCode : m.TenancyContractId.HasValue ? m.TenancyContract.Property.DocCode : string.Empty,
                                         PropertyName = m.CompanyExpenseVoucherDetailsId.HasValue ? m.CompanyExpenseVoucherDetails.Unit.Property.Name : m.BondId.HasValue && m.Bond.PropertyId.HasValue ? m.Bond.Property.Name : m.TenancyContractId.HasValue ? m.TenancyContract.Property.Name : string.Empty,
                                     });

            var data2 = await data.Where
                        (m =>
                          (!unitId.HasValue || unitId.Value == m.UnitId) &&
                          (!property.HasValue || property.Value == m.PropertyId)
                        ).ToListAsync();

            var result = data2.OrderBy(m => m.TransActionDate).ThenBy(m => m.TransActionId).ToList();

            double previosBalance = 0.0;

            if (from.HasValue)
            {
                var temp = await context.FinanceOperations
                                     .IgnoreQueryFilters()
                                     .Where
                                     (m =>
                                       m.AccountId == accountId &&
                                       (!unitId.HasValue || (m.TenancyContractId.HasValue && unitId.Value == m.TenancyContract.UnitId)) &&
                                       (!property.HasValue || (m.TenancyContractId.HasValue && property.Value == m.TenancyContract.Unit.PropertyId)) &&
                                       (!from.HasValue || m.CreateDate.Date < from.Value.Date)
                                     ).Select(m => new
                                     {
                                         value = m.Value,
                                         type = m.Type,
                                     }).ToListAsync();

                previosBalance = temp.Where(m => m.type == FinanceOperationType.Debit).Sum(m => m.value) - temp.Where(m => m.type == FinanceOperationType.Credit).Sum(m => m.value);
            }


            var CreditSum = 0.0;
            var DebitSum = 0.0;
            foreach (var item in result)
            {
                if (item.Type == FinanceOperationType.Credit)
                {
                    CreditSum += item.Value;
                }
                else
                {
                    DebitSum += item.Value;
                }
                item.CurrentBalance = Math.Round(DebitSum - CreditSum, 2);
            }

            var totalCredit = result.Where(m => m.Type == FinanceOperationType.Credit).Sum(m => m.Value);
            var totalDebit = result.Where(m => m.Type == FinanceOperationType.Debit).Sum(m => m.Value);

            return new(
                new FinanceReportDto
                {
                    Operations = result,
                    Total = totalDebit - totalCredit,
                    TotalCredit = totalCredit,
                    TotalDebit = totalDebit,
                    PreviousBalance = previosBalance,
                });
        }

        public async Task<CommonResponseDto<GetAllBondsDto>> AddBond(AddBondDto dto)
        {
            var tran = await context.Database.BeginTransactionAsync();

            #region Validation 
            if (dto.TenantId.HasValue && !await context.Tenant.AnyAsync(m => m.Id == dto.TenantId))
            {
                throw new NotFoundException(ErrorMessages.TenantNotFound);
            }
            if (dto.LandlordId.HasValue && !await context.LandLord.AnyAsync(m => m.Id == dto.LandlordId))
            {
                throw new NotFoundException(ErrorMessages.LandLordNotFound);
            }
            if (dto.UnitId.HasValue && !await context.Unit.AnyAsync(m => m.Id == dto.UnitId))
            {
                throw new NotFoundException(ErrorMessages.UnitNotFound);
            }
            if (dto.CheckId.HasValue && !await context.TenancyContractPaymentPlan.AnyAsync(m => m.Id == dto.CheckId))
            {
                throw new NotFoundException(ErrorMessages.CheckNotFound);
            }
            if (!(dto.TenantId.HasValue ^ dto.LandlordId.HasValue))
            {
                throw new NotFoundException(ErrorMessages.InvalidData);
            }
            #endregion

            Guid? tcId = null;

            if (dto.CheckId.HasValue)
                tcId = await context.TenancyContractPaymentPlan.Where(m => m.Id == dto.CheckId).Select(m => m.TenancyContractId).FirstOrDefaultAsync();

            var tcModel = await context.TenancyContract.Where(m => m.Id == tcId).FirstOrDefaultAsync();

            if (tcModel == null && dto.CheckId.HasValue)
            {
                throw new NotFoundException(ErrorMessages.InvalidData);
            }

            var NextIncrementNumber = await GetNextIncrementNumber<BondsModel>(m => m.IncrementNumber, dto.Type, dto.VoucherBondType);


            DocumentNumber? docCode = null;
            switch (dto.VoucherBondType)
            {
                case VoucherBondTypes.BankReceipt:
                    {
                        docCode = dto.Type == BondType.PaymentVouchers ? DocumentNumber.BPV : DocumentNumber.BRV;
                        break;
                    }
                case VoucherBondTypes.CashReceipt:
                    {
                        docCode = dto.Type == BondType.PaymentVouchers ? DocumentNumber.CPV : DocumentNumber.CRV;
                        break;
                    }
                case VoucherBondTypes.ChequeReceipt:
                    {
                        docCode = dto.Type == BondType.PaymentVouchers ? DocumentNumber.PDCP : DocumentNumber.PDCR;
                        break;
                    }
                default:
                    {
                        throw new Exception(ErrorMessages.InvalidData);
                    }
            }

            var bondModel = new BondsModel
            {
                Id = Guid.NewGuid(),
                IncrementNumber = NextIncrementNumber,
                DocCode = StringHelper.CreateDocumentCode(docCode.Value, NextIncrementNumber),
                BondType = dto.Type,
                VoucherBondType = dto.VoucherBondType,
                Value = dto.Value,
                Date = dto.Date,
                Notes = dto.Notes,
                CheckId = dto.CheckId,
                LandLordId = dto.LandlordId,
                TenantId = dto.TenantId,
                UnitId = dto.UnitId,
                Account = dto.Account,
                PropertyId = dto.PropertyId,
            };

            await context.AddAsync(bondModel);
            await context.SaveChangesAsync();

            if (dto.FileId.HasValue)
                await context.File.Where(m => m.Id == dto.FileId.Value).ExecuteUpdateAsync(m => m.SetProperty(m => m.BondId, bondModel.Id));

            var increament = 1;
            var operations = new List<AddFinanceOperationDto>();
            var accountType =
                dto.VoucherBondType == VoucherBondTypes.CashReceipt ? FinanceAccountType.CashCompanyAccount :
                dto.VoucherBondType == VoucherBondTypes.BankReceipt ? FinanceAccountType.BankAccount :
                FinanceAccountType.PDCReceivablesAccount;

            if (dto.Type == BondType.PaymentVouchers)
            {
                if (dto.TenantId.HasValue)
                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = FinanceAccountType.TenantAccount,
                        Type = FinanceOperationType.Debit,
                        TenantId = dto.TenantId.Value,
                        Value = dto.Value,
                        TransActionIncrement = increament,
                        CheckId = dto.CheckId,
                        TenancyContractId = dto.CheckId.HasValue ? tcModel.Id : null,
                        PaymentDate = dto.Date,
                        Notes1 = dto.Notes,
                        BondId = bondModel.Id,
                    });

                else if (dto.LandlordId.HasValue)
                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = FinanceAccountType.LandLordAccount,
                        Type = FinanceOperationType.Debit,
                        LandLordId = dto.LandlordId.Value,
                        Value = dto.Value,
                        TransActionIncrement = increament,
                        CheckId = dto.CheckId,
                        TenancyContractId = dto.CheckId.HasValue ? tcModel.Id : null,
                        PaymentDate = dto.Date,
                        Notes1 = dto.Notes,
                        BondId = bondModel.Id,
                    });

                increament++;
                operations.Add(new AddFinanceOperationDto()
                {
                    AccountType = accountType,
                    Type = FinanceOperationType.Credit,
                    Value = dto.Value,
                    TransActionIncrement = increament,
                    CheckId = dto.CheckId,
                    TenancyContractId = dto.CheckId.HasValue ? tcModel.Id : null,
                    PaymentDate = dto.Date,
                    Notes1 = dto.Notes,
                    BondId = bondModel.Id,
                });
            }

            else if (dto.Type == BondType.ReceiptVouchers)
            {
                if (dto.Account == FinanceAccountType.RevenueAccount)
                {
                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = accountType,
                        Type = FinanceOperationType.Debit,
                        Value = dto.Value,
                        TransActionIncrement = increament,
                        PaymentDate = dto.Date,
                        Notes1 = dto.Notes,
                        BondId = bondModel.Id,
                        TenantId = dto.TenantId,
                        LandLordId = dto.LandlordId,
                    });
                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = FinanceAccountType.RevenueAccount,
                        Type = FinanceOperationType.Credit,
                        Value = dto.Value,
                        TransActionIncrement = increament,
                        PaymentDate = dto.Date,
                        Notes1 = dto.Notes,
                        BondId = bondModel.Id,
                        TenantId = dto.TenantId,
                        LandLordId = dto.LandlordId,
                    });
                }

                else if (dto.Account == FinanceAccountType.TenantAccount)
                {
                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = accountType,
                        Type = FinanceOperationType.Debit,
                        Value = dto.Value,
                        TransActionIncrement = increament,
                        CheckId = dto.CheckId,
                        TenancyContractId = dto.CheckId.HasValue ? tcModel.Id : null,
                        PaymentDate = dto.Date,
                        Notes1 = dto.Notes,
                        BondId = bondModel.Id,
                    });

                    increament++;

                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = FinanceAccountType.TenantAccount,
                        Type = FinanceOperationType.Credit,
                        TenantId = dto.TenantId,
                        Value = dto.Value,
                        TransActionIncrement = increament,
                        CheckId = dto.CheckId,
                        TenancyContractId = dto.CheckId.HasValue ? tcModel.Id : null,
                        PaymentDate = dto.Date,
                        Notes1 = dto.Notes,
                        BondId = bondModel.Id,
                    });
                }

                else if (dto.Account == FinanceAccountType.LandLordAccount)
                {
                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = accountType,
                        Type = FinanceOperationType.Debit,
                        Value = dto.Value,
                        TransActionIncrement = increament,
                        CheckId = dto.CheckId,
                        TenancyContractId = dto.CheckId.HasValue ? tcModel.Id : null,
                        PaymentDate = dto.Date,
                        Notes1 = dto.Notes,
                        BondId = bondModel.Id,
                    });

                    increament++;

                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = FinanceAccountType.LandLordAccount,
                        Type = FinanceOperationType.Credit,
                        LandLordId = dto.LandlordId,
                        Value = dto.Value,
                        TransActionIncrement = increament,
                        CheckId = dto.CheckId,
                        TenancyContractId = dto.CheckId.HasValue ? tcModel.Id : null,
                        PaymentDate = dto.Date,
                        Notes1 = dto.Notes,
                        BondId = bondModel.Id,
                    });
                    if (dto.CheckId.HasValue)
                    {
                        increament++;
                        operations.Add(new AddFinanceOperationDto()
                        {
                            AccountType = FinanceAccountType.AdvanceRentalAccount,
                            Type = FinanceOperationType.Debit,
                            Value = dto.Value,
                            TransActionIncrement = increament,
                            TenancyContractId = tcId,
                            CheckId = dto.CheckId,
                            PaymentDate = dto.Date,
                            Notes1 = dto.Notes,
                            BondId = bondModel.Id,
                        });
                        increament++;

                        operations.Add(new AddFinanceOperationDto()
                        {
                            AccountType = FinanceAccountType.LandLordAccount,
                            Type = FinanceOperationType.Credit,
                            LandLordId = tcModel.LandLordId,
                            Value = dto.Value,
                            TransActionIncrement = increament,
                            TenancyContractId = tcId,
                            CheckId = dto.CheckId,
                            PaymentDate = dto.Date,
                            Notes1 = dto.Notes,
                            BondId = bondModel.Id,
                        });
                    }
                }
            }

            await AddFinanceOperation(operations, tran);

            NotificationType? notificationType = null;
            if (dto.Type == BondType.ReceiptVouchers)
            {
                if (dto.VoucherBondType == VoucherBondTypes.ChequeReceipt)
                    notificationType = NotificationType.ChequeReceiptVoucher;

                else if (dto.VoucherBondType == VoucherBondTypes.CashReceipt)
                    notificationType = NotificationType.CashReceiptVoucher;

                else if (dto.VoucherBondType == VoucherBondTypes.BankReceipt)
                    notificationType = NotificationType.BankReceiptVoucher;
            }
            else
            {
                if (dto.VoucherBondType == VoucherBondTypes.ChequeReceipt)
                    notificationType = NotificationType.ChequePaymentVoucher;

                else if (dto.VoucherBondType == VoucherBondTypes.CashReceipt)
                    notificationType = NotificationType.CashPaymentVoucher;

                else if (dto.VoucherBondType == VoucherBondTypes.BankReceipt)
                    notificationType = NotificationType.BankPaymentVoucher;
            }

            await notificationRepo.SendAsync(new SendNotificationDto
            {
                NotificationType = notificationType.Value,
                SenderId = CurrentUserId,
                ObjectId = bondModel.Id,
            });

            return await GetBondById(bondModel.Id);
        }
        public async Task<CommonResponseDto<List<GetAllBondsDto>>> GetAllBonds
            (VoucherBondTypes voucherBondType, BondType Type,
             Guid? unitId, Guid? property, Guid? landlord, Guid? tenant, DateTime? from, DateTime? to, Guid? checkId, string search)
        {

            if (CurrentUserRole == UserType.LandLord.ToString())
            {
                landlord = CurrentUserId;
            }

            var data = context.Bonds
                 .IgnoreQueryFilters()
                 .OrderByDescending(m => m.CreateDate)
                 .Where(m =>
                        (m.BondType == Type && m.VoucherBondType == voucherBondType) &&
                        (string.IsNullOrEmpty(search) || search == m.DocCode) &&
                        (!unitId.HasValue || (m.UnitId.HasValue && unitId.Value == m.UnitId.Value)) &&
                        (!property.HasValue || (m.UnitId.HasValue && property.Value == m.Unit.PropertyId)) &&
                        (!landlord.HasValue || (m.LandLordId.HasValue && landlord.Value == m.LandLordId.Value)) &&
                        (!tenant.HasValue || (m.TenantId.HasValue && tenant.Value == m.TenantId.Value)) &&
                        (!checkId.HasValue || (m.CheckId.HasValue && checkId.Value == m.CheckId.Value)) &&
                        (!from.HasValue || m.CreateDate.Date >= from.Value.Date) &&
                        (!to.HasValue || m.CreateDate.Date <= to.Value.Date)
                        )
            .Select(m => new GetAllBondsDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                Notes = m.Notes,
                Value = m.Value,
                Date = m.Date,
                Type = m.BondType,
                VoucherBondType = m.VoucherBondType,
                Account = m.Account,
                ForLandLord = m.LandLordId.HasValue,
                FileUrl = m.Files.Select(m => m.Url).FirstOrDefault(),

                TenantId = m.TenantId.HasValue ? m.Tenant.Id : m.CheckId.HasValue ? m.Check.TenancyContract.Tenant.Id : m.TenantId.HasValue ? m.TenantId : null,
                TenantName = m.TenantId.HasValue ? m.Tenant.FullName : m.CheckId.HasValue ? m.Check.TenancyContract.Tenant.FullName : m.TenantId.HasValue ? m.Tenant.FullName : string.Empty,
                TenantDocCode = m.TenantId.HasValue ? m.Tenant.DocCode : m.CheckId.HasValue ? m.Check.TenancyContract.Tenant.DocCode : m.TenantId.HasValue ? m.Tenant.DocCode : string.Empty,

                LandLordId = m.LandLordId.HasValue ? m.LandLord.Id : m.CheckId.HasValue ? m.Check.TenancyContract.LandLord.Id : m.LandLordId.HasValue ? m.LandLordId : null,
                LandLordName = m.LandLordId.HasValue ? m.LandLord.FullName : m.CheckId.HasValue ? m.Check.TenancyContract.LandLord.FullName : m.LandLordId.HasValue ? m.LandLord.FullName : string.Empty,
                LandLordDocCode = m.LandLordId.HasValue ? m.LandLord.DocCode : m.CheckId.HasValue ? m.Check.TenancyContract.LandLord.DocCode : m.LandLordId.HasValue ? m.LandLord.DocCode : string.Empty,

                UnitId = m.UnitId.HasValue ? m.Unit.Id : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.Id : m.UnitId.HasValue ? m.UnitId : null,
                UNitNumber = m.UnitId.HasValue ? m.Unit.UnitNumber : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.UnitNumber : m.UnitId.HasValue ? m.Unit.UnitNumber : string.Empty,
                UnitDocCode = m.UnitId.HasValue ? m.Unit.DocCode : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.DocCode : m.UnitId.HasValue ? m.Unit.DocCode : string.Empty,
                UnitName = m.UnitId.HasValue ? m.Unit.UnitName : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.UnitName : m.UnitId.HasValue ? m.Unit.UnitName : string.Empty,

                PropertyId = m.PropertyId.HasValue ? m.Property.Id : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.Property.Id : m.UnitId.HasValue ? m.Unit.PropertyId : null,
                PropertyDocCode = m.PropertyId.HasValue ? m.Property.DocCode : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.Property.DocCode : m.UnitId.HasValue ? m.Unit.Property.DocCode : string.Empty,
                PropertyName = m.PropertyId.HasValue ? m.Property.Name : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.Property.Name : m.UnitId.HasValue ? m.Unit.Property.Name : string.Empty,

                CheckDocCode = m.CheckId.HasValue ? m.Check.DocCode : string.Empty,
                CheckId = m.CheckId.HasValue ? m.Check.Id : null,
                CheckNumber = m.CheckId.HasValue ? m.Check.Number : null,
                CheckDate = m.CheckId.HasValue ? m.Check.PayDate : null,


                Ledger = new FinanceReportDto
                {
                    Operations = m.FinanceOperations.OrderBy(m => m.CreateDate).Select(m => new FinanceReportOperationsDto
                    {
                        Id = m.Id,
                        Notes = m.Notes1,
                        TransActionDate = m.CreateDate,
                        TransActionId = m.TransActionId,
                        Value = m.Value,
                        Type = m.Type,
                        AccountType = m.Account.Type,
                    }).OrderBy(m => m.TransActionDate).ToList(),
                },
            });

            var data2 = await data.Where(m =>
                        (!unitId.HasValue || unitId.Value == m.UnitId) &&
                        (!property.HasValue || property.Value == m.PropertyId) &&
                        (!landlord.HasValue || landlord.Value == m.LandLordId) &&
                        (!tenant.HasValue || tenant.Value == m.TenantId)
                        ).ToListAsync();


            foreach (var bond in data2)
            {
                var CreditSum = 0.0;
                var DebitSum = 0.0;
                foreach (var item in bond.Ledger.Operations)
                {
                    if (item.Type == FinanceOperationType.Credit)
                    {
                        CreditSum += item.Value;
                    }
                    else
                    {
                        DebitSum += item.Value;
                    }
                    item.CurrentBalance = Math.Round(DebitSum - CreditSum, 2);
                }
                bond.Ledger.TotalCredit = CreditSum;
                bond.Ledger.TotalDebit = DebitSum;
                bond.Ledger.Total = DebitSum - CreditSum;
            }
            return new(data2);
        }
        public async Task<CommonResponseDto<GetAllBondsDto>> GetBondById(Guid bondId)
        {
            var data = await context.Bonds
                 .IgnoreQueryFilters()
                 .Where(m => m.Id == bondId)
                 .Select(m => new GetAllBondsDto
                 {
                     Id = m.Id,
                     DocCode = m.DocCode,
                     Notes = m.Notes,
                     Value = m.Value,
                     Date = m.Date,
                     Type = m.BondType,
                     VoucherBondType = m.VoucherBondType,
                     Account = m.Account,
                     ForLandLord = m.LandLordId.HasValue,
                     FileUrl = m.Files.Select(m => m.Url).FirstOrDefault(),

                     TenantId = m.TenantId.HasValue ? m.Tenant.Id : m.CheckId.HasValue ? m.Check.TenancyContract.Tenant.Id : m.TenantId.HasValue ? m.TenantId : null,
                     TenantName = m.TenantId.HasValue ? m.Tenant.FullName : m.CheckId.HasValue ? m.Check.TenancyContract.Tenant.FullName : m.TenantId.HasValue ? m.Tenant.FullName : string.Empty,
                     TenantDocCode = m.TenantId.HasValue ? m.Tenant.DocCode : m.CheckId.HasValue ? m.Check.TenancyContract.Tenant.DocCode : m.TenantId.HasValue ? m.Tenant.DocCode : string.Empty,

                     LandLordId = m.LandLordId.HasValue ? m.LandLord.Id : m.CheckId.HasValue ? m.Check.TenancyContract.LandLord.Id : m.LandLordId.HasValue ? m.LandLordId : null,
                     LandLordName = m.LandLordId.HasValue ? m.LandLord.FullName : m.CheckId.HasValue ? m.Check.TenancyContract.LandLord.FullName : m.LandLordId.HasValue ? m.LandLord.FullName : string.Empty,
                     LandLordDocCode = m.LandLordId.HasValue ? m.LandLord.DocCode : m.CheckId.HasValue ? m.Check.TenancyContract.LandLord.DocCode : m.LandLordId.HasValue ? m.LandLord.DocCode : string.Empty,

                     UnitId = m.UnitId.HasValue ? m.Unit.Id : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.Id : m.UnitId.HasValue ? m.UnitId : null,
                     UNitNumber = m.UnitId.HasValue ? m.Unit.UnitNumber : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.UnitNumber : m.UnitId.HasValue ? m.Unit.UnitNumber : string.Empty,
                     UnitDocCode = m.UnitId.HasValue ? m.Unit.DocCode : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.DocCode : m.UnitId.HasValue ? m.Unit.DocCode : string.Empty,
                     UnitName = m.UnitId.HasValue ? m.Unit.UnitName : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.UnitName : m.UnitId.HasValue ? m.Unit.UnitName : string.Empty,

                     PropertyId = m.PropertyId.HasValue ? m.Property.Id : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.Property.Id : m.UnitId.HasValue ? m.Unit.PropertyId : null,
                     PropertyDocCode = m.PropertyId.HasValue ? m.Property.DocCode : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.Property.DocCode : m.UnitId.HasValue ? m.Unit.Property.DocCode : string.Empty,
                     PropertyName = m.PropertyId.HasValue ? m.Property.Name : m.CheckId.HasValue ? m.Check.TenancyContract.Unit.Property.Name : m.UnitId.HasValue ? m.Unit.Property.Name : string.Empty,

                     CheckDocCode = m.CheckId.HasValue ? m.Check.DocCode : string.Empty,
                     CheckId = m.CheckId.HasValue ? m.Check.Id : null,
                     CheckDate = m.CheckId.HasValue ? m.Check.PayDate : null,


                     Ledger = new FinanceReportDto
                     {
                         Operations = m.FinanceOperations.OrderBy(m => m.CreateDate).Select(m => new FinanceReportOperationsDto
                         {
                             Id = m.Id,
                             Notes = m.Notes1,
                             TransActionDate = m.CreateDate,
                             TransActionId = m.TransActionId,
                             Value = m.Value,
                             Type = m.Type,
                             AccountType = m.Account.Type,
                         }).OrderBy(m => m.TransActionDate).ToList(),
                     },
                 }).FirstOrDefaultAsync();



            var CreditSum = 0.0;
            var DebitSum = 0.0;
            foreach (var item in data.Ledger.Operations)
            {
                if (item.Type == FinanceOperationType.Credit)
                {
                    CreditSum += item.Value;
                }
                else
                {
                    DebitSum += item.Value;
                }
                item.CurrentBalance = Math.Round(DebitSum - CreditSum, 2);
            }
            data.Ledger.TotalCredit = CreditSum;
            data.Ledger.TotalDebit = DebitSum;
            data.Ledger.Total = DebitSum - CreditSum;

            return new(data);
        }

        public async Task<CommonResponseDto<GetCompanyExpenseVoucherDto>> AddCompanyExpenseVoucher(AddCompanyExpenseVoucherDto dto)
        {
            var tran = await context.Database.BeginTransactionAsync();

            var validAccountType = new List<FinanceAccountType>()
            {
                FinanceAccountType.LandLordAccount,
                FinanceAccountType.TenantAccount,
                FinanceAccountType.CompanyExpense,
                FinanceAccountType.GeneralExpense,
            };

            if (dto.Details.Any(m => !validAccountType.Contains(m.AccountType)))
            {
                throw new ValidationException(ErrorMessages.InvalidData);
            }
            if (dto.Details.Any(m => m.AccountType == FinanceAccountType.LandLordAccount && !m.LandLordId.HasValue))
            {
                throw new ValidationException(ErrorMessages.LandLordNotFound);
            }
            if (dto.Details.Any(m => m.AccountType == FinanceAccountType.TenantAccount && !m.TenantId.HasValue))
            {
                throw new ValidationException(ErrorMessages.TenantNotFound);
            }
            if (dto.Details.Any(m => m.AccountType == FinanceAccountType.GeneralExpense && !m.ExpenseId.HasValue))
            {
                throw new ValidationException(ErrorMessages.ExpenseNotFound);
            }
            if (dto.Details.Any(m => m.AccountType == FinanceAccountType.CompanyExpense && !m.ExpenseId.HasValue))
            {
                throw new ValidationException(ErrorMessages.ExpenseNotFound);
            }


            var voucherDetailsModel = new List<CompanyExpenseVoucherDetailsModel>();
            var operations = new List<AddFinanceOperationDto>();

            var companyExpenseVoucherId = Guid.NewGuid();
            var totalSub = 0.0;
            var totalVat = 0.0;
            var totalValue = 0.0;
            var currentVatValue = 0.0;
            var currentTotalValue = 0.0;
            var currentSubValue = 0.0;

            var increament = 1;

            foreach (var item in dto.Details)
            {
                if (item.WithVat)
                {
                    if (item.VatType == VatType.Inclusive)
                    {
                        currentSubValue = item.Ammount / (1 + Constants.Vat);
                        currentVatValue = item.Ammount - currentSubValue;
                        currentTotalValue = item.Ammount;
                    }
                    else
                    {
                        currentVatValue = item.Ammount * Constants.Vat;
                        currentTotalValue = item.Ammount + currentVatValue;
                        currentSubValue = item.Ammount;
                    }

                }
                else
                {
                    currentVatValue = 0;
                    currentTotalValue = item.Ammount;
                    currentSubValue = item.Ammount;
                }
                currentTotalValue = Math.Round(currentTotalValue, 2);
                currentSubValue = Math.Round(currentSubValue, 2);
                currentVatValue = Math.Round(currentVatValue, 2);
                if (item.Vat != currentVatValue)
                {
                    throw new ValidationException(ErrorMessages.InvalidVatValue);
                }
                if (item.Total != currentTotalValue)
                {
                    throw new ValidationException(ErrorMessages.InvalidTotalValue);
                }
                totalSub += currentSubValue;
                totalVat += currentVatValue;
                totalValue += currentTotalValue;

                var currentId = Guid.NewGuid();
                voucherDetailsModel.Add(new CompanyExpenseVoucherDetailsModel
                {
                    Id = currentId,
                    AccountType = item.AccountType,
                    ExpenseId = item.ExpenseId,
                    LandLordId = item.LandLordId,
                    TenantId = item.TenantId,
                    UnitId = item.UnitId,
                    WithVat = item.WithVat,
                    Remarks = item.Remarks,
                    VatType = item.VatType,
                    SubTotal = currentSubValue,
                    Vat = currentVatValue,
                    Total = currentTotalValue,
                });

                if (item.AccountType == FinanceAccountType.TenantAccount || item.AccountType == FinanceAccountType.LandLordAccount)
                {
                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = item.AccountType,
                        Type = FinanceOperationType.Debit,
                        LandLordId = item.AccountType == FinanceAccountType.LandLordAccount ? item.LandLordId.Value : null,
                        TenantId = item.AccountType == FinanceAccountType.TenantAccount ? item.TenantId.Value : null,
                        Value = currentTotalValue,
                        TransActionIncrement = increament,
                        Notes1 = item.Remarks,
                        CompanyExpenseVoucherId = currentId
                    });
                    increament++;
                }
                else
                {
                    operations.Add(new AddFinanceOperationDto()
                    {
                        AccountType = item.AccountType,
                        Type = FinanceOperationType.Debit,
                        LandLordId = item.AccountType == FinanceAccountType.LandLordAccount ? item.LandLordId.Value : null,
                        TenantId = item.AccountType == FinanceAccountType.TenantAccount ? item.TenantId.Value : null,
                        Value = currentSubValue,
                        TransActionIncrement = increament,
                        Notes1 = item.Remarks,
                        CompanyExpenseVoucherId = currentId
                    });
                    increament++;
                    if (currentVatValue != 0)
                    {
                        operations.Add(new AddFinanceOperationDto()
                        {
                            AccountType = FinanceAccountType.OutputVATAccount,
                            Type = FinanceOperationType.Debit,
                            Value = currentVatValue,
                            TransActionIncrement = increament,
                            Notes1 = item.Remarks,
                            CompanyExpenseVoucherId = currentId
                        });
                        increament++;
                    }
                }
            }
            operations.Add(new AddFinanceOperationDto()
            {
                AccountType = FinanceAccountType.CashCompanyAccount,
                Type = FinanceOperationType.Credit,
                Value = totalValue,
                TransActionIncrement = increament,
                Notes1 = dto.Remarks,
                PaymentDate = dto.Date,
            });

            var NextIncrementNumber = await GetNextIncrementNumber<CompanyExpenseVoucherModel>(m => m.IncrementNumber);

            var vouhcerExpenseModel = new CompanyExpenseVoucherModel
            {
                Id = companyExpenseVoucherId,
                IncrementNumber = NextIncrementNumber,
                DocCode = StringHelper.CreateDocumentCode(DocumentNumber.CEV, NextIncrementNumber),
                Total = totalValue,
                Vat = totalVat,
                SubTotal = totalSub,
                Remarks = dto.Remarks,
                Date = dto.Date,
                VoucherDetails = voucherDetailsModel,
            };
            await context.AddAsync(vouhcerExpenseModel);
            await context.SaveChangesAsync();

            if (dto.FileId.HasValue)
            {
                await context.File
                             .Where(m => m.Id == dto.FileId.Value)
                             .ExecuteUpdateAsync(m => m.SetProperty(m => m.CompanyExpenseVoucherId, companyExpenseVoucherId));
            }

            await AddFinanceOperation(operations, tran);

            return (await GetCompanyExpenseVoucherById(companyExpenseVoucherId));
        }
        public async Task<CommonResponseDto<List<GetCompanyExpenseVoucherDto>>> GetCompanyExpenseVoucher
            (int pageSize, int pageNum, string search, DateTime? from, DateTime? to,
            Guid? landLord, Guid? tenant, Guid? property, Guid? unit, Guid? companyExpense, Guid? generalExpense)
        {
            Expression<Func<CompanyExpenseVoucherModel, bool>> expression = m =>
            (!from.HasValue || m.Date.Date >= from.Value) &&
            (!to.HasValue || m.Date.Date <= to.Value) &&
            (!landLord.HasValue || m.VoucherDetails.Any(m => m.LandLordId.HasValue && m.LandLordId.Value == landLord.Value)) &&
            (!tenant.HasValue || m.VoucherDetails.Any(m => m.TenantId.HasValue && m.TenantId.Value == tenant.Value)) &&
            (!property.HasValue || m.VoucherDetails.Any(m => m.UnitId.HasValue && m.Unit.PropertyId == property.Value)) &&
            (!unit.HasValue || m.VoucherDetails.Any(m => m.UnitId.HasValue && m.UnitId.Value == unit.Value)) &&
            (!companyExpense.HasValue || m.VoucherDetails.Any(m => m.ExpenseId.HasValue && m.ExpenseId.Value == companyExpense.Value)) &&
            (!generalExpense.HasValue || m.VoucherDetails.Any(m => m.ExpenseId.HasValue && m.ExpenseId.Value == generalExpense.Value));

            var dataQury = context.CompanyExpenseVoucher.Where(expression);
            dataQury = FilterByStringProperty(dataQury, search);

            var data =
                 await dataQury
                .Skip(pageNum * pageSize)
                .Take(pageSize)
                .OrderByDescending(m => m.Date)
                .Select(m => new GetCompanyExpenseVoucherDto
                {
                    Id = m.Id,
                    DocCode = m.DocCode,
                    Date = m.Date,
                    Remarks = m.Remarks,
                    SubTotal = m.SubTotal,
                    Total = m.Total,
                    Vat = m.Vat,
                    FileUrl = m.Files.Select(m => m.Url).FirstOrDefault(),
                    Details = m.VoucherDetails.Select(m => new GetCompanyExpenseVoucherDetailsDto
                    {
                        Vat = m.Vat,
                        SubTotal = m.SubTotal,
                        Total = m.Total,
                        VatType = m.VatType,
                        WithVat = m.WithVat,
                        Remarks = m.Remarks,
                        AccountType = m.AccountType,
                        landLord = m.LandLordId.HasValue ? new REM_Dto.LandLord.GetLandLordSummaryDto
                        {
                            Id = m.LandLord.Id,
                            DocCode = m.LandLord.DocCode,
                            FullName = m.LandLord.FullName,
                            Email = m.LandLord.Email,
                            TaxNumber = m.LandLord.TaxNumber,
                        } : null,
                        Tenant = m.TenantId.HasValue ? new REM_Dto.Tenant.GetTenantSummaryDto
                        {
                            Id = m.Tenant.Id,
                            DocCode = m.Tenant.DocCode,
                            FullName = m.Tenant.FullName,
                            Email = m.Tenant.Email,
                            Phone = m.Tenant.Phone,
                            Nationality = m.Tenant.Nationality,
                        } : null,
                        Expense = m.ExpenseId.HasValue ? new GetAllExpenseDto
                        {
                            Id = m.Expense.Id,
                            EnName = m.Expense.EnName,
                            Name = m.Expense.Name,
                            DocCode = m.Expense.DocCode
                        } : null,
                        UnitId = m.UnitId.HasValue ? m.Unit.Id : null,
                        UnitName = m.UnitId.HasValue ? m.Unit.UnitName : null,
                        UnitDocCode = m.UnitId.HasValue ? m.Unit.DocCode : null,
                        UNitNumber = m.UnitId.HasValue ? m.Unit.UnitNumber : null,
                        LandLordDocCode = m.UnitId.HasValue ? m.Unit.Property.LandLord.DocCode : null,
                        LandLordId = m.UnitId.HasValue ? m.Unit.Property.LandLord.Id : null,
                        LandLordName = m.UnitId.HasValue ? m.Unit.Property.LandLord.FullName : null,
                        PropertyDocCode = m.UnitId.HasValue ? m.Unit.Property.DocCode : null,
                        PropertyId = m.UnitId.HasValue ? m.Unit.Property.Id : null,
                        PropertyName = m.UnitId.HasValue ? m.Unit.Property.Name : null,
                    }).ToList(),
                }).ToListAsync();

            var totalRecords = await dataQury.CountAsync();
            return new(data, pageSize, pageNum, totalRecords);
        }
        public async Task<CommonResponseDto<GetCompanyExpenseVoucherDto>> GetCompanyExpenseVoucherById(Guid Id)
        {
            var data =
                await context.CompanyExpenseVoucher.Where(m => m.Id == Id)
                                   .Select(m => new GetCompanyExpenseVoucherDto
                                   {
                                       Id = m.Id,
                                       DocCode = m.DocCode,
                                       Date = m.Date,
                                       Remarks = m.Remarks,
                                       SubTotal = m.SubTotal,
                                       Total = m.Total,
                                       Vat = m.Vat,
                                       FileUrl = m.Files.Select(m => m.Url).FirstOrDefault(),
                                       Details = m.VoucherDetails.Select(m => new GetCompanyExpenseVoucherDetailsDto
                                       {
                                           Vat = m.Vat,
                                           SubTotal = m.SubTotal,
                                           Total = m.Total,
                                           VatType = m.VatType,
                                           WithVat = m.WithVat,
                                           Remarks = m.Remarks,
                                           AccountType = m.AccountType,
                                           landLord = m.LandLordId.HasValue ? new REM_Dto.LandLord.GetLandLordSummaryDto
                                           {
                                               Id = m.LandLord.Id,
                                               DocCode = m.LandLord.DocCode,
                                               FullName = m.LandLord.FullName,
                                               Email = m.LandLord.Email,
                                               TaxNumber = m.LandLord.TaxNumber,
                                           } : null,
                                           Tenant = m.TenantId.HasValue ? new REM_Dto.Tenant.GetTenantSummaryDto
                                           {
                                               Id = m.Tenant.Id,
                                               DocCode = m.Tenant.DocCode,
                                               FullName = m.Tenant.FullName,
                                               Email = m.Tenant.Email,
                                               Phone = m.Tenant.Phone,
                                               Nationality = m.Tenant.Nationality,
                                           } : null,
                                           Expense = m.ExpenseId.HasValue ? new GetAllExpenseDto
                                           {
                                               Id = m.Expense.Id,
                                               EnName = m.Expense.EnName,
                                               Name = m.Expense.Name,
                                               DocCode = m.Expense.DocCode
                                           } : null,
                                           UnitId = m.UnitId.HasValue ? m.Unit.Id : null,
                                           UnitName = m.UnitId.HasValue ? m.Unit.UnitName : null,
                                           UnitDocCode = m.UnitId.HasValue ? m.Unit.DocCode : null,
                                           UNitNumber = m.UnitId.HasValue ? m.Unit.UnitNumber : null,
                                           LandLordDocCode = m.UnitId.HasValue ? m.Unit.Property.LandLord.DocCode : null,
                                           LandLordId = m.UnitId.HasValue ? m.Unit.Property.LandLord.Id : null,
                                           LandLordName = m.UnitId.HasValue ? m.Unit.Property.LandLord.FullName : null,
                                           PropertyDocCode = m.UnitId.HasValue ? m.Unit.Property.DocCode : null,
                                           PropertyId = m.UnitId.HasValue ? m.Unit.Property.Id : null,
                                           PropertyName = m.UnitId.HasValue ? m.Unit.Property.Name : null,
                                       }).ToList(),
                                   }).FirstOrDefaultAsync();

            return new(data);
        }
        public async Task<List<GetStaticFinanceAccountsDto>> GetFinanceAccount()
        {
            var data = await context.Account
                .Where(m => m.Type != FinanceAccountType.LandLordAccount && m.Type != FinanceAccountType.TenantAccount)
                .Select(m => new GetStaticFinanceAccountsDto()
                {
                    Type = m.Type,
                    Id = m.Id,
                }).ToListAsync();

            return data;
        }
        public async Task SeedFinanceAccount()
        {
            var financeAccountsModel = await context.Account
                .Where(m => m.Type != FinanceAccountType.LandLordAccount && m.Type != FinanceAccountType.TenantAccount)
                .Select(m => m.Type).ToListAsync();

            var Accounts = Enum.GetNames(typeof(FinanceAccountType)).ToList();

            var addedAccountsModel = new List<AccountModel>();
            foreach (var item in Accounts)
            {
                if (item == FinanceAccountType.TenantAccount.ToString() || item == FinanceAccountType.LandLordAccount.ToString())
                    continue;

                if (!financeAccountsModel.Any(m => m.ToString() == item))
                {
                    addedAccountsModel.Add(new AccountModel
                    {
                        Type = (FinanceAccountType)Accounts.IndexOf(item),
                    });
                }
            }
            await context.AddRangeAsync(addedAccountsModel);
            await context.SaveChangesAsync();
        }
    }
}
