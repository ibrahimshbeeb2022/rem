﻿using Microsoft.EntityFrameworkCore.Storage;
using REM_Dto.CommonDto;
using REM_Dto.Expense;
using REM_Dto.Finance;
using REM_Shared.Enum;

namespace REM_Repository.Finance
{
    public interface IFinanceRepo
    {
        Task AddFinanceOperation(List<AddFinanceOperationDto> operations, IDbContextTransaction tran = null);
        Task<CommonResponseDto<FinanceReportDto>> GetAccountFinanceReport(Guid? accountId, Guid? unitId, Guid? property, Guid? expense, DateTime? from, DateTime? to);
        Task<List<GetStaticFinanceAccountsDto>> GetFinanceAccount();
        Task<CommonResponseDto<GetAllBondsDto>> AddBond(AddBondDto dto);
        Task<CommonResponseDto<List<GetAllBondsDto>>> GetAllBonds
           (VoucherBondTypes voucherBondType, BondType Type,
            Guid? unitId, Guid? property, Guid? landlord, Guid? tenant, DateTime? from, DateTime? to, Guid? checkId, string search);

        Task<CommonResponseDto<GetCompanyExpenseVoucherDto>> AddCompanyExpenseVoucher(AddCompanyExpenseVoucherDto dto);
        Task<CommonResponseDto<List<GetCompanyExpenseVoucherDto>>> GetCompanyExpenseVoucher
             (int pageSize, int pageNum, string search, DateTime? from, DateTime? to,
            Guid? landLord, Guid? tenant, Guid? property, Guid? unit, Guid? companyExpense, Guid? generalExpense);
        Task SeedFinanceAccount();
    }
}
