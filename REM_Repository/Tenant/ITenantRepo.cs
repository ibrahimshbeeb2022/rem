﻿using REM_Dto.CommonDto;
using REM_Dto.Tenant;
using REM_Shared.Enum.Sort;

namespace REM_Repository.Tenant;

public interface ITenantRepo
{
    Task<GetTenantDto> AddAsync(AddTenantDto landLordFormDto);
    Task UpdateAsync(UpdateTenantDto landLordFormDto);
    Task RemoveAsync(Guid tenantId);
    Task<CommonResponseDto<List<GetTenantSummaryDto>>> GetAllSummaryAsync();
    Task<CommonResponseDto<List<GetTenantDto>>> GetAllAsync(int pageSize, int pageNum, string search, DateTime? from, DateTime? to,
        int? numberOfResidentsOfTheRentedProperty, TenantSort tenantSort, bool isAsc);
    Task<CommonResponseDto<GetTenantDto>> GetByIdAsync(Guid Id);
}