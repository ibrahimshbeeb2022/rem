﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Finance;
using REM_Database.Model.Master;
using REM_Dto.Base;
using REM_Dto.CommonDto;
using REM_Dto.Notification;
using REM_Dto.Tenant;
using REM_Repository.Base;
using REM_Repository.Notification;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.Tenant;

public class TenantRepo : BaseRepo, ITenantRepo
{
    private readonly INotificationRepo notificationRepo;

    public TenantRepo(
        REMContext context,
        IConfiguration configuration,
        INotificationRepo notificationRepo,
        IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
    {
        this.notificationRepo = notificationRepo;
    }

    public async Task<GetTenantDto> AddAsync(AddTenantDto tenant)
    {

        var NextIncrementNumber = await GetNextIncrementNumber<TenantModel>(m => m.IncrementNumber);
        var tenantId = Guid.NewGuid();
        var tenantModel = new TenantModel()
        {
            Id = tenantId,
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.T, NextIncrementNumber),
            IncrementNumber = NextIncrementNumber,
            FullName = tenant.FullName,
            Email = tenant.Email,
            //Fax = tenant.Fax,
            Phone = tenant.Phone,
            Job = tenant.Job,
            Nationality = tenant.Nationality,
            CardNumber = tenant.CardNumber,
            IdentityCardNumber = tenant.IdentityCardNumber,
            IdentityCardExpiryDate = tenant.IdentityCardExpiryDate,
            PassportExpiryDate = tenant.PassportExpiryDate,
            PassportNumber = tenant.PassportNumber,
            Beneficiary = tenant.Beneficiary,
            Address = tenant.Address,
            BoxMail = tenant.BoxMail,
            NumberOfResidentsOfTheRentedProperty = tenant.NumberOfResidentsOfTheRentedProperty,
            Account = new AccountModel
            {
                Type = FinanceAccountType.TenantAccount,
                TenantId = tenantId,
            },
        };

        await context.AddAsync(tenantModel);
        await context.SaveChangesAsync();

        await context.File
           .Where(m => tenant.FilesId.Contains(m.Id))
           .ExecuteUpdateAsync(m => m.SetProperty(c => c.TenantId, tenantModel.Id));

        await context.SaveChangesAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.NewTenant,
            SenderId = CurrentUserId,
            ObjectId = tenantId,
        });

        return ((await GetByIdAsync(tenantModel.Id)).Data);
    }
    public async Task UpdateAsync(UpdateTenantDto tenant)
    {
        if (!await CheckEntityExist<TenantModel>(m => m.Id == tenant.Id))
        {
            throw new NotFoundException(ErrorMessages.TenantNotFound);
        }
        var tenantModel = await context.Tenant.Where(m => m.Id == tenant.Id).FirstOrDefaultAsync();
        tenantModel.FullName = tenant.FullName;
        tenantModel.Email = tenant.Email;
        //tenantModel.Fax = tenant.Fax;
        tenantModel.Phone = tenant.Phone;
        tenantModel.Job = tenant.Job;
        tenantModel.Nationality = tenant.Nationality;
        tenantModel.CardNumber = tenant.CardNumber;
        tenantModel.IdentityCardNumber = tenant.IdentityCardNumber;
        tenantModel.IdentityCardExpiryDate = tenant.IdentityCardExpiryDate;
        tenantModel.PassportExpiryDate = tenant.PassportExpiryDate;
        tenantModel.PassportNumber = tenant.PassportNumber;
        tenantModel.Beneficiary = tenant.Beneficiary;
        tenantModel.Address = tenant.Address;
        tenantModel.BoxMail = tenant.BoxMail;
        tenantModel.NumberOfResidentsOfTheRentedProperty = tenant.NumberOfResidentsOfTheRentedProperty;

        await context.File
                 .Where(m => tenant.FilesId.Contains(m.Id))
                 .ExecuteUpdateAsync(m => m.SetProperty(c => c.TenantId, tenant.Id));

        var deletedUrls = new List<string>();
        if (tenant.DeletedFiles.Any())
        {
            var deletedFiles = await context.File.Where(m => tenant.DeletedFiles.Contains(m.Id)).ToListAsync();
            deletedUrls = deletedFiles.Select(m => m.Url).ToList();
            context.RemoveRange(deletedFiles);
        }
        await context.SaveChangesAsync();
        FileManagement.DeleteFiles(deletedUrls);

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.UpdateTenant,
            SenderId = CurrentUserId,
            ObjectId = tenantModel.Id,
        });
    }
    public async Task<CommonResponseDto<List<GetTenantDto>>> GetAllAsync(
        int pageSize, int pageNum, string search, DateTime? from, DateTime? to,
        int? numberOfResidentsOfTheRentedProperty, TenantSort tenantSort, bool isAsc)
    {

        Guid? landLordId = null;
        if (CurrentUserRole == UserType.LandLord.ToString())
        {
            landLordId = CurrentUserId;
        }

        Expression<Func<TenantModel, bool>> expression = t =>
        ((!from.HasValue && !to.HasValue) || ((from.HasValue && to.HasValue) ?
        ((t.IdentityCardExpiryDate >= from && t.IdentityCardExpiryDate <= to) || (t.PassportExpiryDate >= from && t.PassportExpiryDate <= to))
        : ((t.IdentityCardExpiryDate >= from) || (t.PassportExpiryDate >= from))))
        && (!numberOfResidentsOfTheRentedProperty.HasValue || t.NumberOfResidentsOfTheRentedProperty == numberOfResidentsOfTheRentedProperty)
        && (!landLordId.HasValue || t.TenancyContract.Any(m => m.LandLordId == landLordId.Value));

        var tenantsQuery = context.Tenant.Where(expression);
        tenantsQuery = FilterByStringProperty(tenantsQuery, search);
        tenantsQuery = CustomOrderBy(tenantsQuery, tenantSort, isAsc);

        var data = await tenantsQuery
            .Skip(pageNum * pageSize)
            .Take(pageSize)
            .Select(tenant => new GetTenantDto
            {
                Id = tenant.Id,
                DocCode = tenant.DocCode,
                FullName = tenant.FullName,
                Email = tenant.Email,
                //Fax = tenant.Fax,
                Phone = tenant.Phone,
                Job = tenant.Job,
                BoxMail = tenant.BoxMail,
                Nationality = tenant.Nationality,
                CardNumber = tenant.CardNumber,
                IdentityCardNumber = tenant.IdentityCardNumber,
                IdentityCardExpiryDate = tenant.IdentityCardExpiryDate,
                PassportExpiryDate = tenant.PassportExpiryDate,
                PassportNumber = tenant.PassportNumber,
                Beneficiary = tenant.Beneficiary,
                Address = tenant.Address,
                NumberOfResidentsOfTheRentedProperty = tenant.NumberOfResidentsOfTheRentedProperty,
                Files = tenant.Files.Select(m => new GetBaseFileDto
                {
                    Id = m.Id,
                    FileKind = m.FileKind,
                    FileType = m.FileType,
                    Url = m.Url
                }).ToList(),
            }).ToListAsync();

        var totalRecords = await tenantsQuery.CountAsync();

        return new(data, pageSize, pageNum, totalRecords);
    }
    public async Task<CommonResponseDto<List<GetTenantSummaryDto>>> GetAllSummaryAsync()
    {
        Guid? landLordId = null;
        if (CurrentUserRole == UserType.LandLord.ToString())
        {
            landLordId = CurrentUserId;
        }
        var data = await context.Tenant
            .Where(m => !landLordId.HasValue || m.TenancyContract.Any(m => m.LandLordId == landLordId.Value))
            .Select(m => new GetTenantSummaryDto
            {
                Id = m.Id,
                AccountId = m.AccountId,
                FullName = m.FullName,
                Email = m.Email,
                Nationality = m.Nationality,
                Phone = m.Phone,
                DocCode = m.DocCode
            }).ToListAsync();

        return new(data);
    }
    public async Task<CommonResponseDto<GetTenantDto>> GetByIdAsync(Guid Id)
    {
        var data = await context.Tenant
                       .Where(m => m.Id == Id)
                       .Select(tenant => new GetTenantDto
                       {
                           Id = tenant.Id,
                           DocCode = tenant.DocCode,
                           FullName = tenant.FullName,
                           Email = tenant.Email,
                           //Fax = tenant.Fax,
                           Phone = tenant.Phone,
                           Job = tenant.Job,
                           Nationality = tenant.Nationality,
                           CardNumber = tenant.CardNumber,
                           IdentityCardNumber = tenant.IdentityCardNumber,
                           IdentityCardExpiryDate = tenant.IdentityCardExpiryDate,
                           PassportExpiryDate = tenant.PassportExpiryDate,
                           PassportNumber = tenant.PassportNumber,
                           Beneficiary = tenant.Beneficiary,
                           Address = tenant.Address,
                           BoxMail = tenant.BoxMail,
                           NumberOfResidentsOfTheRentedProperty = tenant.NumberOfResidentsOfTheRentedProperty,
                           Files = tenant.Files.Select(m => new GetBaseFileDto
                           {
                               Id = m.Id,
                               FileKind = m.FileKind,
                               FileType = m.FileType,
                               Url = m.Url
                           }).ToList(),
                       }).FirstOrDefaultAsync();

        return new(data);
    }
    public async Task RemoveAsync(Guid tenantId)
    {
        if (await context.TenancyContract.AnyAsync(m => m.TenantId == tenantId))
            throw new ValidationException(ErrorMessages.HasTenancyContract);

        var tenantModel = await context.Tenant.Where(m => m.Id == tenantId).FirstOrDefaultAsync() ??
            throw new NotFoundException(ErrorMessages.TenantNotFound);

        tenantModel.IsValid = false;
        await context.SaveChangesAsync();

        await notificationRepo.SendAsync(new SendNotificationDto
        {
            NotificationType = NotificationType.RemoveTenant,
            SenderId = CurrentUserId,
            ObjectId = tenantId,
        });
    }
}