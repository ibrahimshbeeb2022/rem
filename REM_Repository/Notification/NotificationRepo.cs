﻿using FirebaseAdmin.Messaging;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Notification;
using REM_Dto.CommonDto;
using REM_Dto.Notification;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Helper;
using System.Linq.Expressions;

namespace REM_Repository.Notification
{
    public class NotificationRepo : BaseRepo, INotificationRepo
    {
        public NotificationRepo(
            REMContext context,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
        {
        }

        public async Task ChangeFcmTokenAsync(string fcmToken) =>
            await context.User.Where(c => c.Id == CurrentUserId && c.IsValid).ExecuteUpdateAsync(u => u.SetProperty(u => u.FcmToken, fcmToken));

        public async Task ChangeLanguageAsync(LanguageEnum language)
            => await context.User.Where(c => c.Id == CurrentUserId && c.IsValid).ExecuteUpdateAsync(u => u.SetProperty(u => u.Language, language));

        public async Task ReadOneAsync(Guid id)
            => await context.NotificationUsers.Where(x => x.Id == id && x.IsValid).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsRead, true));

        public async Task ReadAllAsync() =>
            await context.NotificationUsers
                         .Where(n => n.IsValid && n.UserId == CurrentUserId)
                         .ExecuteUpdateAsync(n => n.SetProperty(n => n.IsRead, true));

        public async Task<GetNotificationsCountDto> GetCountsAsync()
        {
            var data = await context.NotificationUsers
                .Where(c => c.IsValid && c.UserId == CurrentUserId)
                .Select(m => new
                {
                    isRead = m.IsRead,
                }).ToListAsync();

            return new GetNotificationsCountDto
            {
                ReadCount = data.Where(m => m.isRead).Count(),
                UnReadCount = data.Where(m => !m.isRead).Count(),
                AllCount = data.Count(),
            };
        }


        public async Task<CommonResponseDto<List<NotificationDto>>> GetAllAsync(int pageSize, int page, bool? isUnread)
        {
            var userLanguage = await context.User.Where(u => u.Id == CurrentUserId && u.IsValid).Select(s => s.Language).FirstOrDefaultAsync();

            Expression<Func<NotificationUserModel, bool>> expression = n =>
            n.UserId == CurrentUserId &&
            (!isUnread.HasValue || !n.IsRead == isUnread) && n.IsValid;

            var notifications = await context.NotificationUsers
                .Where(expression)
                .Skip(pageSize * page)
                .Take(pageSize)
                .OrderByDescending(nu => nu.CreateDate)
                .Select(n => new NotificationDto
                {
                    Id = n.Id,
                    IsRead = n.IsRead,
                    Date = n.CreateDate,
                    ObjectId = n.ObjectId,
                    ObjectCode = n.ObjectCode,
                    NotificationType = n.Notification.Type,
                    Body = n.Notification.Body,
                    Title = n.Notification.Title,
                    BodyEn = n.Notification.BodyEn,
                    TitleEn = n.Notification.TitleEn
                }).ToListAsync();

            return new(notifications, pageSize, page, await CountEntityExist(expression));
        }
        public async Task SendAsync(SendNotificationDto sendNotificationDto)
        {
            try
            {
                var notificationModel = await context.Notifications.Where(n => n.Type == sendNotificationDto.NotificationType && n.IsValid).FirstOrDefaultAsync();

                if (!sendNotificationDto.RecipientId.Any())
                {
                    sendNotificationDto.RecipientId = await context.Employee.Select(m => m.Id).ToListAsync();
                }
                if (string.IsNullOrEmpty(sendNotificationDto.ObjectCode))
                {
                    sendNotificationDto.ObjectCode = await GetDocNoById(sendNotificationDto.NotificationType, sendNotificationDto.ObjectId);
                }

                var usersFcm = await context.User.Where(m => !string.IsNullOrEmpty(m.FcmToken) && sendNotificationDto.RecipientId.Contains(m.Id)).Select(m => new
                {
                    m.FcmToken,
                    m.Language,
                    m.Id,
                }).ToListAsync();
                List<NotificationUserModel> notificationUser = [];

                foreach (var employee in usersFcm)
                {
                    try
                    {
                        Dictionary<string, string> extraData = new()
                    {
                        { "objectCode", sendNotificationDto.ObjectCode},
                        { "objectId", sendNotificationDto.ObjectId.ToString()},
                        { "notificationType", ((int)sendNotificationDto.NotificationType).ToString() },
                        { "body", employee.Language == LanguageEnum.Ar ? notificationModel.Body : notificationModel.BodyEn },
                        { "title", employee.Language == LanguageEnum.Ar ? notificationModel.Title : notificationModel.TitleEn },
                    };
                        var message = new Message()
                        {
                            Token = employee.FcmToken,
                            Data = extraData
                        };
                        string response = await FirebaseMessaging.DefaultInstance.SendAsync(message);
                    }
                    catch (Exception)
                    {
                        //continue;
                    }
                    notificationUser.Add(new NotificationUserModel
                    {
                        UserId = employee.Id,
                        NotificationId = notificationModel.Id,
                        ObjectId = sendNotificationDto.ObjectId,
                        SenderId = sendNotificationDto.SenderId,
                        ObjectCode = sendNotificationDto.ObjectCode,
                    });
                }

                await context.AddRangeAsync(notificationUser);
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
            }
        }
        public async Task SeedNotificationType()
        {
            var notificationHelper = new NotificationTypesHelper();

            var notificationsModel = await context.Notifications.ToListAsync();

            var notifications = ((NotificationType[])Enum.GetValues(typeof(NotificationType))).ToList();

            NotificationTypeData notificationNewData = null;

            notificationsModel.ForEach(m =>
                        {
                            notificationNewData = notificationHelper.GetNotificationDataByType(m.Type);
                            if (notificationNewData != null)
                            {
                                m.Title = notificationNewData.Title;
                                m.TitleEn = notificationNewData.EnTitle;
                                m.Body = notificationNewData.Body;
                                m.BodyEn = notificationNewData.EnBody;
                            }
                        });

            foreach (var item in notifications)
            {
                if (!notificationsModel.Any(m => m.Type == item))
                {
                    notificationNewData = notificationHelper.GetNotificationDataByType(item);

                    var temp = new NotificationModel
                    {
                        Type = item,
                        Title = notificationNewData.Title,
                        TitleEn = notificationNewData.EnTitle,
                        Body = notificationNewData.Body,
                        BodyEn = notificationNewData.EnBody,
                    };
                    notificationsModel.Add(temp);
                }
            }
            context.UpdateRange(notificationsModel);
            await context.SaveChangesAsync();
        }

        public async Task<string> GetDocNoById(NotificationType NotificationType, Guid? id)
        {
            if (!id.HasValue)
                return string.Empty;

            switch (NotificationType)
            {
                case NotificationType.NewTenancyContract:
                    {
                        return await context.TenancyContract.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.ChangeTenancyContractStatus:
                    {
                        return await context.TenancyContract.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.RemoveTenancyContract:
                    {
                        return await context.TenancyContract.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.ChangeChequeStatus:
                    {
                        return await context.TenancyContractPaymentPlan.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.NewLandLord:
                    {
                        return await context.LandLord.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.UpdateLandLord:
                    {
                        return await context.LandLord.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.RemoveLandLord:
                    {
                        return await context.LandLord.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.NewTenant:
                    {
                        return await context.Tenant.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.RemoveTenant:
                    {
                        return await context.Tenant.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.UpdateTenant:
                    {
                        return await context.Tenant.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.NewProperty:
                    {
                        return await context.Property.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.UpdateProperty:
                    {
                        return await context.Property.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.RemoveProperty:
                    {
                        return await context.Property.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.NewUnit:
                    {
                        return await context.Unit.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.RemoveUnit:
                    {
                        return await context.Unit.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.UpdateUnit:
                    {
                        return await context.Unit.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.BankPaymentVoucher:
                    {
                        return await context.Bonds.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.CashPaymentVoucher:
                    {
                        return await context.Bonds.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.ChequePaymentVoucher:
                    {
                        return await context.Bonds.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.BankReceiptVoucher:
                    {
                        return await context.Bonds.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.CashReceiptVoucher:
                    {
                        return await context.Bonds.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.ChequeReceiptVoucher:
                    {
                        return await context.Bonds.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }
                case NotificationType.CompanyExpenseVoucher:
                    {
                        return await context.CompanyExpenseVoucher.Where(m => m.Id == id).Select(m => m.DocCode).FirstOrDefaultAsync();
                    }

                default: return string.Empty;
            }
        }
        public async Task<List<NotificationTypeData>> gettypes()
        {
            var ss = await context.Notifications.Select(m => new NotificationTypeData
            {
                EnTitle = m.TitleEn,
                Title = m.Title,
                Body = m.Body,
                EnBody = m.BodyEn,
                Type = m.Type,
            }).ToListAsync();

            return ss;
        }
    }
}
