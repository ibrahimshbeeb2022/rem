﻿using REM_Dto.CommonDto;
using REM_Dto.Notification;
using REM_Shared.Enum;
using REM_Shared.Helper;

namespace REM_Repository.Notification
{
    public interface INotificationRepo
    {
        Task SendAsync(SendNotificationDto sendNotificationDto);
        Task<CommonResponseDto<List<NotificationDto>>> GetAllAsync(int pageSize, int page, bool? isUnread);
        Task<GetNotificationsCountDto> GetCountsAsync();
        Task ReadAllAsync();
        Task ReadOneAsync(Guid id);
        Task ChangeLanguageAsync(LanguageEnum language);
        Task ChangeFcmTokenAsync(string fcmToken);
        Task<List<NotificationTypeData>> gettypes();
        Task SeedNotificationType();
    }
}
