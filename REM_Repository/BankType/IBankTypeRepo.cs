﻿using REM_Dto.BankType;
using REM_Dto.CommonDto;

namespace REM_Repository.BankType
{
    public interface IBankTypeRepo
    {
        Task<GetAllBankTypeDto> Action(ActionBankTypeDto dto);
        Task Remove(Guid id);
        Task<CommonResponseDto<List<GetAllBankTypeDto>>> GetAll(string search);
        Task<GetAllBankTypeDto> GetById(Guid id);
    }
}
