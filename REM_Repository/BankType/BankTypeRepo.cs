﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using REM_Database.Context;
using REM_Database.Model.Master;
using REM_Dto.BankType;
using REM_Dto.CommonDto;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Exception;
using REM_Shared.Helper;

namespace REM_Repository.BankType
{
    public class BankTypeRepo : BaseRepo, IBankTypeRepo
    {
        public BankTypeRepo(
            REMContext context,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor) : base(context, configuration, httpContextAccessor)
        {
        }

        public async Task<GetAllBankTypeDto> Action(ActionBankTypeDto dto)
        {
            var bank = await context.BankType.Where(m => m.Id == dto.Id).FirstOrDefaultAsync();

            var isadded = false;
            if (bank == null)
            {
                isadded = true;
                var NextIncrementNumber = await GetNextIncrementNumber<BankTypeModel>(m => m.IncrementNumber);

                bank = new BankTypeModel();
                bank.Id = Guid.NewGuid();
                bank.DocCode = StringHelper.CreateDocumentCode(DocumentNumber.BT, NextIncrementNumber);
                bank.IncrementNumber = NextIncrementNumber;
            }
            bank.Title = dto.Title;
            bank.TitleEn = dto.TitleEn;

            if (isadded)
                await context.AddAsync(bank);

            await context.SaveChangesAsync();

            return (await GetById(bank.Id));
        }

        public async Task<CommonResponseDto<List<GetAllBankTypeDto>>> GetAll(string search)
        {
            var data = await context.BankType
                                    .Where(m => string.IsNullOrEmpty(search) ||
                                               m.TitleEn.Contains(search) ||
                                               m.Title.Contains(search)
                                           )
                                    .Select(m => new GetAllBankTypeDto
                                    {
                                        Id = m.Id,
                                        DocCode = m.DocCode,
                                        Title = m.Title,
                                        TitleEn = m.TitleEn
                                    }).ToListAsync();
            return new(data);
        }

        public async Task<GetAllBankTypeDto> GetById(Guid id)
        {
            var data = await context.BankType.Where(m => m.Id == id).Select(m => new GetAllBankTypeDto
            {
                Id = m.Id,
                DocCode = m.DocCode,
                Title = m.Title,
                TitleEn = m.TitleEn
            }).FirstOrDefaultAsync();
            return data;
        }

        public async Task Remove(Guid id)
        {

            if (await context.TenancyContractPaymentPlan.AnyAsync(m => m.BankTypeId == id))
            {
                throw new NotFoundException(ErrorMessages.BankTypeHasRelatedData);
            }
            var x = await context.BankType.Where(c => c.Id == id).ExecuteDeleteAsync();
            if (x == 0)
                throw new NotFoundException(ErrorMessages.BankTypeNotFound);
        }
    }
}
