﻿using REM_Dto.CommonDto;
using REM_Dto.Location;
using REM_Shared.Enum.Sort;

namespace REM_Repository.Location;

public interface ILocationRepo
{
    Task<CountryDto> AddCountryAsync(CountryFormDto countryFormDto);
    Task UpdateCountryAsync(CountryFormDto countryFormDto);
    Task RemoveCountryAsync(Guid countryId);
    Task<CommonResponseDto<List<CountryDto>>> GetAllCountryAsync(string search, int pageSize, int pageNum, CountrySort countrySort, bool isAsc, bool? isAvailable);
    Task<CommonResponseDto<CountryDto>> GetCountryAsync(Guid id);

    Task<CityDto> AddCityAsync(CityFormDto cityFormDto);
    Task UpdateCityAsync(CityFormDto cityFormDto);
    Task RemoveCityAsync(Guid cityId);
    Task<CommonResponseDto<List<CityDto>>> GetAllCityAsync(Guid? countryId, string search, int pageSize, int pageNum, CitySort countrySort, bool isAsc, bool? isAvailable);
    Task<CommonResponseDto<CityDto>> GetCityAsync(Guid id);

    Task<RegionDto> AddRegionAsync(RegionFormDto regionFormDto);
    Task UpdateRegionAsync(RegionFormDto regionFormDto);
    Task RemoveRegionAsync(Guid regionId);
    Task<CommonResponseDto<List<RegionDto>>> GetAllRegionAsync(Guid? cityId, string search, int pageSize, int pageNum, RegionSort regionSort, bool isAsc);
    Task<CommonResponseDto<RegionDto>> GetRegionAsync(Guid id);
}