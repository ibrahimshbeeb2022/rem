﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using REM_Database.Context;
using REM_Database.Model.Location;
using REM_Dto.CommonDto;
using REM_Dto.Currency;
using REM_Dto.Location;
using REM_Repository.Base;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;
using REM_Shared.Exception;
using REM_Shared.Helper;
using REM_Shared.Resource;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace REM_Repository.Location;

public class LocationRepo : BaseRepo, ILocationRepo
{
    private readonly IStringLocalizer<LocationResource> locationLocalizer;
    public LocationRepo(REMContext context,
        IHttpContextAccessor httpContextAccessor,
        IConfiguration configuration,
        IStringLocalizer<LocationResource> locationLocalizer)
        : base(context, configuration, httpContextAccessor)
    {
        this.locationLocalizer = locationLocalizer;
    }

    #region Country
    public async Task<CountryDto> AddCountryAsync(CountryFormDto countryFormDto)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<CountryModel>(m => m.IncrementNumber);
        var countryModel = await context.Country.AddAsync(new CountryModel
        {
            Name = countryFormDto.Name,
            NameEn = countryFormDto.NameEn,
            IncrementNumber = NextIncrementNumber,
            CurrencyId = countryFormDto.CurrencyId,
            CountryCode = countryFormDto.CountryCode,
            IsAvailable = countryFormDto.IsAvailable,
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.C, NextIncrementNumber),
        });
        await context.SaveChangesAsync();
        return (await GetCountryAsync(countryModel.Entity.Id)).Data;
    }

    public async Task UpdateCountryAsync(CountryFormDto countryFormDto)
    {
        var x = await context.Country.Where(c => c.Id == countryFormDto.Id)
            .ExecuteUpdateAsync(c => c.SetProperty(c => c.IsAvailable, countryFormDto.IsAvailable)
            .SetProperty(c => c.Name, countryFormDto.Name)
            .SetProperty(c => c.NameEn, countryFormDto.NameEn)
            .SetProperty(c => c.CountryCode, countryFormDto.CountryCode)
            .SetProperty(c => c.CurrencyId, countryFormDto.CurrencyId));
        if (x == 0)
            throw new NotFoundException(locationLocalizer[ErrorMessages.CountryNotFound]);
    }

    public async Task RemoveCountryAsync(Guid countryId)
    {
        if (await context.Country.Where(c => c.Id == countryId && c.Cities.Any()).AnyAsync())
            throw new ValidationException(locationLocalizer[ErrorMessages.CountryUsed]);

        //if (await context.LandLord.AnyAsync(m => m.CountryId == countryId))
        //    throw new ValidationException(ErrorMessages.CountryHasLandLord);

        if (await context.Property.AnyAsync(m => m.CountryId == countryId))
            throw new ValidationException(ErrorMessages.PropertyHasContract);

        var x = await context.Country.Where(c => c.Id == countryId)
            .ExecuteUpdateAsync(c => c.SetProperty(c => c.IsValid, false));

        if (x == 0)
            throw new NotFoundException(locationLocalizer[ErrorMessages.CountryNotFound]);
    }

    public async Task<CommonResponseDto<List<CountryDto>>> GetAllCountryAsync(string search, int pageSize, int pageNum, CountrySort countrySort, bool isAsc, bool? isAvailable)
    {
        Expression<Func<CountryModel, bool>> expression = c => (string.IsNullOrEmpty(search) || c.Name.Contains(search) || c.NameEn.Contains(search)
        || c.DocCode.Contains(search) || c.CountryCode.Contains(search) || c.Currency.Name.Contains(search) || c.Currency.NameEn.Contains(search))
        && (!isAvailable.HasValue || c.IsAvailable == isAvailable);

        var countriesQuery = context.Country.Where(expression);

        switch (countrySort)
        {
            case CountrySort.CurrencyAr:
                if (isAsc)
                    countriesQuery = countriesQuery.OrderBy(c => c.Currency.Name);
                else
                    countriesQuery = countriesQuery.OrderByDescending(c => c.Currency.Name);
                break;
            case CountrySort.CurrencyEn:
                if (isAsc)
                    countriesQuery = countriesQuery.OrderBy(c => c.Currency.NameEn);
                else
                    countriesQuery = countriesQuery.OrderByDescending(c => c.Currency.NameEn);
                break;
            default:
                countriesQuery = CustomOrderBy(countriesQuery, countrySort, isAsc);
                break;
        }

        var countriesDto = await countriesQuery
            .Skip(pageNum * pageNum)
            .Take(pageSize)
            .Select(c => new CountryDto
            {
                Id = c.Id,
                Name = c.Name,
                DocCode = c.DocCode,
                NameEn = c.NameEn,
                IsAvailable = c.IsAvailable,
                CountryCode = c.CountryCode,
                Currency = new CurrencyDto
                {
                    Id = c.CurrencyId,
                    Name = c.Currency.Name,
                    DocCode = c.Currency.DocCode,
                    NameEn = c.Currency.NameEn,
                    Parity = c.Currency.Parity,
                    Symbol = c.Currency.Symbol,
                    IsActive = c.Currency.IsActive
                }
            }).ToListAsync();
        var totalRecords = await CountEntityExist(expression);
        return new(countriesDto, pageSize, pageNum, totalRecords);
    }
    public async Task<CommonResponseDto<CountryDto>> GetCountryAsync(Guid id)
    {
        var countryDto = (await context.Country.Where(c => c.Id == id)
            .Select(c => new CountryDto
            {
                Id = c.Id,
                Name = c.Name,
                DocCode = c.DocCode,
                NameEn = c.NameEn,
                IsAvailable = c.IsAvailable,
                CountryCode = c.CountryCode,
                Currency = new CurrencyDto
                {
                    Id = c.CurrencyId,
                    Name = c.Currency.Name,
                    DocCode = c.Currency.DocCode,
                    NameEn = c.Currency.NameEn,
                    Parity = c.Currency.Parity,
                    Symbol = c.Currency.Symbol,
                    IsActive = c.Currency.IsActive
                }
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(locationLocalizer[ErrorMessages.CountryNotFound]);
        return new(countryDto);
    }
    #endregion

    #region City
    public async Task<CityDto> AddCityAsync(CityFormDto cityFormDto)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<CityModel>(m => m.IncrementNumber);

        var x = await context.City.AddAsync(new CityModel
        {
            Name = cityFormDto.Name,
            NameEn = cityFormDto.NameEn,
            CountryId = cityFormDto.CountryId,
            IsAvailable = cityFormDto.IsAvailable,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.CY, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        return (await GetCityAsync(x.Entity.Id)).Data;
    }

    public async Task UpdateCityAsync(CityFormDto cityFormDto)
    {
        var x = await context.City.Where(c => c.Id == cityFormDto.Id)
            .ExecuteUpdateAsync(c => c.SetProperty(c => c.IsAvailable, cityFormDto.IsAvailable)
            .SetProperty(c => c.Name, cityFormDto.Name)
            .SetProperty(c => c.NameEn, cityFormDto.NameEn)
            .SetProperty(c => c.CountryId, cityFormDto.CountryId));
        if (x == 0)
            throw new NotFoundException(locationLocalizer[ErrorMessages.CityNotFound]);
    }

    public async Task RemoveCityAsync(Guid cityId)
    {
        if (await context.City.Where(c => c.Id == cityId && c.Regions.Any()).AnyAsync())
            throw new ValidationException(locationLocalizer[ErrorMessages.CityUsed]);

        var x = await context.City.Where(c => c.Id == cityId)
            .ExecuteUpdateAsync(c => c.SetProperty(c => c.IsValid, false));

        //if (await context.LandLord.AnyAsync(m => m.CityId == cityId))
        //    throw new ValidationException(ErrorMessages.CityHasLandLord);

        if (await context.Property.AnyAsync(m => m.CityId == cityId))
            throw new ValidationException(ErrorMessages.CityHasContract);

        if (x == 0)
            throw new NotFoundException(locationLocalizer[ErrorMessages.CityNotFound]);
    }
    public async Task<CommonResponseDto<List<CityDto>>> GetAllCityAsync(Guid? countryId, string search, int pageSize, int pageNum, CitySort citySort, bool isAsc, bool? isAvailable)
    {
        Expression<Func<CityModel, bool>> expression = c => (string.IsNullOrEmpty(search) || c.Name.Contains(search) || c.NameEn.Contains(search) || c.DocCode.Contains(search)
        || c.Country.Name.Contains(search) || c.Country.NameEn.Contains(search))
        && (!countryId.HasValue || c.CountryId == countryId)
        && (!isAvailable.HasValue || c.IsAvailable == isAvailable);

        var citiesQuery = context.City.Where(expression).AsQueryable();

        switch (citySort)
        {
            case CitySort.CountryAr:
                if (isAsc)
                    citiesQuery = citiesQuery.OrderBy(c => c.Country.Name);
                else
                    citiesQuery = citiesQuery.OrderByDescending(c => c.Country.Name);
                break;
            case CitySort.CountryEn:
                if (isAsc)
                    citiesQuery = citiesQuery.OrderBy(c => c.Country.NameEn);
                else
                    citiesQuery = citiesQuery.OrderByDescending(c => c.Country.NameEn);
                break;
            default:
                citiesQuery = CustomOrderBy(citiesQuery, citySort, isAsc);
                break;
        }

        var citiesDto = await citiesQuery
            .Skip(pageNum * pageNum)
            .Take(pageSize)
            .Select(c => new CityDto
            {
                Id = c.Id,
                Name = c.Name,
                DocCode = c.DocCode,
                NameEn = c.NameEn,
                IsAvailable = c.IsAvailable,
                Country = new CountryDto
                {
                    Id = c.CountryId,
                    Name = c.Country.Name,
                    DocCode = c.Country.DocCode,
                    NameEn = c.Country.NameEn,
                    IsAvailable = c.Country.IsAvailable,
                    CountryCode = c.Country.CountryCode,
                    Currency = new CurrencyDto
                    {
                        Id = c.Country.CurrencyId,
                        Name = c.Country.Currency.Name,
                        DocCode = c.Country.Currency.DocCode,
                        NameEn = c.Country.Currency.NameEn,
                        Parity = c.Country.Currency.Parity,
                        Symbol = c.Country.Currency.Symbol,
                        IsActive = c.Country.Currency.IsActive
                    }
                }
            }).ToListAsync();
        var totalRecords = await CountEntityExist(expression);
        return new(citiesDto, pageSize, pageNum, totalRecords);
    }
    public async Task<CommonResponseDto<CityDto>> GetCityAsync(Guid id)
    {
        var cityDto = (await context.City.Where(c => c.Id == id)
            .Select(c => new CityDto
            {
                Id = c.Id,
                Name = c.Name,
                DocCode = c.DocCode,
                NameEn = c.NameEn,
                IsAvailable = c.IsAvailable,
                Country = new CountryDto
                {
                    Id = c.CountryId,
                    Name = c.Country.Name,
                    DocCode = c.Country.DocCode,
                    NameEn = c.Country.NameEn,
                    IsAvailable = c.Country.IsAvailable,
                    CountryCode = c.Country.CountryCode,
                    Currency = new CurrencyDto
                    {
                        Id = c.Country.CurrencyId,
                        Name = c.Country.Currency.Name,
                        DocCode = c.Country.Currency.DocCode,
                        NameEn = c.Country.Currency.NameEn,
                        Parity = c.Country.Currency.Parity,
                        Symbol = c.Country.Currency.Symbol,
                        IsActive = c.Country.Currency.IsActive
                    }
                }
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(locationLocalizer[ErrorMessages.CityNotFound]);
        return new(cityDto);
    }
    #endregion

    #region Region
    public async Task<RegionDto> AddRegionAsync(RegionFormDto regionFormDto)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<RegionModel>(m => m.IncrementNumber);

        var x = await context.Region.AddAsync(new RegionModel
        {
            Name = regionFormDto.Name,
            NameEn = regionFormDto.NameEn,
            CityId = regionFormDto.CityId,
            StreetName = regionFormDto.StreetName,
            IncrementNumber = NextIncrementNumber,
            StreetNameEn = regionFormDto.StreetNameEn,
            DocCode = StringHelper.CreateDocumentCode(DocumentNumber.R, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        return (await GetRegionAsync(x.Entity.Id)).Data;
    }

    public async Task UpdateRegionAsync(RegionFormDto regionFormDto)
    {
        var x = await context.Region.Where(r => r.Id == regionFormDto.Id)
            .ExecuteUpdateAsync(r => r.SetProperty(r => r.StreetName, regionFormDto.StreetName)
            .SetProperty(r => r.Name, regionFormDto.Name)
            .SetProperty(r => r.NameEn, regionFormDto.NameEn)
            .SetProperty(r => r.StreetNameEn, regionFormDto.StreetNameEn)
            .SetProperty(r => r.CityId, regionFormDto.CityId));
        if (x == 0)
            throw new NotFoundException(locationLocalizer[ErrorMessages.RegionNotFound]);
    }

    public async Task RemoveRegionAsync(Guid regionId)
    {
        //if (await context.LandLord.AnyAsync(m => m.RegionId == regionId))
        //    throw new ValidationException(ErrorMessages.RegionHasLandLord);

        if (await context.Property.AnyAsync(m => m.RegionId == regionId))
            throw new ValidationException(ErrorMessages.RegionHasContract);

        var x = await context.Region.Where(r => r.Id == regionId).ExecuteUpdateAsync(r => r.SetProperty(r => r.IsValid, false));
        if (x == 0)
            throw new NotFoundException(locationLocalizer[ErrorMessages.RegionNotFound]);
    }
    public async Task<CommonResponseDto<List<RegionDto>>> GetAllRegionAsync(Guid? cityId, string search, int pageSize, int pageNum, RegionSort regionSort, bool isAsc)
    {
        Expression<Func<RegionModel, bool>> expression = r => (string.IsNullOrEmpty(search) || r.Name.Contains(search) || r.NameEn.Contains(search)
        || r.DocCode.Contains(search) || r.StreetNameEn.Contains(search) || r.StreetName.Contains(search) || r.City.Name.Contains(search) || r.City.NameEn.Contains(search))
        && (!cityId.HasValue || r.CityId == cityId);

        var regionsQuery = context.Region.Where(expression);
        switch (regionSort)
        {
            case RegionSort.CityEn:
                if (isAsc)
                    regionsQuery = regionsQuery.OrderBy(r => r.City.NameEn);
                else
                    regionsQuery = regionsQuery.OrderByDescending(r => r.City.NameEn);
                break;
            case RegionSort.CityAr:
                if (isAsc)
                    regionsQuery = regionsQuery.OrderBy(r => r.City.Name);
                else
                    regionsQuery = regionsQuery.OrderByDescending(r => r.City.Name);
                break;
            default:
                regionsQuery = CustomOrderBy(regionsQuery, regionSort, isAsc);
                break;
        }

        var regionsDto = await regionsQuery
            .Skip(pageNum * pageNum)
            .Take(pageSize)
            .Select(r => new RegionDto
            {
                Id = r.Id,
                Name = r.Name,
                DocCode = r.DocCode,
                NameEn = r.NameEn,
                StreetName = r.StreetName,
                StreetNameEn = r.StreetNameEn,
                City = new CityDto
                {
                    Id = r.CityId,
                    Name = r.City.Name,
                    DocCode = r.City.DocCode,
                    NameEn = r.City.NameEn,
                    IsAvailable = r.City.IsAvailable,
                    Country = new CountryDto
                    {
                        Id = r.City.CountryId,
                        Name = r.City.Country.Name,
                        DocCode = r.City.Country.DocCode,
                        NameEn = r.City.Country.NameEn,
                        CountryCode = r.City.Country.CountryCode,
                        Currency = new CurrencyDto
                        {
                            Id = r.City.Country.CurrencyId,
                            Name = r.City.Country.Currency.Name,
                            DocCode = r.City.Country.Currency.DocCode,
                            NameEn = r.City.Country.Currency.NameEn,
                            Parity = r.City.Country.Currency.Parity,
                            Symbol = r.City.Country.Currency.Symbol,
                            IsActive = r.City.Country.Currency.IsActive
                        }
                    }
                }
            }).ToListAsync();
        var totalRecords = await CountEntityExist(expression);
        return new(regionsDto, pageSize, pageNum, totalRecords);
    }
    public async Task<CommonResponseDto<RegionDto>> GetRegionAsync(Guid id)
    {
        var regionDto = (await context.Region.Where(r => r.Id == id)
            .Select(r => new RegionDto
            {
                Id = r.Id,
                Name = r.Name,
                DocCode = r.DocCode,
                NameEn = r.NameEn,
                StreetName = r.StreetName,
                StreetNameEn = r.StreetNameEn,
                City = new CityDto
                {
                    Id = r.CityId,
                    Name = r.City.Name,
                    DocCode = r.City.DocCode,
                    NameEn = r.City.NameEn,
                    IsAvailable = r.City.IsAvailable,
                    Country = new CountryDto
                    {
                        Id = r.City.CountryId,
                        Name = r.City.Country.Name,
                        DocCode = r.City.Country.DocCode,
                        NameEn = r.City.Country.NameEn,
                        IsAvailable = r.City.Country.IsAvailable,
                        CountryCode = r.City.Country.CountryCode,
                        Currency = new CurrencyDto
                        {
                            Id = r.City.Country.CurrencyId,
                            Name = r.City.Country.Currency.Name,
                            DocCode = r.City.Country.Currency.DocCode,
                            NameEn = r.City.Country.Currency.NameEn,
                            Parity = r.City.Country.Currency.Parity,
                            Symbol = r.City.Country.Currency.Symbol,
                            IsActive = r.City.Country.Currency.IsActive
                        }
                    }
                }
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(locationLocalizer[ErrorMessages.RegionNotFound]);
        return new(regionDto);
    }
    #endregion
}