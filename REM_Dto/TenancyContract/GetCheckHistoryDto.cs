﻿using REM_Shared.Enum;

namespace REM_Dto.TenancyContract
{
    public class GetCheckHistoryDto
    {
        public Guid Id { get; set; }
        public string Notes { get; set; }
        public CheckStatus CheckStatus { get; set; }
        public DateTime? PayDate { get; set; }
        public DateTime CreateDate { get; set; }
        public bool? LandLordApproved { get; set; }
    }
}
