﻿using REM_Dto.Base;
using REM_Dto.LandLord;
using REM_Dto.Tenant;
using REM_Dto.Unit;
using REM_Shared.Enum;

namespace REM_Dto.TenancyContract;

public class GetTenancyContractDto
{
    public Guid Id { get; set; }
    public string DocCode { get; set; }
    public string LandLordTenancyContractCode { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public double Value { get; set; }
    public double SubValue { get; set; }
    public double VatValue { get; set; }
    public string TenancyReason { get; set; }
    public string TenancyNumber { get; set; }
    public TenancyContractStatus Status { get; set; }
    public DateTime CreateDate { get; set; }
    public GetLandLordSummaryDto LandLord { get; set; }
    public GetTenantSummaryDto Tenant { get; set; }
    public GetBaseNameWithCode Property { get; set; }
    public GetAllUnitSummaryDto Unit { get; set; }
    public List<TenancyContractDetailsDto> Details { get; set; } = [];
    public List<TenancyContractPaymentPlanDto> PaymentPlan { get; set; } = [];
    public List<GetBaseFileDto> Files { get; set; } = [];
}