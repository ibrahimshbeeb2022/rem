﻿using REM_Dto.BankType;
using REM_Dto.Base;
using REM_Shared.Enum;

namespace REM_Dto.TenancyContract
{
    public class TenancyContractPaymentPlanDto
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public DateTime PayDate { get; set; }
        public GetAllBankTypeDto Bank { get; set; }
        public Guid? BankId { get; set; }
        public string Description { get; set; }
        public double Value { get; set; }
        public TenancyContractPaymentType Type { get; set; }
        public List<Guid> FilesId { get; set; } = [];
        public List<GetBaseFileDto> Files { get; set; } = [];
    }
}
