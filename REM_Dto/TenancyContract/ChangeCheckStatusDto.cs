﻿using REM_Shared.Enum;

namespace REM_Dto.TenancyContract
{
    public class ChangeCheckStatusDto
    {
        public Guid Id { get; set; }
        public string Notes { get; set; }
        public CheckStatus Status { get; set; }
        public bool? LandLordApproved { get; set; }
        public DateTime? NewPayDate { get; set; }
        public List<Guid> FileIds { get; set; } = [];
    }
}
