﻿using REM_Shared.Enum;

namespace REM_Dto.TenancyContract
{
    public class ChangeTenancyContractStatusDto
    {
        public Guid Id { get; set; }
        public TenancyContractStatus Status { get; set; }
        public double Fine { get; set; }
    }
}
