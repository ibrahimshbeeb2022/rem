﻿using REM_Shared.Enum;

namespace REM_Dto.TenancyContract
{
    public class GetAllChecksSummaryDto
    {
        public Guid Id { get; set; }
        public string DocCode { get; set; }
        public string Number { get; set; }
        public double Value { get; set; }
        public double CollectedValue { get; set; }
        public double Balance { get; set; }
        public TenancyContractPaymentType Type { get; set; }
        public CheckStatus LastCheckStatus { get; set; }
    }
}
