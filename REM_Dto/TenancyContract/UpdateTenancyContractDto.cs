﻿namespace REM_Dto.TenancyContract
{
    public class UpdateTenancyContractDto : AddTenancyContractDto
    {
        public Guid Id { get; set; }
    }
}
