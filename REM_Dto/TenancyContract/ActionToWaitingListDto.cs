﻿using REM_Shared.Enum;

namespace REM_Dto.TenancyContract
{
    public class ActionToWaitingListDto
    {
        public Guid Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Value { get; set; }
        public TenancyContractStatus Status { get; set; }
        public string TenancyReason { get; set; }
        public string TenancyNumber { get; set; }
        public Guid LandLordId { get; set; }
        public Guid TenantId { get; set; }
        public Guid UnitId { get; set; }
        public Guid PropertyId { get; set; }
        public List<Guid> FilesIds { get; set; } = [];
        public List<Guid> DeletedFilesIds { get; set; } = [];
        public List<TenancyContractDetailsDto> Details { get; set; } = [];
        public List<TenancyContractPaymentPlanDto> PaymentPlan { get; set; } = [];
    }
}
