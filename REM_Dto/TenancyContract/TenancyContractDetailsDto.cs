﻿using REM_Shared.Enum;

namespace REM_Dto.TenancyContract
{
    public class TenancyContractDetailsDto
    {
        public double Value { get; set; }
        public double Tax { get; set; }
        public double Total { get; set; }
        public TenancyContractOperationType Type { get; set; }
    }
}
