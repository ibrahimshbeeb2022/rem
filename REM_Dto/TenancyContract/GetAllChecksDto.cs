﻿using REM_Dto.BankType;
using REM_Dto.Base;
using REM_Dto.LandLord;
using REM_Dto.Tenant;
using REM_Dto.Unit;
using REM_Shared.Enum;

namespace REM_Dto.TenancyContract;

public class GetAllChecksDto
{
    public Guid Id { get; set; }
    public string DocCode { get; set; }
    public string Number { get; set; }
    public DateTime PayDate { get; set; }
    public GetAllBankTypeDto Bank { get; set; }
    public string Description { get; set; }
    public double Value { get; set; }
    public double CollectedValue { get; set; }
    public double Balance { get; set; }
    public string LastNotes { get; set; }
    public CheckStatus LastCheckStatus { get; set; }
    public string TenantContractDocCode { get; set; }
    public Guid TenantContractId { get; set; }
    public DateTime CreateDate { get; set; }
    public GetLandLordSummaryDto LandLord { get; set; }
    public GetTenantSummaryDto Tenant { get; set; }
    public GetBaseNameWithCode Property { get; set; }
    public GetAllUnitSummaryDto Unit { get; set; }
    public List<GetBaseFileDto> Files { get; set; }
    public List<GetCheckHistoryDto> History { get; set; } = [];
}