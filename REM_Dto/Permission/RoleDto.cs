﻿namespace REM_Dto.Permission;

public class RoleDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public List<GetAllPermissionsDto> Permissions { get; set; }
}