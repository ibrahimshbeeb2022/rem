﻿using REM_Shared.Enum;

namespace REM_Dto.Permission;

public class PermissionDto
{
    public string Name { get; set; }
    public List<PermissionEnum> Permissions { get; set; } = [];
}