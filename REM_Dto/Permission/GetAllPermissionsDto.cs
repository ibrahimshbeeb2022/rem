﻿using REM_Shared.Enum;

namespace REM_Dto.Permission
{
    public class GetAllPermissionsDto
    {
        public Guid Id { get; set; }
        public PermissionEnum EnumValue { get; set; }
    }
}
