﻿namespace REM_Dto.Permission;

public class TokenDto
{
    public TokenDto(string token, DateTime expireDate)
    {
        Token = token;
        ExpireDate = expireDate;
    }
    public string Token { get; set; }
    public DateTime ExpireDate { get; set; }
}