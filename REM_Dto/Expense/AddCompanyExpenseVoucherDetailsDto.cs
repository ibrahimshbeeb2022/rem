﻿using REM_Shared.Enum;

namespace REM_Dto.Expense
{
    public class AddCompanyExpenseVoucherDetailsDto
    {
        public Guid? LandLordId { get; set; }
        public Guid? TenantId { get; set; }
        public Guid? ExpenseId { get; set; }
        public Guid? UnitId { get; set; }
        public FinanceAccountType AccountType { get; set; }
        public string Remarks { get; set; }
        public bool WithVat { get; set; }
        public VatType VatType { get; set; }
        public double Ammount { get; set; }
        public double Vat { get; set; }
        public double Total { get; set; }
    }
}
