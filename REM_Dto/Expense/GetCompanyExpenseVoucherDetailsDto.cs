﻿using REM_Dto.LandLord;
using REM_Dto.Tenant;
using REM_Shared.Enum;

namespace REM_Dto.Expense
{
    public class GetCompanyExpenseVoucherDetailsDto
    {
        public FinanceAccountType AccountType { get; set; }
        public string Remarks { get; set; }
        public bool WithVat { get; set; }
        public VatType VatType { get; set; }
        public double SubTotal { get; set; }
        public double Vat { get; set; }
        public double Total { get; set; }
        public Guid? LandLordId { get; set; }
        public string LandLordName { get; set; }
        public string LandLordDocCode { get; set; }
        public Guid? UnitId { get; set; }
        public string UNitNumber { get; set; }
        public string UnitDocCode { get; set; }
        public string UnitName { get; set; }
        public Guid? PropertyId { get; set; }
        public string PropertyDocCode { get; set; }
        public string PropertyName { get; set; }
        public GetLandLordSummaryDto landLord { get; set; }
        public GetTenantSummaryDto Tenant { get; set; }
        public GetAllExpenseDto Expense { get; set; }

    }
}
