﻿namespace REM_Dto.Expense
{
    public class GetCompanyExpenseVoucherDto
    {
        public Guid Id { get; set; }
        public string DocCode { get; set; }
        public double SubTotal { get; set; }
        public double Vat { get; set; }
        public double Total { get; set; }
        public string Remarks { get; set; }
        public DateTime Date { get; set; }
        public string FileUrl { get; set; }
        public List<GetCompanyExpenseVoucherDetailsDto> Details { get; set; }
    }
}
