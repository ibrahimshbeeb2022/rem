﻿namespace REM_Dto.Expense
{
    public class GetAllExpenseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string EnName { get; set; }
        public string DocCode { get; set; }
    }
}
