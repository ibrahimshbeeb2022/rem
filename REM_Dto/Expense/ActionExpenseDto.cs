﻿namespace REM_Dto.Expense
{
    public class ActionExpenseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string EnName { get; set; }
    }
}
