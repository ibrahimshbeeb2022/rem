﻿namespace REM_Dto.Expense
{
    public class AddCompanyExpenseVoucherDto
    {
        public string Remarks { get; set; }
        public DateTime Date { get; set; }
        public Guid? FileId { get; set; }
        public List<AddCompanyExpenseVoucherDetailsDto> Details { get; set; } = new List<AddCompanyExpenseVoucherDetailsDto>();
    }
}
