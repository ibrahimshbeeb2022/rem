﻿using REM_Shared.Enum;

namespace REM_Dto.Settings;

public class SettingsLawsFormDto
{
    public Guid Id { get; set; }
    public SettingsEnum Key { get; set; }
    public string Value { get; set; }

    public List<Guid> FileIds { get; set; } = [];
    public List<Guid> RemoveFileIds { get; set; } = [];
}