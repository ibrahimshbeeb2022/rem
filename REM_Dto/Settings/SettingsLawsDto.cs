﻿using REM_Dto.Base;
using REM_Shared.Enum;

namespace REM_Dto.Settings;

public class SettingsLawsDto
{
    public Guid Id { get; set; }
    public SettingsEnum Key { get; set; }
    public string Value { get; set; }
    public string FileInBase64 { get; set; }
    public List<GetBaseFileDto> Files { get; set; } = [];
}