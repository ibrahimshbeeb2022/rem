﻿namespace REM_Dto.Location;

public class CityDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public string NameEn { get; set; }
    public bool IsAvailable { get; set; }
    public CountryDto Country { get; set; }
}