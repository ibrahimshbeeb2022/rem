﻿namespace REM_Dto.Location;

public class CityFormDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string NameEn { get; set; }
    public Guid CountryId { get; set; }
    public bool IsAvailable { get; set; }
}