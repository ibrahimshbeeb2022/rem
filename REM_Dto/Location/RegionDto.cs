﻿namespace REM_Dto.Location;

public class RegionDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public string NameEn { get; set; }
    public string StreetName { get; set; }
    public string StreetNameEn { get; set; }
    public CityDto City { get; set; }
}