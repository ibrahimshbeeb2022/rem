﻿using REM_Dto.Currency;

namespace REM_Dto.Location;

public class CountryDto
{
    public Guid? Id { get; set; }
    public string Name { get; set; }
    public string NameEn { get; set; }
    public bool IsAvailable { get; set; }
    public string CountryCode { get; set; }
    public string DocCode { get; set; }
    public CurrencyDto Currency { get; set; }
}