﻿namespace REM_Dto.Location;

public class RegionFormDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string NameEn { get; set; }
    public Guid CityId { get; set; }
    public string StreetName { get; set; }
    public string StreetNameEn { get; set; }
}