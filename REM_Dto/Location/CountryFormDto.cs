﻿namespace REM_Dto.Location;

public class CountryFormDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string NameEn { get; set; }
    public string CountryCode { get; set; }
    public Guid CurrencyId { get; set; }
    public bool IsAvailable { get; set; }
}