﻿namespace REM_Dto.Employee
{
    public class ActionEmployeeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Guid> Permissions { get; set; }
    }
}
