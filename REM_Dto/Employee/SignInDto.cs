﻿namespace REM_Dto.Employee;

public class SignInDto
{
    public string Email { get; set; }
    public string Password { get; set; }
}