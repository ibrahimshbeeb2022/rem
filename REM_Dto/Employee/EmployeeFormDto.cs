﻿namespace REM_Dto.Employee;

public class EmployeeFormDto
{
    public Guid Id { get; set; }
    public string FullName { get; set; }
    public string Email { get; set; }
    public bool IsActive { get; set; }
    public string Password { get; set; }
    public string PhoneNumber { get; set; }
    public List<Guid> RoleIds { get; set; } = [];
}