﻿using REM_Dto.Permission;
using REM_Shared.Enum;

namespace REM_Dto.Employee;

public class SignInEmployeeDto
{
    public Guid Id { get; set; }
    public string FullName { get; set; }
    public string Email { get; set; }
    public string DocCode { get; set; }
    public string PhoneNumber { get; set; }
    public UserType UserType { get; set; }
    public TokenDto Token { get; set; }
    public List<PermissionEnum> Permissions { get; set; } = [];
}