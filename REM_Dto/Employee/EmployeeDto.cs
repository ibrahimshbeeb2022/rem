﻿using REM_Dto.Permission;

namespace REM_Dto.Employee;

public class EmployeeDto
{
    public Guid Id { get; set; }
    public string FullName { get; set; }
    public string DocCode { get; set; }
    public string Email { get; set; }
    public bool IsActive { get; set; }
    public string PhoneNumber { get; set; }
    public DateTime CreateDate { get; set; }
    public List<RoleDto> Roles { get; set; } = [];
}