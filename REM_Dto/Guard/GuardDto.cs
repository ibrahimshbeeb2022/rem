﻿using REM_Dto.Base;

namespace REM_Dto.Guard;

public class GuardDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public string NameEn { get; set; }
    //public string Profession { get; set; }
    //public string ProfessionEn { get; set; }
    //public DateTime BirthDay { get; set; }
    public string IdNumber { get; set; }
    public DateTime IdExpireDate { get; set; }
    public string PassportNumber { get; set; }
    public DateTime PassportExpireDate { get; set; }
    public List<GetBaseFileDto> Files { get; set; } = [];
}