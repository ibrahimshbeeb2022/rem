﻿namespace REM_Dto.Guard;

public class UpdateGuardFormDto : AddGuardFormDto
{
    public Guid RemoveIdFileId { get; set; }
    public Guid RemoveOtherFileId { get; set; }
    public Guid RemovePassportFileId { get; set; }
    public Guid RemoveResidenceFileId { get; set; }
}