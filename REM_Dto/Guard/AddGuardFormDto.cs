﻿namespace REM_Dto.Guard;

public class AddGuardFormDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string NameEn { get; set; }
    //public string Profession { get; set; }
    //public string ProfessionEn { get; set; }
    //public DateTime BirthDay { get; set; }
    public string IdNumber { get; set; }
    public DateTime IdExpireDate { get; set; }
    public string PassportNumber { get; set; }
    public DateTime PassportExpireDate { get; set; }
    public Guid IdFileId { get; set; }
    public Guid PassportFileId { get; set; }
    public Guid ResidenceFileId { get; set; }
    public Guid OtherFileId { get; set; }
}