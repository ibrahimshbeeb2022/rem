﻿using REM_Shared.Enum;

namespace REM_Dto.Unit;

public class GetAllUnitSummaryDto
{
    public Guid Id { get; set; }
    public string DocCode { get; set; }
    public string UnitName { get; set; }
    public string Number { get; set; }
    public double RentValue { get; set; }
    public string FloorNumber { get; set; }
    public string Description { get; set; }
    public Guid PropertyId { get; set; }
    public TenancyContractType TenancyContractType { get; set; }
    public UnitTypeDto UnitType { get; set; }
}