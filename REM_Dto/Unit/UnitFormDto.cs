﻿using REM_Shared.Enum;

namespace REM_Dto.Unit;

public class UnitFormDto
{
    public Guid Id { get; set; }
    public string UnitNumber { get; set; }
    public string UnitName { get; set; }
    public string UnitSpace { get; set; }
    public string Description { get; set; }
    public string SewerAccountNumber { get; set; }
    public string ElectricityAndWaterAccountNumber { get; set; }
    public double RentValue { get; set; }
    public bool HasParking { get; set; }
    public string ParkingNumber { get; set; }
    public string FloorNumber { get; set; }
    public double ParkingRentValue { get; set; }
    public Guid UnitTypeId { get; set; }
    public Guid PropertyId { get; set; }
    public TenancyContractType TenancyContractType { get; set; }
}