﻿namespace REM_Dto.Unit;

public class UnitTypeDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public string NameEn { get; set; }
    public bool IsActive { get; set; }
}