﻿namespace REM_Dto.LandLord;

public class UpdateLandLordDto : AddLandLordDto
{
    public Guid Id { get; set; }
    public List<Guid> RemoveFilesId { get; set; } = [];
}