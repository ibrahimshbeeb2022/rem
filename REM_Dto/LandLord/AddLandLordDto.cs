﻿namespace REM_Dto.LandLord;

public class AddLandLordDto
{
    public string FullName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string SecondPhone { get; set; }
    public string CardNumber { get; set; }
    public string Job { get; set; }
    public string Beneficiary { get; set; }
    public string IdentityCardNumber { get; set; }
    public DateTime IdentityCardExpiryDate { get; set; }
    public string PassportNumber { get; set; }
    public DateTime PassportExpiryDate { get; set; }
    public string TaxNumber { get; set; }
    //public DateTime BirthDate { get; set; }
    public string BankName { get; set; }
    public string BankAddress { get; set; }
    public string Address { get; set; }
    public string AccountName { get; set; }
    public string AccountNumber { get; set; }
    public string Iban { get; set; }
    public string SwiftCode { get; set; }
    public string Nationality { get; set; }
    public string Note { get; set; }
    public string Password { get; set; }
    //public Guid CountryId { get; set; }
    //public Guid CityId { get; set; }
    //public Guid RegionId { get; set; }
    public List<Guid> FilesId { get; set; } = [];
}