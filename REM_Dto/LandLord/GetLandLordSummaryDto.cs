﻿namespace REM_Dto.LandLord;

public class GetLandLordSummaryDto
{
    public Guid Id { get; set; }
    public Guid? AccountId { get; set; }
    public string DocCode { get; set; }
    public string FullName { get; set; }
    public string Email { get; set; }
    public string TaxNumber { get; set; }
    public string Address { get; set; }
    //public CountryDto Country { get; set; }
    //public GetBaseNameDto City { get; set; }
    //public GetBaseNameDto Region { get; set; }
}