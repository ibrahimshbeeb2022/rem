﻿using REM_Shared.Enum;

namespace REM_Dto.Finance
{
    public class GetStaticFinanceAccountsDto
    {
        public Guid Id { get; set; }
        public FinanceAccountType Type { get; set; }
    }
}
