﻿using REM_Shared.Enum;

namespace REM_Dto.Finance
{
    public class FinanceReportOperationsDto
    {
        public Guid Id { get; set; }
        public int TransActionId { get; set; }
        public DateTime TransActionDate { get; set; }
        public string Notes { get; set; }
        public double Value { get; set; }
        public double CurrentBalance { get; set; }
        public Guid? TenantId { get; set; }
        public string TenantName { get; set; }
        public string TenantDocCode { get; set; }
        public Guid? LandLordId { get; set; }
        public string LandLordName { get; set; }
        public string LandLordDocCode { get; set; }
        public Guid? UnitId { get; set; }
        public string UNitNumber { get; set; }
        public string UnitDocCode { get; set; }
        public string UnitName { get; set; }
        public Guid? PropertyId { get; set; }
        public string PropertyDocCode { get; set; }
        public string PropertyName { get; set; }
        public FinanceOperationType Type { get; set; }
        public FinanceAccountType AccountType { get; set; }
    }
}
