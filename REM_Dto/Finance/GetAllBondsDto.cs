﻿using REM_Shared.Enum;

namespace REM_Dto.Finance
{
    public class GetAllBondsDto
    {
        public Guid Id { get; set; }
        public string DocCode { get; set; }
        public BondType Type { get; set; }
        public VoucherBondTypes VoucherBondType { get; set; }
        public FinanceAccountType Account { get; set; }
        public double Value { get; set; }
        public bool ForLandLord { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
        public Guid? TenantId { get; set; }
        public string TenantName { get; set; }
        public string TenantDocCode { get; set; }
        public Guid? LandLordId { get; set; }
        public string LandLordName { get; set; }
        public string LandLordDocCode { get; set; }
        public Guid? UnitId { get; set; }
        public string UNitNumber { get; set; }
        public string UnitDocCode { get; set; }
        public string UnitName { get; set; }
        public Guid? PropertyId { get; set; }
        public string PropertyDocCode { get; set; }
        public string PropertyName { get; set; }
        public Guid? CheckId { get; set; }
        public string CheckDocCode { get; set; }
        public string CheckNumber { get; set; }
        public DateTime? CheckDate { get; set; }
        public string FileUrl { get; set; }
        public FinanceReportDto Ledger { get; set; }
    }
}
