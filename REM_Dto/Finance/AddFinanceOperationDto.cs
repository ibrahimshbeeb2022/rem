﻿using REM_Shared.Enum;

namespace REM_Dto.Finance
{
    public class AddFinanceOperationDto
    {
        public int? TransActionIncrement { get; set; }
        public string Notes1 { get; set; }
        public string Notes2 { get; set; }
        public double Value { get; set; }
        public FinanceOperationType Type { get; set; }
        public FinanceAccountType AccountType { get; set; }
        public DateTime? PaymentDate { get; set; }
        public Guid? TenancyContractId { get; set; }
        public Guid? CheckId { get; set; }
        public Guid? AccountId { get; set; }
        public Guid? TenantId { get; set; }
        public Guid? LandLordId { get; set; }
        public Guid? BondId { get; set; }
        public Guid? CompanyExpenseVoucherId { get; set; }
    }
}
