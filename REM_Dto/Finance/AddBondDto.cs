﻿using REM_Shared.Enum;

namespace REM_Dto.Finance
{
    public class AddBondDto
    {
        public BondType Type { get; set; }
        public VoucherBondTypes VoucherBondType { get; set; }
        public FinanceAccountType Account { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
        public Guid? TenantId { get; set; }
        public Guid? LandlordId { get; set; }
        public Guid? UnitId { get; set; }
        public Guid? PropertyId { get; set; }
        public Guid? CheckId { get; set; }
        public Guid? FileId { get; set; }
    }
}
