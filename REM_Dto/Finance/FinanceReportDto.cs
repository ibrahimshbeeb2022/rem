﻿namespace REM_Dto.Finance
{
    public class FinanceReportDto
    {
        public double TotalCredit { get; set; }
        public double TotalDebit { get; set; }
        public double Total { get; set; }
        public double PreviousBalance { get; set; }
        public List<FinanceReportOperationsDto> Operations { get; set; }
    }
}
