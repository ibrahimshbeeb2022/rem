﻿namespace REM_Dto.BankType
{
    public class GetAllBankTypeDto
    {
        public Guid Id { get; set; }
        public string TitleEn { get; set; }
        public string Title { get; set; }
        public string DocCode { get; set; }
    }
}
