﻿namespace REM_Dto.BankType
{
    public class ActionBankTypeDto
    {
        public Guid Id { get; set; }
        public string TitleEn { get; set; }
        public string Title { get; set; }
    }
}
