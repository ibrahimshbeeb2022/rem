﻿using REM_Shared.Enum;

namespace REM_Dto.PropertyType;

public class PropertyTypeFormDto
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string TitleEn { get; set; }
    public PropertyCategory Category { get; set; }
}