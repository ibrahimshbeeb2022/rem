﻿using REM_Dto.Base;

namespace REM_Dto.Tenant;

public class GetTenantDto
{
    public Guid Id { get; set; }
    public string DocCode { get; set; }
    public string FullName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    //public string Fax { get; set; }
    public string CardNumber { get; set; }
    public string Job { get; set; }
    public string Beneficiary { get; set; }
    public string IdentityCardNumber { get; set; }
    public DateTime IdentityCardExpiryDate { get; set; }
    public string PassportNumber { get; set; }
    public DateTime PassportExpiryDate { get; set; }
    public string Nationality { get; set; }
    public string Address { get; set; }
    public string BoxMail { get; set; }
    public int NumberOfResidentsOfTheRentedProperty { get; set; }
    public List<GetBaseFileDto> Files { get; set; } = [];
}