﻿namespace REM_Dto.Tenant
{
    public class GetTenantSummaryDto
    {
        public Guid Id { get; set; }
        public Guid? AccountId { get; set; }
        public string DocCode { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Nationality { get; set; }
    }
}
