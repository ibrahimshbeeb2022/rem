﻿namespace REM_Dto.Tenant
{
    public class UpdateTenantDto : AddTenantDto
    {
        public Guid Id { get; set; }
        public List<Guid> DeletedFiles { get; set; } = [];
    }
}