﻿namespace REM_Dto.Currency;

public class CurrencyFormDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string NameEn { get; set; }
    public bool IsActive { get; set; }
    public string Symbol { get; set; }
    public double Parity { get; set; }
}