﻿namespace REM_Dto.Currency;

public class CurrencyDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public string NameEn { get; set; }
    public string Symbol { get; set; }
    public double Parity { get; set; }
    public bool IsActive { get; set; }
}