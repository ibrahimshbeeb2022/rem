﻿using REM_Shared.Enum;

namespace REM_Dto.Base;

public class GetBaseFileDto
{
    public Guid Id { get; set; }
    public string Url { get; set; }
    public string Name { get; set; }
    public FileType FileType { get; set; }
    public FileKind FileKind { get; set; }
}