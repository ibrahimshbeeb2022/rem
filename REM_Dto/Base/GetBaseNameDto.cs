﻿namespace REM_Dto.Base;

public class GetBaseNameDto
{
    public Guid? Id { get; set; }
    public string Name { get; set; }
    public string NameEn { get; set; }
}