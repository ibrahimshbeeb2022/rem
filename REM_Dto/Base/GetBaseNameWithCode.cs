﻿namespace REM_Dto.Base
{
    public class GetBaseNameWithCode
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string DocCode { get; set; }
    }
}
