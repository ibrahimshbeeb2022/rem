﻿namespace REM_Dto.Report
{
    public class FinalReportDto
    {
        public Guid Id { get; set; }
        public string UnitNumber { get; set; }
        public string TenantName { get; set; }
        public string TenancyContractNumber { get; set; }
        public int PaymentWay { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double RentValue { get; set; }
        public double OtherValue { get; set; }
        public double InsuranceValue { get; set; }
        public double VatValue { get; set; }
        public double CollectedValue { get; set; }
        public double RemainValue { get; set; }
    }
}
