﻿namespace REM_Dto.Report
{
    public class CompanyExpenseHomeRoprtDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string EnName { get; set; }
        public int Count { get; set; }
        public double Total { get; set; }
    }
}
