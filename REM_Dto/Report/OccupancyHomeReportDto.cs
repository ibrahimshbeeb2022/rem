﻿using REM_Shared.Enum;

namespace REM_Dto.Report
{
    public class OccupancyHomeReportDto
    {
        public TenancyContractStatus Status { get; set; }
        public int Count { get; set; }
        public double Total { get; set; }
    }
}
