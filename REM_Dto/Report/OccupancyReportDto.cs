﻿using REM_Shared.Enum;

namespace REM_Dto.Report
{
    public class OccupancyReportDto
    {
        public Guid LandLordId { get; set; }
        public string LandLord { get; set; }
        public Guid UnitId { get; set; }
        public Guid PropertyId { get; set; }
        public string UnitNumber { get; set; }
        public string UnitName { get; set; }
        public string UnitType { get; set; }
        public Guid? TenantId { get; set; }
        public string TenantName { get; set; }
        public string TenantPhone { get; set; }
        public string TenantEmail { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public double CurrentRent { get; set; }
        public TenancyContractStatus Status { get; set; }
        public DateTime VacantSince { get; set; }
    }
}
