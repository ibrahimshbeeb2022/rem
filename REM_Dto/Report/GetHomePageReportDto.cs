﻿namespace REM_Dto.Report
{
    public class GetHomePageReportDto
    {
        public int EmployeesCount { get; set; }
        public int LandLordsCount { get; set; }
        public int PropertiesCount { get; set; }
        public int UnitsCount { get; set; }
        public int AvailableUnitsCount { get; set; }
        public int RentedUnitsCount { get; set; }
        public int TenancyContactsCount { get; set; }
        public List<CompanyExpenseHomeRoprtDto> CompanyExpenseReport { get; set; } = new List<CompanyExpenseHomeRoprtDto>();
        public List<OccupancyHomeReportDto> OccupancyReport { get; set; } = new List<OccupancyHomeReportDto>();
        public List<ChecksHomePageReportDto> ChecksReport { get; set; } = new List<ChecksHomePageReportDto>();
    }
}
