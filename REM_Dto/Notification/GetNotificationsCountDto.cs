﻿namespace REM_Dto.Notification
{
    public class GetNotificationsCountDto
    {
        public int UnReadCount { get; set; }
        public int ReadCount { get; set; }
        public int AllCount { get; set; }
    }
}
