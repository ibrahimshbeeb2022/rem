﻿using REM_Shared.Enum;

namespace REM_Dto.Notification
{
    public class SendNotificationDto
    {
        public Guid? SenderId { get; set; }
        public Guid? ObjectId { get; set; }
        public string ObjectCode { get; set; }
        public List<Guid> RecipientId { get; set; } = [];
        public NotificationType NotificationType { get; set; }
    }
}
