﻿using REM_Shared.Enum;

namespace REM_Dto.Notification
{
    public class NotificationDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string TitleEn { get; set; }
        public string Body { get; set; }
        public string BodyEn { get; set; }
        public bool IsRead { get; set; }
        public Guid? ObjectId { get; set; }
        public DateTime Date { get; set; }
        public string ObjectCode { get; set; }
        public NotificationType NotificationType { get; set; }
    }
}
