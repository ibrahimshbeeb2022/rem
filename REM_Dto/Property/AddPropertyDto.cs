﻿using REM_Shared.Enum;

namespace REM_Dto.Property;

public class AddPropertyDto
{
    public string Name { get; set; }
    public string PropertyNumber { get; set; }
    public string PieceNumber { get; set; }
    public string SupervisorName { get; set; }
    public string Description { get; set; }
    public PropertyOwnType PropertyOwnType { get; set; }
    public int ApartmentsCount { get; set; }
    public int FloorsCount { get; set; }
    public int ParkingsCount { get; set; }
    public int ParkingFloorsCount { get; set; }
    public int ResidentialFloorsCount { get; set; }
    public int StoresCount { get; set; }
    public string GovernmentNumber { get; set; }
    public string BondNumber { get; set; }
    public string BondType { get; set; }
    public DateTime BondDate { get; set; }
    public bool IsActive { get; set; }
    public Guid LandLordId { get; set; }
    public Guid PropertyTypeId { get; set; }
    public Guid? GuardId { get; set; }
    public Guid? CountryId { get; set; }
    public Guid? CityId { get; set; }
    public Guid? RegionId { get; set; }
    public List<Guid> FilesIds { get; set; } = [];
}