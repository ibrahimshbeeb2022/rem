﻿using REM_Dto.Base;
using REM_Dto.LandLord;
using REM_Dto.Location;
using REM_Dto.PropertyType;
using REM_Shared.Enum;

namespace REM_Dto.Property;

public class GetPropertyDto
{
    public Guid Id { get; set; }
    public string DocCode { get; set; }
    public string Name { get; set; }
    public string PropertyNumber { get; set; }
    public string PieceNumber { get; set; }
    public string SupervisorName { get; set; }
    public string Description { get; set; }
    public PropertyOwnType PropertyOwnType { get; set; }
    public int ApartmentsCount { get; set; }
    public int FloorsCount { get; set; }
    public int ParkingsCount { get; set; }
    public int ParkingFloorsCount { get; set; }
    public int ResidentialFloorsCount { get; set; }
    public int StoresCount { get; set; }
    public string GovernmentNumber { get; set; }
    public string BondNumber { get; set; }
    public string BondType { get; set; }
    public DateTime BondDate { get; set; }
    public bool IsActive { get; set; }
    public PropertyTypeDto PropertyType { get; set; }
    public GetBaseNameDto Guard { get; set; }
    public GetLandLordSummaryDto LandLord { get; set; }
    public CountryDto Country { get; set; }
    public GetBaseNameDto City { get; set; }
    public GetBaseNameDto Region { get; set; }
    public List<GetBaseFileDto> Files { get; set; } = [];
}