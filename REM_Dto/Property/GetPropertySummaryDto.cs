﻿using REM_Dto.Base;
using REM_Dto.LandLord;
using REM_Dto.Location;

namespace REM_Dto.Property;

public class GetPropertySummaryDto
{
    public Guid Id { get; set; }
    public string DocCode { get; set; }
    public string Name { get; set; }
    public string PropertyNumber { get; set; }
    public string PieceNumber { get; set; }
    public string GovernmentNumber { get; set; }
    public string BondNumber { get; set; }
    public string BondType { get; set; }
    public DateTime BondDate { get; set; }
    public CountryDto Country { get; set; }
    public GetBaseNameDto City { get; set; }
    public GetBaseNameDto Region { get; set; }
    public GetLandLordSummaryDto LandLord { get; set; }
}