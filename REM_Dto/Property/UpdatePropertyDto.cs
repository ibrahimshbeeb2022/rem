﻿namespace REM_Dto.Property;

public class UpdatePropertyDto : AddPropertyDto
{
    public Guid Id { get; set; }
    public List<Guid> DeletedFiles { get; set; } = [];
}