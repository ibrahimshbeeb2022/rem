﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using REM_Dto.Location;
using REM_Shared.Exception;
using REM_Shared.Resource;

namespace REM_WebAPI.FluentValidators;

public class CountryFormDtoValidator : AbstractValidator<CountryFormDto>
{
    public CountryFormDtoValidator(IStringLocalizer<LocationResource> locationLocalization)
    {
        RuleFor(dto => dto.Name).NotEmpty().WithMessage(locationLocalization[ErrorMessages.CountryNameRequired]);
        RuleFor(dto => dto.CountryCode).NotEmpty().WithMessage(locationLocalization[ErrorMessages.CountryCodeRequired]);
        RuleFor(dto => dto.CurrencyId).NotEmpty().WithMessage(locationLocalization[ErrorMessages.CountryCurrencyRequired]);
    }
}