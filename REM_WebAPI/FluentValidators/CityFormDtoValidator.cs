﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using REM_Dto.Location;
using REM_Shared.Exception;
using REM_Shared.Resource;

namespace REM_WebAPI.FluentValidators;

public class CityFormDtoValidator : AbstractValidator<CityFormDto>
{
    public CityFormDtoValidator(IStringLocalizer<LocationResource> locationLocalization)
    {
        RuleFor(dto => dto.Name).NotEmpty().WithMessage(locationLocalization[ErrorMessages.CityNameRequired]);
        RuleFor(dto => dto.CountryId).NotEmpty().NotEqual(Guid.Empty).WithMessage(locationLocalization[ErrorMessages.ChooseCountry]);
    }
}