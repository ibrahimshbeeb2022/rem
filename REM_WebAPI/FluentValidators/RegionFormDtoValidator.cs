﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using REM_Dto.Location;
using REM_Shared.Exception;
using REM_Shared.Resource;

namespace REM_WebAPI.FluentValidators;

public class RegionFormDtoValidator : AbstractValidator<RegionFormDto>
{
    public RegionFormDtoValidator(IStringLocalizer<LocationResource> locationLocalization)
    {
        RuleFor(dto => dto.Name).NotEmpty().WithMessage(locationLocalization[ErrorMessages.RegionNameRequired]);
        RuleFor(dto => dto.StreetName).NotEmpty().WithMessage(locationLocalization[ErrorMessages.StreetNameRequired]);
        RuleFor(dto => dto.CityId).NotEmpty().NotEqual(Guid.Empty).WithMessage(locationLocalization[ErrorMessages.ChooseCity]);
    }
}