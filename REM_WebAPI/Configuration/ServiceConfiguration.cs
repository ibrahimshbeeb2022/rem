﻿using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using REM_Database.Context;
using REM_Dto.Location;
using REM_Repository.BankType;
using REM_Repository.Currency;
using REM_Repository.Employee;
using REM_Repository.Expense;
using REM_Repository.File;
using REM_Repository.Finance;
using REM_Repository.Guard;
using REM_Repository.LandLord;
using REM_Repository.Location;
using REM_Repository.Notification;
using REM_Repository.Property;
using REM_Repository.PropertyType;
using REM_Repository.Report;
using REM_Repository.Settings;
using REM_Repository.TenancyContract;
using REM_Repository.Tenant;
using REM_Repository.Unit;
using REM_WebAPI.FluentValidators;
using System.Globalization;
using System.Text;

namespace REM_WebAPI.Configuration;

public static class ServiceConfiguration
{
    public static void ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
        => services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.SaveToken = true;
            options.RequireHttpsMetadata = false;
            options.Events = new JwtBearerEvents
            {
                OnChallenge = context =>
                {
                    context.Response.StatusCode = 401;
                    return Task.CompletedTask;
                }
            };
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidAudience = configuration.GetSection("JwtConfig:validAudience").Value,
                ValidIssuer = configuration.GetSection("JwtConfig:validIssuer").Value,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtConfig:secret"])),
                ClockSkew = TimeSpan.Zero
            };
        });

    public static void ConfigureSwagger(this IServiceCollection services)
        => services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "Real Estate Web API",
                Version = "v1",
                Description = "Real Estate API Services.",
                Contact = new OpenApiContact
                {
                    Name = "Real Estate"
                },
            });
            c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer",
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
                Description = "JWT Authorization header using the Bearer scheme."
            });
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    Array.Empty<string>()
                }
            });
        });

    public static void ConfigureLocalization(this IServiceCollection services)
        => services.AddLocalization()
        .Configure<RequestLocalizationOptions>(options =>
        {
            var supportedCultures = new[]
            {
                new CultureInfo("ar"),
                new CultureInfo("en"),
            };
            options.DefaultRequestCulture = new RequestCulture(culture: "ar", uiCulture: "ar");
            options.SupportedCultures = supportedCultures;
            options.SupportedUICultures = supportedCultures;
        });

    public static void ConfigureValidatorInjection(this IServiceCollection services)
        => services.AddScoped<IValidator<CountryFormDto>, CountryFormDtoValidator>()
        .AddScoped<IValidator<CityFormDto>, CityFormDtoValidator>()
        .AddScoped<IValidator<RegionFormDto>, RegionFormDtoValidator>();

    public static void ConfigureRepositoryInjection(this IServiceCollection services)
        => services
        .AddScoped<ILocationRepo, LocationRepo>()
        .AddScoped<ICurrencyRepo, CurrencyRepo>()
        .AddScoped<IFileRepo, FileRepo>()
        .AddScoped<ILandLordRepo, LandLordRepo>()
        .AddScoped<IPropertyTypeRepo, PropertyTypeRepo>()
        .AddScoped<IGuardRepo, GuardRepo>()
        .AddScoped<IUnitRepo, UnitRepo>()
        .AddScoped<IUnitTypeRepo, UnitTypeRepo>()
        .AddScoped<IPropertyRepo, PropertyRepo>()
        .AddScoped<ITenantRepo, TenantRepo>()
        .AddScoped<IEmployeeRepo, EmployeeRepo>()
        .AddScoped<ITenancyContractRepo, TenancyContractRepo>()
        .AddScoped<IFinanceRepo, FinanceRepo>()
        .AddScoped<IExpenseRepo, ExpenseRepo>()
        .AddScoped<ISettingsRepo, SettingsRepo>()
        .AddScoped<IReportRepo, ReportRepo>()
        .AddScoped<INotificationRepo, NotificationRepo>()
        .AddScoped<IBankTypeRepo, BankTypeRepo>();

    public static void ConfigureDataBase(this IServiceCollection services, string connectionString)
        => services.AddDbContext<REMContext>(op => op.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)));
}