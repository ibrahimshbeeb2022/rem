﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Report;
using REM_Repository.Report;
using REM_Shared.Enum;

namespace REM_WebAPI.Controllers
{
    public class ReportController : GeneralController
    {
        private readonly IReportRepo reportRepo;

        public ReportController(IReportRepo reportRepo)
        {
            this.reportRepo = reportRepo;
        }

        [HttpGet, Produces(typeof(CommonResponseDto<List<FinalReportDto>>))]
        public async Task<ActionResult> GetFinalReport
        (int pageSize, int pageNum, Guid property, DateTime? from, DateTime? to)
       => Ok(
             await reportRepo.GetFinalReport
             (pageSize, pageNum, property, from, to)
            );

        [HttpGet, Produces(typeof(CommonResponseDto<List<OccupancyReportDto>>))]
        public async Task<ActionResult> GetOccupancyReport
        (int pageSize, int pageNum, string search, TenancyContractStatus? status, bool isAsc,
              DateTime? from, DateTime? to, Guid? landlord, Guid? property, Guid? unit, Guid? tenant)
       => Ok(
             await reportRepo.GetOccupancyReport
             (pageSize, pageNum, search, status, isAsc, from, to, landlord, property, unit, tenant)
            );

        [HttpGet, Produces(typeof(CommonResponseDto<List<GetHomePageReportDto>>))]
        [AllowAnonymous]
        public async Task<ActionResult> GetHomePageReport(DateTime? expenseFrom, DateTime? expenseTo, DateTime? occupancyFrom, DateTime? occupancyTo, DateTime? checkFrom, DateTime? checkTo)
        {
            var result = await reportRepo.GetHomePageReport(expenseFrom, expenseTo, occupancyFrom, occupancyTo, checkFrom, checkTo);
            return Ok(result);
        }
    }
}
