﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.BankType;
using REM_Repository.BankType;

namespace REM_WebAPI.Controllers
{
    public class BankTypeController : GeneralController
    {
        private readonly IBankTypeRepo bankTypeRepo;

        public BankTypeController(IBankTypeRepo bankTypeRepo)
        {
            this.bankTypeRepo = bankTypeRepo;
        }

        [HttpPost]
        public async Task<IActionResult> Action(ActionBankTypeDto dto)
        {
            var data = await bankTypeRepo.Action(dto);
            return Ok(data);
        }

        [HttpPut]
        public async Task<IActionResult> Remove(Guid id)
        {
            await bankTypeRepo.Remove(id);
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll(string search)
        {
            var data = await bankTypeRepo.GetAll(search);
            return Ok(data);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetById(Guid id)
        {
            var data = await bankTypeRepo.GetById(id);
            return Ok(data);
        }


    }
}
