﻿using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Location;
using REM_Repository.Location;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers;

public class LocationController : GeneralController
{
    private readonly ILocationRepo locationRepo;
    private readonly IValidator<CityFormDto> cityValidator;
    private readonly IValidator<RegionFormDto> regionValidator;
    private readonly IValidator<CountryFormDto> countryValidator;
    public LocationController(ILocationRepo locationRepo, IValidator<CountryFormDto> countryValidator,
        IValidator<CityFormDto> cityValidator, IValidator<RegionFormDto> regionValidator)
    {
        this.locationRepo = locationRepo;
        this.cityValidator = cityValidator;
        this.regionValidator = regionValidator;
        this.countryValidator = countryValidator;
    }
    #region Country
    [HttpPost, Produces(typeof(CommonResponseDto<CountryDto>))]
    public async Task<IActionResult> AddCountry(CountryFormDto countryFormDto)
    {
        var validatePayload = await countryValidator.ValidateAsync(countryFormDto);
        if (!validatePayload.IsValid)
            throw new ValidationException(string.Join(" ", validatePayload.Errors.Select(e => e.ErrorMessage)));

        return Ok(await locationRepo.AddCountryAsync(countryFormDto));
    }
    [HttpPut]
    public async Task<IActionResult> UpdateCountry(CountryFormDto countryFormDto)
    {
        var validatePayload = await countryValidator.ValidateAsync(countryFormDto);
        if (!validatePayload.IsValid)
            throw new ValidationException(string.Join(" ", validatePayload.Errors.Select(e => e.ErrorMessage)));

        await locationRepo.UpdateCountryAsync(countryFormDto);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveCountry(Guid countryId)
    {
        await locationRepo.RemoveCountryAsync(countryId);
        return Ok();
    }

    [HttpGet, Produces(typeof(CommonResponseDto<List<CountryDto>>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetAllCountry(string search, bool isAsc, bool? isAvailable, int pageSize = 30, int pageNum = 0,
        CountrySort countrySort = CountrySort.CreateDate)
        => Ok(await locationRepo.GetAllCountryAsync(search, pageSize, pageNum, countrySort, isAsc, isAvailable));

    [HttpGet, Produces(typeof(CommonResponseDto<CountryDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetCountry(Guid id)
        => Ok(await locationRepo.GetCountryAsync(id));
    #endregion

    #region City
    [HttpPost, Produces(typeof(CityDto))]
    public async Task<IActionResult> AddCity(CityFormDto cityFormDto)
    {
        var validatePayload = await cityValidator.ValidateAsync(cityFormDto);
        if (!validatePayload.IsValid)
            throw new ValidationException(string.Join(" ", validatePayload.Errors.Select(e => e.ErrorMessage)));

        return Ok(await locationRepo.AddCityAsync(cityFormDto));
    }
    [HttpPut]
    public async Task<IActionResult> UpdateCity(CityFormDto cityFormDto)
    {
        var validatePayload = await cityValidator.ValidateAsync(cityFormDto);
        if (!validatePayload.IsValid)
            throw new ValidationException(string.Join(" ", validatePayload.Errors.Select(e => e.ErrorMessage)));

        await locationRepo.UpdateCityAsync(cityFormDto);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveCity(Guid cityId)
    {
        await locationRepo.RemoveCityAsync(cityId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<CityDto>>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetAllCity(Guid? countryId, string search, bool isAsc, bool? isAvailable, int pageSize = 30, int pageNum = 0,
        CitySort citySort = CitySort.CreateDate)
        => Ok(await locationRepo.GetAllCityAsync(countryId, search, pageSize, pageNum, citySort, isAsc, isAvailable));

    [HttpGet, Produces(typeof(CommonResponseDto<CityDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetCity(Guid id)
        => Ok(await locationRepo.GetCityAsync(id));
    #endregion

    #region Region
    [HttpPost, Produces(typeof(RegionDto))]
    public async Task<IActionResult> AddRegion(RegionFormDto regionFormDto)
    {
        var validatePayload = await regionValidator.ValidateAsync(regionFormDto);
        if (!validatePayload.IsValid)
            throw new ValidationException(string.Join(" ", validatePayload.Errors.Select(e => e.ErrorMessage)));

        return Ok(await locationRepo.AddRegionAsync(regionFormDto));
    }
    [HttpPut]
    public async Task<IActionResult> UpdateRegion(RegionFormDto regionFormDto)
    {
        var validatePayload = await regionValidator.ValidateAsync(regionFormDto);
        if (!validatePayload.IsValid)
            throw new ValidationException(string.Join(" ", validatePayload.Errors.Select(e => e.ErrorMessage)));

        await locationRepo.UpdateRegionAsync(regionFormDto);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveRegion(Guid regionId)
    {
        await locationRepo.RemoveRegionAsync(regionId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<RegionDto>>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetAllRegion(Guid? cityId, string search, bool isAsc, int pageSize = 30, int pageNum = 0, RegionSort regionSort = RegionSort.CreateDate)
        => Ok(await locationRepo.GetAllRegionAsync(cityId, search, pageSize, pageNum, regionSort, isAsc));

    [HttpGet, Produces(typeof(CommonResponseDto<RegionDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetRegion(Guid id)
        => Ok(await locationRepo.GetRegionAsync(id));
    #endregion
}