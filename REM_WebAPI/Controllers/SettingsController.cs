﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Settings;
using REM_Repository.Settings;
using REM_Shared.Enum;

namespace REM_WebAPI.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class SettingsController : ControllerBase
{
    private readonly ISettingsRepo settingsRepo;

    public SettingsController(ISettingsRepo settingsRepo)
    {
        this.settingsRepo = settingsRepo;
    }

    [HttpPost]
    public async Task<IActionResult> UpdateSettingsLaws(List<SettingsLawsFormDto> settingsLawsDtos)
    {
        await settingsRepo.UpdateSettingsAndLawsAsync(settingsLawsDtos);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<SettingsLawsDto>>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetAllSettingsLaws() => Ok(await settingsRepo.GetAllSettingsLawsAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<SettingsLawsDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetSettingsByKey(SettingsEnum key) => Ok(await settingsRepo.GetSettingsByKeyAsync(key));

    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> GetLastDocumentNumber(DocumentNumber documentNumber)
      => Ok(await settingsRepo.GetLastDocumentCodeAsync(documentNumber));
}