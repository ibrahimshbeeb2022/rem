﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.PropertyType;
using REM_Repository.PropertyType;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers
{
    public class PropertyTypeController : GeneralController
    {
        private readonly IPropertyTypeRepo propertyTypeRepo;

        public PropertyTypeController(IPropertyTypeRepo propertyTypeRepo)
        {
            this.propertyTypeRepo = propertyTypeRepo;
        }
        [HttpPost]
        public async Task<ActionResult> AddPropertyType(PropertyTypeFormDto propertyTypeFormDto)
        => Ok(await propertyTypeRepo.AddAsync(propertyTypeFormDto));

        [HttpPut]
        public async Task<ActionResult> UpdatePropertyType(PropertyTypeFormDto propertyTypeFormDto)
        {
            await propertyTypeRepo.UpdateAsync(propertyTypeFormDto);
            return Ok();
        }
        [HttpPut]
        public async Task<ActionResult> RemovePropertyType(Guid id)
        {
            await propertyTypeRepo.RemoveAsync(id);
            return Ok();
        }
        [HttpGet, Produces(typeof(CommonResponseDto<List<PropertyTypeDto>>))]
        [AllowAnonymous]
        public async Task<ActionResult> GetAllPropertyType(string search, PropertyCategory? propertyCategory, bool isAsc,
            PropertyTypeSort propertyTypeSort = PropertyTypeSort.CreateDate)
        => Ok(await propertyTypeRepo.GetAllAsync(search, propertyCategory, propertyTypeSort, isAsc));

        [HttpGet, Produces(typeof(CommonResponseDto<PropertyTypeDto>))]
        [AllowAnonymous]
        public async Task<ActionResult> GetPropertyType(Guid id) => Ok(await propertyTypeRepo.GetAsync(id));
    }
}
