﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Employee;
using REM_Dto.Permission;
using REM_Repository.Employee;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class EmployeeController : ControllerBase
{
    private readonly IEmployeeRepo employeeRepo;
    public EmployeeController(IEmployeeRepo employeeRepo)
    {
        this.employeeRepo = employeeRepo;
    }
    [HttpPost, Produces(typeof(SignInEmployeeDto))]
    [AllowAnonymous]
    public async Task<IActionResult> SignIn(SignInDto signInDto)
        => Ok(await employeeRepo.SignInIntoDashboardAsync(signInDto));

    [HttpPost]
    public async Task<IActionResult> Add(EmployeeFormDto employeeFormDto)
    {
        await employeeRepo.AddAsync(employeeFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> Update(EmployeeFormDto employeeFormDto)
    {
        await employeeRepo.UpdateAsync(employeeFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> ChangeEmployeeRole(Guid employeeId, List<Guid> roleIds)
    {
        await employeeRepo.ChangeEmployeeRoleAsync(employeeId, roleIds);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> Remove(Guid employeeId)
    {
        await employeeRepo.RemoveAsync(employeeId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<EmployeeDto>>))]
    public async Task<IActionResult> GetAll(bool isAsc, string search, bool? isActive, [FromQuery] List<Guid> roleIds,
        int pageSize = 20, int pageNum = 0, EmployeeSort employeeSort = EmployeeSort.CreateDate)
        => Ok(await employeeRepo.GetAllAsync(pageSize, pageNum, employeeSort, isAsc, search, isActive, roleIds));

    [HttpGet, Produces(typeof(CommonResponseDto<EmployeeDto>))]
    public async Task<IActionResult> Get(Guid employeeId)
        => Ok(await employeeRepo.GetAsync(employeeId));

    [HttpPut]
    public async Task<IActionResult> ActionRolePermissionAsync(ActionEmployeeDto dto)
    {
        await employeeRepo.ActionRolePermissionAsync(dto);
        return Ok();
    }

    [HttpDelete]
    public async Task<IActionResult> RemoveRole(Guid roleId)
    {
        await employeeRepo.RemoveRoleAsync(roleId);
        return Ok();
    }

    [HttpGet, Produces(typeof(CommonResponseDto<List<RoleDto>>))]
    public async Task<IActionResult> GetAllRole(string search) => Ok(await employeeRepo.GetAllRoleAsync(search));

    [HttpGet, Produces(typeof(CommonResponseDto<RoleDto>))]
    public async Task<IActionResult> GetRole(Guid roleId) => Ok(await employeeRepo.GetRoleAsync(roleId));

    [HttpGet, Produces(typeof(CommonResponseDto<List<GetAllPermissionsDto>>))]
    public async Task<IActionResult> GetAllPermissions(Guid? roleId) => Ok(await employeeRepo.GetAllPermissions(roleId));


    [HttpPost]
    public async Task<IActionResult> SeedAuth()
    {
        await employeeRepo.SeedAuth();
        return Ok();
    }

}