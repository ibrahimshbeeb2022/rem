﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Notification;
using REM_Repository.Notification;
using REM_Shared.Enum;

namespace REM_WebAPI.Controllers
{
    [AllowAnonymous]
    public class NotificationController : GeneralController
    {
        private readonly INotificationRepo notificationRepo;

        public NotificationController(INotificationRepo notificationRepo)
        {
            this.notificationRepo = notificationRepo;
        }

        [HttpPut]
        public async Task<IActionResult> ChangeFcmToken(string fcmToken)
        {
            await notificationRepo.ChangeFcmTokenAsync(fcmToken);
            return Ok();
        }
        [HttpPut]
        public async Task<IActionResult> ChangeLanguage(LanguageEnum language)
        {
            await notificationRepo.ChangeLanguageAsync(language);
            return Ok();
        }
        [HttpPut]
        public async Task<IActionResult> ReadAll()
        {
            await notificationRepo.ReadAllAsync();
            return Ok();
        }
        [HttpPut]
        public async Task<IActionResult> ReadOne(Guid id)
        {
            await notificationRepo.ReadOneAsync(id);
            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> GetCounts()
        {
            var count = await notificationRepo.GetCountsAsync();
            return Ok(count);
        }
        [HttpGet, Produces(typeof(CommonResponseDto<List<NotificationDto>>))]
        public async Task<IActionResult> GetAll(int pageSize, int pageNum, bool? isUnread)
        {
            var response = await notificationRepo.GetAllAsync(pageSize, pageNum, isUnread);
            return Ok(response);
        }
        [HttpPost]
        public async Task<IActionResult> Send(SendNotificationDto dto)
        {
            await notificationRepo.SendAsync(dto);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> SeedNotificationType()
        {
            await notificationRepo.SeedNotificationType();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> gettypes()
        {
            var response = await notificationRepo.gettypes();
            return Ok(response);
        }
    }
}
