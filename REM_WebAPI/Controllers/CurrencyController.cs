﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Currency;
using REM_Repository.Currency;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class CurrencyController : ControllerBase
{
    private readonly ICurrencyRepo currencyRepo;
    public CurrencyController(ICurrencyRepo currencyRepo)
    {
        this.currencyRepo = currencyRepo;
    }
    [HttpPost]
    public async Task<IActionResult> Add(CurrencyFormDto currencyFormDto)
        => Ok(await currencyRepo.AddAsync(currencyFormDto));

    [HttpPut]
    public async Task<IActionResult> Update(CurrencyFormDto currencyFormDto)
    {
        await currencyRepo.UpdateAsync(currencyFormDto);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> Remove(Guid currencyId)
    {
        await currencyRepo.RemoveAsync(currencyId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<CurrencyDto>>))]
    [AllowAnonymous]

    public async Task<IActionResult> GetAll(string search, bool isAsc, bool? isActive, CurrencySort currencySort = CurrencySort.CreateDate)
        => Ok(await currencyRepo.GetAllAsync(search, currencySort, isAsc, isActive));

    [HttpGet, Produces(typeof(CommonResponseDto<CurrencyDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> Get(Guid id) => Ok(await currencyRepo.GetAsync(id));
}