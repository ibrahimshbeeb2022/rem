﻿using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Expense;
using REM_Dto.Finance;
using REM_Repository.Finance;
using REM_Shared.Enum;

namespace REM_WebAPI.Controllers
{
    public class FinanceController : GeneralController
    {
        private readonly IFinanceRepo financeRepo;
        protected static Semaphore sema;
        public FinanceController(IFinanceRepo financeRepo)
        {
            this.financeRepo = financeRepo;
        }

        [HttpGet, Produces(typeof(FinanceReportDto))]
        public async Task<ActionResult> GetAccountFinanceReport(Guid? accountId, Guid? unitId, Guid? property, Guid? expense, DateTime? from, DateTime? to)
        {
            var response = await financeRepo.GetAccountFinanceReport(accountId, unitId, property, expense, from, to);
            return Ok(response);
        }
        [HttpGet, Produces(typeof(CommonResponseDto<List<GetAllBondsDto>>))]
        public async Task<ActionResult> GetAllBonds(VoucherBondTypes voucherBondType, BondType Type,
            Guid? unitId, Guid? property, Guid? landlord, Guid? tenant, DateTime? from, DateTime? to, Guid? checkId, string search)
        {
            var response = await financeRepo.GetAllBonds(voucherBondType, Type, unitId, property, landlord, tenant, from, to, checkId, search);
            return Ok(response);
        }
        [HttpPost]
        public async Task<ActionResult> AddBond(AddBondDto dto)
        {
            var result = await financeRepo.AddBond(dto);
            return Ok(result);
        }
        [HttpPost]
        public async Task<ActionResult> SeedFinanceAccount()
        {
            await financeRepo.SeedFinanceAccount();
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> AddCompanyExpenseVoucher(AddCompanyExpenseVoucherDto dto)
        {
            var result = await financeRepo.AddCompanyExpenseVoucher(dto);
            return Ok(result);
        }

        [HttpGet, Produces(typeof(CommonResponseDto<List<GetCompanyExpenseVoucherDto>>))]
        public async Task<ActionResult> GetCompanyExpenseVoucher
            (int pageSize, int pageNum, string search, DateTime? from, DateTime? to,
            Guid? landLord, Guid? tenant, Guid? property, Guid? unit, Guid? companyExpense, Guid? generalExpense)
        {
            var data = await financeRepo.GetCompanyExpenseVoucher(pageSize, pageNum, search, from, to, landLord, tenant, property, unit, companyExpense, generalExpense);
            return Ok(data);
        }

        [HttpGet]
        public async Task<ActionResult> GetFinanceAccount()
        {
            var response = await financeRepo.GetFinanceAccount();
            return Ok(response);
        }
    }
}
