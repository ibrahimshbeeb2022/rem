﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.Base;
using REM_Repository.File;
using REM_Shared.Enum;

namespace REM_WebAPI.Controllers;

[AllowAnonymous]
public class FileController : GeneralController
{
    private readonly IFileRepo fileRepo;

    public FileController(IFileRepo fileRepo)
    {
        this.fileRepo = fileRepo;
    }

    [HttpPost, Produces(typeof(GetBaseFileDto))]
    public async Task<ActionResult> SaveFile(IFormFile file, FileKind fileKind) => Ok(await fileRepo.SaveFile(file, fileKind));

    [HttpDelete]
    public async Task<ActionResult> DeleteFile(Guid Id)
    {
        await fileRepo.DeleteFile(Id);
        return Ok();
    }
}