﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.LandLord;
using REM_Repository.LandLord;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers;

public class LandLordController : GeneralController
{
    private readonly ILandLordRepo landLordRepo;

    public LandLordController(ILandLordRepo landLordRepo)
    {
        this.landLordRepo = landLordRepo;
    }

    [HttpPost]
    public async Task<ActionResult> Add(AddLandLordDto landLordForm)
    {
        await landLordRepo.AddAsync(landLordForm);
        return Ok();
    }
    [HttpPut]
    public async Task<ActionResult> Edit(UpdateLandLordDto landLordForm)
    {
        await landLordRepo.UpdateAsync(landLordForm);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<GetLandLordSummaryDto>>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetAllSummary()
    {
        var response = await landLordRepo.GetAllSummaryAsync();
        return Ok(response);
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<LandLordDto>>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetAll(string search, bool isAsc,
        DateTime? from, DateTime? to, int pageNum = 0, int pageSize = 20, LandLordSort landLordSort = LandLordSort.CreateDate)
    {
        var response = await landLordRepo.GetAllAsync(pageSize, pageNum, search, landLordSort, isAsc, from, to);
        return Ok(response);
    }
    [HttpGet, Produces(typeof(CommonResponseDto<LandLordDto>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetDetails(Guid id)
    {
        var response = await landLordRepo.GetByIdAsync(id);
        return Ok(response);
    }
    [HttpPut]
    public async Task<ActionResult> Remove(Guid Id)
    {
        await landLordRepo.RemoveAsync(Id);
        return Ok();
    }
}