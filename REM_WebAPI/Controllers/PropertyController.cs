﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Property;
using REM_Repository.Property;
using REM_Repository.PropertyType;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers;

public class PropertyController : GeneralController
{
    private readonly IPropertyRepo propertyRepo;
    private readonly IPropertyTypeRepo propertyTypeRepo;

    public PropertyController(IPropertyRepo propertyRepo, IPropertyTypeRepo propertyTypeRepo)
    {
        this.propertyRepo = propertyRepo;
        this.propertyTypeRepo = propertyTypeRepo;
    }

    [HttpPost]
    public async Task<ActionResult> Add(AddPropertyDto propertyFormDto)
    {
        await propertyRepo.AddAsync(propertyFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<ActionResult> Update(UpdatePropertyDto propertyFormDto)
    {
        await propertyRepo.UpdateAsync(propertyFormDto);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<GetPropertySummaryDto>>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetAllSummary()
    {
        var data = await propertyRepo.GetAllSummaryAsync();
        return Ok(data);
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<GetPropertyDto>>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetAll(string search, Guid? landLord, Guid? guard, PropertySort propertySort, bool isAsc, DateTime? from, DateTime? to, PropertyOwnType? propertyOwnType, int pageSize = 20, int pageNum = 0)
    {
        var data = await propertyRepo.GetAllAsync(search, landLord, guard, propertySort, isAsc, from, to, propertyOwnType, pageSize, pageNum);
        return Ok(data);
    }
    [HttpGet, Produces(typeof(CommonResponseDto<GetPropertyDto>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetDetails(Guid id)
        => Ok(await propertyRepo.GetByIdAsync(id));

    [HttpPut]
    public async Task<ActionResult> Remove(Guid id)
    {
        await propertyRepo.RemoveAsync(id);
        return Ok();
    }
}
