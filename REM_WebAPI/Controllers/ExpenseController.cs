﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.Expense;
using REM_Repository.Expense;
using REM_Shared.Enum;

namespace REM_WebAPI.Controllers
{
    public class ExpenseController : GeneralController
    {
        private readonly IExpenseRepo expenseRepo;

        public ExpenseController(IExpenseRepo expenseRepo)
        {
            this.expenseRepo = expenseRepo;
        }

        [HttpPost, Produces(typeof(GetAllExpenseDto))]
        public async Task<IActionResult> AddGeneralExpense(ActionExpenseDto dto)
        => Ok(await expenseRepo.AddAsync(dto, ExpenseType.General));


        [HttpPut]
        public async Task<IActionResult> UpdateGeneralExpense(ActionExpenseDto dto)
        {
            await expenseRepo.UpdateAsync(dto);
            return Ok();
        }

        [HttpGet, Produces(typeof(List<GetAllExpenseDto>))]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllGeneralExpense(string search)
        => Ok(await expenseRepo.GetAllAsync(search, ExpenseType.General));

        [HttpGet, Produces(typeof(GetAllExpenseDto))]
        [AllowAnonymous]
        public async Task<IActionResult> GetGeneralExpense(Guid Id)
        => Ok(await expenseRepo.GetAsync(Id));

        [HttpPut]
        public async Task<IActionResult> RemoveGeneralExpense(Guid Id)
        {
            await expenseRepo.RemoveAsync(Id);
            return Ok();
        }

        [HttpPost, Produces(typeof(GetAllExpenseDto))]
        public async Task<IActionResult> AddCompanyExpense(ActionExpenseDto dto)
       => Ok(await expenseRepo.AddAsync(dto, ExpenseType.Company));


        [HttpPut]
        public async Task<IActionResult> UpdateCompanyExpense(ActionExpenseDto dto)
        {
            await expenseRepo.UpdateAsync(dto);
            return Ok();
        }

        [HttpGet, Produces(typeof(List<GetAllExpenseDto>))]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllCompanyExpense(string search)
        => Ok(await expenseRepo.GetAllAsync(search, ExpenseType.Company));

        [HttpGet, Produces(typeof(GetAllExpenseDto))]
        [AllowAnonymous]
        public async Task<IActionResult> GetCompanyExpense(Guid Id)
        => Ok(await expenseRepo.GetAsync(Id));

        [HttpPut]
        public async Task<IActionResult> RemoveCompanyExpense(Guid Id)
        {
            await expenseRepo.RemoveAsync(Id);
            return Ok();
        }
    }
}
