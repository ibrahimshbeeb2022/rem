﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.Base;
using REM_Dto.CommonDto;
using REM_Dto.Tenant;
using REM_Repository.Tenant;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers;

public class TenantController : GeneralController
{
    private readonly ITenantRepo tenantRepo;

    public TenantController(ITenantRepo tenantRepo = null)
    {
        this.tenantRepo = tenantRepo;
    }

    [HttpPost]
    public async Task<ActionResult> Add(AddTenantDto landLordForm)
    {
        var response = await tenantRepo.AddAsync(landLordForm);
        return Ok(response);
    }
    [HttpPut]
    public async Task<ActionResult> Edit(UpdateTenantDto landLordForm)
    {
        await tenantRepo.UpdateAsync(landLordForm);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<GetBaseNameWithCode>>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetAllSummary()
    {
        var response = await tenantRepo.GetAllSummaryAsync();
        return Ok(response);
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<GetTenantDto>>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetAll(string search, DateTime? from, DateTime? to,
        int? numberOfResidentsOfTheRentedProperty, bool isAsc, TenantSort tenantSort = TenantSort.CreateDate, int pageNum = 0, int pageSize = 20)
    {
        var response = await tenantRepo.GetAllAsync(pageSize, pageNum, search, from, to, numberOfResidentsOfTheRentedProperty, tenantSort, isAsc);
        return Ok(response);
    }
    [HttpGet, Produces(typeof(CommonResponseDto<GetTenantDto>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetDetails(Guid id)
    {
        var response = await tenantRepo.GetByIdAsync(id);
        return Ok(response);
    }
    [HttpPut]
    public async Task<ActionResult> Remove(Guid Id)
    {
        await tenantRepo.RemoveAsync(Id);
        return Ok();
    }
}