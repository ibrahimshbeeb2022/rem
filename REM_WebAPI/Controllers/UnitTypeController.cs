﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Unit;
using REM_Repository.Unit;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers
{
    public class UnitTypeController : GeneralController
    {
        private readonly IUnitTypeRepo unitTypeRepo;

        public UnitTypeController(IUnitTypeRepo unitTypeRepo)
        {
            this.unitTypeRepo = unitTypeRepo;
        }
        [HttpPost]
        public async Task<ActionResult> AddUnitType(string name, string nameEn, bool isActive)
            => Ok(await unitTypeRepo.AddAsync(name, nameEn, isActive));
        [HttpPut]
        public async Task<ActionResult> UpdateUnitType(Guid id, string name, string nameEn, bool isActive)
        {
            await unitTypeRepo.UpdateAsync(id, name, nameEn, isActive);
            return Ok();
        }
        [HttpDelete]
        public async Task<ActionResult> RemoveUnitType(Guid id)
        {
            await unitTypeRepo.RemoveAsync(id);
            return Ok();
        }
        [HttpGet, Produces(typeof(CommonResponseDto<List<UnitTypeDto>>))]
        [AllowAnonymous]
        public async Task<ActionResult> GetAllUnitType(string search, bool isAsc, bool? isActive, UnitTypeSort unitTypeSort = UnitTypeSort.CreateDate)
            => Ok(await unitTypeRepo.GetAllAsync(search, unitTypeSort, isAsc, isActive));

        [HttpGet, Produces(typeof(CommonResponseDto<UnitTypeDto>))]
        [AllowAnonymous]
        public async Task<ActionResult> GetUnitType(Guid id) => Ok(await unitTypeRepo.GetAsync(id));
    }
}
