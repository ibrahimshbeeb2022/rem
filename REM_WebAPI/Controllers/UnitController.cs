﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Unit;
using REM_Repository.Unit;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers;

public class UnitController : GeneralController
{
    private readonly IUnitRepo unitRepo;
    private readonly IUnitTypeRepo unitTypeRepo;

    public UnitController(IUnitRepo unitRepo, IUnitTypeRepo unitTypeRepo)
    {
        this.unitRepo = unitRepo;
        this.unitTypeRepo = unitTypeRepo;
    }
    [HttpPost]
    public async Task<ActionResult> Add(UnitFormDto unitDto) => Ok(await unitRepo.AddAsync(unitDto));
    [HttpPut]
    public async Task<ActionResult> Update(UnitFormDto unitDto)
    {
        await unitRepo.UpdateAsync(unitDto);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<GetUnitDto>>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetAll(string search, Guid? unitType, Guid? propertyId, Guid? tenantId, Guid? LandLord, bool isAsc, bool? rentedUnits, TenancyContractStatus? status,
        int pageSize = 20, int pageNum = 0, UnitSort unitSort = UnitSort.CreateDate)
    {
        var data = await unitRepo.GetAllAsync(search, unitType, propertyId, tenantId, LandLord, unitSort, isAsc, pageSize, pageNum, rentedUnits, status);
        return Ok(data);
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<GetAllUnitSummaryDto>>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetAllSummary(Guid? tenantId, Guid? propertyId, bool? rentedUnits)
    {
        var data = await unitRepo.GetAllSummaryAsync(tenantId, propertyId, rentedUnits);
        return Ok(data);
    }
    [HttpGet, Produces(typeof(CommonResponseDto<GetUnitDto>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetDetails(Guid id)
    {
        var data = await unitRepo.GetByIdAsync(id);
        return Ok(data);
    }
    [HttpPut]
    public async Task<ActionResult> Remove(Guid id)
    {
        await unitRepo.RemoveAsync(id);
        return Ok();
    }
}