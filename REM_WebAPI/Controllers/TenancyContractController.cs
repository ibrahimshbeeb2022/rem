﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.TenancyContract;
using REM_Repository.TenancyContract;
using REM_Shared.Enum;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers;

public class TenancyContractController : GeneralController
{
    private readonly ITenancyContractRepo tenancyContractRepo;
    private static readonly Semaphore semaphore = new(1, 1);

    public TenancyContractController(ITenancyContractRepo tenancyContractRepo)
    {
        this.tenancyContractRepo = tenancyContractRepo;
    }

    [HttpPost]
    public async Task<ActionResult> ActionToWaitingList(ActionToWaitingListDto tc)
    {
        semaphore.WaitOne();
        try
        {
            var response = await tenancyContractRepo.ActionToWaitingList(tc);
            semaphore.Release();
            return Ok(response);
        }
        catch (Exception ex)
        {
            semaphore.Release();
            throw ex;
        }
    }

    /*[HttpPost]
    public async Task<ActionResult> Add(AddTenancyContractDto tc)
    {
        semaphore.WaitOne();
        try
        {
            var response = await tenancyContractRepo.AddAsync(tc);
            semaphore.Release();
            return Ok(response);
        }
        catch (Exception ex)
        {
            semaphore.Release();
            throw ex;
        }
    }*/
    [HttpPost]
    public async Task<ActionResult> ApproveWaitingList(Guid id)
    {
        var response = await tenancyContractRepo.ApproveWaitingList(id);
        return Ok(response);
    }
    [HttpPost]
    public async Task<ActionResult> AddFile(Guid id, Guid fileId)
    {
        var response = await tenancyContractRepo.AddFile(id, fileId);
        return Ok(response);
    }
    [HttpPut]
    public async Task<ActionResult> ChangeStatus(ChangeTenancyContractStatusDto dto)
    {
        await tenancyContractRepo.ChangeStatus(dto);
        return Ok();
    }

    [HttpPut]
    public async Task<ActionResult> Remove(Guid Id)
    {
        await tenancyContractRepo.RemoveAsync(Id);
        return Ok();
    }

    [HttpGet, Produces(typeof(CommonResponseDto<List<GetTenancyContractDto>>))]
    public async Task<ActionResult> GetAllWaitindList
        (
          string search, bool isAsc, DateTime? from, DateTime? to,
          Guid? landlord, Guid? property, Guid? unit, Guid? tenant,
          int pageSize = 20, int pageNum = 0,
          TenancyContractSort tenancyContractSort = TenancyContractSort.CreateDate
        )
        => Ok(
              await tenancyContractRepo.GetAllAsync
              (false, pageSize, pageNum, search, tenancyContractSort, isAsc, from, to, landlord, property, unit, tenant)
             );

    [HttpGet, Produces(typeof(CommonResponseDto<List<GetTenancyContractDto>>))]
    public async Task<ActionResult> GetAll
      (
        string search, bool isAsc, DateTime? from, DateTime? to,
        Guid? landlord, Guid? property, Guid? unit, Guid? tenant,
        int pageSize = 20, int pageNum = 0,
        TenancyContractSort tenancyContractSort = TenancyContractSort.CreateDate
      )
      => Ok(
            await tenancyContractRepo.GetAllAsync
            (true, pageSize, pageNum, search, tenancyContractSort, isAsc, from, to, landlord, property, unit, tenant)
           );


    [HttpGet, Produces(typeof(CommonResponseDto<GetTenancyContractDto>))]
    public async Task<ActionResult> GetByIdAsync(Guid Id) =>
        Ok(await tenancyContractRepo.GetByIdAsync(Id));

    [HttpGet, Produces(typeof(CommonResponseDto<GetTenancyContractDto>))]
    public async Task<ActionResult> GetByNumberAsync(string number) =>
        Ok(await tenancyContractRepo.GetByNumberAsync(number));

    [HttpGet, Produces(typeof(CommonResponseDto<List<GetAllChecksDto>>))]
    public async Task<ActionResult> GetAllChecks(
        CheckStatus? chequeStatus, Guid? tenancyContractId, DateTime? from, DateTime? to,
        Guid? landlord, Guid? property, Guid? unit, Guid? tenant,
        string search, bool isAsc, int pageSize = 20,
        int pageNum = 0, ChequeSort chequeSort = ChequeSort.CreateDate)
        => Ok(
              await tenancyContractRepo.GetAllChecks
              (pageSize, pageNum, from, to, search, chequeSort, isAsc, chequeStatus,
               tenancyContractId, landlord, property, unit, tenant)
            );


    [HttpGet, Produces(typeof(CommonResponseDto<List<GetAllChecksSummaryDto>>))]
    [AllowAnonymous]
    public async Task<ActionResult> GetAllChecksNeedPayment(Guid? unitId)
       => Ok(
             await tenancyContractRepo.GetAllChecksNeedPayment(unitId)
           );


    [HttpGet, Produces(typeof(CommonResponseDto<List<GetCheckHistoryDto>>))]
    public async Task<ActionResult> GetCheckHistory(Guid id) =>
       Ok(await tenancyContractRepo.GetCheckHistory(id));

    [HttpPut]
    public async Task<ActionResult> ChangeCheckStatus(ChangeCheckStatusDto dto)
    {
        semaphore.WaitOne();
        try
        {
            await tenancyContractRepo.ChangeCheckStatus(dto);
            semaphore.Release();
            return Ok();
        }
        catch (Exception ex)
        {
            semaphore.Release();
            throw ex;
        }
    }
}