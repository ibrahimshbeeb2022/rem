﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using REM_Dto.CommonDto;
using REM_Dto.Guard;
using REM_Repository.Guard;
using REM_Shared.Enum.Sort;

namespace REM_WebAPI.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class GuardController : ControllerBase
{
    private readonly IGuardRepo guardRepo;
    public GuardController(IGuardRepo guardRepo)
    {
        this.guardRepo = guardRepo;
    }
    [HttpPost]
    public async Task<IActionResult> Add(AddGuardFormDto addGuardFormDto)
    {
        var response = await guardRepo.AddAsync(addGuardFormDto);
        return Ok(response);
    }
    [HttpPut]
    public async Task<IActionResult> Update(UpdateGuardFormDto updateGuardFormDto)
    {
        await guardRepo.UpdateAsync(updateGuardFormDto);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> Remove(Guid guardId)
    {
        await guardRepo.RemoveAsync(guardId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<GuardDto>>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetAll(string search, DateTime? from, DateTime? to, bool isAsc, GuardSort guardSort = GuardSort.CreateDate, int pageSize = 20, int pageNum = 0)
        => Ok(await guardRepo.GetAllAsync(search, pageSize, pageNum, guardSort, isAsc, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<GuardDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> Get(Guid guardId) => Ok(await guardRepo.GetAsync(guardId));
}