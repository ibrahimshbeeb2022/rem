﻿using Microsoft.Extensions.Localization;
using Microsoft.IdentityModel.Tokens;
using REM_Dto.CommonDto;
using REM_Shared.Exception;
using REM_Shared.Resource;
using System.Net;

namespace REM_WebAPI.Middleware;

public class ErrorHandlerMiddleware(IStringLocalizer<SharedResource> sharedLocalizer) : IMiddleware
{
    private readonly IStringLocalizer<SharedResource> sharedLocalizer = sharedLocalizer;

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next(context);
        }
        catch (Exception error)
        {
            Console.WriteLine(error);
            var response = context.Response;
            response.ContentType = "application/json";
            CommonResponseDto<object> customResponse = new()
            {
                ErrorMessage = error.Message,
            };
            switch (error)
            {
                case AccessViolationException:
                    response.StatusCode = (int)HttpStatusCode.Forbidden;
                    customResponse.ErrorMessage = sharedLocalizer[ErrorMessages.NoAccess];
                    break;
                case SecurityTokenExpiredException:
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    customResponse.ErrorMessage = sharedLocalizer[ErrorMessages.RefreshTokenExpired];
                    break;
                case SecurityTokenValidationException:
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    customResponse.ErrorMessage = sharedLocalizer[ErrorMessages.TokenStillWork];
                    break;
                case AlreadyExistException:
                    response.StatusCode = (int)HttpStatusCode.Conflict;
                    break;
                case NotSupportedException:
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    break;
                case UnauthorizedAccessException:
                    response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    customResponse.ErrorMessage = sharedLocalizer[ErrorMessages.NoAccess];
                    break;
                case System.ComponentModel.DataAnnotations.ValidationException:
                    // validation application error
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    break;
                case FluentValidation.ValidationException:
                    // validation application error
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    break;
                case NotFoundException:
                    // not found error
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    break;
                case DirectoryNotFoundException:
                    // not found error
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    break;
                case TaskCanceledException:
                    // Timeout error
                    response.StatusCode = (int)HttpStatusCode.RequestTimeout;
                    customResponse.ErrorMessage = sharedLocalizer[ErrorMessages.TimeoutError];
                    break;
                case TimeoutException:
                    // Timeout error
                    response.StatusCode = (int)HttpStatusCode.RequestTimeout;
                    customResponse.ErrorMessage = sharedLocalizer[ErrorMessages.TimeoutError];
                    break;
                default:
                    // unhandled error
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    customResponse.ErrorMessage = sharedLocalizer[ErrorMessages.InternalServerError];
                    break;
            }
            await response.WriteAsJsonAsync(customResponse);
        }
    }
}