﻿using Microsoft.AspNetCore.Authorization;
using REM_Repository.Employee;
using REM_Shared.Enum;
using REM_WebAPI.ErrorExceptions;
using System.Security.Claims;

namespace REM_WebAPI.Middleware
{
    public class AuthorizationMiddleware : IMiddleware
    {
        public AuthorizationMiddleware()
        {

        }
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var endpoint = context.GetEndpoint() ??
                throw new UnauthorizedAccessException("endpoint not found");


            bool? isAllowAnonymous = endpoint?.Metadata.Any(x => x.GetType() == typeof(AllowAnonymousAttribute));
            if (isAllowAnonymous.HasValue && isAllowAnonymous.Value)
            {
                await next(context);
                return;
            }

            var token = context.Request.Headers.Authorization.ToString();
            if (string.IsNullOrEmpty(token) || !context.User.Identity.IsAuthenticated)
                throw new UnauthorizedAccessException("Unauthorized");

            string controllerName = context.Request.RouteValues["controller"].ToString() ?? throw new Exception("endpoint not found");

            string actionType = context.Request.RouteValues["action"].ToString() ?? throw new Exception("endpoint not found");

            var userId = Guid.Parse(context.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).FirstOrDefault());
            var permissionRepo = context.RequestServices.GetRequiredService<IEmployeeRepo>();

            var userPermissions = (await permissionRepo.GetUserPermissions(userId));


            switch (controllerName)
            {
                case nameof(ControllersNames.BankType):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessBank))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;

                case nameof(ControllersNames.Currency):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessCurrency))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;

                case nameof(ControllersNames.Employee):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessEmployeeManagement))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;


                case nameof(ControllersNames.Expense):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessExpense))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;

                case nameof(ControllersNames.File):
                    await next(context);
                    break;

                case nameof(ControllersNames.Finance):
                    {
                        if (actionType == nameof(ControllersActionsNames.GetAccountFinanceReport) && !userPermissions.Any(m => m == PermissionEnum.CanAccessFinanceReports))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.GetAllBonds) && !userPermissions.Any(m => m == PermissionEnum.CanAccessBonds))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.AddBond) && !userPermissions.Any(m => m == PermissionEnum.CanAddBond))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.AddCompanyExpenseVoucher) && !userPermissions.Any(m => m == PermissionEnum.CanAddBond))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.GetCompanyExpenseVoucher) && !userPermissions.Any(m => m == PermissionEnum.CanAccessCompanyExpenseReport))
                            throw new UnAuthonticateException(controllerName, actionType);

                        break;
                    }

                case nameof(ControllersNames.Guard):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessWatchman))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;

                case nameof(ControllersNames.LandLord):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessLandLord))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;

                case nameof(ControllersNames.Location):
                    {
                        if (actionType == nameof(ControllersActionsNames.AddCountry) && !userPermissions.Any(m => m == PermissionEnum.CanAccessCountry))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.UpdateCountry) && !userPermissions.Any(m => m == PermissionEnum.CanAccessCountry))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.RemoveCountry) && !userPermissions.Any(m => m == PermissionEnum.CanAccessCountry))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.AddCity) && !userPermissions.Any(m => m == PermissionEnum.CanAccessCity))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.UpdateCity) && !userPermissions.Any(m => m == PermissionEnum.CanAccessCity))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.RemoveCity) && !userPermissions.Any(m => m == PermissionEnum.CanAccessCity))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.AddRegion) && !userPermissions.Any(m => m == PermissionEnum.CanAccessRegion))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.UpdateRegion) && !userPermissions.Any(m => m == PermissionEnum.CanAccessRegion))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.RemoveRegion) && !userPermissions.Any(m => m == PermissionEnum.CanAccessRegion))
                            throw new UnAuthonticateException(controllerName, actionType);

                        break;
                    }

                case nameof(ControllersNames.Notification):
                    await next(context);
                    break;


                case nameof(ControllersNames.Property):
                    {
                        if (actionType == nameof(ControllersActionsNames.Add) && !userPermissions.Any(m => m == PermissionEnum.CanActionProperty))
                            throw new UnAuthonticateException(controllerName, actionType);

                        if (actionType == nameof(ControllersActionsNames.Update) && !userPermissions.Any(m => m == PermissionEnum.CanActionProperty))
                            throw new UnAuthonticateException(controllerName, actionType);

                        if (actionType == nameof(ControllersActionsNames.Remove) && !userPermissions.Any(m => m == PermissionEnum.CanActionProperty))
                            throw new UnAuthonticateException(controllerName, actionType);

                        break;
                    }



                case nameof(ControllersNames.PropertyType):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessPropertyType))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;

                case nameof(ControllersNames.Report):
                    {
                        if (actionType == nameof(ControllersActionsNames.GetOccupancyReport) && !userPermissions.Any(m => m == PermissionEnum.CanAccessOccupancyReport))
                            throw new UnAuthonticateException(controllerName, actionType);

                        break;
                    }

                case nameof(ControllersNames.Settings):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessSettings))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;


                case nameof(ControllersNames.TenancyContract):
                    {
                        if (actionType == nameof(ControllersActionsNames.Add) && !userPermissions.Any(m => m == PermissionEnum.CanAddTenancyContract))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.Remove) && !userPermissions.Any(m => m == PermissionEnum.CanDeleteTenancyContract))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.ChangeStatus) && !userPermissions.Any(m => m == PermissionEnum.CanChangeTenancyContractStatus))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.GetAll) && !userPermissions.Any(m => m == PermissionEnum.CanAccessTenancyContract))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.GetByIdAsync) && !userPermissions.Any(m => m == PermissionEnum.CanAccessTenancyContract))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.GetAllChecks) && !userPermissions.Any(m => m == PermissionEnum.CanAccessPDCManagement))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.GetCheckHistory) && !userPermissions.Any(m => m == PermissionEnum.CanAccessPDCManagement))
                            throw new UnAuthonticateException(controllerName, actionType);

                        else if (actionType == nameof(ControllersActionsNames.ChangeCheckStatus) && !userPermissions.Any(m => m == PermissionEnum.CanEditChequeStatus))
                            throw new UnAuthonticateException(controllerName, actionType);

                        break;
                    }


                case nameof(ControllersNames.Tenant):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessTenant))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;



                case nameof(ControllersNames.Unit):
                    {
                        if (actionType == nameof(ControllersActionsNames.Add) && !userPermissions.Any(m => m == PermissionEnum.CanActionUnit))
                            throw new UnAuthonticateException(controllerName, actionType);

                        if (actionType == nameof(ControllersActionsNames.Update) && !userPermissions.Any(m => m == PermissionEnum.CanActionUnit))
                            throw new UnAuthonticateException(controllerName, actionType);

                        if (actionType == nameof(ControllersActionsNames.Remove) && !userPermissions.Any(m => m == PermissionEnum.CanActionUnit))
                            throw new UnAuthonticateException(controllerName, actionType);

                        break;
                    }


                case nameof(ControllersNames.UnitType):
                    if (!userPermissions.Any(m => m == PermissionEnum.CanAccessUnitType))
                        throw new UnAuthonticateException(controllerName, actionType);
                    break;


                default:
                    throw new UnAuthonticateException(controllerName, actionType);
            }
            await next(context);
            return;
        }
    }
}
