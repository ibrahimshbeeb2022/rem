﻿namespace REM_WebAPI.ErrorExceptions
{
    public class UnAuthonticateException : Exception
    {
        public string controllerName;
        public string actionName;
        public UnAuthonticateException(string controllerName, string actionName)
        {
            this.controllerName = controllerName;
            this.actionName = actionName;
        }
    }
}
