using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using REM_WebAPI.Configuration;
using REM_WebAPI.Middleware;
using Swashbuckle.AspNetCore.SwaggerUI;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("Server");

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpContextAccessor();
builder.Services.ConfigureAuthentication(builder.Configuration);
builder.Services.ConfigureSwagger();
builder.Services.ConfigureLocalization();
builder.Services.ConfigureValidatorInjection();
builder.Services.ConfigureRepositoryInjection();
builder.Services.ConfigureDataBase(connectionString);

builder.Services.AddTransient<ErrorHandlerMiddleware>();

var FireBaseConfigrationFilePath = Path.Combine(builder.Environment.ContentRootPath, "qarat-72194-firebase-adminsdk-ycxwh-558fd0ef54.json");

var credential = GoogleCredential.FromFile(FireBaseConfigrationFilePath);

var firebaseApp = FirebaseApp.Create(new AppOptions
{
    Credential = credential
});

builder.Services.AddSingleton(firebaseApp);

var app = builder.Build();

var localizerOption = ((IApplicationBuilder)app).ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
app.UseRequestLocalization(localizerOption.Value);

app.UseCors(cors => cors
.AllowAnyMethod()
.AllowAnyHeader()
.SetIsOriginAllowed(origin => true) // allow any origin
.AllowCredentials());

app.UseSwagger();
app.UseSwaggerUI(s =>
{
    s.DocExpansion(DocExpansion.None);
    s.DisplayRequestDuration();
    s.EnableTryItOutByDefault();
    s.InjectStylesheet("/SwaggerDark.css", "");
});

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.UseStaticFiles();
app.UseMiddleware<ErrorHandlerMiddleware>();
app.MapControllers();

app.Run();