﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using REM_Database.Model;
using REM_Database.Model.Currency;
using REM_Database.Model.Employee;
using REM_Database.Model.Expense;
using REM_Database.Model.Finance;
using REM_Database.Model.Guard;
using REM_Database.Model.Location;
using REM_Database.Model.Master;
using REM_Database.Model.Notification;
using REM_Database.Model.Permission;
using REM_Database.Model.Settings;
using REM_Database.Model.Unit;
using REM_Database.Model.User;

namespace REM_Database.Context;

public class REMContext(DbContextOptions<REMContext> options) : DbContext(options)
{
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.EnableSensitiveDataLogging();
        base.OnConfiguring(optionsBuilder);
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<UnitModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<UserModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<RoleModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<FileModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<CityModel>().HasQueryFilter(c => c.IsValid);
        modelBuilder.Entity<RoleModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<GuardModel>().HasQueryFilter(g => g.IsValid);
        modelBuilder.Entity<RegionModel>().HasQueryFilter(r => r.IsValid);
        modelBuilder.Entity<TenantModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<CountryModel>().HasQueryFilter(c => c.IsValid);
        modelBuilder.Entity<AccountModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<CurrencyModel>().HasQueryFilter(c => c.IsValid);
        modelBuilder.Entity<UnitTypeModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<PropertyModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<PermissionModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<UserRoleModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<PropertyTypeModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<PermissionRoleModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<TenancyContractModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<FinanceOperationsModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<TenancyContractDetailsModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<TenancyContractPaymentPlanModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<BondsModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<ExpenseModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<CompanyExpenseVoucherModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<CompanyExpenseVoucherDetailsModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<NotificationModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<NotificationUserModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<CheckHistoryModel>().HasQueryFilter(f => f.IsValid);
        modelBuilder.Entity<BankTypeModel>().HasQueryFilter(f => f.IsValid);

        modelBuilder.Entity<LandLordModel>()
            .HasOne(m => m.Account)
            .WithOne(m => m.LandLord)
            .HasForeignKey<LandLordModel>(m => m.AccountId);

        modelBuilder.Entity<TenantModel>()
           .HasOne(m => m.Account)
           .WithOne(m => m.Tenant)
           .HasForeignKey<TenantModel>(m => m.AccountId);
    }
    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        BeforeSaving();
        return base.SaveChangesAsync(cancellationToken);
    }
    protected void BeforeSaving()
    {
        IEnumerable<EntityEntry> entityEntries = ChangeTracker.Entries();
        DateTime utcNow = DateTime.UtcNow;
        foreach (var entityEntry in entityEntries)
        {
            if (entityEntry.Entity is BaseModel entity)
            {
                switch (entityEntry.State)
                {
                    case EntityState.Modified:
                        entity.UpdateDate = utcNow;
                        break;
                }
            }
        }
    }

    #region Auth
    public DbSet<UserModel> User { get; set; }
    #endregion

    #region Location
    public DbSet<CountryModel> Country { get; set; }
    public DbSet<CityModel> City { get; set; }
    public DbSet<RegionModel> Region { get; set; }
    #endregion

    #region Master

    public DbSet<LandLordModel> LandLord { get; set; }
    public DbSet<PropertyModel> Property { get; set; }
    public DbSet<UnitModel> Unit { get; set; }
    public DbSet<TenantModel> Tenant { get; set; }
    public DbSet<BankTypeModel> BankType { get; set; }
    #endregion

    #region Setting
    public DbSet<FileModel> File { get; set; }

    #endregion

    #region Currency
    public DbSet<CurrencyModel> Currency { get; set; }
    #endregion

    #region Guard
    public DbSet<GuardModel> Guard { get; set; }
    #endregion

    #region PropertyType
    public DbSet<PropertyTypeModel> PropertyType { get; set; }
    #endregion

    #region Unit
    public DbSet<UnitTypeModel> UnitType { get; set; }
    #endregion

    #region Tenancy Contract

    public DbSet<TenancyContractModel> TenancyContract { get; set; }
    public DbSet<TenancyContractPaymentPlanModel> TenancyContractPaymentPlan { get; set; }
    public DbSet<TenancyContractDetailsModel> TenancyContractDetails { get; set; }
    public DbSet<CheckHistoryModel> CheckHistory { get; set; }
    #endregion

    #region Finance 
    public DbSet<FinanceOperationsModel> FinanceOperations { get; set; }
    public DbSet<AccountModel> Account { get; set; }
    public DbSet<BondsModel> Bonds { get; set; }
    #endregion

    #region Settings
    public DbSet<SettingsLawsModel> SettingsLaws { get; set; }
    #endregion

    #region Employee
    public DbSet<EmployeeModel> Employee { get; set; }
    public DbSet<UserRoleModel> UserRole { get; set; }
    #endregion  

    #region Permission
    public DbSet<RoleModel> Role { get; set; }
    public DbSet<PermissionModel> Permission { get; set; }
    public DbSet<PermissionRoleModel> PermissionRole { get; set; }
    #endregion

    #region Expense
    public DbSet<ExpenseModel> Expenses { get; set; }
    public DbSet<CompanyExpenseVoucherModel> CompanyExpenseVoucher { get; set; }
    public DbSet<CompanyExpenseVoucherDetailsModel> CompanyExpenseVoucherDetails { get; set; }
    #endregion

    #region
    public DbSet<NotificationModel> Notifications { get; set; }
    public DbSet<NotificationUserModel> NotificationUsers { get; set; }
    #endregion
}