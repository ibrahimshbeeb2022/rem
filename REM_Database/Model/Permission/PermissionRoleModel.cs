﻿namespace REM_Database.Model.Permission;

public class PermissionRoleModel : BaseModel
{
    public Guid RoleId { get; set; }
    public RoleModel Role { get; set; }

    public Guid PermissionId { get; set; }
    public PermissionModel Permission { get; set; }
}