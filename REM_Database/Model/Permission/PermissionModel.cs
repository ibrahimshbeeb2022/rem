﻿using REM_Shared.Enum;

namespace REM_Database.Model.Permission;

public class PermissionModel : BaseModel
{
    public string Name { get; set; }
    public PermissionEnum PermissionEnum { get; set; }
    public ICollection<PermissionRoleModel> PermissionRoles { get; set; }
}