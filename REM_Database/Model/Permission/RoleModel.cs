﻿using REM_Database.Model.Employee;

namespace REM_Database.Model.Permission;

public class RoleModel : BaseModel
{
    public string Name { get; set; }

    public ICollection<UserRoleModel> Users { get; set; }
    public ICollection<PermissionRoleModel> RolePermissions { get; set; }
}