﻿using REM_Database.Model.Expense;
using REM_Database.Model.Master;
using REM_Shared.Enum;

namespace REM_Database.Model.Finance;

public class FinanceOperationsModel : BaseModel
{
    public int TransActionId { get; set; }
    public string Notes1 { get; set; }
    public string Notes2 { get; set; }
    public double Value { get; set; }
    public FinanceOperationType Type { get; set; }

    public Guid? BondId { get; set; }
    public BondsModel Bond { get; set; }

    public Guid? TenancyContractId { get; set; }
    public TenancyContractModel TenancyContract { get; set; }

    public Guid? CheckId { get; set; }
    public TenancyContractPaymentPlanModel Check { get; set; }

    public Guid? CompanyExpenseVoucherDetailsId { get; set; }
    public CompanyExpenseVoucherDetailsModel CompanyExpenseVoucherDetails { get; set; }

    public Guid AccountId { get; set; }
    public AccountModel Account { get; set; }
}