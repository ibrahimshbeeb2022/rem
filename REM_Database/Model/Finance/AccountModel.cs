﻿using REM_Database.Model.Master;
using REM_Shared.Enum;

namespace REM_Database.Model.Finance;

public class AccountModel : BaseModel
{
    public FinanceAccountType Type { get; set; }

    public Guid? LandLordId { get; set; }
    public LandLordModel LandLord { get; set; }

    public Guid? TenantId { get; set; }
    public TenantModel Tenant { get; set; }
    public List<FinanceOperationsModel> FinanceOperations { get; set; }
}