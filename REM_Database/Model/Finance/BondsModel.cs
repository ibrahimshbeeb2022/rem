﻿using REM_Database.Model.Master;
using REM_Shared.Enum;

namespace REM_Database.Model.Finance
{
    public class BondsModel : BaseModel
    {
        public BondType BondType { get; set; }
        public VoucherBondTypes VoucherBondType { get; set; }
        public FinanceAccountType Account { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }

        public Guid? UnitId { get; set; }
        public UnitModel Unit { get; set; }

        public Guid? PropertyId { get; set; }
        public PropertyModel Property { get; set; }

        public Guid? CheckId { get; set; }
        public TenancyContractPaymentPlanModel Check { get; set; }

        public Guid? LandLordId { get; set; }
        public LandLordModel LandLord { get; set; }

        public Guid? TenantId { get; set; }
        public TenantModel Tenant { get; set; }

        public List<FinanceOperationsModel> FinanceOperations { get; set; } = [];
        public List<FileModel> Files { get; set; } = [];
    }
}
