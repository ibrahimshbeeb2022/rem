﻿using REM_Database.Model.Currency;

namespace REM_Database.Model.Location;

public class CountryModel : BaseModel
{
    public string Name { get; set; }
    public string NameEn { get; set; }
    public string CountryCode { get; set; }
    public bool IsAvailable { get; set; }

    public Guid CurrencyId { get; set; }
    public CurrencyModel Currency { get; set; }

    public ICollection<CityModel> Cities { get; set; }
}