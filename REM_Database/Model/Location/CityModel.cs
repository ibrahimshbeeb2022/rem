﻿namespace REM_Database.Model.Location;

public class CityModel : BaseModel
{
    public string Name { get; set; }
    public string NameEn { get; set; }
    public bool IsAvailable { get; set; }
    public Guid CountryId { get; set; }
    public CountryModel Country { get; set; }
    public ICollection<RegionModel> Regions { get; set; }
}