﻿namespace REM_Database.Model.Location;

public class RegionModel : BaseModel
{
    public string Name { get; set; }
    public string NameEn { get; set; }
    public string StreetName { get; set; }
    public string StreetNameEn { get; set; }

    public Guid CityId { get; set; }
    public CityModel City { get; set; }
}