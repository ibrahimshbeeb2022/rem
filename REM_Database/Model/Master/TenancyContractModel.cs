﻿using REM_Shared.Enum;

namespace REM_Database.Model.Master;

public class TenancyContractModel : BaseModel
{
    public int LandLordTenancyContractNumber { get; set; }
    public string LandLordTenancyContractCode { get; set; }
    public string TenancyReason { get; set; }
    public string TenancyNumber { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public double Value { get; set; }
    public double SubValue { get; set; }
    public double VatValue { get; set; }
    public TenancyContractStatus Status { get; set; }
    public bool Approved { get; set; }

    public Guid LandLordId { get; set; }
    public LandLordModel LandLord { get; set; }

    public Guid TenantId { get; set; }
    public TenantModel Tenant { get; set; }

    public Guid UnitId { get; set; }
    public UnitModel Unit { get; set; }

    public Guid PropertyId { get; set; }
    public PropertyModel Property { get; set; }

    public ICollection<FileModel> Files { get; set; }
    public ICollection<TenancyContractPaymentPlanModel> PaymentPlan { get; set; } = [];
    public ICollection<TenancyContractDetailsModel> TenancyContractDetails { get; set; } = [];
}