﻿using REM_Shared.Enum;

namespace REM_Database.Model.Master
{
    public class TenancyContractDetailsModel : BaseModel
    {
        public double Value { get; set; }
        public double Tax { get; set; }
        public double Total { get; set; }
        public TenancyContractOperationType Type { get; set; }
        public Guid TenancyContractId { get; set; }
        public TenancyContractModel TenancyContract { get; set; }
    }
}
