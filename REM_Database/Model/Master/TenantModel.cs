﻿using REM_Database.Model.Finance;

namespace REM_Database.Model.Master;

public class TenantModel : BaseModel
{
    public string FullName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    //public string Fax { get; set; }
    public string CardNumber { get; set; }
    public string Job { get; set; }
    public string Beneficiary { get; set; }
    public string IdentityCardNumber { get; set; }
    public DateTime IdentityCardExpiryDate { get; set; }
    public string PassportNumber { get; set; }
    public DateTime PassportExpiryDate { get; set; }
    public string Nationality { get; set; }
    public string Address { get; set; }
    public string BoxMail { get; set; }
    public int NumberOfResidentsOfTheRentedProperty { get; set; }

    public Guid? AccountId { get; set; }
    public AccountModel Account { get; set; }

    public ICollection<FileModel> Files { get; set; }
    public ICollection<TenancyContractModel> TenancyContract { get; set; }
}