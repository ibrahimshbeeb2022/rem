﻿using REM_Shared.Enum;

namespace REM_Database.Model.Master;

public class PropertyTypeModel : BaseModel
{
    public string Title { get; set; }
    public string TitleEn { get; set; }
    public PropertyCategory Category { get; set; }

    public ICollection<PropertyModel> Properties { get; set; }
}