﻿using REM_Shared.Enum;

namespace REM_Database.Model.Master
{
    public class CheckHistoryModel : BaseModel
    {
        public string Notes { get; set; }
        public CheckStatus CheckStatus { get; set; }
        public DateTime? PayDate { get; set; } // for hold status
        public bool? LandLordApproved { get; set; }
        public double? ReplaceCashValue { get; set; } // for Replace status 
        public Guid TenancyContractPaymentPlanId { get; set; }
        public TenancyContractPaymentPlanModel TenancyContractPaymentPlan { get; set; }
    }
}
