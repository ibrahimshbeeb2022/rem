﻿using REM_Database.Model.Finance;
using REM_Database.Model.User;

namespace REM_Database.Model.Master;

public class LandLordModel : UserModel
{
    public string SecondPhone { get; set; }
    public string CardNumber { get; set; }
    public string Job { get; set; }// 
    public string Beneficiary { get; set; }//
    public string IdentityCardNumber { get; set; }
    public DateTime IdentityCardExpiryDate { get; set; }
    public string PassportNumber { get; set; }
    public DateTime PassportExpiryDate { get; set; }
    public string TaxNumber { get; set; } // TRN
    //public DateTime BirthDate { get; set; }
    public string BankName { get; set; }//
    public string BankAddress { get; set; }//
    public string Address { get; set; }
    public string AccountName { get; set; }//
    public string AccountNumber { get; set; }
    public string Iban { get; set; }
    public string SwiftCode { get; set; }
    public string Nationality { get; set; }//
    public string Note { get; set; }

    //public Guid CountryId { get; set; }
    //public CountryModel Country { get; set; }

    //public Guid CityId { get; set; }
    //public CityModel City { get; set; }

    //public Guid RegionId { get; set; }
    //public RegionModel Region { get; set; }

    public Guid? AccountId { get; set; }
    public AccountModel Account { get; set; }

    public ICollection<FileModel> Files { get; set; }
    public ICollection<PropertyModel> Properties { get; set; }
}