﻿using REM_Database.Model.Finance;
using REM_Shared.Enum;

namespace REM_Database.Model.Master;

public class TenancyContractPaymentPlanModel : BaseModel
{
    public string Number { get; set; }
    public DateTime PayDate { get; set; }
    public string Description { get; set; }
    public double Value { get; set; }
    public string LastNotes { get; set; }
    public CheckStatus LastCheckStatus { get; set; }
    public TenancyContractPaymentType Type { get; set; }
    public Guid TenancyContractId { get; set; }
    public TenancyContractModel TenancyContract { get; set; }
    public Guid? BankTypeId { get; set; }
    public BankTypeModel BankType { get; set; }
    public ICollection<CheckHistoryModel> CheckHistory { get; set; }
    public ICollection<FileModel> Files { get; set; }
    public ICollection<FinanceOperationsModel> FinanceOperations { get; set; }
}