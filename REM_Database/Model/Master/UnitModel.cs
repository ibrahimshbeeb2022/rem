﻿using REM_Database.Model.Unit;
using REM_Shared.Enum;

namespace REM_Database.Model.Master;

public class UnitModel : BaseModel
{
    public string UnitName { get; set; }
    public string UnitNumber { get; set; }
    public string UnitSpace { get; set; }
    public string Description { get; set; }
    public string SewerAccountNumber { get; set; }
    public string ElectricityAndWaterAccountNumber { get; set; }
    public double RentValue { get; set; }
    public bool HasParking { get; set; }
    public string ParkingNumber { get; set; }
    public string FloorNumber { get; set; }
    public double ParkingRentValue { get; set; }
    public TenancyContractType TenancyContractType { get; set; }

    public Guid UnitTypeId { get; set; }
    public UnitTypeModel UnitType { get; set; }

    public Guid PropertyId { get; set; }
    public PropertyModel Property { get; set; }

    public ICollection<TenancyContractModel> TenancyContracts { get; set; }
}