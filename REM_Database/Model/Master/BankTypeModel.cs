﻿namespace REM_Database.Model.Master
{
    public class BankTypeModel : BaseModel
    {
        public string Title { get; set; }
        public string TitleEn { get; set; }
    }
}
