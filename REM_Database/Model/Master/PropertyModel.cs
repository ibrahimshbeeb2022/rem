﻿using REM_Database.Model.Guard;
using REM_Database.Model.Location;
using REM_Shared.Enum;

namespace REM_Database.Model.Master;

public class PropertyModel : BaseModel
{
    public string Name { get; set; }
    public string PropertyNumber { get; set; }
    public string PieceNumber { get; set; }
    public string SupervisorName { get; set; }
    public string Description { get; set; }
    public PropertyOwnType PropertyOwnType { get; set; }
    public int ApartmentsCount { get; set; }
    public int FloorsCount { get; set; }
    public int ParkingsCount { get; set; }
    public int ParkingFloorsCount { get; set; }
    public int ResidentialFloorsCount { get; set; }
    public int StoresCount { get; set; }
    public string GovernmentNumber { get; set; }
    public string BondNumber { get; set; }
    public string BondType { get; set; }
    public DateTime BondDate { get; set; }
    public bool IsActive { get; set; }

    public Guid LandLordId { get; set; }
    public LandLordModel LandLord { get; set; }

    public Guid PropertyTypeId { get; set; }
    public PropertyTypeModel PropertyType { get; set; }

    public Guid? GuardId { get; set; }
    public GuardModel Guard { get; set; }

    public Guid? CountryId { get; set; }
    public CountryModel Country { get; set; }

    public Guid? CityId { get; set; }
    public CityModel City { get; set; }

    public Guid? RegionId { get; set; }
    public RegionModel Region { get; set; }

    public ICollection<FileModel> Files { get; set; }
    public ICollection<UnitModel> Units { get; set; }
    public ICollection<TenancyContractModel> TenancyContracts { get; set; }
}