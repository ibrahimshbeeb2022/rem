﻿using REM_Database.Model.Employee;
using REM_Shared.Enum;

namespace REM_Database.Model.User;

public class UserModel : BaseModel
{
    public string FullName { get; set; }
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public string HashPassword { get; set; }
    public string FcmToken { get; set; }
    public UserType UserType { get; set; }
    public LanguageEnum Language { get; set; }
    public List<UserRoleModel> Roles { get; set; }
}