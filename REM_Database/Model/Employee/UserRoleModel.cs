﻿using REM_Database.Model.Permission;
using REM_Database.Model.User;

namespace REM_Database.Model.Employee;

public class UserRoleModel : BaseModel
{
    public Guid UserId { get; set; }
    public UserModel User { get; set; }

    public Guid RoleId { get; set; }
    public RoleModel Role { get; set; }
}