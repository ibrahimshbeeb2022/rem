﻿using REM_Database.Model.User;

namespace REM_Database.Model.Employee;

public class EmployeeModel : UserModel
{
    public bool IsActive { get; set; }
}