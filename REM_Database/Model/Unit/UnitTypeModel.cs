﻿using REM_Database.Model.Master;

namespace REM_Database.Model.Unit;

public class UnitTypeModel : BaseModel
{
    public string Name { get; set; }
    public string NameEn { get; set; }
    public bool IsActive { get; set; }

    public ICollection<UnitModel> Units { get; set; }
}