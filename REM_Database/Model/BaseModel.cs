﻿using System.ComponentModel.DataAnnotations;

namespace REM_Database.Model;

public class BaseModel
{
    [Key]
    public Guid Id { get; set; }
    public DateTime CreateDate { get; set; } = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time"));
    public long CreateBy { get; set; }
    public DateTime? UpdateDate { get; set; }
    public long? UpdateBy { get; set; }
    public bool IsValid { get; set; } = true;
    public int IncrementNumber { get; set; }
    public string DocCode { get; set; }
}