﻿using REM_Database.Model.Expense;
using REM_Database.Model.Finance;
using REM_Database.Model.Guard;
using REM_Database.Model.Master;
using REM_Database.Model.Settings;
using REM_Shared.Enum;

namespace REM_Database.Model;

public class FileModel : BaseModel
{
    public string Url { get; set; }
    public string Name { get; set; }
    public FileType FileType { get; set; }
    public FileKind FileKind { get; set; }

    public Guid? GuardId { get; set; }
    public GuardModel Guard { get; set; }

    public Guid? LandLordId { get; set; }
    public LandLordModel LandLord { get; set; }

    public Guid? PropertyId { get; set; }
    public PropertyModel Property { get; set; }

    public Guid? TenantId { get; set; }
    public TenantModel Tenant { get; set; }

    public Guid? CheckId { get; set; }
    public TenancyContractPaymentPlanModel Check { get; set; }

    public Guid? SettingsLawsId { get; set; }
    public SettingsLawsModel SettingsLaws { get; set; }

    public Guid? TenancyContractId { get; set; }
    public TenancyContractModel TenancyContract { get; set; }

    public Guid? BondId { get; set; }
    public BondsModel Bond { get; set; }

    public Guid? CompanyExpenseVoucherId { get; set; }
    public CompanyExpenseVoucherModel CompanyExpenseVoucher { get; set; }
}