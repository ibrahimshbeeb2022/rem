﻿using REM_Shared.Enum;

namespace REM_Database.Model.Expense
{
    public class ExpenseModel : BaseModel
    {
        public string Name { get; set; }
        public string EnName { get; set; }
        public ExpenseType Type { get; set; }
    }
}
