﻿namespace REM_Database.Model.Expense
{
    public class CompanyExpenseVoucherModel : BaseModel
    {
        public double SubTotal { get; set; }
        public double Vat { get; set; }
        public double Total { get; set; }
        public string Remarks { get; set; }
        public DateTime Date { get; set; }
        public List<CompanyExpenseVoucherDetailsModel> VoucherDetails { get; set; }
        public List<FileModel> Files { get; set; }
    }
}
