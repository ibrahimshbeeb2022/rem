﻿using REM_Database.Model.Finance;
using REM_Database.Model.Master;
using REM_Shared.Enum;

namespace REM_Database.Model.Expense
{
    public class CompanyExpenseVoucherDetailsModel : BaseModel
    {
        public FinanceAccountType AccountType { get; set; }
        public string Remarks { get; set; }
        public bool WithVat { get; set; }
        public VatType VatType { get; set; }
        public double SubTotal { get; set; }
        public double Vat { get; set; }
        public double Total { get; set; }

        public Guid? LandLordId { get; set; }
        public LandLordModel LandLord { get; set; }

        public Guid? TenantId { get; set; }
        public TenantModel Tenant { get; set; }

        public Guid? ExpenseId { get; set; }
        public ExpenseModel Expense { get; set; }

        public Guid? UnitId { get; set; }
        public UnitModel Unit { get; set; }

        public Guid CompanyExpenseVoucherId { get; set; }
        public CompanyExpenseVoucherModel CompanyExpenseVoucher { get; set; }
        public ICollection<FinanceOperationsModel> FinanceOperations { get; set; }
    }
}
