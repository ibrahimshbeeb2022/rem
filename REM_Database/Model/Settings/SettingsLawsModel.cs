﻿using REM_Shared.Enum;

namespace REM_Database.Model.Settings;

public class SettingsLawsModel : BaseModel
{
    public SettingsEnum Key { get; set; }
    public string Value { get; set; }

    public ICollection<FileModel> Files { get; set; }
}