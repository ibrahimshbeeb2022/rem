﻿using REM_Database.Model.Location;

namespace REM_Database.Model.Currency;

public class CurrencyModel : BaseModel
{
    public string Name { get; set; }
    public string NameEn { get; set; }
    public bool IsActive { get; set; }
    public string Symbol { get; set; }
    public double Parity { get; set; }

    public ICollection<CountryModel> Countries { get; set; }
}