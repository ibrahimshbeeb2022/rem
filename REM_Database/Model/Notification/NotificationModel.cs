﻿using REM_Shared.Enum;

namespace REM_Database.Model.Notification
{
    public class NotificationModel : BaseModel
    {
        public NotificationType Type { get; set; }
        public string Body { get; set; }
        public string Title { get; set; }
        public string BodyEn { get; set; }
        public string TitleEn { get; set; }
    }
}
