﻿using REM_Database.Model.User;

namespace REM_Database.Model.Notification
{
    public class NotificationUserModel : BaseModel
    {
        public bool IsRead { get; set; }
        public Guid? ObjectId { get; set; }
        public string ObjectCode { get; set; }

        public Guid NotificationId { get; set; }
        public NotificationModel Notification { get; set; }

        public Guid? UserId { get; set; } //Recipient
        public UserModel User { get; set; }

        public Guid? SenderId { get; set; }
        public UserModel Sender { get; set; }
    }
}
