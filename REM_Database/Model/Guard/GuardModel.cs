﻿using REM_Database.Model.Master;

namespace REM_Database.Model.Guard;

public class GuardModel : BaseModel
{
    public string Name { get; set; }
    public string NameEn { get; set; }
    //public string Profession { get; set; }
    //public string ProfessionEn { get; set; }
    //public DateTime BirthDate { get; set; }
    public string IdNumber { get; set; }
    public DateTime IdExpireDate { get; set; }
    public string PassportNumber { get; set; }
    public DateTime PassportExpireDate { get; set; }

    public ICollection<FileModel> Files { get; set; }
    public ICollection<PropertyModel> Properties { get; set; }
}