﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace REM_Database.Migrations
{
    /// <inheritdoc />
    public partial class Init123 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "PropertyId",
                table: "Bonds",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.CreateTable(
                name: "BankType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    Title = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TitleEn = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreateDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreateBy = table.Column<long>(type: "bigint", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IncrementNumber = table.Column<int>(type: "int", nullable: false),
                    DocCode = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankType", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_Bonds_PropertyId",
                table: "Bonds",
                column: "PropertyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bonds_Property_PropertyId",
                table: "Bonds",
                column: "PropertyId",
                principalTable: "Property",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bonds_Property_PropertyId",
                table: "Bonds");

            migrationBuilder.DropTable(
                name: "BankType");

            migrationBuilder.DropIndex(
                name: "IX_Bonds_PropertyId",
                table: "Bonds");

            migrationBuilder.DropColumn(
                name: "PropertyId",
                table: "Bonds");
        }
    }
}
