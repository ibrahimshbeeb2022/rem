﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace REM_Database.Migrations
{
    /// <inheritdoc />
    public partial class addtenancyReasonInTenancyContract : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TenancyReason",
                table: "TenancyContract",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenancyReason",
                table: "TenancyContract");
        }
    }
}
