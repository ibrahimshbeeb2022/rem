﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace REM_Database.Migrations
{
    /// <inheritdoc />
    public partial class Init312 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bank",
                table: "TenancyContractPaymentPlan");

            migrationBuilder.AddColumn<Guid>(
                name: "BankTypeId",
                table: "TenancyContractPaymentPlan",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_TenancyContractPaymentPlan_BankTypeId",
                table: "TenancyContractPaymentPlan",
                column: "BankTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_TenancyContractPaymentPlan_BankType_BankTypeId",
                table: "TenancyContractPaymentPlan",
                column: "BankTypeId",
                principalTable: "BankType",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TenancyContractPaymentPlan_BankType_BankTypeId",
                table: "TenancyContractPaymentPlan");

            migrationBuilder.DropIndex(
                name: "IX_TenancyContractPaymentPlan_BankTypeId",
                table: "TenancyContractPaymentPlan");

            migrationBuilder.DropColumn(
                name: "BankTypeId",
                table: "TenancyContractPaymentPlan");

            migrationBuilder.AddColumn<string>(
                name: "Bank",
                table: "TenancyContractPaymentPlan",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
