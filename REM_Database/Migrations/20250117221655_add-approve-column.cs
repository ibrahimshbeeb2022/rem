﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace REM_Database.Migrations
{
    /// <inheritdoc />
    public partial class addapprovecolumn : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Approved",
                table: "TenancyContract",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "TenancyNumber",
                table: "TenancyContract",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<Guid>(
                name: "CompanyExpenseVoucherId",
                table: "File",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_File_CompanyExpenseVoucherId",
                table: "File",
                column: "CompanyExpenseVoucherId");

            migrationBuilder.AddForeignKey(
                name: "FK_File_CompanyExpenseVoucher_CompanyExpenseVoucherId",
                table: "File",
                column: "CompanyExpenseVoucherId",
                principalTable: "CompanyExpenseVoucher",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_File_CompanyExpenseVoucher_CompanyExpenseVoucherId",
                table: "File");

            migrationBuilder.DropIndex(
                name: "IX_File_CompanyExpenseVoucherId",
                table: "File");

            migrationBuilder.DropColumn(
                name: "Approved",
                table: "TenancyContract");

            migrationBuilder.DropColumn(
                name: "TenancyNumber",
                table: "TenancyContract");

            migrationBuilder.DropColumn(
                name: "CompanyExpenseVoucherId",
                table: "File");
        }
    }
}
