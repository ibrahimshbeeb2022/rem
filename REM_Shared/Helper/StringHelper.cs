﻿using REM_Shared.Enum;

namespace REM_Shared.Helper;

public static class StringHelper
{
    public static string CreateDocumentCode(DocumentNumber documentNumber, int nextIncrementNumber) => documentNumber.ToString() + $"00{nextIncrementNumber}";
    public static string CreateLandLordTenancyContractCode(int landLordContractNo, string landLordDocCode) => $"{DateTime.Now.Year}/{landLordDocCode}/00{landLordContractNo}";
    public static string FileUrlSimplify(this string url) => string.IsNullOrEmpty(url) ? null : url.Split("wwwroot").ElementAt(1);
}