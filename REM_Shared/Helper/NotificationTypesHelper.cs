﻿using REM_Shared.Enum;

namespace REM_Shared.Helper
{
    public class NotificationTypesHelper
    {
        public List<NotificationTypeData> Data { get; set; } = new List<NotificationTypeData>()
        {
            new NotificationTypeData
            {
                Type = NotificationType.NewTenancyContract,
                Body = "تم انشاء عقد اجار جديد يرجى الاطلاع عليه",
                EnBody = "New tenancy contract has been created, please check it out",
                Title = "عقد اجار جديد",
                EnTitle = "New tenancy contract"
            },
            new NotificationTypeData
            {
                Type = NotificationType.RemoveTenancyContract,
                Body = "تم حذف عقد اجار  يرجى",
                EnBody = "Tenancy contract has been Deleted",
                Title = "تم حذف عقد اجار ",
                EnTitle = "Delete tenancy contract"
            },
            new NotificationTypeData
            {
                Type = NotificationType.ChangeTenancyContractStatus,
                Body = "تم تغيير حاله  عقد الأجار الى    يرجى الاطلاع عليه",
                EnBody = "tenancy contract status has been updated to  , please check it out",
                Title = "حالة عقد الأجار",
                EnTitle = "Tenancy contract status"
            },
            new NotificationTypeData
            {
                Type = NotificationType.ChangeChequeStatus,
                Body = "تم تغيير حاله الشيك صاحب الرقم   الى  , يرجى الاطلاع عليه",
                EnBody = "Cheque statu with number   has been changed, please check it out",
                Title = "عقد اجار جديد",
                EnTitle = "New tenancy contract"
            },
            new NotificationTypeData
            {
                Type = NotificationType.NewLandLord,
                Body = "تم انشاء مالك جديد يرجى الإطلاع عليه",
                EnBody = "New LandLord has been created, please check it out",
                Title = "مالك جديد",
                EnTitle = "New LandLord"
            },
            new NotificationTypeData
            {
                Type = NotificationType.UpdateLandLord,
                Body = "تم تعديل بيانات المالك يرجى الإطلاع عليه",
                EnBody = "landLord information has been updated, please check it out",
                Title = "تعديل بيانات مالك",
                EnTitle = "LandLord Updated"
            },
            new NotificationTypeData
            {
                Type = NotificationType.RemoveLandLord,
                Body = "تم حذف المالك",
                EnBody = "LandLord    has been deleted",
                Title = "حذف مالك",
                EnTitle = "landLord Deleted"
            },
             new NotificationTypeData
            {
                Type = NotificationType.NewTenant,
                Body = "تم انشاء مستأجر جديد يرجى الإطلاع عليه",
                EnBody = "New tenant has been created, please check it out",
                Title = "مستأجر جديد",
                EnTitle = "New tenant"
            },
            new NotificationTypeData
            {
                Type = NotificationType.UpdateTenant,
                Body = "تم تعديل بيانات المستأجر يرجى الإطلاع عليه",
                EnBody = "Tenant information has been updated, please check it out",
                Title = "تعديل بيانات مستأجر",
                EnTitle = "Tenant Updated"
            },
            new NotificationTypeData
            {
                Type = NotificationType.RemoveTenant,
                Body = "تم حذف المستأجر",
                EnBody = "Tenant    has been deleted",
                Title = "حذف مستأجر",
                EnTitle = "Tenant Deleted"
            },
             new NotificationTypeData
            {
                Type = NotificationType.NewProperty,
                Body = "تم انشاء عقار جديد يرجى الإطلاع عليه",
                EnBody = "New property has been created, please check it out",
                Title = "عقار جديد",
                EnTitle = "New peoprty"
            },
            new NotificationTypeData
            {
                Type = NotificationType.UpdateProperty,
                Body = "تم تعديل بيانات العقار يرجى الإطلاع عليه",
                EnBody = "Property information has been updated, please check it out",
                Title = "تعديل بيانات عقار",
                EnTitle = "Property Updated"
            },
            new NotificationTypeData
            {
                Type = NotificationType.RemoveProperty,
                Body = "تم حذف عقار ",
                EnBody = "Property has been deleted",
                Title = "حذف عقار",
                EnTitle = "Property Deleted"
            },
             new NotificationTypeData
            {
                Type = NotificationType.NewUnit,
                Body = "تم انشاء وحدة جديده يرجى الإطلاع عليه",
                EnBody = "New Unit has been created, please check it out",
                Title = "وحده جديده جديد",
                EnTitle = "New Unit"
            },
            new NotificationTypeData
            {
                Type = NotificationType.UpdateUnit,
                Body = "تم تعديل بيانات الوحده يرجى الإطلاع عليه",
                EnBody = "Unit information has been updated, please check it out",
                Title = "تعديل بيانات الوحده",
                EnTitle = "Unit Updated"
            },
            new NotificationTypeData
            {
                Type = NotificationType.RemoveUnit,
                Body = "تم حذف الوحده",
                EnBody = "Unit has been deleted",
                Title = "حذف وحده",
                EnTitle = "Unit Deleted"
            },
            new NotificationTypeData
            {
                Type = NotificationType.BankReceiptVoucher,
                Body = "تم انشاء سند قبض بنكي جديد يرجى الإطلاع عليه",
                EnBody = "New Bank Receipt Voucher has been created, please check it out",
                Title = "سند جديد",
                EnTitle = "New Bond"
            },
            new NotificationTypeData
            {
                Type = NotificationType.CashReceiptVoucher,
                Body = "تم انشاء سند قبض كاش جديد يرجى الإطلاع عليه",
                EnBody = "New Cash Receipt Voucher has been created, please check it out",
                Title = "سند جديد",
                EnTitle = "New Bond"
            },
            new NotificationTypeData
            {
                Type = NotificationType.ChequeReceiptVoucher,
                Body = "تم انشاء شيك قبض  جديد يرجى الإطلاع عليه",
                EnBody = "New Cheque Receipt Voucher has been created, please check it out",
                Title = "سند جديد",
                EnTitle = "New Bond"
            },
            new NotificationTypeData
            {
                Type = NotificationType.BankPaymentVoucher,
                Body = "تم انشاء سند دفع بنكي جديد يرجى الإطلاع عليه",
                EnBody = "New Bank Payment Voucher has been created, please check it out",
                Title = "سند جديد",
                EnTitle = "New Bond"
            },
            new NotificationTypeData
            {
                Type = NotificationType.CashPaymentVoucher,
                Body = "تم انشاء سند دفع كاش  جديد يرجى الإطلاع عليه",
                EnBody = "New Cash Payment Voucher has been created, please check it out",
                Title = "سند جديد",
                EnTitle = "New Bond"
            },
            new NotificationTypeData
            {
                Type = NotificationType.ChequePaymentVoucher,
                Body = "تم انشاء شيك دفع جديد يرجى الإطلاع عليه",
                EnBody = "New Cheque Payment Voucher has been created, please check it out",
                Title = "سند جديد",
                EnTitle = "New Bond"
            },
            new NotificationTypeData
            {
                Type = NotificationType.CompanyExpenseVoucher,
                Body = "تم انشاء سند مصاريف جديد يرجى الإطلاع عليه",
                EnBody = "New Company Expense Voucher has been created, please check it out",
                Title = "سند جديد",
                EnTitle = "New Bond"
            },
        };

        public NotificationTypeData GetNotificationDataByType(NotificationType type)
        {
            return Data.FirstOrDefault(m => m.Type == type);
        }
    }
    public class NotificationTypeData
    {
        public NotificationType Type { get; set; }
        public string Title { get; set; }
        public string EnTitle { get; set; }
        public string Body { get; set; }
        public string EnBody { get; set; }
    }
}
