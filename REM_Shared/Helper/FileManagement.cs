﻿using Microsoft.AspNetCore.Http;
using REM_Shared.Enum;
using System.ComponentModel.DataAnnotations;

namespace REM_Shared.Helper;

public class FileManagement
{
    public static async Task<List<(string url, string name, FileType fileType)>> SaveFiles(List<IFormFile> files, string rootPath)
    {
        List<(string url, string name, FileType fileType)> result = [];
        string utcNow = string.Format("{0:yyyy-MM-dd_hh-mm}", DateTime.UtcNow);
        utcNow = utcNow.Replace("م", "PM").Replace("ص", "AM");

        foreach (var file in files)
        {
            Tuple<string, string, FileType> tuple;

            var extension = Path.GetExtension(file.FileName);

            string fileName = Guid.NewGuid() + "_" + utcNow + extension;

            string serverFolderPath = string.Empty;

            var fileSize = Math.Round(file.Length / (1024.0 * 1024.0), 2);

            if (fileSize > Constants.MaximumFileSizeInMB)
                throw new ValidationException("invalid file size");

            if (Constants.ImageExtensions.ContainsKey(extension))
            {
                serverFolderPath = Path.Combine(rootPath, "Files", "Images");

                CheckIfDirectoryExist(serverFolderPath);
                serverFolderPath = Path.Combine(serverFolderPath, fileName);
                tuple = new(serverFolderPath, file.Name, FileType.Image);
            }
            else if (Constants.PdfExtensions.ContainsKey(extension))
            {
                serverFolderPath = Path.Combine(rootPath, "Files", "Pdfs");

                CheckIfDirectoryExist(serverFolderPath);
                serverFolderPath = Path.Combine(serverFolderPath, fileName);
                tuple = new(serverFolderPath, file.Name, FileType.Pdf);
            }
            else if (Constants.WordExtensions.ContainsKey(extension))
            {
                serverFolderPath = Path.Combine(rootPath, "Files", "Words");

                CheckIfDirectoryExist(serverFolderPath);
                serverFolderPath = Path.Combine(serverFolderPath, fileName);
                tuple = new(serverFolderPath, file.Name, FileType.Word);
            }
            else if (Constants.ExcelExtensions.ContainsKey(extension))
            {
                serverFolderPath = Path.Combine(rootPath, "Files", "Excels");

                CheckIfDirectoryExist(serverFolderPath);
                serverFolderPath = Path.Combine(serverFolderPath, fileName);
                tuple = new(serverFolderPath, file.Name, FileType.Excel);
            }
            else
                throw new ValidationException("invalid file extension");

            var fileStream = new FileStream(serverFolderPath, FileMode.Create);
            await file.CopyToAsync(fileStream);
            await fileStream.DisposeAsync();
            result.Add(tuple.ToValueTuple());
        }
        return result;
    }
    public static void DeleteFiles(IEnumerable<string> urls)
    {
        try
        {
            foreach (string url in urls)
            {
                if (File.Exists(url))
                    File.Delete(url);
            }
        }
        catch (System.Exception)
        {
            return;
        }
    }

    private static void CheckIfDirectoryExist(string serverDirectory)
    {
        if (!Directory.Exists(serverDirectory))
            Directory.CreateDirectory(serverDirectory);
    }
}