﻿namespace REM_Shared.Enum;

public enum FileType
{
    Image,
    Pdf,
    Word,
    Excel
}