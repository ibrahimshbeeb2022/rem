﻿namespace REM_Shared.Enum;

public enum TenancyContractPaymentType
{
    Cash,
    Check,
    Other
}