﻿namespace REM_Shared.Enum;

public enum PropertyCategory
{
    Residential,
    Commercial,
    ResidentialCommercial,
    Investment,
    ResidentialInvestment,
    Industry,
    ResidentialIndustry,
    ResidentialCommercialIndustry,
}