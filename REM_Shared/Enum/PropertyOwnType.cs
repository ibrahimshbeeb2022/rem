﻿namespace REM_Shared.Enum;

public enum PropertyOwnType
{
    Investment, //استثمار
    FreeHold, // تملك حر
}