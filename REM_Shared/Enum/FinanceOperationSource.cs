﻿namespace REM_Shared.Enum
{
    public enum FinanceOperationSource
    {
        PaymentBond,
        ReceiptBond,
    }
}
