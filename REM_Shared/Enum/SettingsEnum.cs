﻿namespace REM_Shared.Enum;

public enum SettingsEnum
{
    CompanyNameAr,
    CompanyNameEn,
    Email,
    CompanyTaxNumber,
    PhoneNumber,
    SecondPhoneNumber,
    Address,
    Logo,
    LawsDescriptionAr,
    LawsDescriptionEN,
    Signature,
    Seal,
    WaterMark,
}