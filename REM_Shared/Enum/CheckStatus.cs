﻿namespace REM_Shared.Enum
{
    public enum CheckStatus
    {
        UnClear, // غير  محصل
        Hold, // مؤجل
        Clear,// تم التحصيل
        Return,// معاد
        Replace,// مستبدل
    }
}
