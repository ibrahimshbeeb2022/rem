﻿namespace REM_Shared.Enum
{
    public enum UserType
    {
        SuperAdmin,
        Employee,
        LandLord,
    }
}
