﻿namespace REM_Shared.Enum;

public enum DocumentNumber
{
    L, // Stand for LandLord
    P, // Stand for Property
    U, // Stand for Unit
    T, // Stand for Tenant
    C, // Stand for Country 
    CY, // Stand for City
    R, // Stand for Region
    CU, // Stand for Currency
    GU, // Stand for Guard
    PT, // Stand for Property Type
    UT, // Stand for Unit Type
    TC, // Stand for Tenancy Contract
    TCC, // Stand for Tenancy Contract Check
    FB, // Stand for Finance Bond سند مالي
    E, // Stand for Employee
    CRV, // Stand for  cach  Reciept voucher
    BRV, // Stand for  bank  Reciept voucher 
    PDCR, // Stand for cheque Reciept voucher
    CPV, // Stand for  cach payment voucher
    BPV, // Stand for  bank payment voucher 
    PDCP, // Stand for  cheque payment voucher
    CEX, // Stand for  Company Expense
    GEX, // Stand for  General Expense
    CEV, // Stand for  Company Expense Voucher
    BT // Stand for  Company Expense Voucher
}