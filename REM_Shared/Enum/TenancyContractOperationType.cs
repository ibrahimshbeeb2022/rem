﻿namespace REM_Shared.Enum
{
    public enum  TenancyContractOperationType
    {
        Rent,
        Insurance,
        Other,
    }
}
