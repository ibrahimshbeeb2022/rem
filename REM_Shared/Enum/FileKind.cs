﻿namespace REM_Shared.Enum;

public enum FileKind
{
    ProfilePhoto,
    Nationality,
    Passport,
    Signature,
    BankAccount,
    Id,
    Residence,
    Other,
    Property,
    PropertyPlan,
    PropertyOwnership,
    TcPdf,
    TcInvoice,
    WaterMark,
    Bond,
}