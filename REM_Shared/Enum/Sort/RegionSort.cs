﻿namespace REM_Shared.Enum.Sort;

public enum RegionSort
{
    DocCode,
    Name,
    CityAr,
    CityEn,
    NameEn,
    StreetName,
    StreetNameEn,
    CreateDate
}