﻿namespace REM_Shared.Enum.Sort;

public enum PropertySort
{
    DocCode,
    LandLordName,
    PropertyTypeAr,
    PropertyTypeEn,
    CountryNameAr,
    CountryNameEn,
    CityNameAr,
    CityNameEn,
    RegionNameAr,
    RegionNameEn,
    Name,
    PropertyNumber,
    PieceNumber,
    SupervisorName,
    Description,
    PropertyOwnType,
    ApartmentsCount,
    FloorsCount,
    ParkingsCount,
    ParkingFloorsCount,
    StoresCount,
    GovernmentNumber,
    BondNumber,
    BondType,
    BondDate,
    IsActive,
    GuardAr,
    GuardEn,
    CreateDate
}