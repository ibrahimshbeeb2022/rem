﻿namespace REM_Shared.Enum.Sort;

public enum CurrencySort
{
    DocCode,
    Name,
    NameEn,
    Parity,
    Symbol,
    IsActive,
    CreateDate
}