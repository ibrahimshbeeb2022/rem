﻿namespace REM_Shared.Enum.Sort;

public enum UnitSort
{
    DocCode,
    UnitNumber,
    UnitSpace,
    Description,
    GovernmentNumber,
    BondNumber,
    BondType,
    BondDate,
    SewerAccountNumber,
    ElectricityAndWaterAccountNumber,
    RentValue,
    HasParking,
    ParkingNumber,
    FloorNumber,
    ParkingRentValue,
    UnitTypeAr,
    UnitTypeEn,
    Property,
    CreateDate,
}