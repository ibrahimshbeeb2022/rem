﻿namespace REM_Shared.Enum.Sort;

public enum TenantSort
{
    DocCode,
    FullName,
    Email,
    Phone,
    Fax,
    CardNumber,
    Job,
    Beneficiary,
    IdentityCardNumber,
    IdentityCardExpiryDate,
    PassportNumber,
    PassportExpiryDate,
    Nationality,
    Address,
    NumberOfResidentsOfTheRentedProperty,
    CreateDate
}