﻿namespace REM_Shared.Enum.Sort;

public enum EmployeeSort
{
    DocCode,
    Name,
    Email,
    PhoneNumber,
    IsActive,
    CreateDate
}