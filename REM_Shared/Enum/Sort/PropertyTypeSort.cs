﻿namespace REM_Shared.Enum.Sort;

public enum PropertyTypeSort
{
    DocCode,
    Title,
    TitleEn,
    Category,
    CreateDate
}