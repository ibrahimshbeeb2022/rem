﻿namespace REM_Shared.Enum.Sort;

public enum LandLordSort
{
    DocCode,
    FullName,
    Email,
    Phone,
    Mobile,
    CardNumber,
    Job,
    Beneficiary,
    IdentityCardNumber,
    IdentityCardExpiryDate,
    PassportNumber,
    PassportExpiryDate,
    TaxNumber,
    BirthDate,
    BankName,
    BankAddress,
    AccountName,
    AccountNumber,
    Iban,
    SwiftCode,
    Nationality,
    Note,
    CountryAr,
    CountryEn,
    CityAr,
    CityEn,
    RegionAr,
    RegionEn,
    CreateDate
}