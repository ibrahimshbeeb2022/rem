﻿namespace REM_Shared.Enum.Sort;

public enum TenancyContractSort
{
    DocCode,
    TaxNumber,
    StartDate,
    EndDate,
    Value,
    Status,
    Type,
    LandLordName,
    TenantName,
    TenantNationality,
    TenantEmail,
    TenantPhone,
    PropertyDocCode,
    UnitDocCode,
    CreateDate,
    ContractNumber
}