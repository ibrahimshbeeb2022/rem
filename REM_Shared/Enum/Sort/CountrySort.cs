﻿namespace REM_Shared.Enum.Sort;

public enum CountrySort
{
    DocCode,
    Name,
    NameEn,
    CountryCode,
    CurrencyAr,
    CurrencyEn,
    IsAvailable,
    CreateDate
}