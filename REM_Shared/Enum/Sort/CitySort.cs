﻿namespace REM_Shared.Enum.Sort;

public enum CitySort
{
    DocCode,
    Name,
    NameEn,
    CountryAr,
    CountryEn,
    IsAvailable,
    CreateDate
}