﻿namespace REM_Shared.Enum.Sort;

public enum ChequeSort
{
    DocCode,
    Number,
    PayDate,
    Bank,
    Description,
    Value,
    LastNotes,
    CreateDate
}