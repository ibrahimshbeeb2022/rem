﻿namespace REM_Shared.Enum.Sort;

public enum UnitTypeSort
{
    DocCode,
    Name,
    NameEn,
    IsActive,
    CreateDate
}