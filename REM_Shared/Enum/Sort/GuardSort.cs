﻿namespace REM_Shared.Enum.Sort;

public enum GuardSort
{
    DocCode,
    Name,
    NameEn,
    Profession,
    ProfessionEn,
    BirthDay,
    IdNumber,
    IdExpireDate,
    PassportNumber,
    PassportExpireDate,
    CreateDate
}