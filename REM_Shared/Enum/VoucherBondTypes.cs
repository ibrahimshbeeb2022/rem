﻿namespace REM_Shared.Enum
{
    public enum VoucherBondTypes
    {
        CashReceipt,
        BankReceipt,
        ChequeReceipt,
    }
}
