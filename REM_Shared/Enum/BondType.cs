﻿namespace REM_Shared.Enum
{
    public enum BondType
    {
        ReceiptVouchers,
        PaymentVouchers,
        CompanyExpenses
    }
}
