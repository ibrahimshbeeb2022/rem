﻿namespace REM_Shared.Enum;

public enum TenancyContractType
{
    Residential,
    Commercial,
    Industry,
    Investment,
}