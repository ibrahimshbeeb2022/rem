﻿namespace REM_Shared.Enum
{
    public enum TenancyContractStatus
    {
        New,
        ReNew,
        Expired,
        Occupied,
        Vacant,
        Closed,
        UnRented,
    }
}
