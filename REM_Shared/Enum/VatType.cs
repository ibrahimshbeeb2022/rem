﻿namespace REM_Shared.Enum
{
    public enum VatType
    {
        Exclusive,
        Inclusive,
    }
}
