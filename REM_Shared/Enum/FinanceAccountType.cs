﻿namespace REM_Shared.Enum
{
    public enum FinanceAccountType
    {
        LandLordAccount,
        TenantAccount,
        CashCompanyAccount,
        BankAccount,
        OutputVATAccount,
        AdvanceRentalAccount,
        SecuityDepositAccount,
        PDCReceivablesAccount,
        PDCReturnAccount,
        CompanyExpense,
        GeneralExpense,
        RevenueAccount,
    }
}
