﻿namespace REM_Shared.Enum
{
    public enum ControllersNames
    {
        Currency,
        Employee,
        Expense,
        File,
        Finance,
        Guard,
        LandLord,
        Location,
        Notification,
        Property,
        PropertyType,
        Report,
        Settings,
        TenancyContract,
        Tenant,
        Unit,
        UnitType,
        BankType
    }
}
