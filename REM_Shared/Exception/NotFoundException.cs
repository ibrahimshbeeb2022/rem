﻿namespace REM_Shared.Exception;

public class NotFoundException(string message) : System.Exception(message)
{
}