﻿namespace REM_Shared.Exception;

public static class ErrorMessages
{
    public static string NoAccess => nameof(NoAccess);
    public static string TimeoutError => nameof(TimeoutError);
    public static string TokenStillWork => nameof(TokenStillWork);
    public static string InternalServerError => nameof(InternalServerError);
    public static string RefreshTokenExpired => nameof(RefreshTokenExpired);
    public static string InvalidData => nameof(InvalidData);

    #region Location
    public static string CountryNameRequired => nameof(CountryNameRequired);
    public static string CityNameRequired => nameof(CityNameRequired);
    public static string RegionNameRequired => nameof(RegionNameRequired);
    public static string CountryCodeRequired => nameof(CountryCodeRequired);
    public static string CountryCurrencyRequired => nameof(CountryCurrencyRequired);
    public static string ChooseCountry => nameof(ChooseCountry);
    public static string ChooseCity => nameof(ChooseCity);
    public static string CountryUsed => nameof(CountryUsed);
    public static string CityUsed => nameof(CityUsed);
    public static string CurrencyUsed => nameof(CurrencyUsed);
    public static string CurrencyNotFound => nameof(CurrencyNotFound);
    public static string StreetNameRequired => nameof(StreetNameRequired);
    public static string CountryNotFound => nameof(CountryNotFound);
    public static string CityNotFound => nameof(CityNotFound);
    public static string RegionNotFound => nameof(RegionNotFound);
    public static string CountryHasLandLord => nameof(CountryHasLandLord);
    public static string CityHasLandLord => nameof(CityHasLandLord);
    public static string CityHasContract => nameof(CityHasContract);
    public static string RegionHasContract => nameof(RegionHasContract);
    public static string RegionHasLandLord => nameof(RegionHasLandLord);
    #endregion

    #region LandLord
    public static string LandLordNotFound => nameof(LandLordNotFound);
    public static string LandLordHasProperty => nameof(LandLordHasProperty);

    #endregion

    #region Tenant
    public static string TenantNotFound => nameof(TenantNotFound);
    public static string TenantHasContracts => nameof(TenantHasContracts);

    #endregion

    #region File
    public static string FileFound => nameof(FileFound);

    #endregion

    #region Guard
    public static string GuardNotFound => nameof(GuardNotFound);
    public static string GuardHasProperty => nameof(GuardHasProperty);
    #endregion

    #region Property
    public static string PropertyTypeUsed => nameof(PropertyTypeUsed);
    public static string PropertyTypeNotFound => nameof(PropertyTypeNotFound);
    public static string PropertyNotFound => nameof(PropertyNotFound);
    public static string PropertyHasUnits => nameof(PropertyHasUnits);
    public static string PropertyHasContract => nameof(PropertyHasContract);
    #endregion

    #region Unit
    public static string UnitNotFound => nameof(UnitNotFound);
    public static string UnityTypeUsed => nameof(UnityTypeUsed);
    public static string UnitTypeNotFound => nameof(UnitTypeNotFound);
    public static string UnitTypeHasUnit => nameof(UnitTypeHasUnit);
    public static string UnitHasContract => nameof(UnitHasContract);

    #endregion

    #region TenancyContract
    public static string TenancyContractNotFound => nameof(TenancyContractNotFound);
    public static string AllowOnlyOneRentValue => nameof(AllowOnlyOneRentValue);
    public static string DetailsValueNotEqualToTotalValue => nameof(DetailsValueNotEqualToTotalValue);
    public static string PaymentPlanValueNotEqualToTotalValue => nameof(PaymentPlanValueNotEqualToTotalValue);
    public static string InvalidPaymentDate => nameof(InvalidPaymentDate);
    public static string InvalidVatValue => nameof(InvalidVatValue);
    public static string InvalidPaymentTotalValue => nameof(InvalidPaymentTotalValue);
    public static string CheckNotFound => nameof(CheckNotFound);
    public static string HasTenancyContract => nameof(HasTenancyContract);
    public static string TenancyContractNumberAlreadyExsist => nameof(TenancyContractNumberAlreadyExsist);
    #endregion

    #region Employee
    public static string RoleNotFound => nameof(RoleNotFound);
    public static string RoleRequired => nameof(RoleRequired);
    public static string EmployeeInActive => nameof(EmployeeInActive);
    public static string EmployeeNotFound => nameof(EmployeeNotFound);
    public static string EmployeeAlreadyExit => nameof(EmployeeAlreadyExit);
    public static string WrongEmailOrPassword => nameof(WrongEmailOrPassword);
    #endregion

    #region Expense 

    public static string ExpenseNotFound => nameof(ExpenseNotFound);
    public static string ExpenseHasRelatedData => nameof(ExpenseHasRelatedData);
    public static string InvalidTotalValue => nameof(InvalidTotalValue);


    #endregion

    #region BankType
    public static string BankTypeNotFound => nameof(BankTypeNotFound);
    public static string BankTypeHasRelatedData => nameof(BankTypeHasRelatedData);
    #endregion


}