﻿namespace REM_Shared.Exception;

public class AlreadyExistException(string message) : System.Exception(message)
{
}